const package = require('./package.json');
const {exec} = require('child_process');
let shell = require('shelljs');
let path = require('path');

function cleanFromThirdPartLib(npmList) {
  if(npmList.dependencies) {
    delete npmList.dependencies['reinform-core'];
    Object.values(npmList.dependencies).forEach(d => {
      cleanFromThirdPartLib(d);
  })
  }
}

exec('npm list --json', {maxBuffer: 1024 * 999999}, (err, stdout, stderr) => {
  let npmList = JSON.parse(stdout);
// удалить все что не задекларированно в package.json
for (let prop in npmList.dependencies) {
  if (!existInPackage(prop)) {
    delete npmList.dependencies[prop];
  }
}

// удалить сторонние библиотеки для чистоты картины
cleanFromThirdPartLib(npmList);

let versions = buildPlainArray(npmList);
// адаптированный массив для визуализации
saveToFile(versions, './src/data/', 'plain-versions.json');
});

function buildPlainArray(npmList) {
  let subBranchArray = [];

  Object.values(npmList.dependencies).forEach(d => {
    let entry = {};
  entry['name'] = d.from;
  entry['version'] = d.version;

  if (d.dependencies) {
    entry['dependencies'] = buildPlainArray(d);
  }
  subBranchArray.push(entry);
});

  return subBranchArray;
}

function existInPackage(prop) {

  for (let pack in package.dependencies) {
    if (pack === prop)
      return true;
  }
  return false;

}

function saveToFile(object, fileDir, fileName) {
  shell.mkdir('-p', path.resolve(process.cwd(), fileDir));
  let stringDeps = JSON.stringify(object);
  let fs = require('fs');
  fs.writeFile(fileDir + fileName, stringDeps, function (err) {
    if (err) throw err;
  });

}
