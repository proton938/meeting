'use strict';
/* use this file for scss compiling only */
module.exports = {
   indexScssPath: '/src/styles.scss',
   inlineCssResource: true
};
