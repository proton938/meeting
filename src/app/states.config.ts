import {AppComponent} from './app.component';
import {QuestionComponent} from "./components/question/card/question.component";
import {CalendarComponent} from "./components/calendar/calendar.component";
import {AgendaComponent} from "./components/agenda/card/agenda.component";
import {ProtocolComponent} from "./components/protocol/card/protocol.component";
import {ShowcaseComponent} from "./components/showcase/showcase.component";
import {ShowcaseExpressComponent} from "./components/showcase/express/express.component";
import {ShowcaseExpressMKAComponent} from "./components/showcase/express-mka/express-mka.component";
import {MeetingMkaShowcaseComponent} from './components/showcase/meeting-mka/meeting-mka.component';
import {AgendaReviewMKADocComponent} from "./components/tasks/review-mka-doc/agenda-review-doc/agenda-review-mka-doc.component";
import {ProtocolReviewMkaDocComponent} from "./components/tasks/review-mka-doc/protocol-review/protocol-review-mka-doc.component";
import {ProtocolSignComponent} from './components/tasks/protocol-sign/protocol-sign.component';
import {MeetingEditProtocolAsComponent} from "./components/tasks/approve-protocol-as/meeting-edit-protocol-as/meeting-edit-protocol-as.component";
import {PageNotFoundComponent} from '@reinform-cdp/skeleton';
import { TaskWrapperComponent} from '@reinform-cdp/bpm-components';
import { CdpDependenciesTreeComponent } from '@reinform-cdp/widgets'

export const states = [
{
    name: 'app.versions',
    url: '/versions',
    component: CdpDependenciesTreeComponent,
},
  {
    name: '404',
    url: '/*path',
    component: PageNotFoundComponent
  },
  {
    name: 'app.execution',
    url: '/execution/:system/:taskId',
    component: TaskWrapperComponent
  },
  {
    name: 'app.meeting',
    url: '/meeting',
    component: AppComponent
  },
  {
    name: 'app.meeting.calendar',
    url: '/calendar',
    component: CalendarComponent,
    params: {
      useBreadcrumbs: true
    }
  },
  {
    name: 'app.meeting.question',
    url: '/question/:id',
    component: QuestionComponent,
    params: {
      tab: null,
      useBreadcrumbs: true
    }
  },
  {
    name: 'app.meeting.agenda',
    url: '/agenda/:id?express',
    component: AgendaComponent,
    params: {
      editing: null,
      express: null,
      useBreadcrumbs: true,
      data: null,
      origId: null
    }
  },
  {
    name: 'app.meeting.protocol',
    url: '/protocol/:id',
    component: ProtocolComponent,
    params: {
      id: null,
      agendaID: null,
      editing: false,
      useBreadcrumbs: true
    }
  },
  {
    name: 'app.meeting.showcase',
    url: '/showcase',
    component: ShowcaseComponent,
    params: {
      tab: null,
      useBreadcrumbs: true
    }
  },
  {
    name: 'app.meeting.showcase-express',
    url: '/showcase/admin-meetings',
    component: ShowcaseExpressComponent,
    params: {
      tab: null,
      useBreadcrumbs: true
    }
  },
  {
    name: 'app.meeting.showcase-express-mka',
    url: '/showcase/mka-admin-meetings',

    component: ShowcaseExpressMKAComponent,
    params: {
      tab: null,
      useBreadcrumbs: true
    }
  },
  {
    name: 'app.meeting.showcase-mka',
    url: '/showcase/mka-meeting',
    component: MeetingMkaShowcaseComponent,
    params: {
      tab: null,
      useBreadcrumbs: true
    }
  },
  {
    name: 'app.execution.sdoagendaReviewMKADoc',
    url: '/sdoagendaReviewMKADoc',
    component: AgendaReviewMKADocComponent
  },
  {
    name: 'app.execution.sdoprotocolReviewMKADoc',
    url: '/sdoprotocolReviewMKADoc',
    component: ProtocolReviewMkaDocComponent
  },
  {
    name: 'app.execution.sdoprotocolmeetingSignProtocolAS',
    url: '/sdoprotocolapprovemeetingSignProtocolAS',
    component: ProtocolSignComponent
  },
  {
    name: 'app.execution.sdomeetingEditProtocolAS',
    url: '/sdoprotocolapprovemeetingEditProtocolAS',
    component: MeetingEditProtocolAsComponent
  }

];
