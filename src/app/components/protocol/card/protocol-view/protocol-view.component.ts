import { BsModalService } from "ngx-bootstrap";
import { StateService, Transition } from '@uirouter/core';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MeetingType } from "../../../../models/nsi/MeetingType";
import { FileType } from "../../../../models/FileType";
import { Protocol, ProtocolQuestion } from "../../../../models/Protocol";
import { LoadingStatus } from "@reinform-cdp/widgets";
import { NsiResourceService } from "@reinform-cdp/nsi-resource";
import { Template } from "../../../../models/nsi/Template";
import { AuthorizationService, SessionStorage } from "@reinform-cdp/security";
import { MeetingTypeService } from "../../../../services/meeting-type.service";
import { InstructionsService, ProtocolQuestionsInstructions } from "../../../../services/instructions.service";
import { SearchResultDocument } from "@reinform-cdp/search-resource";
import { CdpReporterPreviewService } from "@reinform-cdp/reporter-resource";
import { ICdpBpmProcessManagerConfig } from "@reinform-cdp/bpm-components";
import { PlatformConfig } from "@reinform-cdp/core";
import * as _ from "lodash";
import { ToastrService } from "ngx-toastr";
import { ProtocolService } from "../../../../services/protocol.service";
import { ProtocolMaterialsModalComponent } from "./protocol-materials.modal";
import { mergeMap, tap, map } from "rxjs/internal/operators";
import { ProtocolInstructionsUtils } from "../../../../utils/protocol-instructions.utils";
import { AgendaUser } from "../../../../models/Agenda";

@Component({
  selector: 'meeting-protocol-view',
  templateUrl: './protocol-view.component.html'
})
export class ProtocolViewComponent implements OnInit {

  MEETING_TYPE_PZZ = "PZZ";

  hasQuestionViewPermission: boolean;

  loadingStatus: LoadingStatus;
  success: boolean;

  @Input() protocol: Protocol;
  @Input() public reload: () => void;
  instructions: InstrQuestion[];
  fileProtocolProject: FileType;
  fileProtocol: FileType;
  meetingType: MeetingType;
  hasApprovalSheetViewPermission: boolean;
  canViewProjectFiles: boolean;
  canViewFiles: boolean;
  @Output() public edit: EventEmitter<any> = new EventEmitter();

  dicts: any = {
    statuses: [],
    priorities: []
  };
  insideOrder: SearchResultDocument[];

  processManagerConfig: ICdpBpmProcessManagerConfig;

  regularExternal: boolean;

  approvedBy: AgendaUser;

  constructor(private $state: StateService,
    private nsiResourceService: NsiResourceService,
    private authorizationService: AuthorizationService,
    private meetingTypeService: MeetingTypeService,
    private instructionsService: InstructionsService,
    private protocolService: ProtocolService,
    private reporterPreviewService: CdpReporterPreviewService,
    private modalService: BsModalService,
    private session: SessionStorage,
    private transition: Transition,
    private platformConfig: PlatformConfig,
    private toastr: ToastrService) {
  }

  ngOnInit() {
    this.loadingStatus = LoadingStatus.LOADING;
    this.success = false;

    let id = this.transition.params()['id'];

    this.processManagerConfig = {
      sysName: this.platformConfig.systemCode.toLowerCase(),
      linkVarValue: this.protocol.protocolID
    };

    this.protocolService.getInsideProtocol(id).pipe(
      tap(response => {
        this.insideOrder = response;

        this.hasApprovalSheetViewPermission = this.authorizationService.check('MEETING_PERFORM_VOTING');
        this.hasQuestionViewPermission = this.authorizationService.check('MEETING_CARD_QUESTION');

        return this.processFiles().then(() => {
          if (this.protocol.meetingType === this.MEETING_TYPE_PZZ) {
            const groups = this.session.groups();
            this.canViewProjectFiles = groups.some(gr => gr === 'OASI_MEETING_GK_PZZ_ACCESS_PROJECT_FILES');
            this.canViewFiles = groups.some(gr => gr === 'OASI_MEETING_GK_PZZ_ACCESS_FILES');
          } else {
            this.canViewProjectFiles = true;
            this.canViewFiles = true;
          }
          return Promise.all([
            this.meetingTypeService.getMeetingType(this.protocol.meetingType),
            this.instructionsService.getProtocolInstructions(this.protocol).toPromise(),
          ]).then((result: [MeetingType, ProtocolQuestionsInstructions]) => {
            this.meetingType = result[0];
            this.regularExternal = this.meetingType && !this.meetingType.Express && this.meetingType.meetingExternal;

            let protocolQuestionsInstructions = result[1];
            const questionMap = protocolQuestionsInstructions.questionMap;
            this.instructions = _.chain(protocolQuestionsInstructions.instructions.docs).groupBy(doc => doc.questionPrimaryID).toPairs().map((el: any) => {
              const primaryQuestionID = el[0];
              const questionId = questionMap[primaryQuestionID];
              return {
                question: this.protocol.question.find(q => q.questionID === questionId),
                instructions: el[1]
              };
            }).value();

            return this.nsiResourceService.getDictsFromCache(['InstructionPriority', 'InstructionStatus'])
          }).then(response => {
            this.dicts.statuses = response.InstructionStatus;
            this.dicts.priorities = response.InstructionPriority;

            return this.getMeetingTypeApproved();
          }).then((response) => {
            this.approvedBy = response;
          });
        }).then(() => {
          this.loadingStatus = LoadingStatus.SUCCESS;
          this.success = true;
        }).catch(() => {
          this.loadingStatus = LoadingStatus.ERROR;
        })
      })
    ).subscribe()
  }

  getMeetingTypeApproved(): ng.IPromise<AgendaUser> {
    if (this.meetingType.approvedLogin) {
      return this.nsiResourceService.ldapUser(this.meetingType.approvedLogin).then(response => {
        return {
          post: response.post,
          iofShort: response.iofShort,
          fioFull: response.displayName,
          accountName: response.accountName,
        };
      });
    } else {
      return null;
    }
  }

  processFiles(): Promise<any> {
    return Promise.resolve(this.nsiResourceService.get('mggt_meeting_Templates')).then((templates: Template[]) => {
      const template = templates.find(t => _.some(t.meetingType, mt => mt.meetingType === this.protocol.meetingType));
      if (this.protocol.fileProtocol && template) {
        this.fileProtocolProject = this.protocol.fileProtocol.find(file => {
          return file.typeFile === template.meetingProtocolProjectClass;
        });
        this.fileProtocol = this.protocol.fileProtocol.find(file => {
          return file.typeFile === template.meetingProtocolClass;
        });
      }
      return true;
    });
  }

  editProtocol() {
    this.edit.emit(true);
  }

  createReport() {
    this.protocolService.createReport(this.protocol.protocolID).toPromise().then(() => {
      return this.reload();
    })
  }

  sendDecision() {
    this.protocolService.sendDecision(this.protocol.protocolID).toPromise().then(() => {
      this.toastr.info('Решение успешно отправлено');
    })
  }

  showAddFileDialog() {
    const options = {
      initialState: {
        protocol: this.protocol,
      },
      'class': 'modal-lg'
    };
    const ref = this.modalService.show(ProtocolMaterialsModalComponent, options);
    let subscription = this.modalService.onHide.subscribe(() => {
      let component = <ProtocolMaterialsModalComponent>ref.content;
      if (component.submit) {
        this.reload()
      }
      subscription.unsubscribe();
    });
  }

  createProtocolWithInstructionsPrintForm() {
    const json = ProtocolInstructionsUtils.getProtocolWithInstructions(this.protocol, this.meetingType, this.instructions.map((instr) => ({
      questionId: instr.question.questionID,
      instructions: instr.instructions
    })), null, this.approvedBy);
    this.reporterPreviewService.nsi2Preview({
      options: {
        jsonSourceDescr: "",
        onProcess: "Сформировать протокол с поручениями",
        placeholderCode: "",
        strictCheckMode: true,
        xwpfResultDocumentDescr: "Протокол с поручениями.docx",
        xwpfTemplateDocumentDescr: ""
      },
      jsonTxt: JSON.stringify({
        document: {
          protocol: json
        }
      }),
      rootJsonPath: "$",
      nsiTemplate: {
        templateCode: 'ssMeetingProtocolRs'
      }
    });
  }

  getQuestions() {
    return this.protocol.question.filter(q => !q.excluded);
  }

}

export class InstrQuestion {
  question: ProtocolQuestion;
  instructions: SearchResultDocument[];
}
