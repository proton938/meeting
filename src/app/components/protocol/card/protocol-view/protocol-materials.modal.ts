import {Component, OnInit, ViewChild} from '@angular/core';
import {BsModalRef} from "ngx-bootstrap";
import {ToastrService} from "ngx-toastr";
import {DropzoneConfigInterface, DropzoneDirective} from "ngx-dropzone-wrapper";
import {FileService} from "../../../../services/file.service";
import {Protocol} from "../../../../models/Protocol";
import {formatAsDateTime} from "../../../../services/date-util.service";
import {ProtocolService} from "../../../../services/protocol.service";

@Component({
  selector: 'meeting-modal-content',
  templateUrl: './protocol-materials.modal.html',
  styleUrls: ['./protocol-materials.modal.scss']
})
export class ProtocolMaterialsModalComponent implements OnInit {

  @ViewChild(DropzoneDirective) dropzone: DropzoneDirective;

  protocol: Protocol;

  dropzoneConfig: DropzoneConfigInterface = {
    url: '/filestore/v1/files/file?systemCode=sdo',
    autoProcessQueue: false,
    parallelUploads: 1,
    paramName: 'file',
    dictDefaultMessage: 'Загрузить файл. <br/> Допустимое расширение: pdf',
    headers: {
      'Accept': 'text/plain'
    }, accept: (file, done) => {
      let fileName: string = file.name;
      let match = /.*\.pdf/.exec(fileName.toLowerCase());
      if (match) {
        done();
      } else {
        done("Допустимое расширение: pdf");
      }
    },
  };

  submit: boolean;

  constructor(private bsModalRef: BsModalRef, private toastr: ToastrService, private fileService: FileService,
              private protocolService: ProtocolService) {
  }

  ngOnInit() {
    this.submit = false;
  }

  save() {
    this.upload(this.dropzone, this.protocol.folderId).then(() => {
      this.submit = true;
      this.bsModalRef.hide();
    })
  }

  upload(dropzone: DropzoneDirective, folderGuid: string): Promise<any> {
    return this.fileService.uploadDropzone(dropzone, this.protocol.protocolID, 'default', folderGuid, (fileType) => {
      return this.protocolService.addMaterial(this.protocol, {
        idFile: fileType.idFile,
        nameFile: fileType.nameFile,
        sizeFile: '' + fileType.sizeFile,
        signed: false,
        dateFile: formatAsDateTime(new Date()),
        typeFile: fileType.typeFile,
        classFile: fileType.mimeType
      }).toPromise().then((protocol: Protocol) => {
        this.protocol = protocol;
      }).catch(() => {
        this.toastr.error('Не удалось добавить файл ' + fileType.nameFile);
      });
    },(errorMessage) => {
      this.toastr.warning(errorMessage.message ? errorMessage.message : errorMessage, 'Ошибка');
    });
  }

  cancel() {
    this.submit = false;
    this.bsModalRef.hide();
  }

}
