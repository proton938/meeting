import {StateService} from '@uirouter/core';
import {Component, Input, OnInit} from '@angular/core';
import {LoadingStatus} from "@reinform-cdp/widgets";
import {InstrQuestion} from "./protocol-view.component";
import {AuthorizationService} from "@reinform-cdp/security";
import {NsiResourceService} from "@reinform-cdp/nsi-resource";

@Component({
  selector: 'mggt-protocol-instructions',
  templateUrl: './protocol-instructions.component.html'
})
export class ProtocolInstructionsComponent implements OnInit {

  loadingStatus: LoadingStatus;
  success = false;

  hasQuestionViewPermission: boolean;

  statuses: any[];
  @Input() data: InstrQuestion[];

  constructor(public $state: StateService,
              private nsiResourceService: NsiResourceService,
              private authorizationService: AuthorizationService) {
  }

  ngOnInit() {
    this.loadingStatus = LoadingStatus.LOADING;

    this.hasQuestionViewPermission = this.authorizationService.check('MEETING_QUESTION_CARD');

    this.nsiResourceService.get('InstructionStatus').then((statuses) => {
      this.statuses = statuses;
    }).then(() => {
      this.loadingStatus = LoadingStatus.SUCCESS;
      this.success = true;
    }).catch(() => {
      this.loadingStatus = LoadingStatus.ERROR;
    })
  }

}
