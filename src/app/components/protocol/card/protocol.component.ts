import {Component, OnInit} from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {Protocol} from "../../../models/Protocol";
import {ProtocolService} from "../../../services/protocol.service";
import {MeetingTypeService} from "../../../services/meeting-type.service";
import {StateService, Transition} from '@uirouter/core';
import {compare} from 'fast-json-patch';
import * as angular from "angular";
import * as _ from "lodash";
import {AgendaService} from "../../../services/agenda.service";
import {BreadcrumbsService} from "@reinform-cdp/skeleton";
import {LoadingStatus} from "@reinform-cdp/widgets";

@Component({
  selector: 'meeting-protocol',
  templateUrl: './protocol.component.html'
})
export class ProtocolComponent implements OnInit {

  origProtocol: Protocol;
  protocol: Protocol;

  loadingStatus: LoadingStatus;
  success: boolean;

  editing: boolean;

  constructor(private protocolService: ProtocolService,
              private agendaService: AgendaService,
              private meetingTypeService: MeetingTypeService,
              private toastr: ToastrService,
              private breadcrumbsService: BreadcrumbsService,
              private transition: Transition,
              private $state: StateService) {
  }

  ngOnInit() {
    this.editing = this.transition.params()['editing'];
    const agendaID: string = this.transition.params()['agendaID'];

    const id: string = this.transition.params()['id'];
    let data: Promise<Protocol>;
    this.loadingStatus = LoadingStatus.LOADING;
    if (!id) {
      data = this.newProtocol(agendaID);
      this.editing = true;
    } else {
      data = this.loadProtocol(id);
    }
    data.then((val: Protocol) => {
      this.origProtocol = angular.copy(val);
      this.protocol = angular.copy(val);
      this.loadingStatus = LoadingStatus.SUCCESS;
      this.updateBreadcrumbs();
      this.success = true;
    }).catch(() => {
      this.loadingStatus = LoadingStatus.ERROR;
    })
  }

  reload(): void {
    this.loadProtocol(this.protocol.protocolID);
  }

  newProtocol(agendaID: string): Promise<Protocol> {
    if (agendaID) {
      return this.agendaService.createProtocol(agendaID, false).toPromise();
    } else {
      return Promise.resolve(new Protocol());
    }
  }

  testVar: any = 'test';
  loadProtocol(id: string): Promise<Protocol> {
    return this.protocolService.get(id).toPromise().then(wrapper => {
      if (!this.meetingTypeService.checkMeetingType(wrapper.document.protocol.meetingType)) {
        this.toastr.warning("У вас не прав для просмотра этой страницы.", "Ошибка");
        return <Promise<Protocol>>Promise.reject("У вас не прав для просмотра этой страницы.")
      }
      this.testVar = JSON.stringify(wrapper.document.protocol);
      this.testVar = JSON.stringify(Promise.resolve(wrapper.document.protocol));
      return Promise.resolve(wrapper.document.protocol);
    });
  }

  private createProtocol(): Promise<string> {
    return this.protocolService.create(this.protocol).toPromise();
  }

  private updateProtocol(): Promise<Protocol> {
    let diff = this.protocolService.diff(this.origProtocol, this.protocol);
    return this.protocolService.update(this.origProtocol.protocolID, diff).toPromise();
  }

  saveProtocol(): void {
    if (this.protocol.protocolID) {
      this.updateProtocol().then((result) => {
        this.protocol = angular.copy(result);
        this.origProtocol = angular.copy(result);
        this.editing = false;
      })
    } else {
      this.createProtocol().then((id: string) => {
        this.$state.go('app.meeting.protocol', {id: id, editing: false});
      })
    }
  }

  cancelEdit() {
    this.protocol = angular.copy(this.origProtocol);
    this.edit(false);
  }

  edit(edit: boolean) {
    this.editing = edit;
  }

  updateBreadcrumbs() {
    this.breadcrumbsService.setBreadcrumbsChain([{
      title: this.protocol.receivedFromMKA && this.protocol.receivedFromMKA.FromMKA ? 'Регламентные совещания МКА' : 'Регламентные совещания',
      url: this.protocol.receivedFromMKA && this.protocol.receivedFromMKA.FromMKA ? this.$state.href('app.meeting.showcase-mka', {}) : this.$state.href('app.meeting.showcase', {})
    }, {
      title: 'Протокол',
      url: null
    }]);
  }

}
