import {StateService} from '@uirouter/core';
import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {
  Protocol, ProtocolApprovedBy, ProtocolAttended, ProtocolDraftingGroupInvest, ProtocolDraftingGroupMKA,
  ProtocolProtocolGroup, ProtocolQuestion, ProtocolSigner
} from "../../../../models/Protocol";
import {LoadingStatus} from "@reinform-cdp/widgets";
import {BsLocaleService} from "ngx-bootstrap/datepicker";
import {RoleParticipant} from "../../../../models/nsi/RoleParticipant";
import {Question, QuestionDocumentWrapper} from "../../../../models/Question";
import {Decision} from "../../../../models/nsi/Decision";
import {CategoryQuestion} from "../../../../models/nsi/CategoryQuestion";
import {AuthorizationService} from "@reinform-cdp/security";
import {ToastrService} from "ngx-toastr";
import {QuestionService} from "../../../../services/question.service";
import {DepartmentBean, NsiResourceService, UserBean} from "@reinform-cdp/nsi-resource";
import * as _ from "lodash";
import * as angular from "angular";

@Component({
  selector: 'meeting-protocol-edit',
  templateUrl: './protocol-edit.component.html',
  styleUrls: ['./protocol-edit.component.scss']
})
export class ProtocolEditComponent implements OnInit {

  hasQuestionViewPermission: boolean;

  @Input() protocol: Protocol;
  questions: { [key: string]: Question };
  origMeetingAttendees: RoleParticipant[];
  meetingAttendees: ProtocolAttended[];
  decisionTypes: { [key: string]: Decision[] };
  redactorGroupMemberInvest: ProtocolSigner[];
  redactorGroupMemberMKA: ProtocolSigner[];
  protocolGroupMember: ProtocolSigner[];
  protocolApprovers: ProtocolApprovedBy[];
  questionCategories: CategoryQuestion[];
  @Output() saveProtocol: EventEmitter<any> = new EventEmitter();
  @Output() cancel: EventEmitter<any> = new EventEmitter();

  departments: any[];
  internalAttendees: any[];
  updatingUsersListInProgress: boolean = false;
  externalAttendees: any[];

  loadingStatus: LoadingStatus;
  success: boolean;

  constructor(private $state: StateService,
              private authorizationService: AuthorizationService,
              private questionService: QuestionService,
              private nsiResourceService: NsiResourceService,
              private localeService: BsLocaleService,
              private toastr: ToastrService) {
  }

  ngOnInit() {
    this.loadingStatus = LoadingStatus.LOADING;
    this.success = false;
    this.localeService.use('ru');
    this.hasQuestionViewPermission = this.authorizationService.check('MEETING_QUESTION_CARD');
    if (!this.authorizationService.check('MEETING_PROTOCOL_FORM')) {
      this.toastr.warning("У вас нет прав для просмотра этой страницы.", "Ошибка");
      return Promise.reject("У вас нет прав для просмотра этой страницы.");
    }

    this.protocol.draftingGroupMKA = this.protocol.draftingGroupMKA || new ProtocolDraftingGroupMKA();
    this.protocol.draftingGroupInvest = this.protocol.draftingGroupInvest || new ProtocolDraftingGroupInvest();
    this.protocol.protocolGroup = this.protocol.protocolGroup || new ProtocolProtocolGroup();

    return this.loadQuestions().then(() => {
      return this.loadDictionaries();
    }).then(() => {
      this.loadingStatus = LoadingStatus.SUCCESS;
      this.success = true;
    }).catch(() => {
      this.loadingStatus = LoadingStatus.ERROR;
    });
  }

  loadQuestions(): Promise<any> {
    if (!this.protocol.question) {
      return Promise.resolve();
    }
    return Promise.all(
      this.protocol.question.map(q => this.questionService.get(q.questionID).toPromise())
    ).then((result: QuestionDocumentWrapper[]) => {
      this.questions = {};
      result.forEach(wrapper => {
        let question: Question = wrapper.document.question;
        this.questions[question.questionID] = question;
      });
      return true;
    })
  }

  loadDictionaries(): Promise<any> {
    let convertSigner = (user: UserBean): ProtocolSigner => {
      return {
        iofShort: user.iofShort,
        post: user.post,
        fioShort: user.fioShort,
        accountName: user.accountName
      }
    };
    let convertApprover = (user: UserBean): ProtocolApprovedBy => {
      return {
        iofShort: user.iofShort,
        post: user.post,
        fioFull: user.displayName,
        accountName: user.accountName
      }
    };
    return Promise.all([
      this.nsiResourceService.get('mggt_meeting_RolesParticipants'),
      this.nsiResourceService.getBy('mggt_meeting_Decisions', {
        nickAttr: 'meetingTypeCode',
        values: [this.protocol.meetingType]
      }),
      this.nsiResourceService.ldapUsers('OASI_MEETING_REDACTORGROUP', 'MKA_OTHER_INVEST'),
      this.nsiResourceService.ldapUsers('OASI_MEETING_REDACTORGROUP', 'MKA'),
      this.nsiResourceService.ldapUsers('OASI_MEETING_PROTOCOLGROUP'),
      this.nsiResourceService.ldapUsers('OASI_MEETING_PROTOCOLAPPROVE'),
      this.nsiResourceService.get('mggt_meeting_CategoryQuestion'),
      this.nsiResourceService.departments()
    ]).then((values: [RoleParticipant[], Decision[], UserBean[], UserBean[], UserBean[], UserBean[], CategoryQuestion[], DepartmentBean[]]) => {
      this.origMeetingAttendees = values[0].filter((el) => {
        if (el.LevelRoleCode === 'organization') {
          return el;
        }
      });
      this.meetingAttendees = _.map(this.origMeetingAttendees, (val: RoleParticipant) => {
        const result = new ProtocolAttended();
        result.name = val.RoleName;
        result.code = val.RoleCode;
        result.role = {
          roleCode: val.RoleCode,
          roleName: val.RoleName
        };
        return result;
      });
      this.decisionTypes = _.groupBy(values[1], (v: Decision) => v.questionCode);
      this.redactorGroupMemberInvest = _.map(values[2], convertSigner);
      this.redactorGroupMemberMKA = _.map(values[3], convertSigner);
      this.protocolGroupMember = _.map(values[4], convertSigner);
      this.protocolApprovers = _.map(values[5], convertApprover);
      this.questionCategories = values[6];
      this.departments = values[7];
      this.prepareAttendees();
      return true;
    });
  }

  decisionTypeChanged(question: ProtocolQuestion) {
    let questionCategoryCode = this.questions[question.questionID].questionConsider.questionCategoryCode;
    let decisionType: Decision = _.find(this.decisionTypes[questionCategoryCode], (decisionType: Decision) => {
      return decisionType.decisionTypeCode === question.decisionTypeCode;
    });
    question.decisionType = decisionType.decisionType;
  }

  attendedAdded() {
    this.externalAttendees = _.sortBy(this.externalAttendees, (att: any) => {
      return _.findIndex(this.origMeetingAttendees, (orig) => {
        return orig.RoleCode === att.code;
      });
    })
  }

  save() {
    this.protocol.attended = [];
    if (this.internalAttendees.length && this.internalAttendees[0].result) {
      this.internalAttendees.map(el => {
        this.protocol.attended.push(el.result);
      });
    }
    if (this.externalAttendees.length) {
      this.protocol.attended = this.protocol.attended.concat(this.externalAttendees);
    }
    this.saveProtocol.emit();
  }

  clearAttendees() {
    this.externalAttendees = [];
  }

  departmentsChange(department, index) {
    this.updatingUsersListInProgress = true;
    this.internalAttendees[index].users = [];
    this.nsiResourceService.ldapUsers(null, department.description).then((users: any) => {
      this.internalAttendees[index].users = users;
      this.internalAttendees[index].result = {};
      this.updatingUsersListInProgress = false;
    });
  }

  prepareAttendees() {
    this.internalAttendees = [];
    this.externalAttendees = [];

    if (this.protocol.attended && this.protocol.attended.length) {

      this.protocol.attended.map((el) => {
        if (el.internal && el.internal === true) {
          this.internalAttendees.push({result: el, department: el.name});
        }
        else {
          this.externalAttendees.push(el);
        }
      });

      if (!this.internalAttendees.length) {
        this.internalAttendees.push({});
      }

      if (!this.externalAttendees.length && !this.protocol.agendaID) {
        this.externalAttendees = this.meetingAttendees;
      }

    }
    else {
      this.internalAttendees.push({});
      if (!this.protocol.agendaID) {
        this.externalAttendees = this.meetingAttendees;
      }
    }


  }

  internalAttendeeChange(attendee, index) {
    attendee.name = attendee.departmentFullName;
    attendee.code = attendee.departmentCode;
    attendee.internal = true;
    attendee.fioFull = attendee.displayName;
    this.internalAttendees[index].result = attendee;
  }

  addInternalAttendee() {
    this.internalAttendees.push({});
  }

  removeInternalAttendee(index) {
    if (this.internalAttendees.length === 1) {
      this.internalAttendees[0] = {};
      return;
    }
    this.internalAttendees.splice(index, 1);
  }

}
