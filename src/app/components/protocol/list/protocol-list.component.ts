import { Component, OnInit, Input } from '@angular/core';
import { FacetedSearchResult, CdpSearchExtData, SearchExtDataItem } from '@reinform-cdp/search-resource';
import { MeetingType } from '../../../models/nsi/MeetingType';
import { MeetingTypeService } from '../../../services/meeting-type.service';
import { Sorting } from '../../../models/Sorting';
import { DateRangeFilter } from '../../../models/DateRangeFilter';
import { formatDate } from '@angular/common';
import {SolrMediatorService} from '../../../services/solr-mediator.service';
import {formatAsDate} from '../../../services/date-util.service';
import {HelperService} from '../../../services/helper.service';

export class Filter {
  protocolApproved: string;
  meetingDate: DateRangeFilter = new DateRangeFilter();
  meetingTypes: string[];
  meetingNumber: string;

  toSearchFields(): SearchExtDataItem[] {
    const result = [];
    if (this.protocolApproved) {
      result.push(new SearchExtDataItem('protocolApproved', this.protocolApproved));
    }
    if (this.meetingDate) {
      this.addDateRangeFilter(result, 'meetingDate', this.meetingDate);
    }
    if (this.meetingTypes && this.meetingTypes.length) {
      result.push(new SearchExtDataItem('meetingType', this.meetingTypes));
    }
    if (this.meetingNumber) {
      result.push(new SearchExtDataItem('meetingNumber', this.meetingNumber));
    }
    return result;
  }

  toQueryString(): string {
    let r = '';
    const _and = () => r ? ' AND ' : '';
    const _stars = str => '(*' + str.split(/\s+/).join('* AND *') + '*)';
    const _range = (name, source) => {
      return '(' + name + ':['
        + (source.from ? formatAsDate(source.from) + 'T00:00:00Z' : '*') + ' TO '
        + (source.to ? formatAsDate(source.to) + 'T23:59:59Z' : '*')
        + '])';
    };

    if (this.protocolApproved) {
      r += _and() + 'protocolApproved:' + this.protocolApproved;
    }
    if (this.meetingDate && (this.meetingDate.from || this.meetingDate.to)) {
     r += _and() + _range('meetingDate', this.meetingDate);
    }
    if (this.meetingTypes && this.meetingTypes.length) {
      r += _and() +  '(meetingType:(' + this.meetingTypes.join(') OR meetingType:(') + '))';
    }
    if (this.meetingNumber) {
      r += _and() + '(meetingNumber:(' + _stars(this.meetingNumber) + '))';
    }
    return r;
  }

  addDateRangeFilter(result, propName, value: DateRangeFilter) {
    const obj = { from: null, to: null };
    if (value.from) { obj.from = `${formatDate(value.from, 'yyyy-MM-dd', 'en')}T00:00:00Z`; }
    if (value.to) { obj.to = `${formatDate(value.to, 'yyyy-MM-dd', 'en')}T00:00:00Z`; }
    if (value.from || value.to) { result.push(new SearchExtDataItem(propName, obj)); }
  }

}

@Component({
  selector: 'meeting-protocol-list',
  templateUrl: './protocol-list.component.html',
  styleUrls: ['./protocol-list.component.scss']
})
export class ProtocolListComponent implements OnInit {
  @Input() isExternal = false;
  isLoading: boolean;
  searchRequest: CdpSearchExtData;
  searchResult: FacetedSearchResult;
  currentPage: number;
  view = 'list';
  extSearchCollapsed = true;
  sorting: Sorting;
  filter: Filter;
  common: string;

  meetingTypes: MeetingType[];

  constructor(private solrMediator: SolrMediatorService,
    private meetingTypeService: MeetingTypeService,
    private helper: HelperService) {
    this.isLoading = true;
  }

  ngOnInit() {
    this.searchResult = { facets: {}, docs: [], numFound: 0, pageSize: 0, start: 0 };
    this.searchRequest = new CdpSearchExtData(null, 0, 10, null, null, [], [this.solrMediator.types.protocol]);
    this.sorting = new Sorting('meetingDate', 'desc', 'По дате рассмотрения');
    this.filter = new Filter();

    if (this.isExternal) {
      this.meetingTypeService.getNonExpressExternalMeetingTypes().then(meetingTypes => {
        this.meetingTypes = meetingTypes;
        this.search();
      });
    } else {
      this.meetingTypeService.getNonExpressMeetingTypes().then(meetingTypes => {
        this.meetingTypes = meetingTypes;
        this.search();
      });
    }
  }

  search() {
    this.isLoading = true;
    this.searchRequest.sort = this.sorting.name + ' ' + this.sorting.value;
    // this.searchRequest.common = this.common;
    // this.searchRequest.fields = this.getSearchFields();
    this.searchRequest.query = this.getSearchQuery();
    this.solrMediator.query(this.searchRequest).subscribe(resp => {
      this.searchResult = resp;
      this.isLoading = false;
    }, err => {
      this.helper.error(err);
      console.log(err);
      this.isLoading = false;
    });
  }

  getSearchFields(): SearchExtDataItem[] {
    let result = [];
    result.push(new SearchExtDataItem('meetingType', this.meetingTypes.map(mt => mt.meetingType)));
    if (this.filter) {
      result = result.concat(this.filter.toSearchFields());
    }
    return result;
  }

  getSearchQuery(): string {
    let r = '';
    r += '(meetingType:(' + this.meetingTypes.map(mt => mt.meetingType).join(') OR meetingType:(') + '))';
    if (this.filter) {
      const queryFilter = this.filter.toQueryString();
      if (queryFilter) {
        r += ' AND ' + '(' + queryFilter + ')';
      }
    }
    return r;
  }

  sortingChange(sorting: Sorting) {
    this.sorting = sorting;
    this.searchRequest.page = 0;
    this.search();
  }

  filterChange(filter: Filter) {
    this.filter = filter;
    this.searchRequest.page = 0;
    this.search();
  }

  commonChange(common: string) {
    this.searchRequest.common = common;
    this.searchRequest.page = 0;
    this.search();
  }

  changePage() {
    this.searchRequest.page = this.currentPage - 1;
    this.search();
  }
}
