import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {MeetingType} from "../../../models/nsi/MeetingType";
import {MeetingTypeService} from "../../../services/meeting-type.service";
import { BsLocaleService } from "ngx-bootstrap/datepicker";
import {Filter} from "./protocol-list.component";

@Component({
  selector: 'meeting-protocol-list-filters',
  templateUrl: './protocol-list-filters.component.html',
  styleUrls: ['./protocol-list-filters.component.scss']
})
export class ProtocolListFiltersComponent implements OnInit {

  @Input() filter: Filter;
  @Output() filterChanged: EventEmitter<any> = new EventEmitter();
  @Input() isExternal: boolean = false;

  protocolApproved: {protocolApproved: string, protocolApprovedName: string}[];
  meetingTypes: MeetingType[];

  constructor(private meetingTypeService: MeetingTypeService,
              private localeService: BsLocaleService) {
  }

  ngOnInit() {
    this.localeService.use('ru');
    this.loadDictionaries();
  }

  loadDictionaries(): Promise<any> {
    this.protocolApproved = [{
      protocolApproved: 'true',
      protocolApprovedName: 'Утвержден'
    }, {
      protocolApproved: 'false',
      protocolApprovedName: 'Не утвержден'
    }];
    return Promise.all([
      this.isExternal ? this.meetingTypeService.getNonExpressExternalMeetingTypes() 
      : this.meetingTypeService.getNonExpressMeetingTypes()
    ]).then((result: [MeetingType[]]) => {
      this.meetingTypes = result[0];
    })
  }

  search() {
    this.filterChanged.emit(this.filter);
  }


  clear() {
    this.filter.protocolApproved = null;
    this.filter.meetingDate.clear();
    this.filter.meetingTypes = [];
    this.filter.meetingNumber = null;
  }
}
