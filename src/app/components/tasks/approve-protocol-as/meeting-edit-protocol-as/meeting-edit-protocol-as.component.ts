import { PlatformConfig } from '@reinform-cdp/core';
import { ActiveTaskService, ActivityResourceService, ITask } from '@reinform-cdp/bpm-components';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { CdpReporterResourceService } from '@reinform-cdp/reporter-resource';
import { IWordReporterResult } from "@reinform-cdp/reporter-resource/dist/models/IWordReporterResult";
import { NsiResourceService } from '@reinform-cdp/nsi-resource';
import { SearchResultDocument } from '@reinform-cdp/search-resource';
import { ToastrService } from 'ngx-toastr';
import { AlertService, LoadingStatus } from '@reinform-cdp/widgets';
import { Component, OnInit } from '@angular/core';
import { forkJoin, from, of, throwError } from 'rxjs/index';
import { catchError, mergeMap, tap } from 'rxjs/internal/operators';
import { ActivitiService } from '../../../../services/activity.service';
import {
  Protocol, ProtocolApproval, ProtocolApprovalCycleAgreed, ProtocolApprovalCycleAgreedBy,
  ProtocolQuestion,
  DsType
} from '../../../../models/Protocol';
import { ProtocolService } from '../../../../services/protocol.service';
import { AgendaService } from '../../../../services/agenda.service';
import { Agenda, AgendaQuestion, AgendaQuestionStatus, AgendaUser } from '../../../../models/Agenda';
import { Question, QuestionDocumentWrapper } from '../../../../models/Question';
import { QuestionService } from '../../../../services/question.service';
import { ExpressService } from '../../../../services/express.service';
import { Observable } from 'rxjs/Rx';
import * as angular from 'angular';
import * as _ from 'lodash';
import { InstructionsService } from '../../../../services/instructions.service';
import { MeetingTypeService } from '../../../../services/meeting-type.service';
import { MeetingType } from '../../../../models/nsi/MeetingType';
import { FileType } from '../../../../models/FileType';
import { ProtocolInstructionsUtils } from '../../../../utils/protocol-instructions.utils';

@Component({
  selector: 'mggt-meeting-edit-protocol-as',
  templateUrl: './meeting-edit-protocol-as.component.html'
})
export class MeetingEditProtocolAsComponent implements OnInit {

  @BlockUI('editProtocol') blockUI: NgBlockUI;

  protocol: Protocol;
  copyProtocol: Protocol;
  agenda: Agenda;
  agendaQuestions: AgendaQuestion[];
  questions: Question[] = [];
  meetingType: MeetingType;
  instructions: _.Dictionary<SearchResultDocument[]>;
  task: ITask;
  loadingStatus: LoadingStatus = LoadingStatus.LOADING;
  success = false;

  attended: string[];

  // itemNumbers: string[] = [];
  decisionTexts: string[] = [];

  responsibles1 = [];

  statuses: any[];

  files: FileType[];

  historyCycleExpanded: any = {};

  constructor(public activeTaskService: ActiveTaskService,
    public activityRestService: ActivityResourceService,
    public activitiService: ActivitiService,
    public protocolService: ProtocolService,
    public agendaService: AgendaService,
    public questionService: QuestionService,
    public expressService: ExpressService,
    public instructionsService: InstructionsService,
    public meetingTypesService: MeetingTypeService,
    public wordReporterService: CdpReporterResourceService,
    public nsiRestService: NsiResourceService,
    public alertService: AlertService,
    public toastrService: ToastrService,
    public platformConfig: PlatformConfig) {
  }

  ngOnInit() {
    from(this.activeTaskService.getTask()).pipe(
      mergeMap(() => {
        return this.activeTaskService.getTask();
      }),
      mergeMap(task => {
        this.task = task;
        return this.activitiService.getEntityIdVar(this.task);
      }),
      mergeMap(resp => {
        return this.protocolService.get(resp);
      }),
      mergeMap(response => {
        this.setProtocol(response.document.protocol);
        this.attended = this.protocol.attended.map(a => a.iofShort);
        return this.agendaService.get(this.protocol.agendaID);
      }),
      mergeMap(response => {
        this.agenda = response.document.agenda;
        return this.loadDictionaries();
      }),
      mergeMap(() => {
        return this.agendaService.includedQuestions(this.agenda.agendaID);
      }),
      mergeMap((included: AgendaQuestion[]) => {
        this.agendaQuestions = included;
        return this.loadQuestions(included);
      }),
      mergeMap(() => {
        return this.loadInstructions();
      })
    ).subscribe(() => {
      this.loadingStatus = LoadingStatus.SUCCESS;
      this.success = true;
    }, error => {
      console.log(error);
      this.loadingStatus = LoadingStatus.ERROR;
    });
  }

  loadDictionaries(): Observable<any> {
    return forkJoin([
      this.meetingTypesService.getMeetingType(this.agenda.meetingType),
      this.nsiRestService.get('InstructionStatus'),
    ]).pipe(
      tap((result: any) => {
        this.meetingType = result[0];
        this.statuses = result[1];
      })
    );
  }

  setProtocol(protocol: Protocol): void {
    this.copyProtocol = angular.copy(protocol);

    if (!protocol.question) {
      protocol.question = [];
    }

    this.protocol = angular.copy(protocol);
    this.files = this.protocol.fileProtocolDraft ? [this.protocol.fileProtocolDraft] : [];
  }


  loadQuestions(agendaQuestions: AgendaQuestion[]): Observable<any> {
    if (!agendaQuestions || !agendaQuestions.length) {
      return of([]);
    }

    const questions = agendaQuestions.filter(aq => aq.status !== AgendaQuestionStatus.EXCLUDED)
      .map(aq => this.questionService.get(aq.questionId));

    if (!questions.length) {
      return of([]);
    }

    return forkJoin(questions).pipe(
      tap((wrappers: QuestionDocumentWrapper[]) => {
        this.questions = wrappers.map(w => w.document.question);
        this.decisionTexts = this.questions.map((q, idx) => {
          const pq = this.protocol.question.find(pq => pq.questionID === q.questionID);
          const aq = agendaQuestions.find(pq => pq.questionId === q.questionID);
          const oldDecisionText = this.decisionTexts[idx];

          q.questionConsider.itemNumber = aq.itemNumber || q.questionConsider.itemNumber;

          if (oldDecisionText !== undefined) {
            return oldDecisionText;
          }

          return pq ? pq.decisionText : '';
        });
      })
    );
  }

  loadInstructions(): Observable<any> {
    const primaryQuestionsIds = _.values(this.questions).map(q => q.questionPrimaryID);
    if (!primaryQuestionsIds || !primaryQuestionsIds.length) {
      return of('');
    }

    this.instructions = {};
    const date = new Date(this.agenda.meetingDate.getTime());
    date.setDate(date.getDate() + 1);
    return from(this.instructionsService.getAgendaInstructions(this.agenda, this.meetingType, primaryQuestionsIds)).pipe(
      tap(result => {
        this.questions.forEach(q => {
          this.instructions[q.questionID] = result.docs ? result.docs.filter(d => d.questionPrimaryID === q.questionPrimaryID) : [];
        });
      })
    );
  }

  allQuestionsSaved(): boolean {
    return !this.questions.some(q => !q.questionID);
  }

  addQuestion(): void {
    if (!this.allQuestionsSaved()) {
      this.toastrService.error('Не все вопросы сохранены!');
      return;
    }
    this.questions.push(new Question());
    this.decisionTexts.push('');
  }

  removeQuestion(index: number): void {
    let questionID = this.questions[index].questionID;
    if (this.instructions[questionID] && this.instructions[questionID].length) {
      this.alertService.message({
        message: 'По данному вопросу есть поручения. Для того чтобы удалить вопрос, необходимо сначала удалить поручение',
        type: 'warning'
      });
      return;
    }
    this.alertService.confirm({
      message: 'Вы уверены, что хотите удалить вопрос?',
      okButtonText: 'Удалить'
    }).then(() => {
      const questionID = this.questions[index].questionID;

      this.questions.splice(index, 1);
      this.decisionTexts.splice(index, 1);
      if (questionID) {
        this.blockUI.start();
        this.expressService.deleteAgendaQuestion(this.agenda.agendaID, questionID).pipe(
          mergeMap((agendaQuestions: AgendaQuestion[]) => {
            this.agendaQuestions = agendaQuestions;

            return this.loadQuestions(agendaQuestions);
          }),
          mergeMap(() => {
            return this.deleteProtocolQuestion(questionID);
          }),
          tap((protocol: Protocol) => {
            this.setProtocol(protocol);
          })
        ).subscribe(() => {
          this.blockUI.stop();
        }, error => {
          console.log(error);
          this.toastrService.error('Произошла ошибка при удалении вопроса.');
          this.blockUI.stop();
        });
      }
    });
  }

  saveQuestion(index: number): void {
    const question = this.questions[index];

    if (question.questionID) {
      this.toastrService.error('Вопрос уже сохранен!');
      return;
    }

    this.alertService.confirm({
      message: 'Вы уверены, что хотите добавить вопрос?',
      okButtonText: 'Добавить'
    }).then(() => {
      this.blockUI.start();
      this.expressService
        .addAgendaQuestion(this.agenda.agendaID, question, this.agenda)
        .pipe(
          mergeMap((agendaQuestions: AgendaQuestion[]) => {
            this.agendaQuestions = agendaQuestions;

            return this.loadQuestions(agendaQuestions);
          }),
          mergeMap(() => {
            const aq = this.agendaQuestions[index];
            const protocolQuestion = new ProtocolQuestion();
            protocolQuestion.questionID = aq.questionId;
            protocolQuestion.itemNumber = aq.itemNumber;
            protocolQuestion.decisionText = this.decisionTexts[index];
            protocolQuestion.question = question.questionConsider.question;

            this.instructions[aq.questionId] = [];

            return this.addProtocolQuestion(protocolQuestion);
          }),
          tap((protocol: Protocol) => {
            this.setProtocol(protocol);
          })
        ).subscribe(() => {
          this.blockUI.stop();
        }, error => {
          console.log(error);
          this.toastrService.error('Произошла ошибка при сохранении вопроса.');
          this.blockUI.stop();
        });
    });
  }

  private addProtocolQuestion(pq: ProtocolQuestion): Observable<Protocol> {
    const protocol = angular.copy(this.protocol);
    // const copyProtocol = angular.copy(this.protocol);

    protocol.question.forEach((q) => {
      const aq = this.questions.find(aq => aq.questionID === q.questionID);
      q.itemNumber = aq.questionConsider.itemNumber;

      return q;
    });

    protocol.question.push(pq);

    return this.protocolService.update(protocol.protocolID, this.protocolService.diff(this.copyProtocol, protocol));
  }

  private deleteProtocolQuestion(questionId: string): Observable<Protocol> {
    const protocol = angular.copy(this.protocol);
    // copyProtocol = angular.copy(this.protocol);
    protocol.question = protocol.question.filter(q => q.questionID !== questionId).map((q) => {
      const aq = this.questions.find(aq => aq.questionID === q.questionID);
      q.itemNumber = aq.questionConsider.itemNumber;

      return q;
    });
    return this.protocolService.update(protocol.protocolID, this.protocolService.diff(this.copyProtocol, protocol));
  }

  private swapProtocolQuestions(questionId1: string, questionId2: string): Observable<Protocol> {
    const protocol = angular.copy(this.protocol);
    // copyProtocol = angular.copy(this.protocol);

    const index1 = protocol.question.findIndex(iq => iq.questionID === questionId1);
    const index2 = protocol.question.findIndex(iq => iq.questionID === questionId2);

    const q = protocol.question[index1];
    protocol.question[index1] = protocol.question[index2];
    protocol.question[index2] = q;

    const itemNumber = protocol.question[index1].itemNumber;
    protocol.question[index1].itemNumber = protocol.question[index2].itemNumber;
    protocol.question[index2].itemNumber = itemNumber;

    return this.protocolService.update(protocol.protocolID, this.protocolService.diff(this.copyProtocol, protocol));
  }

  openResponsibles(): void {
    this.responsibles1 = this.agenda.attended.map((u: any) => ({
      departmentPrepareFull: u.departmentFullName,
      departmentPrepareShort: null,
      departmentPrepareCode: u.departmentCode,
      contractorPrepare: u.accountName,
      contractorPrepareFIO: u.fioFull,
      contractorPreparePhone: u.telephoneNumber
    }));
  }

  addInstruction(index: number): void {
    const question = this.questions[index];

    let href = window.location.href;
    href = href.substring(href.lastIndexOf('execution/') + 'execution/'.length);
    href = href.indexOf('?') ? href.substring(0, href.indexOf('?')) : href;
    const params = href.split('/');
    const processPrefix = params[0];

    const url = `${window.location.protocol}//${window.location.host}/` +
      `sdo/instruction/#/app/instruction/instruction/createFromTask/${params[0]}/${params[1]}/${params[2]}/${question.questionID}/${question.questionPrimaryID}` +
      `?expressMeetingID=${this.agenda.agendaID}&systemCode=${this.platformConfig.systemCode}`;
    window.open(url, '_blank');
  }

  createProtocolDraft(): any {
    this.blockUI.start();

    const protocol = angular.copy(this.protocol);
    this.decisionTexts.forEach((decisionText, index) => {
      const questionId = this.questions[index].questionID;
      const protocolQuestion = protocol.question.find(q => q.questionID === questionId);
      if (protocolQuestion) {
        protocolQuestion.itemNumber = this.questions[index].questionConsider.itemNumber;
        protocolQuestion.decisionText = decisionText;
      }
    });
    this._saveProtocol(this.copyProtocol, protocol).pipe(
      mergeMap(() => {
        return this.protocolService.get(this.protocol.protocolID);
      }),
      mergeMap((response) => {
        this.setProtocol(response.document.protocol);
        this.attended = this.protocol.attended.map(a => a.iofShort);

        return this.protocolService
          .generatePrintForm(this.protocol, this.meetingType, this.questions.map((q) => ({
            questionId: q.questionID,
            instructions: this.instructions[q.questionID] || [],
          })), this.agenda, true);
      }),
    ).subscribe((file: FileType) => {
      this.files = [file];

      this.protocol.fileProtocolDraft = _.first(this.files);
      this.blockUI.stop();
    }, e => {
      console.log(e);
      this.toastrService.error('При формировании протокола возникла ошибка. Повторите попытку');
      this.blockUI.stop();
    });
  }

  save(): any {
    if (!this.allQuestionsSaved()) {
      this.toastrService.warning('Сохраните добавленные вопросы');
      return;
    }
    this.blockUI.start();
    const protocol = angular.copy(this.protocol);
    // copyProtocol = angular.copy(this.protocol);
    protocol.fileProtocolDraft = _.first(this.files);
    this.decisionTexts.forEach((decisionText, index) => {
      const questionId = this.questions[index].questionID;
      const protocolQuestion = protocol.question.find(q => q.questionID === questionId);
      if (protocolQuestion) {
        protocolQuestion.itemNumber = this.questions[index].questionConsider.itemNumber;
        protocolQuestion.decisionText = decisionText;
      }
    });
    this._saveProtocol(this.copyProtocol, protocol).subscribe(
      () => {
        this.blockUI.stop();
        window.location.href = '/main/#/app/tasks';
      },
      (error) => {
        console.log(error);
        this.blockUI.stop();
      });
  }

  sendToApproval(): void {
    if (!this.files.length) {
      this.toastrService.success('Не задан файл проекта протокола!');
      return;
    }
    if (!this.allQuestionsSaved()) {
      this.toastrService.warning('Сохраните добавленные вопросы');
      return;
    }
    this.blockUI.start();
    const protocol = angular.copy(this.protocol);
    // copyProtocol = angular.copy(this.protocol);
    protocol.fileProtocolDraft = _.first(this.files);
    this.decisionTexts.forEach((decisionText, index) => {
      const questionId = this.questions[index].questionID;
      const protocolQuestion = protocol.question.find(q => q.questionID === questionId);
      if (protocolQuestion) {
        protocolQuestion.decisionText = decisionText;
      }
    });
    if (!protocol.approval) {
      const approvalCycleNum = protocol.approvalHistory ? protocol.approvalHistory.approvalCycle.length + 1 : 1;
      protocol.approval = new ProtocolApproval();
      protocol.approval.approvalCycle.approvalCycleNum = '' + approvalCycleNum;
    }
    protocol.approval.approvalCycle.agreed.push(this.createInitiatorAgreed(this.agenda.initiator, 'approval', 'Утверждение'));
    this._saveProtocol(this.copyProtocol, protocol).pipe(
      mergeMap(() => {
        return this._finishTask();
      })
    ).subscribe(() => {
      this.blockUI.stop();
      window.location.href = '/main/#/app/tasks';
    }, (error) => {
      console.log(error);
      this.blockUI.stop();
    });
  }

  swapQuestions(index1: number, index2: number): void {
    this.alertService.confirm({
      message: 'Вы уверены, что хотите поменять вопросы местами?',
      okButtonText: 'Поменять'
    }).then(() => {
      const q1 = this.questions[index1], q2 = this.questions[index2];
      this.blockUI.start();
      const id1 = q1.questionID, id2 = q2.questionID;
      this.expressService.swapAgendaQuestion(this.agenda.agendaID, id1, id2).pipe(
        mergeMap((agendaQuestions: AgendaQuestion[]) => {
          this.agendaQuestions = agendaQuestions;
          const decisionText1 = this.decisionTexts[index1] || '';
          const decisionText2 = this.decisionTexts[index2] || '';

          this.decisionTexts[index1] = decisionText2;
          this.decisionTexts[index2] = decisionText1;

          return this.loadQuestions(agendaQuestions);
        }),
        mergeMap(() => {
          return this.swapProtocolQuestions(id1, id2);
        }),
        tap((protocol: Protocol) => {
          this.setProtocol(protocol);
        })
      ).subscribe(() => {
        this.blockUI.stop();
      }, error => {
        console.log(error);
        this.toastrService.error('Произошла ошибка при изменении вопросов местами.');
        this.blockUI.stop();
      });
    });
  }

  private _saveProtocol(copyProtocol: Protocol, protocol: Protocol): Observable<any> {
    return this.protocolService.update(protocol.protocolID, this.protocolService.diff(copyProtocol, protocol)).pipe(
      tap(() => {
        this.toastrService.success('Документ успешно сохранен!');
      }),
      catchError(error => {
        this.toastrService.success('Произошла ошибка при сохранении документа!');
        return throwError(error);
      })
    );
  }

  private _finishTask(): Observable<any> {
    return from(this.activityRestService.finishTask(parseInt(this.task.id))).pipe(
      tap(() => {
        this.toastrService.success('Задача успешно завершена!');
      }),
      catchError(error => {
        this.toastrService.success('Произошла ошибка при завершении задачи!');
        return throwError(error);
      })
    );
  }

  private createInitiatorAgreed(initiator: AgendaUser, approvalTypeCode: string, approvalType: string): ProtocolApprovalCycleAgreed {
    const agreed = new ProtocolApprovalCycleAgreed();
    agreed.approvalTypeCode = approvalTypeCode;
    agreed.approvalType = approvalType;
    agreed.agreedBy = new ProtocolApprovalCycleAgreedBy();
    agreed.agreedBy.accountName = initiator.accountName;
    agreed.agreedBy.fioFull = initiator.fioFull;
    agreed.agreedBy.iofShort = initiator.iofShort;
    agreed.agreedBy.post = initiator.post;
    return agreed;
  }

}
