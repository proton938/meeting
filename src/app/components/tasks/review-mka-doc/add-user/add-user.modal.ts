import {Component, OnInit} from '@angular/core';
import {BsModalRef} from 'ngx-bootstrap';

@Component({
  selector: 'add-user-modal',
  templateUrl: './add-user.modal.html'
})
export class AddUserModalComponent implements OnInit {

  submit: boolean;
  user: any;

  constructor(private bsModalRef: BsModalRef) {}

  ngOnInit() {
    this.submit = false;
  }

  create() {
    this.submit = true;
    this.bsModalRef.hide();
  }

  cancel() {
    this.submit = false;
    this.bsModalRef.hide();
  }

}
