import * as angular from 'angular';
import * as _ from 'lodash';
import {StateService, Transition} from '@uirouter/core';
import {ActiveTaskService, ActivityResourceService, ITask} from '@reinform-cdp/bpm-components';
import {LoadingStatus} from '@reinform-cdp/widgets';
import {Component, OnInit, OnDestroy} from '@angular/core';
import {map, mergeMap, tap} from 'rxjs/internal/operators';
import {Observable, from, of} from 'rxjs';
import {ModalService} from '../../../../services/modal.service';
import {ActivitiService} from '../../../../services/activity.service';
import {AgendaService} from '../../../../services/agenda.service';
import {AgendaDocument, AgendaReview, AgendaUserType, AgendaDocumentWrapper} from '../../../../models/Agenda';
import {MeetingTypeService} from '../../../../services/meeting-type.service';
import {SessionStorage} from '@reinform-cdp/security';
import {ToastrService} from 'ngx-toastr';
import {SearchResultDocument} from '@reinform-cdp/search-resource';
import {NsiResourceService} from '@reinform-cdp/nsi-resource';
import {MeetingType} from '../../../../models/nsi/MeetingType';
import {HelperService} from '../../../../services/helper.service';

@Component({
  selector: 'mggt-agenda-review-mka-doc',
  templateUrl: './agenda-review-mka-doc.component.html'
})
export class AgendaReviewMKADocComponent implements OnInit, OnDestroy {

  agenda: AgendaDocumentWrapper;
  copyDocument: AgendaDocumentWrapper;
  document: AgendaDocument;
  usersList: any;
  task: ITask;
  loadingStatus: LoadingStatus = LoadingStatus.LOADING;
  success = false;
  reviewNote: any;
  agendaType: any;
  currentUser: AgendaUserType = new AgendaUserType;
  processId: string;
  sentForReviewExpanded = false;
  internalInstructionsExpanded = false;
  dicts: any = {
    statuses: [],
    themes: [],
    priorities: [],
    difficulties: [],
    executors: [],
    _themes: []
  };
  insideOrder: SearchResultDocument[] = [];
  meetingType: MeetingType;

  constructor(public activeTaskService: ActiveTaskService,
              public activitiService: ActivitiService,
              public $state: StateService,
              public transition: Transition,
              public modalService: ModalService,
              public agendaService: AgendaService,
              public activityRestService: ActivityResourceService,
              public meetingTypeService: MeetingTypeService,
              public session: SessionStorage,
              public toastr: ToastrService,
              private nsiResourceService: NsiResourceService,
              public helper: HelperService) {
  }

  ngOnInit() {
    this.helper.beforeUnload.init();
    this.currentUser.accountName = this.session.login();
    this.currentUser.fio = this.session.fioShort();
    this.currentUser.post = this.session.post();
    from(this.loadDictionaries()).pipe(
      mergeMap(() => {
        return this.activeTaskService.getTask();
      }),
      mergeMap(task => {
        this.task = task;
        return this.activitiService.getEntityIdVar(this.task);
      }),
      mergeMap(resp => {
        return this.agendaService.get(resp);
      }),
      mergeMap(response => {
        this.agenda = <AgendaDocumentWrapper>response;
        this.document = this.agenda.document;
        this.copyDocument = angular.copy(this.agenda);
        return this.meetingTypeService.getMeetingType(this.document.agenda.meetingType);
      }),
      mergeMap(response => {
        this.agendaType = response.meetingFullName;
        return this.agendaService.getInsideAgenda(this.document.agenda.agendaID);
      }),
      tap(response => {
        this.insideOrder = response || [];
      }),
      mergeMap(() => {
        const review: AgendaReview = _.find(this.agenda.document.agenda.review, f => {
          return f.reviewBy && f.reviewBy.accountName === this.currentUser.accountName;
        });
        if (review && !review.reviewPlanDate) {
          review.reviewPlanDate = this.task.dueDate;
          return this.agendaService.updateDocument(this.document.agenda.agendaID, this.copyDocument, this.agenda).pipe(
            tap(response => {
              this.agenda = {document: {agenda: response}};
              this.document = this.agenda.document;
              this.copyDocument = angular.copy(this.agenda);
            })
          );
        }
        return of(this.document.agenda);
      })
    ).subscribe(() => {
      this.loadingStatus = LoadingStatus.SUCCESS;
      this.success = true;
    }, error => {
      console.log(error);
      this.loadingStatus = LoadingStatus.ERROR;
    });

  }

  ngOnDestroy() {
    this.helper.beforeUnload.destroy();
  }

  loadDictionaries(): Promise<any> {
    return Promise.all([
      this.nsiResourceService.getDictsFromCache(['InstructionPriority', 'InstructionStatus'])
    ]).then((response: any) => {
      this.dicts.statuses = response[0].InstructionStatus;
      this.dicts.priorities = response[0].InstructionPriority;
    });
  }

  createInstruction() {
    const host = `${window.location.protocol}//${window.location.host}`;
    const backUrl = encodeURIComponent(window.location.href);
    window.location.href = host + '/sdo/instruction/#/app/instruction/instruction/new?field=AgendaID' +
      '&id=' + this.document.agenda.agendaID + '&backUrl=' + backUrl;
  }

  addUser() {
    this.helper.beforeUnload.start();
    this.modalService.addUser().pipe(
      mergeMap(response => {
        this.usersList = response;
        return this.getProcessId('sdoagenda_reviewMkaDocumentHand');
      }), mergeMap(response => {
        this.processId = response.data[0].id;
        return this.agendaService.get(this.document.agenda.agendaID);
      }), mergeMap(response => {
        this.agenda = <AgendaDocumentWrapper>response;
        this.document = this.agenda.document;
        this.copyDocument = angular.copy(this.agenda);
        if (this.usersList.length > 0) {
          this.usersList.forEach(l => {
            const reviewBy: any = new AgendaUserType();
            reviewBy.accountName = l.accountName;
            reviewBy.post = l.post;
            reviewBy.fio = l.displayName;

            const newReview: AgendaReview = new AgendaReview();
            newReview.sentReviewBy = this.currentUser;
            newReview.reviewBy = reviewBy;
            this.document.agenda.review = this.document.agenda.review ? this.document.agenda.review : [];
            this.document.agenda.review.push(newReview);

            this.activityRestService.initProcess({
              processDefinitionId: this.processId,
              variables: [
                {'name': 'EntityIdVar', 'value': this.document.agenda.agendaID},
                {'name': 'sdoUserVar', 'value': l.accountName}
              ]
            });
          });
        }
        this.agenda.document = this.document;
        return this.agendaService.updateDocument(this.document.agenda.agendaID, this.copyDocument,
          this.agenda, 'Задача успешно обновлена!');
      })
    ).subscribe(() => {
      this.helper.beforeUnload.stop();
    }, error => {
      this.helper.beforeUnload.stop();
      console.log(error);
    });
  }


  finishTask() {
    this.helper.beforeUnload.start();
    this.agendaService.get(this.document.agenda.agendaID).pipe(
      mergeMap(response => {
        this.copyDocument = <AgendaDocumentWrapper>response;
        this.document.agenda.review = this.document.agenda.review ? this.document.agenda.review : [];

        let review: AgendaReview = this.document.agenda.review.find(r => {
          return r.reviewBy && r.reviewBy.accountName === this.session.login() && !r.factReviewBy;
        });
        if (review) {
          review.reviewFactDate = new Date();
          review.reviewNote = this.reviewNote;
          review.factReviewBy = this.currentUser;
        } else {
          review = new AgendaReview();
          review.reviewFactDate = new Date();
          review.reviewNote = this.reviewNote;
          review.factReviewBy = this.currentUser;
          this.document.agenda.review.push(review);
        }
        this.agenda.document = this.document;
        return this.agendaService.updateDocument(this.document.agenda.agendaID, this.copyDocument,
          this.agenda, 'Задача успешно обновлена!');
      }),
      mergeMap(() => {
        return this.activityRestService.finishTask(parseInt(this.task.id));
      })
    ).subscribe(() => {
      this.helper.beforeUnload.stop();
      this.toastr.success('Задача успешно завершена!');
      window.location.href = '/main/#/app/tasks';
    }, error => {
      this.helper.beforeUnload.stop();
      this.toastr.error('Ошибка при завершении задачи!');
      console.log(error);
    });
  }

  getProcessId(name: string): Observable<any> {
    return from(this.activityRestService.getProcessDefinitions({key: name, latest: true})).pipe(
      map(id => {
        return id;
      })
    );
  }

}
