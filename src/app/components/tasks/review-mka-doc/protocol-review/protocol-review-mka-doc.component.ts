import * as angular from 'angular';
import * as _ from 'lodash';
import {StateService} from '@uirouter/core';
import {ActiveTaskService, ActivityResourceService, ITask} from '@reinform-cdp/bpm-components';
import {LoadingStatus} from '@reinform-cdp/widgets';
import {Component, OnInit, OnDestroy} from '@angular/core';
import {catchError, mergeMap, tap, map} from 'rxjs/internal/operators';
import {ActivitiService} from '../../../../services/activity.service';
import {ProtocolService} from '../../../../services/protocol.service';
import {SessionStorage} from '@reinform-cdp/security';
import {ToastrService} from 'ngx-toastr';
import {ProtocolDocument, ProtocolDocumentWrapper, ProtocolUserType, ProtocolReview} from '../../../../models/Protocol';
import {MeetingTypeService} from '../../../../services/meeting-type.service';
import {ModalService} from '../../../../services/modal.service';
import {Observable, EMPTY, from, of, throwError} from 'rxjs';
import {SearchResultDocument} from '@reinform-cdp/search-resource';
import {NsiResourceService} from '@reinform-cdp/nsi-resource';
import {HelperService} from '../../../../services/helper.service';

@Component({
  selector: 'mggt-protocol-review-mka-doc',
  templateUrl: './protocol-review-mka-doc.component.html'
})
export class ProtocolReviewMkaDocComponent implements OnInit, OnDestroy {

  protocol: ProtocolDocumentWrapper;
  copyDocument: ProtocolDocumentWrapper;
  document: ProtocolDocument;
  usersList: any;
  task: ITask;
  loadingStatus: LoadingStatus = LoadingStatus.LOADING;
  success = false;
  reviewNote: any;
  protocolType: any;
  currentUser: ProtocolUserType = new ProtocolUserType;
  processId: string;
  sentForReviewExpanded = false;
  internalInstructionsExpanded = false;
  dicts: any = {
    statuses: [],
    themes: [],
    priorities: [],
    difficulties: [],
    executors: [],
    _themes: []
  };
  insideOrder: SearchResultDocument[] = [];

  constructor(public activeTaskService: ActiveTaskService,
              public activitiService: ActivitiService,
              public $state: StateService,
              public meetingTypeService: MeetingTypeService,
              public protocolService: ProtocolService,
              public activityRestService: ActivityResourceService,
              public modalService: ModalService,
              public session: SessionStorage,
              public toastr: ToastrService,
              private nsiResourceService: NsiResourceService,
              public helper: HelperService) {
  }

  ngOnInit() {
    this.helper.beforeUnload.init();
    this.currentUser.accountName = this.session.login();
    this.currentUser.fioFull = this.session.fullName();
    this.currentUser.post = this.session.post();
    from(this.loadDictionaries()).pipe(
      mergeMap(() => {
        return this.activeTaskService.getTask();
      }),
      mergeMap(task => {
        this.task = task;
        return this.activitiService.getEntityIdVar(this.task);
      }),
      mergeMap(resp => {
        return this.protocolService.get(resp);
      }),
      mergeMap(response => {
        this.protocol = <ProtocolDocumentWrapper>response;
        this.document = this.protocol.document;
        this.copyDocument = angular.copy(this.protocol);
        return this.meetingTypeService.getMeetingType(this.document.protocol.meetingType);
      }), mergeMap(result => {
        this.protocolType = result.meetingFullName;
        return this.protocolService.getInsideProtocol(this.document.protocol.protocolID);
      }), tap(response => {
        this.insideOrder = response || [];
      }),
      mergeMap(() => {
        const review: ProtocolReview = _.find(this.protocol.document.protocol.review, f => {
          return f.reviewBy && f.reviewBy.accountName === this.currentUser.accountName;
        });
        if (review && !review.reviewPlanDate) {
          review.reviewPlanDate = this.task.dueDate;
          return this.protocolService.updateDocument(this.document.protocol.protocolID, this.copyDocument, this.protocol).pipe(
            tap(response => {
              this.protocol = {document: {protocol: response}};
              this.document = this.protocol.document;
              this.copyDocument = angular.copy(this.protocol);
            })
          );
        }
        return of(this.document.protocol);
      })
    ).subscribe(() => {
      this.loadingStatus = LoadingStatus.SUCCESS;
      this.success = true;
    }, error => {
      console.log(error);
      this.loadingStatus = LoadingStatus.ERROR;
    });
  }

  ngOnDestroy() {
    this.helper.beforeUnload.destroy();
  }

  createInstruction() {
    const host = `${window.location.protocol}//${window.location.host}`;
    const backUrl = encodeURIComponent(window.location.href);
    window.location.href = host + '/sdo/instruction/#/app/instruction/instruction/new?field=ProtocolID' +
      '&id=' + this.document.protocol.protocolID + '&backUrl=' + backUrl;
  }

  addUser() {
    this.helper.beforeUnload.start();
    this.modalService.addUser().pipe(
      mergeMap(response => {
        this.usersList = response;
        return this.getProcessId('sdoprotocol_reviewMkaDocumentHand');
      }), mergeMap(response => {
        this.processId = response.data[0].id;
        return this.protocolService.get(this.document.protocol.protocolID);
      }), mergeMap(response => {
        this.protocol = <ProtocolDocumentWrapper>response;
        this.document = this.protocol.document;
        this.copyDocument = angular.copy(this.protocol);
        if (this.usersList.length > 0) {
          this.usersList.forEach(l => {
            const reviewBy = new ProtocolUserType();
            reviewBy.accountName = l.accountName;
            reviewBy.post = l.post;
            reviewBy.fioFull = l.displayName;

            const newReview = new ProtocolReview();
            newReview.sentReviewBy = this.currentUser;
            newReview.reviewBy = reviewBy;
            this.document.protocol.review = this.document.protocol.review ? this.document.protocol.review : [];
            this.document.protocol.review.push(newReview);

            this.activityRestService.initProcess({
              processDefinitionId: this.processId,
              variables: [
                {'name': 'EntityIdVar', 'value': this.document.protocol.protocolID},
                {'name': 'sdoUserVar', 'value': l.accountName}
              ]
            });
          });
        }
        this.protocol.document = this.document;
        return this.protocolService.updateDocument(this.document.protocol.protocolID, this.copyDocument, this.protocol);
      })
    ).subscribe(() => {
      this.helper.beforeUnload.stop();
    }, error => {
      this.helper.beforeUnload.stop();
      console.log(error);
    });
  }

  loadDictionaries(): Promise<any> {
    return Promise.all([
      this.nsiResourceService.getDictsFromCache(['InstructionPriority', 'InstructionStatus'])
    ]).then((response: any) => {
      this.dicts.statuses = response[0].InstructionStatus;
      this.dicts.priorities = response[0].InstructionPriority;
    });
  }

  finishTask() {
    this.helper.beforeUnload.start();
    this.protocolService.get(this.document.protocol.protocolID).pipe(
      mergeMap(response => {
        this.copyDocument = <ProtocolDocumentWrapper>response;
        this.document.protocol.review = this.document.protocol.review ? this.document.protocol.review : [];

        let review: ProtocolReview = this.document.protocol.review.find(r => {
          return r.reviewBy.accountName === this.currentUser.accountName && !r.factReviewBy.accountName;
        });
        if (review) {
          review.reviewFactDate = new Date();
          review.reviewNote = this.reviewNote;
          review.factReviewBy = this.currentUser;
        } else {
          review = new ProtocolReview();
          review.reviewFactDate = new Date();
          review.reviewNote = this.reviewNote;
          review.factReviewBy = this.currentUser;
          this.document.protocol.review.push(review);
        }

        this.protocol.document = this.document;
        return this.protocolService.updateDocument(this.document.protocol.protocolID, this.copyDocument, this.protocol);
      }), mergeMap(() => {
        return this.activityRestService.finishTask(parseInt(this.task.id));
      }), tap(() => {
        this.helper.beforeUnload.stop();
        this.toastr.success('Задача успешно завершена!');
        window.location.href = '/main/#/app/tasks';
      }), catchError(error => {
        this.helper.beforeUnload.stop();
        this.toastr.error('Ошибка при завершении задачи!');
        console.log(error);
        return throwError(error);
      })
    ).subscribe();
  }

  getProcessId(name: string): Observable<any> {
    return from(this.activityRestService.getProcessDefinitions({key: name, latest: true})).pipe(
      map(id => {
        return id;
      })
    );
  }


}
