import * as angular from 'angular';
import * as _ from 'lodash';
import * as moment_ from 'moment';
import { StateService, } from '@uirouter/core';
import { BlockUI, NgBlockUI } from "ng-block-ui";
import { ActiveTaskService, ActivityResourceService, ITask } from '@reinform-cdp/bpm-components';
import { LoadingStatus, ExFileType } from '@reinform-cdp/widgets';
import { Component, OnInit } from '@angular/core';
import { catchError, mergeMap, tap, map } from 'rxjs/internal/operators';
import { ICertificateInfoEx, SessionStorage, SignService } from '@reinform-cdp/security';
import { FileResourceService, IFileInFolderInfo } from '@reinform-cdp/file-resource';
import { CdpReporterResourceService } from "@reinform-cdp/reporter-resource";
import { ActivitiService } from '../../../services/activity.service';
import { ProtocolService } from '../../../services/protocol.service';
import { ToastrService } from 'ngx-toastr';
import { ProtocolDocument, ProtocolDocumentWrapper, ProtocolUserType, ProtocolApprovalCycleAgreedDs, ProtocolApprovalHistory, ProtocolApprovalHistoryCycle } from '../../../models/Protocol';
import { MeetingTypeService } from '../../../services/meeting-type.service';
import { EMPTY, from, of, throwError, forkJoin, } from 'rxjs';
import { Observable } from 'rxjs/Rx';
import { NsiResourceService } from '@reinform-cdp/nsi-resource';
import { Agenda } from '../../../models/Agenda';
import { AgendaService } from '../../../services/agenda.service';
import { RenameFileModalComponent } from '../../common/rename-file/rename-file.modal';
import { FileType } from '../../../models/FileType';
import { BsModalService } from 'ngx-bootstrap';

const moment = moment_;

@Component({
  selector: 'mggt-protocol-sign',
  templateUrl: './protocol-sign.component.html'
})
export class ProtocolSignComponent implements OnInit {

  @BlockUI('signProtocol') blockUI: NgBlockUI;

  protocol: ProtocolDocumentWrapper;
  copyDocument: ProtocolDocumentWrapper;
  document: ProtocolDocument;
  agenda: Agenda;
  task: ITask;
  loadingStatus: LoadingStatus = LoadingStatus.LOADING;
  success = false;
  approvalNote: any;
  currentUser: ProtocolUserType = new ProtocolUserType;
  processId: string;
  dicts: any = {
    statuses: [],
    themes: [],
    priorities: [],
    difficulties: [],
    executors: [],
    _themes: []
  };
  files: FileType[] = [];

  openSignDialog: () => void;
  openMobileSignDialog: () => void;

  protocolFile: ExFileType;

  uploading = false;
  fileQueue: File[] = [];
  fileQueueToDisplay: FileType[] = [];

  checkSign: string;

  sendingBackToWork = false;
  approving = false;

  isMobile: boolean = false;

  constructor(
    public activeTaskService: ActiveTaskService,
    public activitiService: ActivitiService,
    public $state: StateService,
    public meetingTypeService: MeetingTypeService,
    public protocolService: ProtocolService,
    public activityRestService: ActivityResourceService,
    public modalService: BsModalService,
    public session: SessionStorage,
    public toastr: ToastrService,
    private nsiResourceService: NsiResourceService,
    private agendaService: AgendaService,
    private fileResourceService: FileResourceService,
    private signService: SignService,
    private reporterResourceService: CdpReporterResourceService,
  ) {
  }

  ngOnInit() {
    this.currentUser.accountName = this.session.login();
    this.currentUser.fioFull = this.session.fullName();
    this.currentUser.post = this.session.post();
    from(this.loadDictionaries()).pipe(
      mergeMap(() => {
        return this.activeTaskService.getTask();
      }),
      mergeMap(task => {
        this.task = task;
        return this.activitiService.getEntityIdVar(this.task);
      }),
      mergeMap(resp => {
        return this.protocolService.get(resp);
      }),
      mergeMap(response => {
        this.protocol = <ProtocolDocumentWrapper>response;
        this.document = this.protocol.document;
        this.copyDocument = angular.copy(this.protocol);
        this.protocolFile = FileType.toExFileType(this.document.protocol.fileProtocolDraft);

        return this.meetingTypeService.getMeetingType(this.document.protocol.meetingType);
      }),
      mergeMap(() => {
        return this.agendaService.get(this.document.protocol.agendaID);
      }),
      mergeMap(response => {
        this.agenda = response.document.agenda;

        return this.signService.checkSign();
      }),
      tap((res) => {
        this.checkSign = res;
        return of(this.document.protocol);
      }),
    ).subscribe(() => {
      this.loadingStatus = LoadingStatus.SUCCESS;
      this.success = true;
    }, error => {
      this.loadingStatus = LoadingStatus.ERROR;
    });
  }

  loadDictionaries(): Promise<any> {
    return Promise.all([
      this.nsiResourceService.getDictsFromCache(['InstructionPriority', 'InstructionStatus'])
    ]).then((response: any) => {
      this.dicts.statuses = response[0].InstructionStatus;
      this.dicts.priorities = response[0].InstructionPriority;
    });
  }

  approve() {
    if (this.isMobile) {
      this.openMobileSignDialog();
    } else {
      this.openSignDialog();
    }
  }

  test() {
    const ds = {
      dsLastName: 'Ivanov',
      dsName: 'Ivan',
      dsPosition: '-',
      dsCN: '123123123',
      dsFrom: moment().format('YYYY-MM-DD'),
      dsTo: moment().format('YYYY-MM-DD')
    };

    return this.applyProtocolStamp(this.protocolFile, { ds: ds }).pipe(
      mergeMap(id => {
        // @ts-ignore
        return this.convertFileTypeToPdf(id, 'test');
      }),
      mergeMap((response: IFileInFolderInfo) => {
        const { versionSeriesGuid, fileName, fileSize, dateCreated, fileType, mimeType } = response;
        const exFile = ExFileType.create(versionSeriesGuid, fileName, fileSize, false, new Date(dateCreated).getTime(), fileType, mimeType);
        const file = FileType.create(versionSeriesGuid, fileName, fileSize.toString(), false, new Date(dateCreated).toISOString(), fileType, mimeType);

        _.extend(this.protocolFile, exFile);

        this.document.protocol.fileProtocol = [file];

        return of(this.protocolFile);
      })
    ).toPromise();
  }

  applyProtocolStamp(file: ExFileType, signInfo: { ds: any }): Observable<String> {
    return from(this.reporterResourceService.filenet2Filenet(<any>{
      jsonTxt: JSON.stringify({
        document: {
          protocol: {
            dsProtocolChairman: signInfo.ds,
          }
        }
      }),
      filenetTemplate: {
        templateVersionSeriesGuid: file.idFile
      },
      filenetDestination: {
        reportFileName: file.nameFile,
        reportFileType: file.typeFile,
        reportFolderGuid: this.agenda.folderId,
        fileName: file.nameFile,
        fileType: file.typeFile,
        folderGuid: this.agenda.folderId,
      },
      rootJsonPath: "$",
      options: {
        onProcess: file.nameFile.replace('.docx', ''),
        placeholderCode: 'sn',
        strictCheckMode: true,
        xwpfResultDocumentDescr: file.nameFile,
        xwpfTemplateDocumentDescr: ""
      }
    })).pipe(
      map(response => response.versionSeriesGuid)
    );
  }

  // tslint:disable-next-line: member-ordering
  beforeSign = function (fileInfo: ExFileType, folderGuid, certEx) {
    const ds = {
      dsLastName: certEx.lastName,
      dsName: certEx.firstName,
      dsPosition: certEx.position,
      dsCN: certEx.serialNumber,
      dsFrom: moment(certEx.from, 'DD.MM.YYYY HH:mm:ss').format('YYYY-MM-DD'),
      dsTo: moment(certEx.till, 'DD.MM.YYYY HH:mm:ss').format('YYYY-MM-DD')
    };

    console.log('beforeSign', JSON.stringify(certEx));

    return this.applyProtocolStamp(this.protocolFile, { ds: ds }).pipe(
      mergeMap(id => {
        return this.convertFileTypeToPdf(id, fileInfo.nameFile);
      }),
      mergeMap((response: IFileInFolderInfo) => {
        const { versionSeriesGuid, fileName, fileSize, dateCreated, fileType, mimeType } = response;
        const exFile = ExFileType.create(versionSeriesGuid, fileName, fileSize, false, new Date(dateCreated).getTime(), fileType, mimeType);
        const file = FileType.create(versionSeriesGuid, fileName, fileSize.toString(), false, new Date(dateCreated).toISOString(), fileType, mimeType);

        _.extend(this.protocolFile, exFile);

        this.document.protocol.fileProtocol = [file];

        return of(this.protocolFile);
      })
    ).toPromise();
  }.bind(this);

  // tslint:disable-next-line: member-ordering
  afterSign = function (result: any) {
    if (result.error) {
      console.error(result);
      this.approving = false;
      return;
    }

    let certEx: ICertificateInfoEx = result.result.certEx;
    console.log('beforeSign', JSON.stringify(certEx));
    this.protocolFile.signed = true;

    _.find(this.document.protocol.fileProtocol, (f: FileType) => f.idFile === this.protocolFile.idFile).signed = true;

    this.document.protocol.approval.approvalCycle.agreed.some((agreed) => {
      if (agreed.agreedBy.accountName === this.session.login()) {
        agreed.approvalResult = 'Утверждено';
        agreed.approvalNote = this.approvalNote;
        agreed.approvalFactDate = new Date();
        agreed.agreedDs = {
          agreedDsLastName: certEx.lastName,
          agreedDsName: certEx.firstName,
          agreedDsPosition: certEx.position,
          agreedDsCN: certEx.serialNumber,
          agreedDsFrom: moment(certEx.from, 'DD.MM.YYYY HH:mm:ss').toDate(),
          agreedDsTo: moment(certEx.till, 'DD.MM.YYYY HH:mm:ss').toDate()
        };

        return true;
      }
      return false;
    });

    this.document.protocol.dsProtocolChairman = {
      dsLastName: certEx.lastName,
      dsName: certEx.firstName,
      dsPosition: certEx.position,
      dsCN: certEx.serialNumber,
      dsFrom: moment(certEx.from, 'DD.MM.YYYY HH:mm:ss').toDate(),
      dsTo: moment(certEx.till, 'DD.MM.YYYY HH:mm:ss').toDate(),
    };

    this.blockUI.start();
    this.protocolService.updateDocument(this.document.protocol.protocolID, this.copyDocument, this.protocol).pipe(
      tap(() => {
        return this.activityRestService.finishTask(parseInt(this.task.id), [
          { name: 'IsApproved', value: true }
        ]);
      })
    ).subscribe(
      () => {
        this.approving = false;
        this.blockUI.stop();
        this.toastr.success('Электронный документ успешно создан и подписан');
        window.location.href = '/main/#/app/tasks';
      },
      (err) => {
        console.error(err);
        if (err.documentID && err.documentTypeValue) {
          this.redirectToTask({
            documentID: err.documentID,
            documentTypeValue: err.documentTypeValue
          });
        }
        this.approving = false;
        this.blockUI.stop();
      });
  }.bind(this);

  reject() {
    if (!this.approvalNote) {
      this.toastr.error('Не указан комментарий');
      return;
    }
    this.blockUI.start();
    this.sendingBackToWork = true;

    this.document.protocol.approval.approvalCycle.agreed.some((agreed) => {
      if (agreed.agreedBy.accountName === this.session.login()) {
        agreed.approvalNote = this.approvalNote;
        agreed.fileApproval = this.files;
        agreed.approvalResult = 'Не утверждено';
        agreed.approvalFactDate = new Date();

        return true;
      }
      return false;
    });

    const approvalHistory = this.document.protocol.approvalHistory || new ProtocolApprovalHistory();
    const protocolApprovalHistoryCycle = new ProtocolApprovalHistoryCycle();

    protocolApprovalHistoryCycle.approvalCycleNum = this.document.protocol.approval.approvalCycle.approvalCycleNum;
    protocolApprovalHistoryCycle.approvalCycleDate = this.document.protocol.approval.approvalCycle.approvalCycleDate;
    protocolApprovalHistoryCycle.approvalCycleFile = [this.document.protocol.fileProtocolDraft];
    protocolApprovalHistoryCycle.agreed = angular.copy(this.document.protocol.approval.approvalCycle.agreed);

    approvalHistory.approvalCycle = approvalHistory.approvalCycle || [];
    approvalHistory.approvalCycle.push(protocolApprovalHistoryCycle);

    this.document.protocol.approvalHistory = approvalHistory;
    delete this.document.protocol.approval;

    this.protocolService.updateDocument(this.document.protocol.protocolID, this.copyDocument, this.protocol).pipe(
      mergeMap(() => {
        return this.activityRestService.finishTask(parseInt(this.task.id), [
          { name: 'IsApproved', value: false }
        ]);
      })
    ).subscribe(
      () => {
        this.sendingBackToWork = false;
        this.toastr.success('Документ отправлен на доработку.');
        this.blockUI.stop();
        window.location.href = '/main/#/app/tasks';
      },
      (err) => {
        console.log(err);

        this.sendingBackToWork = false;
        this.blockUI.stop();
      }
    );
  }

  getProcessId(name: string): Observable<any> {
    return from(this.activityRestService.getProcessDefinitions({ key: name, latest: true })).pipe(
      map(id => {
        return id;
      })
    );
  }

  /**
   *  Добавляем файл в очередь, имитируя загрузку
   */
  addFileToQueue(file) {
    this.fileQueue.push(file);
    const fileType: FileType = FileType.create(undefined, file.name, file.size, false, new Date().toISOString(), file.type, 'MkaDocOther');
    this.fileQueueToDisplay.push(fileType);
  }

  /**
   *  Загрузка файла с проверкой имени
   */
  validateAndUploadFile(file, entityID: string, folderID: string): Observable<FileType> {
    this.uploading = true;
    const fd = new FormData();
    fd.append('file', file);
    fd.append('folderGuid', folderID);
    fd.append('fileType', 'MkaDocOther');
    fd.append('docEntityID', entityID);
    fd.append('docSourceReference', 'UI');
    return from(this.fileResourceService.handleFileUploadNew(fd)).pipe(
      map(res => {
        const fileType: FileType = FileType.create(res.guid, file.name, file.size, false, new Date().toISOString(), file.type, 'MkaDocOther');
        this.removeFileQueued(file);
        this.uploading = false;
        return fileType;
      }),
      catchError(err => {
        const { data } = err;
        let reason = '';
        // Иногда приходит HTML, например, при ошибке большого файла
        try {
          reason = JSON.parse(data).message;
        } catch (error) {
        }
        if (~reason.indexOf('File already exists')) {
          return this.renameFile(file.name).pipe(
            mergeMap((newName: string) => {
              const _file = new File([file], newName, { type: file.type });
              return this.validateAndUploadFile(_file, this.agenda.agendaID, this.agenda.folderId);
            }),
            catchError(cancel => {
              this.removeFileQueued(file);
              this.uploading = false;
              return EMPTY;
            })
          );
        } else {
          this.removeFileQueued(file);
          this.uploading = false;
          return EMPTY;
        }
      })
    );
  }

  renameFile(oldName: string): Observable<string> {
    const options = {
      initialState: {
        oldName: oldName
      },
      'class': 'modal-md'
    };
    const ref = this.modalService.show(RenameFileModalComponent, options);
    return this.modalService.onHide.pipe(
      mergeMap(() => {
        const component = <RenameFileModalComponent>ref.content;
        if (component.submit) {
          const newName = component.newName + '.' + component.extension;
          return of(newName);
        } else {
          return throwError('canceled');
        }
      })
    );
  }

  /**
   *  Удаление файла из filenet или из очереди
   */
  removeFile(file: FileType) {
    if (file.idFile) {
      from(this.fileResourceService.deleteFile(file.idFile)).subscribe(() => {
        const ind = this.agenda.filesAttach.findIndex(el => el.idFile === file.idFile);
        this.agenda.filesAttach.splice(ind, 1);
      });
    } else {
      let ind = this.fileQueue.findIndex(el => el.name === file.nameFile);
      this.fileQueue.splice(ind, 1);
      ind = this.fileQueueToDisplay.findIndex(el => el.nameFile === file.nameFile);
      this.fileQueueToDisplay.splice(ind, 1);
    }
  }

  /**
   *  Удаление файлa из очереди после загрузки или после отмены переименования
   */
  removeFileQueued(file) {
    const ind = this.fileQueue.findIndex(el => el.name === file.name);
    this.fileQueue.splice(ind, 1);
  }

  convertFileTypeToPdf(fileId: string, fileName: string): Observable<IFileInFolderInfo> {
    let request: any = {
      filenetDestination: {
        fileAttrs: [
          { attrName: 'docEntityID', attrValues: [this.document.protocol.protocolID] },
          { attrName: 'docSourceReference', attrValues: ['UI'] }
        ],
        fileName: this.getFileName(fileName) + '.pdf',
        fileType: 'MkaDocOther',
        folderGuid: this.document.protocol.folderId
      },
      wordVersionSeriesGuid: fileId
    };
    return from(this.reporterResourceService.word2pdf(request));
  }

  getFileName(fullFileName: string): string {
    let dotIndex = fullFileName.lastIndexOf(".");
    if (dotIndex < 0) {
      return fullFileName;
    } else {
      return fullFileName.substring(0, dotIndex);
    }
  }
}
