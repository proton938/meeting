import {StateService} from '@uirouter/core';
import {Component, OnInit} from '@angular/core';
import {EventObject, Options} from 'fullcalendar';
import {MeetingTypeService} from '../../services/meeting-type.service';
import {SessionStorage} from '@reinform-cdp/security';
import {AgendaService} from '../../services/agenda.service';
import {MeetingType} from '../../models/nsi/MeetingType';
import {Agenda, AgendaDocumentWrapper} from '../../models/Agenda';
import * as _ from 'lodash';
import * as moment_ from 'moment';
import {Protocol} from '../../models/Protocol';
import {ProtocolService} from '../../services/protocol.service';
import {BreadcrumbsService} from '@reinform-cdp/skeleton';
import {SolrMediatorService} from '../../services/solr-mediator.service';

const moment = moment_;

@Component({
  selector: 'meeting-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss']
})
export class CalendarComponent implements OnInit {

  meetingTypes: MeetingType[];
  meetingTypeMap: { [key: string]: MeetingType };
  calendarOptions: Options;

  constructor(private $state: StateService,
              private meetingTypeService: MeetingTypeService,
              private agendaService: AgendaService,
              private solrMediator: SolrMediatorService,
              private protocolService: ProtocolService,
              private breadcrumbsService: BreadcrumbsService,
              private session: SessionStorage) {
  }

  ngOnInit() {
    this.updateBreadcrumbs();
    this.meetingTypeService.getMeetingTypes().then(meetingTypes => {
      this.meetingTypes = meetingTypes;
      this.meetingTypeMap = _.keyBy(meetingTypes, 'meetingType');

      this.calendarOptions = {
        header: {
          left: 'prev,next today',
          center: 'title',
          right: 'month,agendaWeek,agendaDay',
        },
        buttonText: {
          prev: 'Пред',
          next: 'След',
          today: 'Сегодня',
          month: 'Месяц',
          week: 'Неделя',
          day: 'День'
        },
        defaultDate: new Date(),
        aspectRatio: 2.5,
        fixedWeekCount: false,
        locale: 'ru',
        buttonIcons: false, // show the prev/next text
        weekNumbers: false,
        navLinks: true, // can click day/week names to navigate views
        editable: false,
        eventLimit: true, // allow "more" link when too many events
        timeFormat: 'HH:mm',
        events: (start, end, timezone, callback) => this.getEvents(start, end, timezone, callback)
      };

    });
  }

  getEvents(start, end, timezone, callback: (events: EventObject[]) => void) {

    const login = this.session.login();
    if (this.meetingTypes.length === 0) {
      return;
    }
    const availableMeetingTypes = this.meetingTypes.map(el => el.meetingType);
    this.solrMediator.searchExt({
      pageSize: 1000,
      page: 0,
      sort: 'meetingDate asc',
      fields: [{
        name: 'meetingDate',
        value: {from: start.format(), to: end.format()}
      }, {
        name: 'meetingType',
        value: availableMeetingTypes
      }, {
        name: 'agendaPublished',
        value: 'NOT_NULL'
      }],
      types: [ this.solrMediator.types.agenda ]
    }).toPromise().then(result => {
      return result.docs ? Promise.all(
        result.docs.map(doc => this.agendaService.get(doc.documentId).toPromise())
      ) : Promise.resolve([]);
    }).then((wrappers: AgendaDocumentWrapper[]) => {
      const agendas = wrappers.map(wrapper => wrapper.document.agenda).filter(agenda => {
        const meetingType = this.meetingTypeMap[agenda.meetingType];
        return (agenda.status && agenda.status.code !== 'refused') &&
          (!meetingType.Express ||
          (agenda.attended && agenda.attended.some(att => att.accountName === login)) ||
          (agenda.authorCreate && agenda.authorCreate.accountName === login) ||
          (agenda.initiator && agenda.initiator.accountName === login));
      });
      this.loadProtocols(agendas).then(protocols => {
        const events = this.convertEvents(agendas, protocols);
        callback(events);
      });
    });
  }

  loadProtocols(agendas: Agenda[]): Promise<Protocol[]> {
    const load = agendas.filter(agenda => {
      return !this.meetingTypeMap[agenda.meetingType].Express;
    }).map(agenda => this.loadProtocol(agenda));
    return Promise.all(load);
  }

  convertEvents(agendas: Agenda[], protocols: Protocol[]): EventObject[] {
    const protocolMap = _.keyBy(protocols.filter(protocol => protocol), 'agendaID');
    return agendas.map(agenda => {
      const meetingType = this.meetingTypeMap[agenda.meetingType];
      return this.createEvent(meetingType, agenda, protocolMap[agenda.agendaID]);
      });
  }

  createEvent(meetingType: MeetingType, agenda: Agenda, protocol: Protocol): EventObject {
    let url: string;
    if (protocol && !meetingType.Express) {
      url = this.$state.href('app.meeting.protocol', {id: protocol.protocolID});
    } else {
      url = this.$state.href('app.meeting.agenda', {id: agenda.agendaID});
    }
    const { backgroundColor, textColor } = this.getEventColor(agenda);
    const result: EventObject = {
      id: agenda.agendaID,
      title: meetingType ? meetingType.meetingShortName : agenda.meetingType,
      start: moment(this.getDateTime(agenda.meetingDate, agenda.meetingTime)),
      url: url,
      backgroundColor,
      textColor
    };
    if (agenda.meetingTimeEnd) {
      result.end = moment(this.getDateTime(agenda.meetingDate, agenda.meetingTimeEnd));
    }
    return result;
  }

  getDateTime(date: Date, time: Date) {
    const dateTime: Date = new Date(date.getTime());
    dateTime.setHours(time.getHours());
    dateTime.setMinutes(time.getMinutes());
    return dateTime;
  }

  getEventColor(agenda: Agenda): {backgroundColor: string, textColor: string} {
    const meetingType: MeetingType = this.meetingTypes.find(mtd => mtd.meetingType === agenda.meetingType);
    if ( meetingType && meetingType.meetingColor ) {
      return {
        backgroundColor: meetingType.meetingColor,
        textColor: '#FFF',
      };
    }
    return {
      backgroundColor: '#DCDCDC',
      textColor: '#000',
    };
  }

  loadProtocol(agenda: Agenda): Promise<Protocol> {
    if (agenda.meetingNumber) {
      return this.protocolService.getByMeetingNumber(agenda.meetingType, agenda.meetingNumber, agenda.meetingDate.getFullYear())
        .toPromise().then(wrapper => {
          return wrapper ? wrapper.document.protocol : null;
        });
    } else {
      return Promise.resolve(null);
    }
  }

  updateBreadcrumbs() {
    this.breadcrumbsService.setBreadcrumbsChain([{
      title: 'Календарь совещаний',
      url: null
    }]);
  }

}
