import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {QuestionRG} from "../../../../models/Question";

@Component({
  selector: 'meeting-question-view-rg',
  templateUrl: './question-view-rg.component.html'
})
export class QuestionViewRgComponent implements OnInit {

  @Input() questionRG: QuestionRG;

  constructor() {
  }

  ngOnInit() {
  }

}
