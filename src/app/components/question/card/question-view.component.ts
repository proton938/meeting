import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {StateService} from '@uirouter/core';
import {LoadingStatus} from '@reinform-cdp/widgets';
import {BsModalService} from 'ngx-bootstrap';
import {SearchResultDocument} from '@reinform-cdp/search-resource';
import {MeetingTypeService} from '../../../services/meeting-type.service';
import * as _ from 'lodash';
import {ToastrService} from 'ngx-toastr';
import {Question} from '../../../models/Question';
import {Protocol} from '../../../models/Protocol';
import {FileType} from '../../../models/FileType';
import {QuestionService} from '../../../services/question.service';
import {AuthorizationService} from '@reinform-cdp/security';
import {AddMaterialsModalComponent, QuestionAddMaterialType} from './add-materials.modal';
import {SolrMediatorService} from '../../../services/solr-mediator.service';

@Component({
  selector: 'meeting-question-view',
  templateUrl: './question-view.component.html'
})
export class QuestionViewComponent implements OnInit {

  QUESTION_ADD_MATERIAL_TYPE_INQUIRY = 'Справка';
  QUESTION_ADD_MATERIAL_TYPE_PRESENTATION = 'Презентация';
  QUESTION_ADD_MATERIAL_TYPE_OTHER = 'Дополнительные материалы';

  @Input() question: Question;
  @Input() questionHistory: Question[];
  @Input() protocol: Protocol;
  @Output() edit: EventEmitter<any> = new EventEmitter();

  loadingStatus: LoadingStatus = LoadingStatus.LOADING;
  loadingMaterialsStatus: LoadingStatus;
  success = false;

  regularExternal: boolean;
  canDeleteMaterials = false;
  canViewProtocol = false;

  materialTypes: QuestionAddMaterialType[];
  availablePzz: SearchResultDocument | null;

  constructor(private authorizationService: AuthorizationService,
              private meetingTypeService: MeetingTypeService,
              private solrMediator: SolrMediatorService,
              private questionService: QuestionService,
              private toastr: ToastrService,
              private modalService: BsModalService) {
  }

  ngOnInit() {
    this.canDeleteMaterials = this.authorizationService.check('MEETING_QUESTION_MATERIALS_DELETE_BUTTON');
    this.canViewProtocol = this.authorizationService.check('MEETING_PROTOCOL_CARD');

    Promise.all([
      this.meetingTypeService.getMeetingType(this.question.meetingType)
    ]).then((data: any[]) => {
      //TODO: uncomment
      // const possiblePzzNumber = _.get(this.question, 'questionConsider.basis.number');
      const possiblePzzNumber = null;
      if (!possiblePzzNumber) {
        return [...data, {}];
      }
      return Promise.all([
        ...data,
        this.solrMediator.searchExt({
          type: 'PZZ',
          fields: [{
            name: 'docNumberPzz',
            value: possiblePzzNumber,
          }]
        })
      ]);
    }).then((data: any[]) => {
      const meetingType = data[0];
      this.regularExternal = meetingType && !meetingType.Express && meetingType.meetingExternal;
      this.question.planningMeeting = meetingType.planningMeeting;

      this.materialTypes = [{
        materialType: this.QUESTION_ADD_MATERIAL_TYPE_INQUIRY,
        maxFiles: 1, fileType: 'default',
        meetingType: null,
        editable: null
      }, {
        materialType: this.QUESTION_ADD_MATERIAL_TYPE_PRESENTATION,
        maxFiles: 1, fileType: 'default',
        meetingType: null,
        editable: null
      }, {
        materialType: this.QUESTION_ADD_MATERIAL_TYPE_OTHER,
        maxFiles: null,
        fileType: 'default',
        meetingType: null,
        editable: null
      }];

      if (data[1].docs && data[1].docs.length === 1) {
        this.availablePzz = data[1].docs[0];
      } else {
        this.availablePzz = null;
      }

      this.loadingStatus = LoadingStatus.SUCCESS;
      this.loadingMaterialsStatus = LoadingStatus.SUCCESS;
      this.success = true;
      //TODO:
      // this.$uiViewScroll(angular.element('ui-view'));

    }, () => {
      this.loadingStatus = LoadingStatus.ERROR;
      //TODO:
      // this.$uiViewScroll(angular.element('ui-view'));
    });
  }

  editCard() {
    this.edit.emit(true);
  }

  reloadMaterials() {
    this.loadingMaterialsStatus = LoadingStatus.LOADING;
    this.questionService.get(this.question.questionID).toPromise().then(wrapper => {
      this.question.questionConsider.materials = wrapper.document.question.questionConsider.materials;
      this.loadingMaterialsStatus = LoadingStatus.SUCCESS;
    }).catch(() => {
      this.loadingMaterialsStatus = LoadingStatus.ERROR;
    });
  }

  deleteDocument = ((fileInfo: FileType): any => {
    this.questionService.deleteMaterial(this.question, fileInfo.idFile).toPromise().then(() => {
      this.reloadMaterials();
    }).catch(() => {
      this.reloadMaterials();
    });
  }).bind(this);

  showAddMaterialsDialog() {
    if (!this.authorizationService.check('MEETING_QUESTION_MATERIALS_FORM')) {
      this.toastr.warning('У вас не прав для просмотра этой страницы.', 'Ошибка');
      return;
    }
    const meetingType = this.question.meetingType;
    const materialTypes: QuestionAddMaterialType[] = this.materialTypes.filter((mt) => {
      return (!mt.meetingType || mt.meetingType === meetingType) && this.isEditableMaterialType(mt);
    });
    const options = {
      initialState: {
        question: this.question,
        questionHistory: this.questionHistory,
        materialTypes: materialTypes
      },
      'class': 'modal-lg'
    };
    const ref = this.modalService.show(AddMaterialsModalComponent, options);
    const subscription = this.modalService.onHide.subscribe(() => {
      const component = <AddMaterialsModalComponent> ref.content;
      if (component.submit) {
        this.loadingMaterialsStatus = LoadingStatus.LOADING;
        this.reloadMaterials();
      }
      subscription.unsubscribe();
    });
  }

  getUrbanLimitsString() {
    if (this.question && this.question.questionConsider) {
      const urbanLimitsCode = this.question.questionConsider.urbanLimitsCode;
      return urbanLimitsCode === 'OTHER' ? this.question.questionConsider.urbanLimitsOther
        : this.question.questionConsider.urbanLimits;
    }
  }

  getNetworkEngineeringsString() {
    if (this.question && this.question.questionConsider) {
      const networkEngineering = this.question.questionConsider.networkEngineering;
      if (networkEngineering) {
        const arr = _.clone(networkEngineering);
        const ind = this.question.questionConsider.networkEngineeringCode.indexOf('OTHER');
        if (ind >= 0) {
          arr[ind] = this.question.questionConsider.networkEngineeringOther;
        }
        return arr.join(', ');
      }
    }
  }

  isEditableMaterialGroup(materialGroup: string) {
    if (this.question.create !== '0') {
      return true;
    }
    const materialType: QuestionAddMaterialType = this.materialTypes.find(mt => {
      return mt.materialType === materialGroup;
    });
    return this.isEditableMaterialType(materialType);
  }

  isEditableMaterialType(materialType: QuestionAddMaterialType) {
    if (this.question.create !== '0') {
      return true;
    }
    if (!materialType) {
      return true;
    }
    return _.isNull(materialType.editable) ? true : materialType.editable;
  }

  isUnauthBuild() {
    return this.question && this.question.meetingType === 'UNAUTHBUILD';
  }

  /**
   *  Признак, что вопрос касается ГПЗУ
   */
  isGpzu() {
    return this.question && this.question.questionConsider && this.question.questionConsider.basis.basisType === 'Заявка на ГПЗУ';
  }

  /**
   *  Ссылка на ГПЗУ
   */
  getGpzuLink() {
    const gpzuNum = this.question.questionConsider.basis.number;
    if (gpzuNum) {
      const templ = _.template('/gpzu/#/app/gpzu/card/<%= gpzuNum %>/main/view');
      return templ({gpzuNum: gpzuNum.replace('ГПЗУ', 'GPZU').replace('/', '-').replace('(', '').replace(')', '')});
    }
    return null;
  }

  isShowExternalProtocol() {
    const externalProtocol = this.question.questionConsider.externalProtocol;
    return externalProtocol && (
      externalProtocol.commission || externalProtocol.protocolNumber || externalProtocol.protocolDate ||
      externalProtocol.pointNumber || externalProtocol.decision
    );
  }

  /**
   *  Проверка наличия документа ПЗЗ для показа ссылки
   */
  hasPzz() {
    return !!this.availablePzz;
  }

  /**
   *  Ссылка на документ ПЗЗ при наличии или пустая строка
   */
  getPzzLink() {
    if (this.hasPzz()) {
      return this.availablePzz.link;
    }
    return '';
  }

}
