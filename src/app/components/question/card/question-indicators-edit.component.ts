import {Component, EventEmitter, Input, OnInit, Output} from "@angular/core";
import {NsiResourceService} from "@reinform-cdp/nsi-resource";
import {DictMetaData, LoadingStatus} from "@reinform-cdp/widgets";
import {Question, QuestionParameters, VriVersionEnum} from "../../../models/Question";
import {PzzParameter} from "../../../models/nsi/PzzParameter";
import * as angular from "angular";
import * as _ from "lodash";

@Component({
  selector: 'meeting-question-indicators-edit',
  templateUrl: './question-indicators-edit.component.html'
})
export class QuestionIndicatorsEditComponent implements OnInit {

  @Input() question: Question;
  @Output() saveQuestion: EventEmitter<any> = new EventEmitter();
  @Output() cancelEdit: EventEmitter<any> = new EventEmitter();

  mainVRIDictData = new DictMetaData('vriZu120', 'code',
    'item.value + \' [\' + item.code + \']\'', 'children', true);

  loadingStatus: LoadingStatus = LoadingStatus.LOADING;
  success: boolean = false;

  constructor(private nsiResourceService: NsiResourceService) {
  }

  ngOnInit() {
    if (!this.question.questionConsider.parameters) {
      this.question.questionConsider.parameters = new QuestionParameters();
    }

    if (!this.question.questionConsider.parameters.vriVersion) {
      this.question.questionConsider.parameters.vriVersion = VriVersionEnum.Dictionary_120;
    }

    if (!this.question.questionConsider.parameters.tep) this.question.questionConsider.parameters.tep = [];
    this.success = true;
    this.loadingStatus = LoadingStatus.SUCCESS;
  }

  save() {
    this.saveQuestion.emit();
  }

  cancel() {
    this.cancelEdit.emit();
  }

  normalize(string) {
    //TODO: normalize
    return string;
  }

}
