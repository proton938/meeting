import {Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation} from '@angular/core';
import {DictMetaData, LoadingStatus} from "@reinform-cdp/widgets";
import {ToastrService} from "ngx-toastr";
import {BsModalService} from "ngx-bootstrap";
import * as angular from "angular";
import * as _ from "lodash";
import {NsiResourceService, UserBean} from "@reinform-cdp/nsi-resource";
import {AuthorizationService, SessionStorage} from "@reinform-cdp/security";
import {BsLocaleService} from "ngx-bootstrap/datepicker";
import {Question, QuestionResponsible, QuestionUserType} from "../../../../models/Question";
import {MeetingType} from "../../../../models/nsi/MeetingType";
import {CategoryQuestion} from "../../../../models/nsi/CategoryQuestion";
import {Prefect} from "../../../../models/nsi/Prefect";
import {District} from "../../../../models/nsi/District";
import {BaseType} from "../../../../models/nsi/BaseType";
import {ResponsibleDepartment} from "../../../../models/nsi/ResponsibleDepartment";
import {UrbanLimit} from "../../../../models/nsi/UrbanLimit";
import {NetworkEngineering} from "../../../../models/nsi/NetworkEngineering";
import {ExternalAgree} from "../../../../models/nsi/ExternalAgree";
import {Speaker} from "../../../../models/nsi/Speaker";
import {MeetingTypeService} from "../../../../services/meeting-type.service";
import {UserUtilService} from "../../../../services/user-util.service";
import {QuestionService} from "../../../../services/question.service";
import {Observable, Subject} from "rxjs/Rx";
import {catchError, debounceTime, distinctUntilChanged, filter, map, switchMap, tap} from "rxjs/internal/operators";
import {concat, from, of} from "rxjs/index";

@Component({
  selector: 'meeting-question-edit',
  templateUrl: './question-edit.component.html',
  styleUrls: ['./question-edit.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class QuestionEditComponent implements OnInit {

  @Input() question: Question;
  @Output() saveQuestion: EventEmitter<any> = new EventEmitter();
  @Output() cancel: EventEmitter<any> = new EventEmitter();

  meetingTypes: MeetingType[];
  questionCategories: CategoryQuestion[];
  origPrefects: Prefect[];
  prefects: string[];
  origDistricts: District[];
  districts: string[];
  basisTypes: BaseType[];
  presentationResponsibles: QuestionUserType[];
  urbanLimits: UrbanLimit[];
  networkEngineerings: NetworkEngineering[];
  planDateEditable: boolean = true;
  hasOtherUrbanLimits: Boolean = false;
  hasOtherNetworkEngineering: Boolean = false;
  restrictionCreationMeetings: any[];
  pzzExternalAgree: ExternalAgree[];
  externalAgrees: { [key: string]: boolean } = {};

  speakerDictData = new DictMetaData('mggt_meeting_Speakers', 'code',
    'item.children ? item.group : item.name', 'children', false, 'organizationCode',
    (item) => !item.children);
  cospeakerDictData = new DictMetaData('mggt_meeting_Speakers', 'code',
    'item.children ? item.group : item.name', 'children', true, 'organizationCode',
    (item) => !item.children);

  meetingResponsibles$: Observable<UserBean[]>;
  meetingResponsiblesLoading = false;
  meetingResponsiblesInput$ = new Subject<string>();

  loadingStatus: LoadingStatus = LoadingStatus.LOADING;
  success: boolean = false;

  constructor(private authorizationService: AuthorizationService,
              private session: SessionStorage,
              private meetingTypeService: MeetingTypeService,
              private nsiResourceService: NsiResourceService,
              private questionService: QuestionService,
              private userUtilService: UserUtilService,
              private modalService: BsModalService,
              private localeService: BsLocaleService,
              private toastr: ToastrService) {
  }

  ngOnInit() {
    this.localeService.use('ru');
    if (!this.authorizationService.check('MEETING_QUESTION_FORM')) {
      this.toastr.warning("У вас нет прав для просмотра этой страницы.", "Ошибка");
      return Promise.reject("У вас не прав для просмотра этой страницы.")
    }

    this.meetingResponsibles$ = concat(
      of([]),
      this.meetingResponsiblesInput$.pipe(
        debounceTime(300),
        distinctUntilChanged(),
        filter(term => term && term.length > 2),
        tap(() => this.meetingResponsiblesLoading = true),
        switchMap((term: string) => this.loadMeetingResponsibles(term)),
        catchError(() => of([])),
        tap(() => this.meetingResponsiblesLoading = false)
      )
    );

    this.question.questionConsider.responsiblePrepare = this.question.questionConsider.responsiblePrepare || <QuestionResponsible>{};
    return this.checkPlanDate(this.question.questionID).then((planDateEditable) => {
      this.planDateEditable = planDateEditable;
      return this.loadDictionaries();
    }).then(() => {

      if (this.question.questionConsider.externalAgree) {
        this.question.questionConsider.externalAgree.forEach(agr => {
          this.externalAgrees[agr.organizationCode] = true;
        })
      }

      this.loadingStatus = LoadingStatus.SUCCESS;
      this.success = true;
    }).catch(() => {
      this.loadingStatus = LoadingStatus.ERROR;
    });
  }

  loadDictionaries() {
    return Promise.all([
      this.meetingTypeService.getNonExpressMeetingTypes(),
      this.nsiResourceService.get('Prefect'),
      this.nsiResourceService.get('District'),
      this.nsiResourceService.get('mggt_meeting_BaseTypes'),
      this.nsiResourceService.ldapUsers('MGGT_MEETING_PRESENTATION'),
      this.nsiResourceService.get('mggt_meeting_UrbanLimits'),
      this.nsiResourceService.get('mggt_meeting_NetworkEngineering'),
      this.nsiResourceService.get('mggt_meeting_RestrictionCreationMeetings'),
      this.nsiResourceService.get('mggt_meeting_ExternalAgree')
    ]).then((values: any[]) => {

      this.meetingTypes = values[0];
      this.restrictionCreationMeetings = values[7];

      this.meetingTypes = _.filter(this.meetingTypes, (el: MeetingType) => {
        let elInRestriction = _.find(this.restrictionCreationMeetings, (i: any) => (i.MeetingTypeCode === el.meetingType));
        if (elInRestriction && elInRestriction.CategoryQuestionCode || !elInRestriction) {
          return el;
        }
      });

      this.origPrefects = values[1];
      this.prefects = _.map(this.origPrefects, (prefect) => {
        return prefect.name;
      });
      this.origDistricts = values[2];
      this.districts = [];
      this.basisTypes = values[3].filter((basisType: BaseType) => {
        return basisType.active;
      });
      this.presentationResponsibles = this.convertUsers(values[4]);
      this.urbanLimits = values[5];
      this.networkEngineerings = values[6];
      this.pzzExternalAgree = values[8];
      this.loadingStatus = LoadingStatus.SUCCESS;

      if (this.question.meetingType) {
        this.loadQuestionCategories(this.question.meetingType);
      }
      let prefectsNames: string[] = this.question.questionConsider ? this.question.questionConsider.prefect : [];
      if (prefectsNames) {
        this.filterDistricts(prefectsNames);
      }
      let departmentPrepareCode = this.question.questionConsider.responsiblePrepare ? this.question.questionConsider.responsiblePrepare.departmentPrepareCode : null;
      if (departmentPrepareCode) {
        this.loadMeetingResponsibles(departmentPrepareCode);
      }

      this.checkHasOtherNetworkEngineering();
      this.checkHasOtherUrbanLimits();

      if (this.question.hasOwnProperty('Renovation') &&
        this.question.Renovation === true) {
        this.question.Renovation = true;
      }
      else {
        this.question.Renovation = false;
      }
    });
  }

  checkPlanDate(questionID: string): Promise<boolean> {
    if (!this.question.questionID) {
      return Promise.resolve(true);
    }
    return this.questionService.isPlanDateEditable(questionID).toPromise();
  }

  convertUsers(users: UserBean[]): QuestionUserType[] {
    return _.map(users, (user: UserBean) => {
      return this.userUtilService.toQuestionUserType(user);
    });
  }

  loadQuestionCategories(meetingType) {
    this.nsiResourceService.getBy('mggt_meeting_CategoryQuestion', {
      nickAttr: 'meetingType',
      values: [meetingType]
    }).then((questionCategories: CategoryQuestion[]) => {
      this.questionCategories = questionCategories.filter(category => {
        let meeting = this.restrictionCreationMeetings.find(i => i.MeetingTypeCode === this.question.meetingType);
        if (meeting && meeting.CategoryQuestionCode.indexOf(category.questionCode) === -1) {
          return category;
        }
        else if (!meeting) {
          return category;
        }
      });
    });
  }

  meetingTypeChanged(meetingType: MeetingType, createQuestion?) {
    if (meetingType) {
      this.question.meetingShortName = meetingType.meetingShortName;
      this.question.meetingFullName = meetingType.meetingFullName;
      this.question.planningMeeting = meetingType.planningMeeting;

      if (this.question.meetingType === 'RENOV' && createQuestion) {
        this.question.Renovation = true;
      }
      else if (createQuestion) {
        this.question.Renovation = false;
      }
      this.loadQuestionCategories(meetingType.meetingType);
    } else {

      this.question.meetingShortName = null;
      this.question.meetingFullName = null;
      this.question.planningMeeting = false;

      this.questionCategories = [];
    }

  }

  questionCategoryChanged() {
    let questionCategory = this.questionCategories.find(cat =>
      cat.questionCode === this.question.questionConsider.questionCategoryCode
    );
    this.question.questionConsider.questionCategory = questionCategory.questionCategory;
    this.question.questionConsider.question = questionCategory.questionCategory;
  }

  filterDistricts(prefectsNames: string[]) {
    this.districts = _.chain(this.origDistricts).filter((district) => {
      return _.intersection(prefectsNames, _.map(district.perfectId, pref => pref.name)).length > 0;
    }).map((district) => {
      return district.name;
    }).value();
  }

  prefectChanged(prefectsNames: string[]) {
    this.filterDistricts(prefectsNames);
    if (this.question.questionConsider) {
      this.question.questionConsider.district = [];
    }
  }

  loadMeetingResponsibles(query: string): Observable<UserBean[]> {
    return from(this.nsiResourceService.searchUsers({
      fio: query
    }))
  }

  departmentContractorChanged(departmentContractor: UserBean) {
    let contractorPrepareLogin = this.question.questionConsider.responsiblePrepare ? this.question.questionConsider.responsiblePrepare.contractorPrepare : null;
    if (departmentContractor) {
      this.question.questionConsider.responsiblePrepare.contractorPrepareFIO = departmentContractor.displayName;
      this.question.questionConsider.responsiblePrepare.contractorPreparePhone = departmentContractor.telephoneNumber;
    } else {
      if (this.question.questionConsider.responsiblePrepare) {
        this.question.questionConsider.responsiblePrepare.contractorPrepareFIO = null;
        this.question.questionConsider.responsiblePrepare.contractorPreparePhone = null;
      }
    }

  }

  isUnauthBuild() {
    return this.question && this.question.meetingType === 'UNAUTHBUILD';
  }

  checkHasOtherUrbanLimits() {
    let urbanLimitsCode = this.question.questionConsider.urbanLimitsCode;
    this.hasOtherUrbanLimits = urbanLimitsCode === "OTHER";
  }

  urbanLimitsChanged(urbanLimit: UrbanLimit) {
    this.checkHasOtherUrbanLimits();
    if (!this.hasOtherUrbanLimits) {
      delete this.question.questionConsider.urbanLimitsOther;
    }
    if (urbanLimit) {
      this.question.questionConsider.urbanLimits = urbanLimit.limitName;
    } else {
      delete this.question.questionConsider.urbanLimits;
    }
  }

  checkHasOtherNetworkEngineering() {
    let networkEngineeringCode = this.question.questionConsider.networkEngineeringCode;
    this.hasOtherNetworkEngineering = networkEngineeringCode &&
      _.some(networkEngineeringCode, (ne) => {
        return ne === "OTHER";
      })
  }

  networkEngineeringChanged(networkEngineerings: NetworkEngineering[]) {
    this.checkHasOtherNetworkEngineering();
    if (!this.hasOtherNetworkEngineering) {
      delete this.question.questionConsider.networkEngineeringOther;
    }
    this.question.questionConsider.networkEngineering = networkEngineerings ? _.map(networkEngineerings, 'engName') : [];
  }

  planDateChanged(value: Date) {
    if (value) {
      this.question.questionConsider.planDateEditor = this.session.fullName();
    }
  }

  validate() {
    if (!this.question.meetingType) {
      this.toastr.warning("Не выбран вид совещания");
      return false;
    }
    if (!this.question.questionConsider.questionCategory) {
      this.toastr.warning("Не выбрана категория вопроса");
      return false;
    }
    if (!this.question.questionConsider.question) {
      this.toastr.warning("Не указан вопрос");
      return false;
    }
    if (this.question.planningMeeting && !this.question.questionConsider.address) {
      this.toastr.warning("Не указан адрес");
      return false;
    }
    if (this.question.planningMeeting && !this.question.questionConsider.prefect) {
      this.toastr.warning("Не указан округ");
      return false;
    }
    if (this.question.planningMeeting && !this.question.questionConsider.cadastralNumber) {
      this.toastr.warning("Не указан кадастровый номер");
      return false;
    }
    if (!this.question.questionConsider.basis.basisType) {
      this.toastr.warning("Не указан тип основания");
      return false;
    }
    if (!this.question.questionConsider.basis.number) {
      this.toastr.warning("Не указан номер основания");
      return false;
    }
    if (!this.question.questionConsider.basis.date) {
      this.toastr.warning("Не указана дата основания");
      return false;
    }
    return true;
  }

  save() {
    if (!this.validate()) {
      return;
    }
    if (!this.question.questionConsider.passDate) {
      this.question.questionConsider.passDate = new Date();
    }
    this.saveQuestion.emit();
  }

  cancelEdit() {
    this.cancel.emit();
  }

  clearSpeaker() {
    delete this.question.questionConsider.speaker;
  }

  speakerChanged(speakers: Speaker[]) {
    this.question.questionConsider.speaker = this.convertSpeakerOrganizationToQuestionUserType(speakers[0]);
  }

  cospeakersChanged(cospeakers: Speaker[]) {
    this.question.questionConsider.cospeaker = cospeakers.map(cospeaker => this.convertSpeakerOrganizationToQuestionUserType(cospeaker));
  }

  convertSpeakerOrganizationToQuestionUserType(org: Speaker): QuestionUserType {
    let result = new QuestionUserType();
    result.organization = org.name;
    result.organizationShort = org.nameShort;
    result.organizationCode = org.code;
    return result;
  }

  isHasPrefect() {
    return !_.isEmpty(this.question.questionConsider.prefect);
  }

  switchExternalAgree(code, val) {
    if (val) {
      let pzzExternalAgree = _.find(this.pzzExternalAgree, agr => agr.code === code);
      this.question.questionConsider.externalAgree = this.question.questionConsider.externalAgree || [];
      this.question.questionConsider.externalAgree.push({
        organization: pzzExternalAgree.name,
        organizationShort: pzzExternalAgree.shortName,
        organizationCode: pzzExternalAgree.code
      });
    } else {
      _.remove(this.question.questionConsider.externalAgree, agr => agr.organizationCode === code);
    }
  }


}
