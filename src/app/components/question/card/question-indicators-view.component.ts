import {Component, EventEmitter, Input, OnInit, Output} from "@angular/core";
import {NsiResourceService} from "@reinform-cdp/nsi-resource";
import * as _ from "lodash";
import {Question, QuestionParameters} from "../../../models/Question";
import {PzzParameter} from "../../../models/nsi/PzzParameter";

@Component({
  selector: 'meeting-question-indicators-view',
  templateUrl: './question-indicators-view.component.html'
})
export class QuestionIndicatorsViewComponent implements OnInit {

  @Input() question: Question;
  loading: boolean = true;

  @Output() public edit: EventEmitter<any> = new EventEmitter();

  constructor(private nsiResourceService: NsiResourceService) {
  }

  ngOnInit() {
    if (!this.question.questionConsider.parameters) {
      this.question.questionConsider.parameters = new QuestionParameters();
    }
    if (!this.question.questionConsider.parameters.tep.length) this.question.questionConsider.parameters.tep = [];
    this.loading = false;
  }

  editCard() {
    this.edit.emit(true);
  }

}
