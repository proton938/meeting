import {Component, Input, OnInit} from "@angular/core";
import {NsiResourceService} from "@reinform-cdp/nsi-resource";
import {LoadingStatus} from "@reinform-cdp/widgets";
import * as _ from "lodash";
import {Question, QuestionComment} from "../../../models/Question";
import {ExternalAgree} from "../../../models/nsi/ExternalAgree";
import {IPromise} from "angular";

@Component({
  selector: 'meeting-question-external-agree',
  templateUrl: './question-external-agree.component.html'
})
export class QuestionExternalAgreeComponent implements OnInit {

  @Input() question: Question;

  loadingStatus: LoadingStatus = LoadingStatus.LOADING;
  loading: boolean = true;

  pzzExternalAgree: { [key: string]: ExternalAgree };
  comments: {
    [key: string]: QuestionComment
  };

  constructor(private nsiResourceService: NsiResourceService) {
  }

  ngOnInit() {
    this.comments = _.keyBy(this.question.comments, comment => comment.author.organizationCode);
    return this.nsiResourceService.get('mggt_meeting_ExternalAgree').then((pzzExternalAgree: ExternalAgree[]) => {
      this.pzzExternalAgree = _.keyBy(pzzExternalAgree, 'code');
      this.loadingStatus = LoadingStatus.SUCCESS;
      this.loading = false;
    }).catch(() => {
      this.loadingStatus = LoadingStatus.ERROR;
      this.loading = false;
    });
  }

}
