import { Component, OnInit } from '@angular/core';
import { LoadingStatus } from "@reinform-cdp/widgets";
import { StateService, Transition } from '@uirouter/core';
import { QuestionService } from "../../../services/question.service";
import { NsiResourceService, UserBean } from "@reinform-cdp/nsi-resource";
import { CdpReporterPreviewService } from "@reinform-cdp/reporter-resource";
import * as _ from 'lodash';
import * as angular from 'angular';
import { AuthorizationService, SessionStorage } from "@reinform-cdp/security";
import { MeetingTypeService } from "../../../services/meeting-type.service";
import { ToastrService } from "ngx-toastr";
import { ProtocolService } from "../../../services/protocol.service";
import { BsModalService } from "ngx-bootstrap";
import { QuestionCommentModalComponent } from "./question-comment.modal";
import { MeetingType } from "../../../models/nsi/MeetingType";
import {
  Consideration, Question, QuestionBasis, QuestionComment, QuestionConsider, QuestionDocumentWrapper,
  QuestionResponsible, QuestionUserType
} from "../../../models/Question";
import { Protocol } from "../../../models/Protocol";
import { UserUtilService } from "../../../services/user-util.service";
import { BreadcrumbsService } from "@reinform-cdp/skeleton";

export class QuestionData {
  question: Question;
  protocol?: Protocol;
  questionHistory?: Question[]
}

@Component({
  selector: 'meeting-question',
  templateUrl: './question.component.html'
})
export class QuestionComponent implements OnInit {

  loadingStatus: LoadingStatus = LoadingStatus.LOADING;
  success: boolean = false;

  origQuestion: Question;
  question: Question;
  questionHistory: Question[];
  protocol: Protocol;

  hasEditCommentPermission: boolean;
  canViewProtocol: boolean;
  showIndicatorsTab: boolean = false;

  editingCard: boolean;
  editingIndicators: boolean = false;
  availableToAddInstruction: any;
  availableToCopy: any;
  regularExternal: boolean;

  accountName: string;

  activeTab: any;
  externalAgreeCodes: string[];
  pzzExternalAgree: any;

  constructor(private $state: StateService,
    private transition: Transition,
    private authorizationService: AuthorizationService,
    private questionService: QuestionService,
    private protocolService: ProtocolService,
    private nsiResourceService: NsiResourceService,
    private session: SessionStorage,
    private meetingTypeService: MeetingTypeService,
    private modalService: BsModalService,
    private userUtilService: UserUtilService,
    private reporterPreviewService: CdpReporterPreviewService,
    private breadcrumbsService: BreadcrumbsService,
    private toastr: ToastrService) {
  }

  ngOnInit(): void {
    const id: string = this.transition.params()['id'];
    this.editingCard = this.transition.params()['editingCard'];

    this.accountName = this.session.login();

    this.hasEditCommentPermission = this.authorizationService.check('MEETING_QUESTION_COMMENT_EDIT_BUTTON');
    this.canViewProtocol = this.authorizationService.check('MEETING_PROTOCOL_CARD');

    this.activeTab = this.transition.params()['tab'];



    if (!id) {
      this.editingCard = true;
    }
    let data = id ? this.loadQuestion(id) : this.newQuestion();
    data.then((data) => {
      this.origQuestion = angular.copy(data.question);
      this.question = angular.copy(data.question);
      this.protocol = data.protocol;
      this.questionHistory = data.questionHistory;
      this.loadingStatus = LoadingStatus.SUCCESS;
      this.updateBreadcrumbs();
      this.success = true;
      return data;
    }, (reason) => {
      this.loadingStatus = LoadingStatus.ERROR;
      return Promise.reject(reason);
    });
  }

  newQuestion(): Promise<QuestionData> {
    let question = <Question>{};
    question.questionIDHistory = [];
    question.questionConsider = <QuestionConsider>{};
    question.questionConsider.passDate = undefined;
    question.questionConsider.consideration = Consideration.ACCEPTED;
    question.questionConsider.basis = new QuestionBasis();
    question.questionConsider.basis.date = undefined;
    question.questionConsider.planDate = undefined;
    question.questionConsider.meetingDate = undefined;
    question.questionConsider.materials = [];
    question.questionConsider.responsiblePrepare = <QuestionResponsible>{};
    return Promise.resolve({ question: question });
  }

  loadQuestion(id: string): Promise<QuestionData> {
    return this.questionService.get(id).toPromise().then(wrapper => {
      if (this.meetingTypeService.checkMeetingType(wrapper.document.question.meetingType)) {
        let question = wrapper.document.question;
        this.showIndicatorsTab = question.meetingType === 'PZZ' ||
          (question.meetingType.replace(/\s+/g, '') === 'SSMKA' &&
            question.questionConsider.questionCategory.replace(/\s\s+/g, ' ') == 'Рассмотрение вопроса для вынесения на ГК');

        return Promise.all([
          this.loadHistory(question),
          this.loadProtocol(question),
          this.loadDictionaries(question)
        ]).then((result: any[]) => {
          let questionHistory = _.sortBy(result[0], ['questionConsider.meetingDate']);
          let protocol = result[1];
          return Promise.resolve({
            question: question,
            questionHistory: questionHistory,
            protocol: protocol
          })
        });
      } else {
        this.toastr.warning("У вас не прав для просмотра этой страницы.", "Ошибка");
        return Promise.reject("У вас не прав для просмотра этой страницы.")
      }
    });
  }

  loadProtocol(question: Question): Promise<Protocol> {
    let meetingType: string = question.meetingType;
    let meetingNumber: string = question.questionConsider.meetingNumber;
    let meetingDate: Date = question.questionConsider.meetingDate;
    if (meetingNumber && meetingDate) {
      this.protocolService.getByMeetingNumber(meetingType, meetingNumber, meetingDate.getFullYear()).toPromise().then(wrapper => {
        return wrapper.document.protocol;
      });
    } else {
      return Promise.resolve(null);
    }
  }

  loadHistory(question: Question): Promise<Question[]> {
    let questionIDHistory = question.questionIDHistory || [];
    return this.questionService.getQuestionsIdsWithHistoryContaining(question.questionID).toPromise().then(ids => {
      if (ids) {
        questionIDHistory = questionIDHistory.concat(ids);
      }
      if (questionIDHistory.length > 0) {
        let promises: Promise<QuestionDocumentWrapper>[] = questionIDHistory.map((id) => this.questionService.get(id).toPromise());
        return Promise.all(promises).then(questionHistory => {
          return _.map(questionHistory, (data) => data.document.question);
        });
      } else {
        return Promise.resolve([]);
      }
    });
  }

  loadDictionaries(question: Question): Promise<any> {
    return Promise.all([
      this.nsiResourceService.get('InstructionExecutors'),
      this.nsiResourceService.get('mggt_meeting_ExternalAgree'),
      this.meetingTypeService.getMeetingType(question.meetingType)
    ]).then((result: any[]) => {
      this.availableToAddInstruction = _.find(result[0], (el: any) => {
        return el.creator == this.session.login() || (el.assistant && el.assistant.indexOf(this.session.login()) >= 0);
      });

      this.pzzExternalAgree = result[1];
      this.externalAgreeCodes = _.map(question.questionConsider.externalAgree, (el) => el.organizationCode);

      let meetingType: MeetingType = result[2];
      this.availableToCopy = meetingType && meetingType.creatingCopies;
      this.regularExternal = meetingType && !meetingType.Express && meetingType.meetingExternal;
    })
  }

  copy() {
    this.questionService.copy(this.question.questionID).toPromise().then((result: any) => {
      this.toastr.success('Копия вопроса успешно создана');
      this.$state.go('app.meeting.question', { id: result.document.question.questionID, editingCard: true });
    }).catch(() => {
      this.toastr.warning('Ошибка при копировании вопроса');
    });
  }

  private createQuestion(): Promise<Question> {
    return this.questionService.create(this.question).toPromise();
  }

  private updateQuestion(): Promise<Question> {
    let diff = this.questionService.diff(this.origQuestion, this.question);
    return this.questionService.update(this.origQuestion.questionID, diff).toPromise();
  }

  saveQuestion(): void {
    if (this.question.questionID) {
      this.updateQuestion().then((result) => {
        this.question = angular.copy(result);
        this.origQuestion = angular.copy(result);
        this.editingCard = false;
        this.editingIndicators = false;
      });
    } else {
      this.createQuestion().then(question => {
        this.$state.go('app.meeting.question', { id: question.questionID, editingCard: false });
      });
    }
  }

  saveComment(comment: string, author: QuestionUserType): void {
    this.questionService.saveComment(this.question, { date: new Date(), author: author, comment: comment })
      .toPromise().then((result) => {
        this.question.comments = result.comments;
      });
  }

  showAddCommentDialog(): void {
    if (!this.authorizationService.check('MEETING_QUESTION_COMMENT_FORM')) {
      this.toastr.warning("У вас не прав для просмотра этой страницы.", "Ошибка");
      return;
    }
    const options = {
      initialState: {},
      'class': 'modal-lg'
    };
    const ref = this.modalService.show(QuestionCommentModalComponent, options);
    let subscription = this.modalService.onHide.subscribe(() => {
      let component = <QuestionCommentModalComponent>ref.content;
      if (component.submit) {
        let user = this.getCurrentUser();
        this.saveComment(component.comment, this.userUtilService.toQuestionUserType(user, user.departmentCode));
      }
      subscription.unsubscribe();
    });
  }

  showEditCommentDialog(ind: number): void {
    if (!this.authorizationService.check('MEETING_QUESTION_COMMENT_FORM')) {
      this.toastr.warning("У вас не прав для просмотра этой страницы.", "Ошибка");
      return;
    }
    const options = {
      initialState: {
        comment: this.question.comments[ind].comment
      },
      'class': 'modal-lg'
    };
    const ref = this.modalService.show(QuestionCommentModalComponent, options);
    let subscription = this.modalService.onHide.subscribe(() => {
      let component = <QuestionCommentModalComponent>ref.content;
      if (component.submit) {
        let comment = this.question.comments[ind];
        comment.comment = component.comment;
        comment.date = new Date();
        comment.author = this.userUtilService.toQuestionUserType(this.getCurrentUser());
        this.updateQuestion().then((result) => {
          this.question.comments = result.comments;
        });
      }
      subscription.unsubscribe();
    });
  }

  getCurrentUser(): UserBean {
    return <UserBean>{
      accountName: this.session.login(),
      displayName: this.session.fullName(),
      fioShort: this.session.fioShort(),
      iofShort: this.session.iofShort(),
      telephoneNumber: this.session.telephoneNumber(),
      mail: this.session.mail(),
      post: this.session.post(),
      departmentCode: this.session.departmentCode(),
      departmentFullName: this.session.departmentFullName(),
      authenticated: true,
      distinguishedName: null
    }
  }

  editCard(edit: boolean) {
    this.editingCard = edit;
  }

  editIndicators(edit: boolean) {
    this.editingIndicators = edit;
  }

  cancelEditIndicators() {
    if (this.question.questionID) {
      this.editIndicators(false);
    } else {
      this.$state.go('app.meeting.questions');
    }
  }

  cancelEditCard() {
    if (this.question.questionID) {
      this.editCard(false);
      this.question = angular.copy(this.origQuestion);
    } else {
      this.$state.go('app.meeting.questions');
    }
  }

  previewExcerpt() {
    this.reporterPreviewService.nsi2Preview({
      options: {
        jsonSourceDescr: "",
        onProcess: "Сформировать выписку",
        placeholderCode: "",
        strictCheckMode: true,
        xwpfResultDocumentDescr: "Выписка по вопросу.docx",
        xwpfTemplateDocumentDescr: ''
      },
      jsonTxt: JSON.stringify({ document: { question: this.question } }),
      rootJsonPath: "$",
      nsiTemplate: {
        templateCode: 'ssSSMKAExcerpt'
      }
    });
  }

  saveQuestionPrimaryId(): Promise<any> {
    this.question.questionPrimaryID = this.question.questionID;
    return this.updateQuestion();
  }

  createInstruction() {
    const url = window.location.protocol + '//' + window.location.host +
      '/sdo/instruction/#/app/instruction/instruction/createFromMeeting/' + this.question.questionID + '/' + this.question.questionPrimaryID;
    window.open(url, '_blank');
  }

  getCommentOrganization(comment: QuestionComment) {
    let organizationCode = comment.author.organizationCode;
    if (organizationCode && this.externalAgreeCodes.indexOf(organizationCode) !== -1) {
      return _.find(this.pzzExternalAgree, (el) => el.code === organizationCode).shortName;
    }
    else {
      return comment.author.organization;
    }
  }

  updateBreadcrumbs() {
    this.breadcrumbsService.setBreadcrumbsChain([{
      title: this.question.receivedFromMKA && this.question.receivedFromMKA.FromMKA ? 'Регламентные совещания МКА' : 'Регламентные совещания',
      url: this.question.receivedFromMKA && this.question.receivedFromMKA.FromMKA ? this.$state.href('app.meeting.showcase-mka', {}) : this.$state.href('app.meeting.showcase', {})
    }, {
      title: 'Вопрос',
      url: null
    }]);
  }

}
