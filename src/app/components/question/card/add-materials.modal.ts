import {StateService} from '@uirouter/core';
import {Component, OnInit, QueryList, ViewChildren} from '@angular/core';
import {BsModalRef} from "ngx-bootstrap";
import {Question} from "../../../models/Question";
import {AuthorizationService, SessionStorage} from "@reinform-cdp/security";
import {ToastrService} from "ngx-toastr";
import {DropzoneConfigInterface, DropzoneDirective} from "ngx-dropzone-wrapper";
import * as _ from "lodash";
import {NsiResourceService} from "@reinform-cdp/nsi-resource";
import {FileResourceService} from "@reinform-cdp/file-resource";
import {QuestionService} from "../../../services/question.service";
import {formatAsDateTime} from "../../../services/date-util.service";
import {FileService} from "../../../services/file.service";

export class QuestionAddMaterialType {
  materialType: string;
  maxFiles: number;
  fileType: string;
  meetingType: string;
  editable: boolean;
}

@Component({
  selector: 'meeting-modal-content',
  templateUrl: './add-materials.modal.html',
  styleUrls: ['./add-materials.modal.scss']
})
export class AddMaterialsModalComponent implements OnInit {

  @ViewChildren(DropzoneDirective) dropzones: QueryList<DropzoneDirective>;

  saving: boolean = false;
  hasQuestionViewPermission: boolean;
  copies: any = {};

  config: DropzoneConfigInterface = {
    url: '/filestore/v1/files/file?systemCode=sdo',
    autoProcessQueue: false,
    parallelUploads: 1,
    paramName: 'file',
    headers: {
      'Accept': 'text/plain'
    }
  };

  question: Question;
  questionHistory: Question[];
  materialTypes: QuestionAddMaterialType[];
  submit: boolean;

  constructor(private $state: StateService,
              private authorizationService: AuthorizationService,
              private nsiResourceService: NsiResourceService,
              private fileResourceService: FileResourceService,
              private fileService: FileService,
              private questionService: QuestionService,
              private bsModalRef: BsModalRef,
              private session: SessionStorage,
              private toastr: ToastrService) {
  }

  ngOnInit() {
    this.hasQuestionViewPermission = this.authorizationService.check('MEETING_QUESTION_CARD');
    this.submit = false;
  }

  save() {
    this.saving = true;
    this.saveDropzoneAndQueue(0, this.question.folderID).then(() => {
      let copies: any[] = [];
      _.each(this.copies, (val, idFile) => {
        copies.push({
          idFile: idFile,
          val: val
        });
      });
      return this.copyMaterialAndQueue(copies, 0);
    }).then(() => {
      this.submit = true;
      this.saving = false;
      this.bsModalRef.hide();
    }).catch(() => {
      this.saving = false;
    });
  }

  cancel() {
    this.submit = false;
    this.bsModalRef.hide();
  }

  upload(dropzone: DropzoneDirective, materialType: QuestionAddMaterialType, folderGuid: string): Promise<any> {
    return this.fileService.uploadDropzone(dropzone, this.question.questionID, materialType.fileType, folderGuid, (fileType) => {
      return this.questionService.addMaterial(this.question, materialType.fileType, this.session.login(), this.session.fullName(), {
        idFile: fileType.idFile,
        nameFile: fileType.nameFile,
        sizeFile: '' + fileType.sizeFile,
        signed: false,
        dateFile: formatAsDateTime(new Date()),
        typeFile: fileType.typeFile,
        classFile: fileType.mimeType
      }).toPromise().then((question: Question) => {
        this.question = question;
      }).catch(() => {
        this.toastr.error('Не удалось добавить файл ' + fileType.nameFile);
      });
    }, (errorMessage) => {
      this.toastr.warning(errorMessage.message ? errorMessage.message : errorMessage, 'Ошибка');
    });
  }

  copy(copy: any): Promise<any> {
    if (copy.val) {
      //TODO: copy materials
      return Promise.resolve();
      // this.questionResource.copyMaterials({
      //   id: this.questionID,
      //   fileGuid: copy.idFile
      // }, function () {
      //   deferred.resolve();
      // })
    } else {
      return Promise.resolve()
    }
  }

  saveDropzoneAndQueue(ind: number, folderGuid: string): Promise<any> {
    if (ind < this.dropzones.length) {
      let dropzone: DropzoneDirective = this.dropzones.toArray()[ind];
      let materialType = this.materialTypes[ind];
      return this.upload(dropzone, materialType, folderGuid).then(() => {
        return this.saveDropzoneAndQueue(ind + 1, folderGuid);
      });
    } else {
      return Promise.resolve();
    }
  }

  copyMaterialAndQueue(copies: any[], ind: number): Promise<any> {
    if (ind < copies.length) {
      let copy: any = copies[ind];
      return this.copy(copy).then(() => {
        return this.copyMaterialAndQueue(copies, ind + 1);
      });
    } else {
      return Promise.resolve();
    }
  }

  getDropzoneConfig(materialType: QuestionAddMaterialType): DropzoneConfigInterface {
    let extension: any = {};
    if (materialType.maxFiles) {
      extension.maxFiles = materialType.maxFiles;
    }
    return _.extend(this.config, extension);
  }

}
