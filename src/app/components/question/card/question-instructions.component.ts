import {Component, Input, OnInit} from "@angular/core";
import {NsiResourceService} from "@reinform-cdp/nsi-resource";
import {LoadingStatus} from "@reinform-cdp/widgets";
import {SearchResultDocument} from "@reinform-cdp/search-resource";
import {InstructionsService} from "../../../services/instructions.service";
import {Question} from "../../../models/Question";

@Component({
  selector: 'meeting-question-instructions',
  templateUrl: './question-instructions.component.html'
})
export class QuestionInstructionsComponent implements OnInit {

  @Input() question: Question;

  loadingStatus: LoadingStatus = LoadingStatus.LOADING;
  success: boolean = false;

  lists: any = {
    conditions: [],
    priority: []
  };
  documents: SearchResultDocument[] | any[] = [];

  constructor(private nsiResourceService: NsiResourceService,
              private instructionsService: InstructionsService) {
  }

  ngOnInit() {
    return Promise.all([
      this.nsiResourceService.get('InstructionStatus'),
      this.nsiResourceService.get('InstructionPriority'),
      this.instructionsService.getQuestionInstructions(this.question).toPromise()
    ]).then((result: any[]) => {
      this.lists.conditions = result[0];
      this.lists.priority = result[1];
      this.documents = result[2].docs;
      this.loadingStatus = LoadingStatus.SUCCESS;
      this.success = true;
    }).catch(() => {
      this.loadingStatus = LoadingStatus.ERROR;
    })
  }

  getPriority(name: string) {
    return this.lists.priority.find(pr => pr.name === name)
  }

  getCondition(name: string) {
    return this.lists.conditions.find(pr => pr.name === name)
  }


}
