import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'meeting-question-list-view-card',
  templateUrl: './question-list-view-card.component.html',
  styleUrls: ['./question-list-view-card.component.scss']
})
export class QuestionListViewCardComponent implements OnInit {

  @Input() doc: any;
  @Input() hideLine: boolean;
  @Input() showStatus: boolean;

  constructor() { }

  ngOnInit() {
  }

}
