import {DateRangeFilter} from '../../../models/DateRangeFilter';
import {Decision} from '../../../models/nsi/Decision';
import {CategoryQuestion} from '../../../models/nsi/CategoryQuestion';
import {ResponsibleDepartment} from '../../../models/nsi/ResponsibleDepartment';
import {Prefect} from '../../../models/nsi/Prefect';
import { formatDate } from '@angular/common';
import { SearchExtDataItem } from '@reinform-cdp/search-resource';
import {formatAsDate} from '../../../services/date-util.service';

 export class Filter {
  address: string;
  prefects: Prefect[];
  responsibleDepartments: ResponsibleDepartment;
  questionCategory: CategoryQuestion;
  meetingDate: DateRangeFilter = new DateRangeFilter();
  decision: Decision;
  responsibleContractor: any;
  questionStatus: boolean;
  cadastralNumbers: string;
  basisNumber: string;
  gpzuNumber: string;
  customer: string;
  rightHolder: string;

  toSearchFields(): SearchExtDataItem[] {
    const result = [];
    if (this.address) {
      result.push(new SearchExtDataItem('address', this.address));
    }
    if (this.prefects && this.prefects.length) {
      result.push(new SearchExtDataItem('prefect', this.prefects.map(i => i.name)));
    }
    if (this.responsibleContractor) {
      if (this.responsibleContractor.displayName) {
        result.push(new SearchExtDataItem('contractorPrepareFioFull', this.responsibleContractor.displayName));
      } else {
        result.push(new SearchExtDataItem('contractorPrepareFioFull', this.responsibleContractor));
      }
    }
    if (this.questionCategory) {
      result.push(new SearchExtDataItem('questionCategory', this.questionCategory.questionCategory));
    }
    if (this.meetingDate) {
      this.addDateRangeFilter(result, 'planDate,meetingDate', this.meetingDate);
    }
    if (this.decision && this.decision) {
      result.push(new SearchExtDataItem('decisionText', this.decision.decisionType));
    }
    if (this.questionStatus) {
      result.push(new SearchExtDataItem('questionStatus', this.questionStatus));
    }
    if (this.cadastralNumbers) {
      result.push(new SearchExtDataItem('cadastralNumber', [].concat(this.cadastralNumbers.split(/\s*,\s*/))));
    }
    if (this.basisNumber) {
      result.push(new SearchExtDataItem('basisNumber', this.basisNumber));
    }
    if (this.gpzuNumber) {
      result.push(new SearchExtDataItem('gpzuNumber', this.gpzuNumber));
    }
    if (this.customer) {
      result.push(new SearchExtDataItem('customer', this.customer));
    }
    if (this.rightHolder) {
      result.push(new SearchExtDataItem('rightHolder', this.rightHolder));
    }
    return result;
  }

  toQueryString(): string {
    let r = '';
    const _and = () => r ? ' AND ' : '';
    const _stars = str => '(*' + str.split(/\s+/).join('* AND *') + '*)';
    const _range = (name, source) => {
      return '(' + name + ':['
        + (source.from ? formatAsDate(source.from) + 'T00:00:00Z' : '*') + ' TO '
        + (source.to ? formatAsDate(source.to) + 'T23:59:59Z' : '*')
        + '])';
    };

    if (this.address) {
      r += _and() + '(address:(' + _stars(this.address) + '))';
    }
    if (this.prefects && this.prefects.length) {
      r += _and() +  '(prefect:(' + this.prefects.map(i => _stars(i.name)).join(') OR prefect:(') + '))';
    }
    if (this.responsibleContractor) {
      if (this.responsibleContractor.displayName) {
        r += _and() + '(contractorPrepareFioFull:(' + _stars(this.responsibleContractor.displayName) + '))';
      } else {
        r += _and() + '(contractorPrepareFioFull:(' + _stars(this.responsibleContractor) + '))';
      }
    }
    if (this.questionCategory) {
      r += _and() + '(questionCategory:(' + _stars(this.questionCategory.questionCategory) + '))';
    }
    if (this.meetingDate && (this.meetingDate.from || this.meetingDate.to)) {
      r += _and() + _range('planDate', this.meetingDate) + ' OR ' + _range('meetingDate', this.meetingDate);
    }
    if (this.decision && this.decision.decisionType) {
      r += _and() + '(decisionText:(' + _stars(this.decision.decisionType) + '))';
    }
    if (this.questionStatus) {
      r += _and() + '(questionStatus:(' + _stars(this.questionStatus) + '))';
    }
    if (this.cadastralNumbers) {
      r += _and() +  '(cadastralNumber:(' + this.cadastralNumbers.split(/\s*,\s*/).join(') OR cadastralNumber:(') + '))';
    }
    if (this.basisNumber) {
      r += _and() + '(basisNumber:(' + _stars(this.basisNumber) + '))';
    }
    if (this.gpzuNumber) {
      r += _and() + '(gpzuNumber:(' + _stars(this.gpzuNumber) + '))';
    }
    if (this.customer) {
      r += _and() + '(customer:(' + _stars(this.customer) + '))';
    }
    if (this.rightHolder) {
      r += _and() + '(rightHolder:(' + _stars(this.rightHolder) + '))';
    }
    return r;
  }

  addDateRangeFilter(result, propName, value: DateRangeFilter) {
    const obj = { from: null, to: null };
    if (value.from) { obj.from = `${formatDate(value.from, 'yyyy-MM-dd', 'en')}T00:00:00Z`; }
    if (value.to) { obj.to = `${formatDate(value.to, 'yyyy-MM-dd', 'en')}T00:00:00Z`; }
    if (value.from || value.to) { result.push(new SearchExtDataItem(propName, obj)); }
  }

}
