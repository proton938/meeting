import {ToastrService} from 'ngx-toastr';
import {Component, Injectable, OnInit, Input} from '@angular/core';
import {QuestionListViewComponent} from './question-list-view.component';
import {FacetedSearchResult} from '@reinform-cdp/search-resource';
import {MeetingTypeService} from '../../../services/meeting-type.service';
import {SolrMediatorService} from '../../../services/solr-mediator.service';
import {HelperService} from "../../../services/helper.service";
@Injectable()
@Component({
  selector: 'meeting-question-list-view-plain',
  templateUrl: './question-list-view-plain.component.html',
  styleUrls: ['./question-list-view-plain.component.scss']
})
export class QuestionListViewPlainComponent extends QuestionListViewComponent implements OnInit {

  constructor(public solrMediator: SolrMediatorService,
              public meetingTypeService: MeetingTypeService,
              public toastr: ToastrService,
              public helper: HelperService) {
    super(solrMediator, meetingTypeService, toastr, helper);
  }

  ngOnInit() {
    super.ngOnInit();
  }

  doSearch(): Promise<FacetedSearchResult> {
    this.searchRequest.sort = this.sorting.name + ' ' + this.sorting.value;
    // this.searchRequest.common = this.common;
    // this.searchRequest.fields = this.getSearchFields();
    this.searchRequest.query = this.getSearchQuery();
    return this.solrMediator.query(this.searchRequest).toPromise();
  }

}
