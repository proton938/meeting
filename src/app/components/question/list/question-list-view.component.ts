import { ToastrService } from 'ngx-toastr';
import { Injectable } from '@angular/core';
import { EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FacetedSearchResult, SearchExtData, SearchExtDataItem } from '@reinform-cdp/search-resource';
import { Sorting } from '../../../models/Sorting';
import { MeetingType } from '../../../models/nsi/MeetingType';
import { MeetingTypeService } from '../../../services/meeting-type.service';
import {Filter} from './Filter';
import {SolrMediatorService} from '../../../services/solr-mediator.service';
import {HelperService} from "../../../services/helper.service";

@Injectable()
export abstract class QuestionListViewComponent implements OnInit {

  @Input() searchRequest: any;
  @Input() searchResult: FacetedSearchResult;
  @Output() searchResultChange: EventEmitter<any> = new EventEmitter();
  @Input() sorting: Sorting;
  @Input() filter: Filter;
  @Input() common: string;
  @Input() loading: boolean;
  @Output() loadingChange: EventEmitter<any> = new EventEmitter();
  @Input() isExternal: boolean;

  meetingTypes: MeetingType[];

  constructor(public solrMediator: SolrMediatorService,
    public meetingTypeService: MeetingTypeService,
    public toastr: ToastrService,
    public helper: HelperService) {
  }

  ngOnInit() {
    this.setLoading(true);
    if (this.isExternal) {
      this.meetingTypeService.getNonExpressExternalMeetingTypes().then(meetingTypes => {
        this.meetingTypes = meetingTypes;
        this.search();
      });
    } else {
      this.meetingTypeService.getNonExpressMeetingTypes().then(meetingTypes => {
        this.meetingTypes = meetingTypes;
        this.search();
      });
    }
  }

  search() {
    this.setLoading(true);
    this.doSearch().then((resp) => {
      this.searchResult = resp;
      this.searchResultChange.emit(resp);
      this.setLoading(false);
    }).catch(err => {
      // this.toastr.error(err.data.exception);
      this.helper.error(err);
      console.log(err);
      this.setLoading(false);
    });
  }

  private setLoading(loading: boolean) {
    this.loading = loading;
    this.loadingChange.emit(loading);
  }

  abstract doSearch(): Promise<FacetedSearchResult>;

  getSearchFields(): SearchExtDataItem[] {
    let result = [];
    result.push(new SearchExtDataItem('meetingType', this.meetingTypes.map(mt => mt.meetingType)));
    if (this.filter) {
      result = result.concat(this.filter.toSearchFields());
    }
    return result;
  }

  getSearchQuery(): string {
    let r = '';
    r += '(meetingType:(' + this.meetingTypes.map(mt => mt.meetingType).join(') OR meetingType:(') + '))';
    if (this.filter) {
      const queryFilter = this.filter.toQueryString();
      if (queryFilter) {
        r += ' AND ' + '(' + queryFilter + ')';
      }
    }
    return r;
  }

}
