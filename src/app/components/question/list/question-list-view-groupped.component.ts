import {ToastrService} from 'ngx-toastr';
import {Component, OnInit} from '@angular/core';
import {QuestionListViewComponent} from './question-list-view.component';
import {FacetedSearchResult} from '@reinform-cdp/search-resource';
import {from} from 'rxjs';
import {map} from 'rxjs/internal/operators';
import * as _ from 'lodash';
import {MeetingTypeService} from '../../../services/meeting-type.service';
import {SearchSection, SearchSubsection} from './presentation/question-presentation-list.component';
import {SolrMediatorService} from '../../../services/solr-mediator.service';
import {HelperService} from "../../../services/helper.service";

@Component({
  selector: 'meeting-question-list-view-groupped',
  templateUrl: './question-list-view-groupped.component.html',
  styleUrls: ['./question-list-view-groupped.component.scss']
})
export class QuestionListViewGrouppedComponent extends QuestionListViewComponent implements OnInit {
  loading: boolean;
  searchSections: any[];

  constructor(public solrMediator: SolrMediatorService, public meetingTypeService: MeetingTypeService,
              public toastr: ToastrService,
              public helper: HelperService) {
    super(solrMediator, meetingTypeService, toastr, helper);
  }

  ngOnInit() {
    super.ngOnInit();
  }

  doSearch(): Promise<FacetedSearchResult> {
    this.searchRequest.sort = this.sorting.name + ' ' + this.sorting.value;
    // this.searchRequest.common = this.common;
    this.searchRequest.fields = this.getSearchFields();
    // this.searchRequest.jsonFacet = '{categories: {type: \'terms\', field: \'groupDateTypeNumberMeet\', ' +
    //   'limit: -1, missing: true, numBuckets: true}}';
    return this.solrMediator.searchExt(this.searchRequest).pipe(
      map(resp => {
        this.searchSections = this.convertSections(resp.facets);
        this.sortSections();
        this.setFirstSectionsPageVisible();
        let numFound = 0;
        if (resp.facets.categories) {
          numFound += resp.facets.categories.numBuckets;
        }
        if (resp.facets.categories.missing.count > 0) {
          numFound++;
        }
        resp.numFound = numFound;
        return resp;
      })
    ).toPromise();
  }

  fetchSubsections(searchSection) {
    searchSection.docs = [];
    searchSection.loading = true;
    return this.solrMediator.searchExt({
      common: this.common,
      fields: this.getSearchFields().concat([searchSection.filter]),
      pageSize: 0,
      types: [ this.solrMediator.types.question ],
      jsonFacet: '{categories: {type: \'terms\', field: \'decisionText\', limit: -1, missing: true}}'
    }).subscribe(result => {
      searchSection.subsections = this.convertSubsections(result.facets);
      searchSection.loading = false;
    }, error => {
      console.log(error);
      searchSection.loading = false;
    });
  }

  fetchDocuments(searchSection, searchSubsection) {
    if (!searchSubsection.docs) {
      searchSubsection.docs = [];
    }
    searchSubsection.loading = true;
    return this.solrMediator.query({
      common: this.common,
      query: this.getSearchQuery(),
      // fields: this.getSearchFields().concat([searchSection.filter, searchSubsection.filter]),
      pageSize: this.searchRequest.pageSize,
      page: searchSubsection.docs.length / this.searchRequest.pageSize,
      types: [ this.solrMediator.types.question ]
    }).subscribe(result => {
      searchSubsection.docs = searchSubsection.docs.concat(result.docs);
      searchSubsection.hasMoreDocs = result.numFound > searchSubsection.docs.length;
      searchSubsection.loading = false;
    }, error => {
      console.log(error);
      searchSubsection.loading = false;
    });
  }

  showMoreDocs(searchSection, searchSubsection) {
    this.fetchDocuments(searchSection, searchSubsection);
  }

  convertSections(facets) {
    const result = [];
    if (facets.categories) {
      if (facets.categories.numBuckets > 0) {
        facets.categories.buckets.forEach(bucket => {
          const vals = bucket.val.split('#');
          result.push({
            id: bucket.val,
            meetingDate: vals[0] ? new Date(vals[0]) : null,
            meetingType: this.meetingTypes.find(mt => mt.meetingType === vals[1]),
            meetingNumber: vals[2],
            count: bucket.count,
            filter: {name: 'groupDateTypeNumberMeet', value: bucket.val},
            expanded: false,
            loading: false
          });
        });
      }
    }
    return result;
  }

  convertSubsections(facets) {
    const result = [];
    if (facets.categories) {
      if (facets.categories.numBuckets > 0) {
        facets.categories.buckets.forEach(bucket => {
          result.push({
            id: bucket.val,
            decisionText: bucket.val,
            count: bucket.count,
            filter: {name: 'decisionText', value: bucket.val},
            expanded: false,
            loading: false
          });
        });
      }
      if (facets.categories.missing.count > 0) {
        result.push({
          id: 'NULL',
          decisionText: null,
          count: facets.categories.missing.count,
          filter: {name: 'decisionText', value: 'NULL'},
          expanded: false,
          loading: false
        });
      }
    }
    return result;
  }

  sortSections() {
    if (this.sorting) {
      const predicate = this.sorting.value === 'asc' ? '+' : '-';
      this.searchSections = _.orderBy(this.searchSections, [this.sorting.name], [this.sorting.value]);
    }
  }

  setFirstSectionsPageVisible() {
    this.searchSections.forEach((section, index) => {
      section.visible = index >= this.searchRequest.page * this.searchResult.pageSize
        && index < (this.searchRequest.page + 1) * this.searchResult.pageSize;
    });
  }

  getVisibleSearchSections() {
    return this.searchSections.filter(s => s.visible);
  }

  triggerSection(searchSection: SearchSection) {
    if (searchSection.expanded) {
      this.fetchSubsections(searchSection);
    }
  }

  triggerSubsection(searchSection: SearchSection, searchSubsection: SearchSubsection) {
    if (searchSubsection.expanded) {
      this.fetchDocuments(searchSection, searchSubsection);
    }
  }

}
