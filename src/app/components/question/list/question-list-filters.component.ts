import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {uniqBy} from 'lodash';
import {NsiResourceService, UserBean} from '@reinform-cdp/nsi-resource';
import {BsLocaleService} from 'ngx-bootstrap/datepicker';
import {Prefect} from '../../../models/nsi/Prefect';
import {ResponsibleDepartment} from '../../../models/nsi/ResponsibleDepartment';
import {CategoryQuestion} from '../../../models/nsi/CategoryQuestion';
import {Decision} from '../../../models/nsi/Decision';
import {Filter} from './Filter';

@Component({
  selector: 'meeting-question-list-filters',
  templateUrl: './question-list-filters.component.html',
  styleUrls: ['./question-list-filters.component.scss']
})
export class QuestionListFiltersComponent implements OnInit {

  @Input() filter: Filter;
  @Input() isExternal?: boolean;
  @Output() filterChanged: EventEmitter<any> = new EventEmitter();

  prefects: Prefect[];
  responsibleDepartments: ResponsibleDepartment[];
  responsibleContractors: UserBean[];
  questionCategories: CategoryQuestion[];
  decisions: Decision[];
  decisionList: any[] = [];

  constructor(private nsiResourceService: NsiResourceService,
              private localeService: BsLocaleService) {
  }

  ngOnInit() {
    this.localeService.use('ru');
    this.loadDictionaries();
  }

  loadDictionaries(): Promise<any> {
    return Promise.all([
      this.nsiResourceService.get('Prefect'),
      this.nsiResourceService.get('mggt_meeting_ResponsibleDepartments'),
      this.getResponsibleContractors(),
      this.nsiResourceService.get('mggt_meeting_CategoryQuestion'),
      this.nsiResourceService.get('mggt_meeting_Decisions')
    ]).then((result: [Prefect[], ResponsibleDepartment[], UserBean[], CategoryQuestion[], Decision[]]) => {
      this.prefects = result[0];
      this.responsibleDepartments = result[1];
      this.responsibleContractors = result[2];
      this.questionCategories = result[3];
      this.decisions = result[4];
      this.decisionList = this.getDecisions();
    });
  }

  search() {
    this.filterChanged.emit(this.filter);
  }

  getDecisions(): Decision[] {
    return this.filter.questionCategory
    ? this.decisions.filter(d => d.questionCode === this.filter.questionCategory.questionCode)
    : uniqBy(this.decisions, d => d.decisionType);
  }

  getResponsibleContractors(responsibleDepartment?: ResponsibleDepartment): Promise<UserBean[]> {
    return Promise.resolve(this.nsiResourceService.searchUsers({
      group: ['MGGT_MEETING_RESPONSIBLE'],
      department: responsibleDepartment ? [responsibleDepartment.departmentPrepareCode] : []
    }));
  }

  clear() {
    this.filter.address = null;
    this.filter.prefects = [];
    this.filter.responsibleDepartments = null;
    this.filter.questionCategory = null;
    this.filter.meetingDate.clear();
    this.filter.decision = null;
    this.filter.responsibleContractor = null;
    this.filter.questionStatus = null;
    this.filter.cadastralNumbers = null;
    this.filter.basisNumber = null;
    this.filter.gpzuNumber = null;
    this.filter.customer = null;
    this.filter.rightHolder = null;
  }

}
