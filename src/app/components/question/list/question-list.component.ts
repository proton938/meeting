import {Component, OnInit, ViewChild, Input} from '@angular/core';
import {FacetedSearchResult, CdpSearchExtData} from '@reinform-cdp/search-resource';
import {Sorting} from '../../../models/Sorting';
import {QuestionListViewPlainComponent} from './question-list-view-plain.component';
import {QuestionListViewGrouppedComponent} from './question-list-view-groupped.component';
import {Filter} from './Filter';
import {SolrMediatorService} from '../../../services/solr-mediator.service';


@Component({
  selector: 'meeting-question-list',
  templateUrl: './question-list.component.html',
  styleUrls: ['./question-list.component.scss']
})
export class QuestionListComponent implements OnInit {
  @Input() isExternal = false;

  isLoading: boolean;
  searchRequest: any;
  searchResult: FacetedSearchResult;
  currentPage: number;
  view: string;
  extSearchCollapsed = true;
  sorting: Sorting;
  filter: Filter;
  common: string;

  @ViewChild(QuestionListViewPlainComponent)
  plainView: QuestionListViewPlainComponent;

  @ViewChild(QuestionListViewGrouppedComponent)
  groupedView: QuestionListViewGrouppedComponent;

  constructor(private solrMediator: SolrMediatorService) {
  }

  ngOnInit() {

    this.view = 'list';

    this.searchResult = {facets: {}, docs: [], numFound: 0, pageSize: 0, start: 0};
    this.searchRequest = {
      page: 0,
      pageSize: 10,
      types: [ this.solrMediator.types.question ]
    }; // new CdpSearchExtData(null, 0, 10, null, null, [], [this.solrMediator.types.question]);
    this.sorting = new Sorting('meetingDate', 'desc', 'По дате рассмотрения');
    this.filter = new Filter();
  }

  search() {
    if (this.plainView) {
      this.plainView.search();
    } else if (this.groupedView) {
      this.groupedView.search();
    }
  }

  sortingChange(sorting: Sorting) {
    this.sorting = sorting;
    this.searchRequest.page = 0;
    this.search();
  }

  filterChange(filter: Filter) {
    this.filter = filter;
    this.searchRequest.page = 0;
    this.search();
  }

  commonChange(common: string) {
    this.searchRequest.common = common;
    this.searchRequest.page = 0;
    this.search();
  }

  changePage() {
    this.searchRequest.page = this.currentPage - 1;
    this.search();
  }

  isListView() {
    return this.view === 'list';
  }

  isIndentView() {
    return this.view === 'indent';
  }

}
