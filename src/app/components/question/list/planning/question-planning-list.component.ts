import {Component, OnInit} from '@angular/core';
import {FacetedSearchResult, SearchExtDataItem} from '@reinform-cdp/search-resource';
import {MeetingType} from '../../../../models/nsi/MeetingType';
import {MeetingTypeService} from '../../../../services/meeting-type.service';
import {Sorting} from '../../../../models/Sorting';
import * as _ from 'lodash';
import {Filter} from '../Filter';
import {SolrMediatorService} from '../../../../services/solr-mediator.service';
import {HelperService} from '../../../../services/helper.service';

export class SearchSection {
  id: string;
  planDate: Date;
  meetingType: string;
  count: number;
  filter: {
    name: string;
    value: string;
  };
  expanded: false;
  loading: false;
  visible?: boolean;
}

@Component({
  selector: 'meeting-question-planning-list',
  templateUrl: './question-planning-list.component.html',
  styleUrls: ['./question-planning-list.component.scss']
})
export class QuestionPlanningListComponent implements OnInit {
  isLoading: boolean;
  docsFound: number;
  searchRequest: any;
  searchResult: FacetedSearchResult;
  searchSections: SearchSection[];
  currentPage: number;
  extSearchCollapsed = true;
  sorting: Sorting;
  filter: Filter;
  common: string;

  meetingTypes: MeetingType[];

  constructor(private solrMediator: SolrMediatorService,
              private meetingTypeService: MeetingTypeService,
              private helper: HelperService) {
    this.isLoading = true;
  }

  ngOnInit() {
    this.searchResult = {facets: {}, docs: [], numFound: 0, pageSize: 0, start: 0};
    this.searchSections = [];
    this.searchRequest = {
      page: 0,
      pageSize: 10,
      types: [ this.solrMediator.types.question ]
    }; // new CdpSearchExtData(null, 0, 10, null, null, [], [this.solrMediator.types.question]);
    this.sorting = new Sorting('planDate', 'desc', 'По дате рассмотрения');
    this.filter = new Filter();
    this.meetingTypeService.getNonExpressMeetingTypes().then(meetingTypes => {
      this.meetingTypes = meetingTypes;
      this.search();
    });
  }

  search() {
    this.isLoading = true;
    this.searchRequest.sort = this.sorting.name + ' ' + this.sorting.value;
    // this.searchRequest.common = this.common;
    // this.searchRequest.fields = this.getSearchFields();
    this.searchRequest.query = this.getSearchFields();
    this.searchRequest.jsonFacet = '{categories: {type: \'terms\', field: \'groupPlanDateTypeMeet\', limit: -1, numBuckets: true}}';
    this.solrMediator.query(this.searchRequest).subscribe(resp => {
      this.searchResult = resp;
      this.docsFound = resp.numFound;
      this.searchSections = this.convertSections(resp.facets);
      this.sortSections();
      this.setFirstSectionsPageVisible();
      this.searchResult.numFound = resp.facets.categories ? resp.facets.categories.numBuckets : 0;
      this.isLoading = false;
      return true;
    }, err => {
      this.helper.error(err);
      console.log(err);
      this.isLoading = false;
    });
  }

  fetchDocuments(searchSection) {
    searchSection.docs = [];
    searchSection.loading = true;
    return this.solrMediator.query({
      common: this.common,
      // fields: this.getSearchFields().concat([searchSection.filter]),
      query: this.getSearchQuery(),
      pageSize: 200,
      types: [ this.solrMediator.types.question ]
    }).toPromise().then(result => {
      searchSection.docs = result.docs;
      return true;
    }).then(() => {
      searchSection.loading = false;
    }).catch(error => {
      console.log(error);
      searchSection.loading = false;
    });
  }

  sortSections() {
    if (this.sorting) {
      this.searchSections = _.orderBy(this.searchSections, [this.sorting.name, 'meetingType'],
        [this.sorting.value, 'asc']);
    }
  }

  setFirstSectionsPageVisible() {
    this.searchSections.forEach((section, index) => {
      section.visible = index >= this.searchRequest.page * this.searchResult.pageSize
        && index < (this.searchRequest.page + 1) * this.searchResult.pageSize;
    });
  }

  convertSections(facets): SearchSection[] {
    if (facets.categories) {
      return facets.categories.buckets.map((bucket) => {
        const vals = bucket.val.split('#');
        return {
          id: bucket.val,
          planDate: vals[0] ? new Date(vals[0]) : null,
          meetingType: this.meetingTypes.find(mt => mt.meetingType === vals[1]),
          count: bucket.count,
          filter: {name: 'groupPlanDateTypeMeet', value: bucket.val}, // TODO нет такого поля
          expanded: false,
          loading: false
        };
      });
    }
    return [];
  }

  triggerSection(searchSection: SearchSection) {
    if (searchSection.expanded) {
      this.fetchDocuments(searchSection);
    }
  }

  getSearchFields(): SearchExtDataItem[] {
    let result = [];
    result.push(new SearchExtDataItem('meetingType', this.meetingTypes.map(mt => mt.meetingType)));
    if (this.filter) {
      result = result.concat(this.filter.toSearchFields());
    }
    return result;
  }

  getSearchQuery(): string {
    let r = '';
    r += '(meetingType:(' + this.meetingTypes.map(mt => mt.meetingType).join(') OR meetingType:(') + '))';
    if (this.filter) {
      const queryFilter = this.filter.toQueryString();
      if (queryFilter) {
        r += ' AND ' + '(' + queryFilter + ')';
      }
    }
    return r;
  }

  sortingChange(sorting: Sorting) {
    this.sorting = sorting;
    this.searchRequest.page = 0;
    this.search();
  }

  filterChange(filter: Filter) {
    this.filter = filter;
    this.searchRequest.page = 0;
    this.search();
  }

  commonChange(common: string) {
    this.searchRequest.common = common;
    this.searchRequest.page = 0;
    this.search();
  }

  changePage() {
    this.searchRequest.page = this.currentPage - 1;
    this.search();
  }

  getVisibleSearchSections() {
    return this.searchSections.filter(s => s.visible);
  }

  getPrefectsDistrictsString(prefectNames: string[] , districtNames: string[]) {
    const strings = [];
    if (prefectNames) {
      strings.push(prefectNames.join(','));
    }
    if (districtNames) {
      strings.push(districtNames.join(','));
    }
    return strings.join(', ');
  }
}
