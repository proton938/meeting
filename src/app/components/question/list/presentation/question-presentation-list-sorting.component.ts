import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Sorting} from "../../../../models/Sorting";

@Component({
  selector: 'meeting-question-presentation-list-sorting',
  templateUrl: './question-presentation-list-sorting.component.html',
  styleUrls: ['./question-presentation-list-sorting.component.scss']
})
export class QuestionPresentationListSortingComponent implements OnInit {

  @Input() sorting: Sorting;
  @Output() sortingChanged: EventEmitter<any> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  change(sortingName: string, sortingValue: string, sortingText: string) {
    this.sorting = new Sorting(sortingName, sortingValue, sortingText);
    this.sortingChanged.emit(this.sorting);
  }

  changeValue() {
    if (this.sorting) {
      this.sorting.value = this.sorting.value === 'asc' ? 'desc' : 'asc';
      this.sortingChanged.emit(this.sorting);
    }
  }

}
