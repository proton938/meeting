import { Component, OnInit } from '@angular/core';
import {FacetedSearchResult, SearchExtDataItem} from '@reinform-cdp/search-resource';
import {MeetingType} from '../../../../models/nsi/MeetingType';
import {MeetingTypeService} from '../../../../services/meeting-type.service';
import * as _ from 'lodash';
import {Sorting} from '../../../../models/Sorting';
import {Filter} from '../Filter';
import {SolrMediatorService} from '../../../../services/solr-mediator.service';
import {HelperService} from '../../../../services/helper.service';

export class SearchSection {
  id: string;
  planDate: Date;
  meetingType: string;
  count: number;
  filter: {
    name: string;
    value: string;
  };
  expanded: false;
  loading: false;
  visible?: boolean;
}

export class SearchSubsection {
  id: string;
  planDate: Date;
  meetingType: string;
  count: number;
  filter: {
    name: string;
    value: string;
  };
  expanded: false;
  loading: false;
}

@Component({
  selector: 'meeting-question-presentation-list',
  templateUrl: './question-presentation-list.component.html',
  styleUrls: ['./question-presentation-list.component.scss']
})
export class QuestionPresentationListComponent implements OnInit {
  isLoading: boolean;
  searchRequest: any;
  searchResult: FacetedSearchResult;
  searchSections: SearchSection[];
  currentPage: number;
  extSearchCollapsed = true;
  sorting: Sorting;
  filter: Filter;
  common: string;

  meetingTypes: MeetingType[];

  constructor(private solrMediator: SolrMediatorService,
              private meetingTypeService: MeetingTypeService,
              private helper: HelperService) {
    this.isLoading = true;
  }

  ngOnInit() {
    this.searchResult = {facets: {}, docs: [], numFound: 0, pageSize: 0, start: 0};
    this.searchSections = [];
    this.searchRequest = {
      page: 0,
      pageSize: 10,
      types: [ this.solrMediator.types.question ]
    }; // new CdpSearchExtData(null, 0, 10, null, null, [], [this.solrMediator.types.question]);
    this.sorting = new Sorting('presentPrepareFioFull', 'desc', 'По ответственному');
    this.filter = new Filter();
    this.meetingTypeService.getNonExpressMeetingTypes().then(meetingTypes => {
      this.meetingTypes = meetingTypes;
      this.search();
    });
  }

  search() {
    this.isLoading = true;
    this.searchRequest.sort = this.sorting.name + ' ' + this.sorting.value;
    // this.searchRequest.common = this.common;
    // this.searchRequest.fields = this.getSearchFields();
    this.searchRequest.query = this.getSearchQuery();
    this.searchRequest.jsonFacet = '{categories: {type: \'terms\', field: \'presentPrepareFioFull\', ' +
      'limit: -1, missing: true, numBuckets: true, sort: \'index asc\'}}';
    this.solrMediator.searchExt(this.searchRequest).subscribe(resp => {
      this.searchResult = resp;
      this.searchSections = this.convertSections(resp.facets);
      this.sortSections();
      this.setFirstSectionsPageVisible();
      let numFound = 0;
      if (resp.facets.categories) {
        numFound += resp.facets.categories.numBuckets;
      }
      if (resp.facets.categories.missing.count > 0) {
        numFound++;
      }
      this.searchResult.numFound = numFound;
      this.isLoading = false;
      return true;
    }, err => {
      this.helper.error(err);
      console.log(err);
      this.isLoading = false;
    });
  }

  fetchSubsections(searchSection) {
    searchSection.docs = [];
    searchSection.loading = true;
    return this.solrMediator.searchExt({
      common: this.common,
      fields: this.getSearchFields().concat([searchSection.filter]),
      pageSize: 200,
      types: [ this.solrMediator.types.question ],
      jsonFacet: '{categories: {type: \'terms\', field: \'groupPlanDateTypeMeet\', limit: -1, missing: true}}'
    }).toPromise().then(result => {
      searchSection.subsections = this.convertSubsections(result.facets);
      return true;
    }).then(response => {
      searchSection.loading = false;
    }).catch(error => {
      console.log(error);
      searchSection.loading = false;
    });
  }

  fetchDocuments(searchSection, searchSubsection) {
    searchSubsection.docs = [];
    searchSubsection.loading = true;
    return this.solrMediator.searchExt({
      common: this.common,
      fields: this.getSearchFields().concat([searchSection.filter, searchSubsection.filter]),
      pageSize: 200,
      types: [ this.solrMediator.types.question ]
    }).toPromise().then(result => {
      searchSubsection.docs = result.docs;
      return true;
    }).then(() => {
      searchSubsection.loading = false;
    }).catch(error => {
      console.log(error);
      searchSubsection.loading = false;
    });
  }

  sortSections() {
    if (this.sorting) {
      this.searchSections = _.orderBy(this.searchSections, [this.sorting.name], [this.sorting.value]);
    }
  }

  setFirstSectionsPageVisible() {
    this.searchSections.forEach((section, index) => {
      section.visible = index >= this.searchRequest.page * this.searchResult.pageSize
        && index < (this.searchRequest.page + 1) * this.searchResult.pageSize;
    });
  }

  convertSections(facets): SearchSection[] {
    if (facets.categories) {
      const result = [];
      if (facets.categories.numBuckets > 0) {
        facets.categories.buckets.forEach(bucket => {
          result.push({
            id: bucket.val,
            presentPrepareFioFull: bucket.val,
            count: bucket.count,
            filter: {name: 'presentPrepareFioFull', value: bucket.val},
            expanded: false,
            loading: false
          });
        });
      }
      if (facets.categories.missing.count > 0) {
        result.push({
          id: 'NULL',
          presentPrepareFioFull: null,
          count: facets.categories.missing.count,
          filter: {name: 'presentPrepareFioFull', value: 'NULL'},
          expanded: false,
          loading: false
        });
      }
      return result;
    }
    return [];
  }

  convertSubsections(facets): SearchSubsection[] {
    const result = [];
    if (facets.categories) {
      facets.categories.buckets.forEach(bucket => {
        const vals = bucket.val.split('#');
        result.push({
          id: bucket.val,
          planDate: vals[0] ? new Date(vals[0]) : null,
          meetingType: this.meetingTypes.find(mt => mt.meetingType === vals[1]),
          count: bucket.count,
          filter: {name: 'groupPlanDateTypeMeet', value: bucket.val},
          expanded: false,
          loading: false
        });
      });
    }
    return _.orderBy(result, ['planDate', 'meetingType.sortValue'], ['asc', 'asc']);
  }

  triggerSection(searchSection: SearchSection) {
    if (searchSection.expanded) {
      this.fetchSubsections(searchSection);
    }
  }

  triggerSubsection(searchSection: SearchSection, searchSubsection: SearchSubsection) {
    if (searchSubsection.expanded) {
      this.fetchDocuments(searchSection, searchSubsection);
    }
  }

  getSearchFields(): SearchExtDataItem[] {
    let result = [];
    result.push(new SearchExtDataItem('meetingType', this.meetingTypes.map(mt => mt.meetingType)));
    // result.push(new SearchExtDataItem('includedInAgendaSs,passDateSs', 'NOT_NULL')); // TODO нет такого поля
    if (this.filter) {
      result = result.concat(this.filter.toSearchFields());
    }
    return result;
  }

  getSearchQuery(): string {
    let r = '';
    r += '(meetingType:(' + this.meetingTypes.map(mt => mt.meetingType).join(') OR meetingType:(') + '))';
    if (this.filter) {
      const queryFilter = this.filter.toQueryString();
      if (queryFilter) {
        r += ' AND ' + '(' + queryFilter + ')';
      }
    }
    return r;
  }

  sortingChange(sorting: Sorting) {
    this.sorting = sorting;
    this.searchRequest.page = 0;
    this.search();
  }

  filterChange(filter: Filter) {
    this.filter = filter;
    this.searchRequest.page = 0;
    this.search();
  }

  commonChange(common: string) {
    this.searchRequest.common = common;
    this.searchRequest.page = 0;
    this.search();
  }

  changePage() {
    this.searchRequest.page = this.currentPage - 1;
    this.search();
  }

  getVisibleSearchSections() {
    return this.searchSections.filter(s => s.visible);
  }

  getPrefectsDistrictsString(prefectNames: string[] , districtNames: string[]) {
    const strings = [];
    if (prefectNames) {
      strings.push(prefectNames.join(','));
    }
    if (districtNames) {
      strings.push(districtNames.join(','));
    }
    return strings.join(', ');
  }

  getFileLink(idFile) {
    return `/filestore/v1/files/${idFile}?systemCode=sdo`;
  }

}
