import { SessionStorage } from '@reinform-cdp/security';
import { NsiResourceService } from '@reinform-cdp/nsi-resource';
import * as _ from 'lodash';
import { LoadingStatus } from '@reinform-cdp/widgets';
import { StateService } from '@uirouter/core';
import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { forkJoin } from 'rxjs/index';
import { mergeMap } from 'rxjs/internal/operators';
import { OasiShowcaseTabModel } from '../../../models/tab/OasiShowcaseTabModel';
import { MeetingTypeService } from '../../../services/meeting-type.service';
import { OasiShowcaseAdminMeetingsTabBuilder } from '../../../models/tab/builders/ShowcaseExpressMeetingsTabBuilder';
import { BreadcrumbsService } from '@reinform-cdp/skeleton';
import { MeetingListExpressComponent } from './meetings-list.component';


export interface IExpressMeetingsShowcaseDicts {
  prefect: any;
  meetingTypes: any;
  meetingStatuses: any;
  meetingThemes: any;
  oasiInstructionExecutors: any;
  users?: any;
}

@Component({
  selector: 'mggt-showcase-express',
  templateUrl: './express.component.html',
  styleUrls: ['./express.component.scss']
})
export class ShowcaseExpressComponent implements OnInit, OnDestroy {

  loading: LoadingStatus = LoadingStatus.LOADING;
  success = false;
  dicts: IExpressMeetingsShowcaseDicts;

  _search = '';
  search = '';

  activeTab: string;
  tabs: OasiShowcaseTabModel[] = [];

  @ViewChild(MeetingListExpressComponent)
  meetingList: MeetingListExpressComponent;

  constructor(public $state: StateService,
              private nsiRestService: NsiResourceService,
              private meetingTypeService: MeetingTypeService,
              private session: SessionStorage,
              private breadcrumbsService: BreadcrumbsService) {
  }

  ngOnInit() {
    this.updateBreadcrumbs();
    window.addEventListener('storage', this.onStorageChanges);
    forkJoin([
      this.nsiRestService.getDictsFromCache([
        'Prefect',
        'InstructionExecutors',
        'mggt_meeting_Theme',
        'mggt_meeting_statusAc'
      ]),
      this.meetingTypeService.getExpressMeetingTypes()
    ]).subscribe(response => {
        const dict: any = response[0];
        this.dicts = {
          meetingTypes: response[1],
          prefect: dict.Prefect,
          oasiInstructionExecutors: dict.InstructionExecutors,
          meetingThemes: dict.mggt_meeting_Theme,
          meetingStatuses: dict.mggt_meeting_statusAc.map(statusDetails => statusDetails.name)
        };

        const params: any = {};
        params.fio = this.session.fio();
       // return this.nsiRestService.searchUsers(params);

      const tabBuilder = new OasiShowcaseAdminMeetingsTabBuilder(this.session,
        this.dicts.meetingTypes, this.dicts.oasiInstructionExecutors);
      this.tabs = tabBuilder.build();

      this.getFromStorage();
      this.saveToStorage();

      this.loading = LoadingStatus.SUCCESS;
      this.success = true;

    }, () => {
      this.loading = LoadingStatus.ERROR;
    });

  }

  ngOnDestroy() {
    window.removeEventListener('storage', this.onStorageChanges);
  }

  isAvailableToMeetingAgendaCreate(): boolean {
    return this.session.hasPermission('MEETING_AGENDA_CREATE');
  }

  onStorageChanges = function (changes) {
    if (changes.key === `${this.session.login()}.adminMeetings.revision`) {
      this.searchChanged();
    }
  }.bind(this);

  onCommonSearchChange() {
    this.search = this._search;
    this.saveToStorage();
  }

  saveToStorage(): void {
    localStorage[`${this.session.login()}.adminMeetings.common`] = this.search;
    localStorage[`${this.session.login()}.adminMeetings.tab`] = this.activeTab;
    localStorage[`${this.session.login()}.adminMeetings.revision`] = _.uniqueId();
  }

  getFromStorage(): void {
    if (localStorage[`${this.session.login()}.adminMeetings.common`]) {
      this.search = localStorage[`${this.session.login()}.adminMeetings.common`];
      this._search = this.search;
    }

    if (localStorage[`${this.session.login()}.adminMeetings.tab`]) {
      this.activeTab = localStorage[`${this.session.login()}.adminMeetings.tab`];
    } else {
      this.activeTab = this.tabs && this.tabs.length && (this.tabs.length > 0) ? this.tabs[0].alias : null;
    }
  }

  isActive(index: string): boolean {
    return index === this.activeTab;
  }

  activate(index: string): boolean {
    if (this.activeTab === index) {
      return false;
    } else {
      this.activeTab = index;
      this.saveToStorage();
      return true;
    }
  }

  updateBreadcrumbs() {
    this.breadcrumbsService.setBreadcrumbsChain([{
      title: 'Административные совещания',
      url: null
    }]);
  }

  searchChanged() {
    if (this.meetingList) {
      this.meetingList.commonChange(this.search);
    }
  }

}
