import * as _ from 'lodash';
import * as angular from 'angular';
import {FacetedSearchResult, QuerySearchData} from '@reinform-cdp/search-resource';
import {NsiResourceService} from '@reinform-cdp/nsi-resource';
import {ToastrService} from 'ngx-toastr';
import {SessionStorage} from '@reinform-cdp/security';
import {Component, Input, OnInit} from '@angular/core';
import {ShowcaseSorting} from '../../../models/ShowcaseSorting';
import {IExpressMeetingsShowcaseDicts} from './express.component';
import {ExpressMeetingsFilter} from '../../../models/filters/ExpressMeetingsFilter';
import {ExpressMeetingsTableFilter} from '../../../models/table-filters/ExpressMeetingsTableFilter';
import {ShowcaseExpressMeetingsSortingBuilder} from '../../../models/sorting/ShowcaseExpressMeetingsSortingBuilder';
import {Observable, Subject, concat, of, from} from 'rxjs';
import {catchError, debounceTime, distinctUntilChanged, filter, switchMap, mergeMap} from 'rxjs/internal/operators';
import {AgendaStatus} from '../../../models/Agenda';
import {SolrMediatorService} from '../../../services/solr-mediator.service';

@Component({
  selector: 'mggt-express-meetings-list',
  templateUrl: './meetings-list.component.html',
  styleUrls: ['./meetings-list.component.scss']
})
export class MeetingListExpressComponent implements OnInit {

  @Input() tabFilter: string;
  @Input() dicts: IExpressMeetingsShowcaseDicts;
  commonSearch: string;
  extSearchCollapsed = true;
  showClockRange = false;
  statuses: any = {};

  filter: ExpressMeetingsFilter = new ExpressMeetingsFilter();
  prevFilter: ExpressMeetingsFilter = new ExpressMeetingsFilter();
  prevTableFilter: ExpressMeetingsTableFilter = new ExpressMeetingsTableFilter();
  dataIsUpdating = true;
  sortings: ShowcaseSorting[] = [];
  searchRequest: QuerySearchData;
  searchResult: FacetedSearchResult;
  currentPage: number;
  block = false;
  tableFilter = new ExpressMeetingsTableFilter();

  initiators$: Observable<any>;
  initiatorsInput$ = new Subject<string>();

  authors$: Observable<any>;
  authorsInput$ = new Subject<string>();

  constructor(protected solrMediator: SolrMediatorService,
              private nsiRestService: NsiResourceService,
              private toastr: ToastrService,
              private session: SessionStorage) {
  }

  ngOnInit() {
    this.filter.clear();
    this.searchResult = {facets: {}, docs: [], numFound: 0, pageSize: 0, start: 0};
    this.searchRequest = {
      query: null,
      sort: null,
      page: 0,
      pageSize: 20,
      types: [ this.solrMediator.types.agenda ],
      common: null,
      jsonFacet: null
    };

    this.initiators$ = concat(
      of([]),
      this.initiatorsInput$.pipe(
        debounceTime(300),
        distinctUntilChanged(),
        filter(term => {
          return term && term.length > 2;
        }),
        switchMap((term: string) => this.searchUser(term)),
        catchError((error) => {
          console.log(error);
          return of([]);
        })
      )
    );

    this.authors$ = concat(
      of([]),
      this.authorsInput$.pipe(
        debounceTime(300),
        // distinctUntilChanged(),
        filter(term => {
          return term && term.length > 2;
        }),
        switchMap((term: string) => this.searchUser(term)),
        catchError((error) => {
          console.log(error);
          return of([]);
        })
      )
    );

    const sortingBuilder = new ShowcaseExpressMeetingsSortingBuilder();
    this.sortings = sortingBuilder.build();
    this.getSortingFromStorage();
    this.saveSortingToStorage();
    this.getTableFilterFromStorage();
    this.saveTableFilterToStorage();
    this.getFilterFromStorage();
    this.saveFilterToStorage();
    this.getPageFromStorage();
    this.savePageToStorage();
    this.updateData(true);
    this.getMeetingStatusesDetails();
  }

  sortingChanged(s: ShowcaseSorting) {
    this.sortings.forEach(_s => {
      if (s !== _s) {
        _s.value = 'none';
      } else {
        _s.value = (_s.value === 'none') ? 'desc' : ((_s.value === 'desc') ? 'asc' : 'desc');
      }
    });
    this.saveSortingToStorage();
    this.updateData(false);
  }

  savePageToStorage(): void {
    const sorting = _.find(this.sortings, (s: ShowcaseSorting) => {
      return (s.value === 'desc' || s.value === 'asc');
    });
    localStorage[`${this.session.login()}.adminMeetings.page`] = this.searchRequest.page;
  }

  updateData(loading: boolean) {
    if (loading) {
      this.dataIsUpdating = true;
    } else {
      this.block = true;
    }
    this.searchRequest.sort = this.sortingsToString();
    this.searchRequest.common = this.commonSearch;
    this.searchRequest.query = this.buildFilter();
    this.solrMediator.query(this.searchRequest).subscribe(response => {
      this.searchResult = response;
    }, error => {
      console.log(error);
    }, () => {
      this.dataIsUpdating = false;
      this.block = false;
    });
  }

  buildFilter(): string {
    let result = '';
    if (this.tabFilter) {
      result += `${this.tabFilter}`;
    }
    if (!this.filter.isEmpty()) {
      const _filter = this.filter.toString();
      if (_filter) {
        result += (result) ? ' AND ' : '';
        result += `(${_filter})`;
      }
    }

    if (!this.tableFilter.isEmpty()) {
      const _filter = this.tableFilter.toString();
      if (_filter) {
        result += (result) ? ' AND ' : '';
        result += `(${_filter})`;
      }
    }
    return result;
  }

  sortingsToString(): string {
    const result = this.sortings.filter((s) => {
      if (s.value !== 'none') {
        return s;
      }
    }).map((s) => {
      return `${s.code} ${s.value}`;
    }).join(',');
    return result;
  }

  getSortingFromStorage(): void {
    const sorting = localStorage[`${this.session.login()}.adminMeetings.sorting`];
    if (sorting) {
      this.sortings.forEach(s => {
        if (s.code === sorting.code) {
          s.value = sorting.value;
        } else {
          if (s.value === 'desc' || s.value === 'asc') {
            s.value = 'none';
          }
        }
      });
    }
  }

  saveSortingToStorage(): void {
    const sorting = _.filter(this.sortings, (s: ShowcaseSorting) => {
      return (s.value === 'desc' || s.value === 'asc');
    }).map((s) => {
      return {
        code: s.code,
        value: s.value
      };
    });
    localStorage[`${this.session.login()}.adminMeetings.sorting`] = sorting;
  }

  getTableFilterFromStorage(): void {
    const table = localStorage[`${this.session.login()}.adminMeetings.table`];
    if (table) {
      this.prevTableFilter = angular.copy(this.tableFilter);
    }
  }

  saveTableFilterToStorage(): void {
    if (!this.tableFilter.isEmpty()) {
      localStorage[`${this.session.login()}.adminMeetings.table`] = {};
    } else {
      localStorage[`${this.session.login()}.adminMeetings.table`] = undefined;
    }
  }

  getFilterFromStorage(): void {
    const table = localStorage[`${this.session.login()}.adminMeetings.filter`];
    if (table) {
      this.filter.meetingType = (table.meetingType) ? table.meetingType : [];
      this.filter.meetingTheme = (table.meetingTheme) ? table.meetingTheme : [];
      this.filter.statusName = (table.statusName) ? table.statusName : [];
      this.filter.meetingNumber = (table.meetingNumber) ? table.meetingNumber : undefined;
      this.filter.authorCreateName = (table.authorCreateName) ? table.authorCreateName : undefined;
      this.filter.initiatorName = (table.initiatorName) ? table.initiatorName : undefined;

      if (table.meetingDate) {
        this.filter.meetingDate.from = (table.meetingDate.from) ? new Date(table.meetingDate.from) : undefined;
        this.filter.meetingDate.to = (table.meetingDate.to) ? new Date(table.meetingDate.to) : undefined;
      }

      this.prevFilter = angular.copy(this.filter);
    }
  }

  saveFilterToStorage(): void {
    if (!this.filter.isEmpty()) {
      localStorage[`${this.session.login()}.instruction.table`] = {
        meetingTheme: this.filter.meetingTheme ? this.filter.meetingTheme : undefined,
        statusName: this.filter.statusName ? this.filter.statusName : undefined,
        meetingType: this.filter.meetingType ? this.filter.meetingType : undefined,
        meetingNumber: this.filter.meetingNumber ? this.filter.meetingNumber : undefined,
        authorCreateName: this.filter.authorCreateName ? this.filter.authorCreateName : undefined,
        initiatorName: this.filter.initiatorName ? this.filter.initiatorName : undefined,
        meetingDate: (!this.filter.meetingDate.isEmpty()) ? {
          from: this.filter.meetingDate.from ? this.formatDate(this.filter.meetingDate.from) : undefined,
          to: this.filter.meetingDate.to ? this.formatDate(this.filter.meetingDate.to) : undefined
        } : undefined
      };
    } else {
      localStorage[`${this.session.login()}.adminMeetings.filter`] = undefined;
    }
  }

  formatDate(date: Date) {
    return date.getFullYear() + '-' + date.getMonth() + '-' + date.getDay() + 'T00:00:00Z';
  }

  getPageFromStorage(): void {
    const page = localStorage[`${this.session.login()}.adminMeetings.page`];
    if (page) {
      this.searchRequest.page = page;
    }
  }

  filterChange() {
    if ((this.filter.toString() !== this.prevFilter.toString()) || (this.tableFilter.toString() !== this.prevTableFilter.toString())) {
      if (this.filter.toString() !== this.prevFilter.toString()) {
        this.prevFilter = angular.copy(this.filter);
      }

      if (this.tableFilter.toString() !== this.prevTableFilter.toString()) {
        this.prevTableFilter = angular.copy(this.tableFilter);
      }

      this.searchRequest.page = 0;
      this.savePageToStorage();
      this.saveTableFilterToStorage();
      this.saveFilterToStorage();
      this.updateData(true);
    }
  }

  updateUserList(fio?: string) {
    if (fio) {
      const params: any = {};
      params.fio = fio;
      this.nsiRestService.searchUsers(params).then(response => {
        this.dicts.users = response;
      });
    } else {
      this.dicts.users = [];
    }
  }

  toggleTimeFilter() {
    if (!this.showClockRange) {
      this.showClockRange = true;
    } else {
      this.showClockRange = false;
      this._onFilterChange();
    }
  }

  handleKeyPress(e) {
    if (e.keyCode === 13) {
      this.showClockRange = false;
      this._onFilterChange();
    }
  }

  clearTimeFilter() {
    this.tableFilter.meetingTime.clear();
    this.tableFilter.meetingTimeEnd.clear();
    this.showClockRange = false;
    this._onFilterChange();
  }

  _onFilterChange = function() {
    this.authorsInput$.next([]);
    this.filterChange();
  }.bind(this);

  private _updateUserList(fio?: string) {
    const f = fio ? fio : '';
    this.updateUserList(f);
  }

  changePage() {
    this.searchRequest.page = this.currentPage - 1;
    this.savePageToStorage();
    this.updateData(false);
  }

  clearFilters() {
    this.filter.clear();
    this.updateData(false);
  }

  commonChange(common: string) {
    this.commonSearch = common;
    this.searchRequest.page = 0;
    this.updateData(false);
  }

  searchUser(query: string) {
    if (query.length > 2) {
      const params: any = {};
      params.fio = query;
      return from(this.nsiRestService.searchUsers(params)).pipe(
        mergeMap((res) => {
          return of(res);
        }),
      );
    }

    return of([]);
  }

  getMeetingStatusesDetails() {
    this.nsiRestService.get('mggt_meeting_statusAc').then((data) => {
      const statusMap = {};
      data.map((assignStatusDetails: AgendaStatus)  => {
        const {color, name} = assignStatusDetails;
        statusMap[name] = color;
      });
      this.statuses = statusMap;
    });
  }
}
