import {SessionStorage} from "@reinform-cdp/security";
import {NsiResourceService} from "@reinform-cdp/nsi-resource";
import {LoadingStatus} from "@reinform-cdp/widgets";
import {StateService} from '@uirouter/core';
import {Component, OnInit, ViewChild} from '@angular/core';
import {forkJoin} from "rxjs/index";
import {mergeMap} from "rxjs/internal/operators";
import {OasiShowcaseTabModel} from "../../../models/tab/OasiShowcaseTabModel";
import {MeetingTypeService} from "../../../services/meeting-type.service";
import {OasiShowcaseMKAMeetingsTabBuilder} from "../../../models/tab/builders/ShowcaseExpressMKAMeetingsTabBuilder";
import {BreadcrumbsService} from "@reinform-cdp/skeleton";
import {MeetingListMKAExpressComponent} from "./meetings-list.component";


export interface IExpressMeetingsShowcaseDicts {
  prefect: any;
  meetingTypes: any;
  meetingThemes: any;
  oasiInstructionExecutors: any;
  users?: any;
}

@Component({
  selector: 'mggt-showcase-express-mka',
  templateUrl: './express-mka.component.html',
  styleUrls: ['./express-mka.component.scss']
})
export class ShowcaseExpressMKAComponent implements OnInit {

  loading: LoadingStatus = LoadingStatus.LOADING;
  success = false;
  dicts: IExpressMeetingsShowcaseDicts;

  _search: string = '';
  search: string = '';

  activeTab: string;
  tabs: OasiShowcaseTabModel[] = [];

  @ViewChild(MeetingListMKAExpressComponent)
  meetingList: MeetingListMKAExpressComponent;

  constructor(public $state: StateService, private nsiRestService: NsiResourceService, private meetingTypeService: MeetingTypeService,
              private session: SessionStorage, private breadcrumbsService: BreadcrumbsService) {
  }

  ngOnInit() {
    this.updateBreadcrumbs();
    forkJoin([
      this.nsiRestService.getDictsFromCache([
        'Prefect',
        'InstructionExecutors',
        'mggt_meeting_Theme'
      ]),
      this.meetingTypeService.getExpressNonExternalMeetingTypes()
    ]).subscribe(response => {

      this.dicts = {
        meetingTypes: response[1],
        prefect: response[0].Prefect,
        oasiInstructionExecutors: response[0].InstructionExecutors,
        meetingThemes: response[0].mggt_meeting_Theme
      };

      //return this.nsiRestService.searchUsers({});
      // this.dicts.users = response;

      let tabBuilder = new OasiShowcaseMKAMeetingsTabBuilder(this.session, this.dicts.meetingTypes, this.dicts.oasiInstructionExecutors);
      this.tabs = tabBuilder.build();

      this.getFromStorage();
      this.saveToStorage();

      this.loading = LoadingStatus.SUCCESS;
      this.success = true;

    }, () => {
      this.loading = LoadingStatus.ERROR;
    });

  }

  onCommonSearchChange() {
    this.search = this._search;
    this.saveToStorage();
  }

  saveToStorage(): void {
    localStorage[`${this.session.login()}.adminMeetings.common`] = this.search;
    localStorage[`${this.session.login()}.adminMeetings.tab`] = this.activeTab;
  }

  getFromStorage(): void {
    if (localStorage[`${this.session.login()}.adminMeetings.common`]) {
      this.search = localStorage[`${this.session.login()}.adminMeetings.common`];
      this._search = this.search;
    }

    if (localStorage[`${this.session.login()}.adminMeetings.tab`]) {
      this.activeTab = localStorage[`${this.session.login()}.adminMeetings.tab`]
    } else {
      this.activeTab = this.tabs && this.tabs.length && (this.tabs.length > 0) ? this.tabs[0].alias : null;
    }
  }

  isActive(index: string): boolean {
    return index === this.activeTab;
  }

  activate(index: string): boolean {
    if (this.activeTab === index) {
      return false;
    } else {
      this.activeTab = index;
      this.saveToStorage();
      return true;
    }
  }

  updateBreadcrumbs() {
    this.breadcrumbsService.setBreadcrumbsChain([{
      title: 'Административные совещания МКА',
      url: null
    }]);
  }

  searchChanged() {
    if (this.meetingList) {
      this.meetingList.commonChange(this.search);
    }
  }

}
