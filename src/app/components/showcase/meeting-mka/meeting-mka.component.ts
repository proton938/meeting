import {Transition} from '@uirouter/core';
import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {QuestionListComponent} from "../../question/list/question-list.component";
import {AgendaListComponent} from "../../agenda/list/agenda-list.component";
import {AgendaProjectsListComponent} from "../../agenda/list/agenda-projects-list.component";
import {ProtocolListComponent} from "../../protocol/list/protocol-list.component";
import {QuestionPlanningListComponent} from "../../question/list/planning/question-planning-list.component";
import {QuestionPresentationListComponent} from "../../question/list/presentation/question-presentation-list.component";
import {BreadcrumbsService} from "@reinform-cdp/skeleton";

@Component({
  selector: 'meeting-mka-showcase',
  templateUrl: './meeting-mka.component.html',
  styleUrls: ['./meeting-mka.component.scss']
})
export class MeetingMkaShowcaseComponent implements OnInit {

  search: string;
  activeTab: string;

  @ViewChild(QuestionListComponent)
  questions: QuestionListComponent;

  @ViewChild(AgendaListComponent)
  agendas: AgendaListComponent;

  @ViewChild(ProtocolListComponent)
  protocols: ProtocolListComponent;

  constructor(private transition: Transition,
              private breadcrumbsService: BreadcrumbsService) {
  }

  ngOnInit() {
    this.activeTab = this.transition.params()['tab'] || 'questions';
    this.updateBreadcrumbs();
  }

  searchChanged() {
    if (this.questions) {
      this.questions.commonChange(this.search);
    }
    if (this.agendas) {
      this.agendas.commonChange(this.search);
    }
    if (this.protocols) {
      this.protocols.commonChange(this.search);
    }
  }

  activate(index: string): boolean {
    if (this.activeTab === index) {
      return false;
    } else {
      this.activeTab = index;
      return true;
    }
  }

  isActive(index: string): boolean {
    return index === this.activeTab;
  }

  updateBreadcrumbs() {
    this.breadcrumbsService.setBreadcrumbsChain([{
      title: 'Регламентные совещания МКА',
      url: null
    }]);
  }
}
