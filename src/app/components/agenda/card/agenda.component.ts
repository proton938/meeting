import { Component, OnInit } from '@angular/core';
import { Agenda, AgendaDocumentWrapper, AgendaQuestion, AgendaQuestionStatus } from "../../../models/Agenda";
import { LoadingStatus } from "@reinform-cdp/widgets";
import { StateService, Transition } from '@uirouter/core';
import { MeetingTypeService } from "../../../services/meeting-type.service";
import { MeetingType } from "../../../models/nsi/MeetingType";
import { AgendaService } from "../../../services/agenda.service";
import { ToastrService } from "ngx-toastr";
import * as angular from 'angular';
import { BreadcrumbItem, BreadcrumbsService } from "@reinform-cdp/skeleton";
import { forkJoin, from, of, throwError } from "rxjs/index";
import { map, mergeMap } from "rxjs/internal/operators";
import { Observable, Subscription } from "rxjs/Rx";

export class AgendaData {
  agenda: Agenda;
  express: boolean;
}

@Component({
  selector: 'meeting-agenda',
  templateUrl: './agenda.component.html',
  styleUrls: ['./agenda.component.scss']
})
export class AgendaComponent implements OnInit {

  origAgenda: Agenda;
  agenda: Agenda;
  express: boolean;
  loadingStatus: LoadingStatus;
  success: boolean;

  editing: boolean;

  constructor(private transition: Transition,
    private $state: StateService,
    private agendaService: AgendaService,
    private meetingTypeService: MeetingTypeService,
    private breadcrumbsService: BreadcrumbsService,
    private toastr: ToastrService) {
  }

  ngOnInit() {

    this.editing = this.transition.params()['editing'];

    let id: string = this.transition.params()['id'];
    let agenda: Agenda = this.transition.params()['agenda'];
    let express: boolean = this.transition.params()['express'] || false;
    this.loadingStatus = LoadingStatus.LOADING;
    let data: Observable<AgendaData>;
    if (!id) {
      data = this.newAgenda(express);
      this.editing = true;
    } else {
      data = this.loadAgenda(id);
    }
    data.subscribe((val: AgendaData) => {
      this.origAgenda = angular.copy(val.agenda);
      this.agenda = angular.copy(val.agenda);
      this.express = val.express;
      this.updateBreadcrumbs(this.express);
      this.loadingStatus = LoadingStatus.SUCCESS;
      this.success = true;
    }, () => {
      this.loadingStatus = LoadingStatus.ERROR;
    });
  }

  reload = function (): Observable<AgendaData> {
    return this.loadAgenda(this.agenda.agendaID);
  }.bind(this);

  newAgenda(express: boolean): Observable<AgendaData> {
    return of({ agenda: new Agenda(), express: express });
  }

  loadAgenda(id: string): Observable<AgendaData> {
    return this.agendaService.get(id).pipe(
      mergeMap(result => {
        const agenda: Agenda = result.document.agenda;

        return from(this.meetingTypeService.getMeetingType(agenda.meetingType)).pipe(
          mergeMap(meetingType => {
            if (!meetingType) {
              this.toastr.warning("У вас не прав для просмотра этой страницы.", "Ошибка");
              return throwError("У вас не прав для просмотра этой страницы.");
            } else {
              this.agenda = agenda;
              return of({ agenda: agenda, express: meetingType.Express });
            }
          })
        );
      })
    );
  }

  private createAgenda(questions: AgendaQuestion[], questionsToCopy?: string[]): Observable<string> {
    let agendaId;
    return this.agendaService.create(this.agenda).pipe(
      mergeMap((result) => {
        agendaId = result;
        return questions && questions.length ? this.agendaService.setAgendaQuestions(agendaId, questions) : of([])
      }),
      mergeMap(() => {
        return questionsToCopy && questionsToCopy.length ? this.agendaService.copyAgendaQuestions(agendaId, questionsToCopy) : of('');
      }),
      map(() => agendaId)
    );
  }

  private updateAgenda(questions: AgendaQuestion[], questionsToCopy?: string[]): Observable<Agenda> {
    let res;
    let diff = this.agendaService.diff(this.origAgenda, this.agenda);
    return this.agendaService.update(this.origAgenda.agendaID, diff).pipe(
      mergeMap((result) => {
        res = result;
        return this.agendaService.includedQuestions(this.agenda.agendaID).pipe(
          mergeMap((storedAgendaQuestions: AgendaQuestion[]) => {
            let agendaQuestions = angular.copy(questions);
            storedAgendaQuestions.filter(storedQuestion => !questions.find(question => question.questionId === storedQuestion.questionId))
              .forEach(exludedAgendaQuestion => {
                exludedAgendaQuestion.status = AgendaQuestionStatus.EXCLUDED;
                exludedAgendaQuestion.reasonChange = "Автоматическое исключение вопроса из повестки.";
                agendaQuestions.push(exludedAgendaQuestion);
              });
            return this.agendaService.setAgendaQuestions(this.agenda.agendaID, agendaQuestions);
          })
        );
      }),
      mergeMap(() => {
        return questionsToCopy && questionsToCopy.length ? this.agendaService.copyAgendaQuestions(res.agendaID, questionsToCopy) : of('');
      }),
      map(() => res)
    );
  }


  saveAgenda(questions: AgendaQuestion[], questionsToCopy?: string[], stayInEditMode?: boolean) {
    if (this.agenda.agendaID) {
      return this.updateAgenda(questions, questionsToCopy).subscribe((result) => {
        this.agenda = angular.copy(result);
        this.origAgenda = angular.copy(result);
        if (!stayInEditMode) {
          this.editing = false;
        }
      });
    } else {
      return this.createAgenda(questions, questionsToCopy).subscribe((id: string) => {
        this.$state.go('app.meeting.agenda', { id: id, editing: !!stayInEditMode });
      });
    }
  }

  publishAgenda(id: string) {
    this.agendaService.publish(id).subscribe(() => {
      this.$state.go('app.meeting.showcase', { tab: 'agendas' });
    }, () => {
    });
  }

  edit(edit: boolean) {
    this.editing = edit;
  }

  cancelEdit() {
    this.agenda = angular.copy(this.origAgenda);
    this.edit(false);
  }

  updateBreadcrumbs(express: boolean) {
    let breadcrumbsChain: BreadcrumbItem[] = [];
    if (express) {
      breadcrumbsChain.push({
        title: this.agenda.receivedFromMKA && this.agenda.receivedFromMKA.FromMKA ? 'Административные совещания МКА' : 'Административные совещания',
        url: this.agenda.receivedFromMKA && this.agenda.receivedFromMKA.FromMKA ? this.$state.href('app.meeting.showcase-express-mka', {}) : this.$state.href('app.meeting.showcase-express', {})
      });
    } else {
      breadcrumbsChain.push({
        title: this.agenda.receivedFromMKA && this.agenda.receivedFromMKA.FromMKA ? 'Регламентные совещания МКА' : 'Регламентные совещания',
        url: this.agenda.receivedFromMKA && this.agenda.receivedFromMKA.FromMKA ? this.$state.href('app.meeting.showcase-mka', {}) : this.$state.href('app.meeting.showcase', {})
      });
    }
    breadcrumbsChain.push({
      title: 'Повестка',
      url: null
    });
    this.breadcrumbsService.setBreadcrumbsChain(breadcrumbsChain);
  }

}
