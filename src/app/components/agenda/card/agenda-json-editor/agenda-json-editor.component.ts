import { Component, OnInit, Input, OnDestroy, ViewChild } from '@angular/core';
import * as angular from "angular";
import {compare} from "fast-json-patch";
import {ToastrService} from "ngx-toastr";
import {StateService} from '@uirouter/core';
import {BlockUI, NgBlockUI} from "ng-block-ui";
import {catchError, tap} from "rxjs/internal/operators";
import {throwError} from "rxjs/index";
import * as JSONEditor from 'jsoneditor';
import { AgendaService } from '../../../../services/agenda.service';


@Component({
  selector: 'mggt-agenda-json-editor',
  templateUrl: './agenda-json-editor.component.html',
  styleUrls: ['./agenda-json-editor.component.scss']
})
export class AgendaJsonEditorComponent implements OnInit, OnDestroy {

  @Input() id: string;
  container: any;
  options: any;
  editor: any;
  document;
  documentEdited;
  loadingStatus: string = "LOADING";
  success: boolean = false;
  @BlockUI('editDocument') blockUI: NgBlockUI;

  constructor( private agendaService: AgendaService, private $state: StateService, private toastr: ToastrService ) { }

  ngOnInit() {
    this.agendaService.get(this.id).pipe(
      tap(response => {
        // alert('11111111111');
        this.document = response.document;
        this.documentEdited = angular.copy(this.document);
        this.loadingStatus = "SUCCESS";
        this.success = true;
        setTimeout(() => {
          this.container = document.getElementById("jsoneditor");
          this.options = {};
          this.editor = new JSONEditor(this.container, this.options);
          this.editor.set(this.documentEdited);
        }, 100);
      }), catchError(error => {
        console.log(error);
        this.loadingStatus = "ERROR";
        return throwError(error);
      })
    ).subscribe();

  }

  ngOnDestroy(): void {
    this.blockUI.reset();
  }

  save() {
    this.blockUI.start();
    this.documentEdited = this.editor.get();
    let diff = compare({"document": this.document}, {"document": this.documentEdited});
    if (diff.length > 0) {
      this.agendaService.update(this.id, <any>JSON.stringify(diff)).pipe(
        tap(response => {
          this.toastr.success("Документ успешно сохранен!");
          this.blockUI.stop();
          this.$state.reload();
        }), catchError(error => {
          this.toastr.error("Ошибка при сохранении документа!");
          this.blockUI.stop();
          return throwError(error);
        })
      ).subscribe();
    } else {
      this.toastr.warning("В документе изменений нет!");
      this.blockUI.stop();
    }
  }
}
