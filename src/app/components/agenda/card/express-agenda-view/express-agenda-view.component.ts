import { ToastrService } from "ngx-toastr";
import {
  ActivityResourceService, CdpBpmHistoryResourceService,
  ICdpBpmProcessManagerConfig
} from '@reinform-cdp/bpm-components';
import { BsModalService } from 'ngx-bootstrap';
import { AlertService, LoadingStatus } from '@reinform-cdp/widgets';
import { PlatformConfig } from '@reinform-cdp/core';
import * as angular from 'angular';
import * as fs from 'file-saver';
import * as JSONEditor from 'jsoneditor';
import { CdpReporterPreviewService } from '@reinform-cdp/reporter-resource';
import { StateService, Transition } from '@uirouter/core';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Question, QuestionDocumentWrapper } from '../../../../models/Question';
import { Agenda, AgendaQuestion, AgendaQuestionStatus, AgendaStatus, AgendaUser } from '../../../../models/Agenda';
import { MeetingType } from '../../../../models/nsi/MeetingType';
import {
  Protocol, ProtocolApproval, ProtocolApprovalCycleAgreed, ProtocolApprovalCycleAgreedBy, ProtocolDocumentWrapper,
  ProtocolStatus,
  DsType
} from '../../../../models/Protocol';
import { SearchResultDocument } from '@reinform-cdp/search-resource';
import { AgendaService } from '../../../../services/agenda.service';
import { QuestionService } from '../../../../services/question.service';
import { ProtocolService } from '../../../../services/protocol.service';
import { MeetingTypeService } from '../../../../services/meeting-type.service';
import { NsiResourceService } from '@reinform-cdp/nsi-resource';
import { SessionStorage } from '@reinform-cdp/security';
import { InstructionsService } from '../../../../services/instructions.service';
import * as _ from 'lodash';
import { FileType } from '../../../../models/FileType';
import { Observable } from 'rxjs/Rx';
import { forkJoin, from, of, throwError } from 'rxjs/index';
import { catchError, map, mergeMap, tap } from 'rxjs/internal/operators';
import { ExpressService } from '../../../../services/express.service';
import { AgendaData } from '../agenda.component';
import { RescheduleAgendaModalComponent } from '../reschedule-agenda/reschedule-agenda.modal';
import { MeetingPlace } from '../../../../models/nsi/MeetingPlace';
import { AttachFileModalComponent } from '../../../common/attach-file-modal/attach-file-modal.component';
import { FileResourceService } from '@reinform-cdp/file-resource';
import { ChooseSigningTypeModalComponent } from '../choose-signing-type-modal/choose-signing-type-modal.component';
import {HelperService} from "../../../../services/helper.service";

@Component({
  selector: 'mggt-express-agenda-view',
  templateUrl: './express-agenda-view.component.html',
  styleUrls: ['./express-agenda-view.component.scss']
})
export class ExpressAgendaViewComponent implements OnInit {

  @Input() agenda: Agenda;
  @Input() public reload: () => Observable<AgendaData>;
  @Output() public edit: EventEmitter<any> = new EventEmitter<any>();

  availableToAddInstruction: boolean;
  sendingMailNotifications: boolean;
  creatingProtocolPrintForm: boolean;
  creatingProtocol: boolean;
  markingMeetingPassed: boolean;
  savingProtocol: boolean;
  creatingProtocolWithInstructionsPrintForm: boolean;

  loadingStatus: LoadingStatus;
  success = false;

  insideOrder: SearchResultDocument[];
  dicts: any = {
    statuses: [],
    agendaStatuses: [],
    themes: [],
    priorities: [],
    difficulties: [],
    executors: [],
    _themes: [],
    statusAc: []
  };

  origProtocol: Protocol;
  protocol: Protocol;

  editingProtocol = false;

  meetingType: MeetingType;
  included: AgendaQuestion[];
  questions: { [key: string]: Question };
  includedCount: number;

  meetingPlaces: MeetingPlace[];

  instructions: { question: Question, instructions: SearchResultDocument[] }[] = [];
  statuses: any[];
  agendaStatuses: any[];

  activeTab = 0;

  processManagerConfig: ICdpBpmProcessManagerConfig;

  sendingProtocolToApproval = false;

  showFileProtocolDraft = true;
  openFileProtocolDraft = true;

  showMeetingHeld = false;
  showCreateProtocol = false;
  showMailNotification = false;
  showCopyExpressAgenda = false;
  showEditAgenda = false;
  showRescheduleAgenda = false;
  showCancelAgenda = false;
  showDeleteAgenda = false;
  showCreateInstruction = false;
  showProtocolActions = false;
  showAddScanProtocol = false;

  showApprovalList = false;
  showProcesses = false;
  counterPrimaryID: number = 0;

  constructor(private $state: StateService,
    private agendaService: AgendaService,
    private questionService: QuestionService,
    private expressService: ExpressService,
    private protocolService: ProtocolService,
    private meetingTypesService: MeetingTypeService,
    private nsiRestService: NsiResourceService,
    private session: SessionStorage,
    private transition: Transition,
    private instructionsService: InstructionsService,
    private platformConfig: PlatformConfig,
    private alertService: AlertService,
    private toastr: ToastrService,
    private modalService: BsModalService,
    private wordReporterPreviewService: CdpReporterPreviewService,
    private fileRestService: FileResourceService,
    private activityRestService: ActivityResourceService,
    private historyResourceService: CdpBpmHistoryResourceService,
    private helper: HelperService) {
  }

  ngOnInit() {
    this.calculateActionPermissions();
    this.loadingStatus = LoadingStatus.LOADING;
    const id = this.transition.params()['id'];

    this.loadDictionaries().pipe(
      mergeMap(() => {
        return this.loadIncludedQuestions();
      }),
      mergeMap(() => {
        return this.loadProtocol();
      }),
      mergeMap(() => {

        this.processManagerConfig = {
          sysName: this.platformConfig.systemCode.toLowerCase(),
          linkVarValue: this.protocol ? this.protocol.protocolID : this.agenda.agendaID
        };
        return this.loadInstructions();
      }), mergeMap(response => {
        return this.historyResourceService.listOfHistoricProcessInstances({
          variables: [{
            name: 'EntityIdVar',
            value: this.protocol ? this.protocol.protocolID : this.agenda.agendaID,
            operation: 'equals',
            type: 'string'
          }],
          size: 100000,
          includeProcessVariables: true
        });
      }), mergeMap(response => {
        this.showProcesses = ((!this.agenda.receivedFromMKA || (this.agenda.receivedFromMKA && !this.agenda.receivedFromMKA.FromMKA)) && response.data.length > 0)
          || (this.agenda.receivedFromMKA && this.agenda.receivedFromMKA.FromMKA);

        this.showApprovalList = response.data.length > 0;
        return this.nsiRestService.getDictsFromCache(['InstructionPriority', 'InstructionStatus', 'mggt_meeting_statusAc']);
      }), mergeMap(response => {
        this.dicts.statuses = response.InstructionStatus;
        this.dicts.priorities = response.InstructionPriority;
        this.dicts.statusAc = response['mggt_meeting_statusAc'];
        return this.agendaService.getInsideAgenda(id);
      }), tap(response => {
        this.insideOrder = response;
      })
    ).subscribe((result: any) => {
      const login = this.session.login();
      this.showFileProtocolDraft = _.get(this.agenda, 'status.code') !== 'approved';
      this.openFileProtocolDraft = this.agenda.initiator.accountName === login ||
        this.agenda.authorCreate.accountName === login;

      this.loadingStatus = LoadingStatus.SUCCESS;
      this.success = true;

      this.controlPrimaryID();

    }, error => {
      console.error(error);
      this.loadingStatus = LoadingStatus.ERROR;
    });

  }


  controlPrimaryID(): void {
    for (var key in this.questions) {
      if (this.questions[key]['questionPrimaryID'] === undefined) {
        this.counterPrimaryID++;
      }
    }
    if (this.counterPrimaryID > 0) {
      localStorage.setItem('toSave', 'on')
      this.editAgenda();
    }
  }

  calculateActionPermissions() {
    const status = _.get(this, 'agenda.status.code', '');

    this.showMeetingHeld = false;
    this.showCreateProtocol = false;
    this.showMailNotification = false;
    this.showCopyExpressAgenda = false;
    this.showEditAgenda = false;
    this.showRescheduleAgenda = false;
    this.showCancelAgenda = false;
    this.showDeleteAgenda = false;
    this.showCreateInstruction = false;
    this.showProtocolActions = false;
    this.showAddScanProtocol = false;

    switch (status) {
      case 'assign':
        this.showMeetingHeld = true;
        this.showMailNotification = true;
        this.showCopyExpressAgenda = true;
        this.showEditAgenda = true;
        this.showRescheduleAgenda = true;
        this.showCancelAgenda = true;
        this.showDeleteAgenda = true;
        this.showCreateInstruction = true;
        break;
      case 'post':
        this.showCreateProtocol = true;
        this.showCopyExpressAgenda = true;
        this.showEditAgenda = true;
        this.showProtocolActions = true;
        this.showCreateInstruction = true;
        this.showAddScanProtocol = true;
        break;
      case 'refused':
        this.showCopyExpressAgenda = true;
        break;
      case 'approval':
        this.showCopyExpressAgenda = true;
        break;
      case 'approved':
        this.showCopyExpressAgenda = true;
        break;
      case '':
        this.showMeetingHeld = true;
        this.showCreateProtocol = true;
        this.showMailNotification = true;
        this.showCopyExpressAgenda = true;
        this.showEditAgenda = true;
        this.showRescheduleAgenda = true;
        this.showCancelAgenda = true;
        this.showDeleteAgenda = true;
        this.showCreateInstruction = true;
        this.showProtocolActions = true;
        break;
    }
  }

  loadDictionaries(): Observable<any> {
    return forkJoin([
      this.meetingTypesService.getMeetingType(this.agenda.meetingType),
      this.nsiRestService.get('InstructionExecutors'),
      this.nsiRestService.get('InstructionStatus'),
      this.nsiRestService.get('mggt_meeting_statusAc'),
      this.nsiRestService.get('mggt_meeting_MeetingPlace'),
    ]).pipe(
      map((result: any) => {
        this.meetingType = result[0];
        this.availableToAddInstruction = _.find(result[1], (el: any) => {
          return el.creator == this.session.login() || (el.assistant && el.assistant.indexOf(this.session.login()) >= 0);
        });
        this.statuses = result[2];
        this.agendaStatuses = result[3].map(assignStatusDetails => {
          const { code, color, name } = assignStatusDetails;
          return {
            code,
            color,
            name
          };
        });
        this.meetingPlaces = result[4];
        return true;
      })
    );
  }

  loadIncludedQuestions(): Observable<any> {
    return forkJoin([
      this.agendaService.includedQuestions(this.agenda.agendaID),
      this.agendaService.includedQuestionsCount(this.agenda.agendaID)
    ]).pipe(
      mergeMap((result: any) => {
        this.included = result[0].filter(question => question.status !== AgendaQuestionStatus.EXCLUDED);
        this.includedCount = result[1];
        return (this.included && this.included.length ? forkJoin(_.map(this.included, (question: AgendaQuestion) => {
          return this.questionService.get(question.questionId);
        })) : of([])).pipe(
          tap((result) => {
            this.questions = {};
            _.each(result, (wrapper: QuestionDocumentWrapper) => {
              const question: Question = wrapper.document.question;
              this.questions[question.questionID] = question;
            });
          })
        );
      })
    );
  }

  loadProtocol(): Observable<any> {
    return this.protocolService.getByAgendaId(this.agenda.agendaID).pipe(
      catchError(error => {
        return of({});
      }),
      tap((wrapper: ProtocolDocumentWrapper) => {
        if (wrapper && wrapper.document) {
          this.setProtocol(wrapper.document.protocol);
        }
      })
    );
  }

  setProtocol(protocol: Protocol) {
    this.protocol = angular.copy(protocol);
    this.origProtocol = angular.copy(protocol);
  }

  loadInstructions(): Observable<any> {
    const primaryQuestionsIds = _.values(this.questions).filter(q => q.questionPrimaryID).map(q => q.questionPrimaryID);
    if (!primaryQuestionsIds || !primaryQuestionsIds.length) {
      return of({ docs: [] });
    }

    const questionMap: { [key: string]: Question } = {};
    _.values(this.questions).forEach(q => questionMap[q.questionPrimaryID] = q);

    const date = new Date(this.agenda.meetingDate.getTime());
    date.setDate(date.getDate() + 1);
    return from(this.instructionsService.getAgendaInstructions(this.agenda, this.meetingType, primaryQuestionsIds)).pipe(
      tap(result => {
        this.instructions = _.chain(result.docs).groupBy((doc: any) => doc.questionPrimaryID).toPairs().map((el: any) => {
          return {
            question: questionMap[el[0]],
            instructions: el[1]
          };
        }).value();
      })
    );
  }

  editAgenda() {
    this.edit.emit(true);
    localStorage.setItem('toEdit', 'on');
  }

  rescheduleAgenda() {
    const options = {
      initialState: {
        agenda: angular.copy(this.agenda),
        meetingPlaces: this.meetingPlaces,
        agendaStatuses: this.agendaStatuses,
        onReschedule: () => {
          this.reload().subscribe((data: any) => {
            this.agenda = data.agenda;
          });
        },
      },
      'class': 'modal-md'
    };

    this.modalService.show(RescheduleAgendaModalComponent, options);
  }

  cancelAgenda() {
    const status = this.agendaStatuses.filter(status => status.code === 'refused')[0];
    this.expressService.mailCancelNotification(this.agenda, status).pipe(
      mergeMap(() => {
        return this.reload();
      }
      ),
      mergeMap((data: any) => {
        this.agenda = data.agenda;
        this.calculateActionPermissions();
        return this.loadIncludedQuestions();
      })).subscribe();
  }

  deleteAgenda() {
    this.getAllInstructions().pipe(
      mergeMap(instructions => {
        if (instructions.length > 0) {
          return from(this.alertService.message({
            message: 'По данному совещанию созданы поручения. Необходимо удалить поручения перед удалением карточки совещания',
            type: 'warning'
          })).pipe(
            mergeMap(_ => throwError('cancelled'))
          );
        } else {
          return this.alertService.confirm({
            message: 'Вы действительно хотите удалить данное совещание?',
            okButtonText: 'OK'
          });
        }
      }),
      mergeMap(async () => {
        const recipientLogins = this.agenda.attended
          .map(attendee => attendee.accountName);
        const recipients = await Promise.all(recipientLogins.map(
          login => this.nsiRestService.ldapUser(login).then((user: any) => ({
            login: login,
            email: user.mail
          }))
        )
        );
        const recipientsWithoutEmail = recipients.filter(recipient => !recipient.email);
        if (recipientsWithoutEmail.length !== 0) {
          const noEmailsList = recipientsWithoutEmail.map(recipient => recipient.login).join(', ');
          return this.alertService.message({
            message: `Не задан email у пользователей: ${noEmailsList}. Сообщение не будет разослано участникам. Просьба обратиться в службу тех. поддержки`,
            type: 'warning'
          });
        }
        return of('');
      }),
      mergeMap(() => {
        if (this.agenda.mailingInfo && this.agenda.mailingInfo.length) {
          return this.expressService.mailDeleteNotification(this.agenda.agendaID);
        } else {
          return of('');
        }
      }),
      mergeMap(() => {
        return this.expressService.deleteAgenda(this.agenda.agendaID);
      })
    ).subscribe(() => {
      this.$state.go('app.meeting.showcase-express');
    }, error => {
      console.log(error);
      this.helper.error(error);
    });
  }

  private getAllInstructions(): Observable<any[]> {
    const primaryQuestionsIds = _.compact(_.values(this.questions).map(q => q.questionPrimaryID));
    if (primaryQuestionsIds.length) {
      return this.instructionsService.getAgendaInstructions(this.agenda, this.meetingType, primaryQuestionsIds).pipe(
        map(_ => _.docs || [])
      );
    } else {
      return of([]);
    }
  }

  mailNotification() {
    const alreadySent = this.agenda.mailingInfo && this.agenda.mailingInfo.length;
    of(alreadySent).pipe(
      mergeMap(showConfirmation => {
        if (showConfirmation) {
          return this.alertService.confirm({
            message: 'Рассылка совещания была произведена ранее. Повторить отправку уведомлений?',
            okButtonText: 'Ок',
            type: 'warning'
          });
        } else {
          return of('');
        }
      }),
      mergeMap(async () => {
        const recipientLogins = this.agenda.attended
          .map(attendee => attendee.accountName);
        const recipients = await Promise.all(recipientLogins.map(
          login => this.nsiRestService.ldapUser(login).then((user: any) => ({
            login: login,
            email: user.mail
          }))
        )
        );
        const recipientsWithoutEmail = recipients.filter(recipient => !recipient.email);
        if (recipientsWithoutEmail.length !== 0) {
          const noEmailsList = recipientsWithoutEmail.map(recipient => recipient.login).join(', ');
          return this.alertService.message({
            message: `Не задан email у пользователей: ${noEmailsList}. Сообщение не будет разослано участникам. Просьба обратиться в службу тех. поддержки`,
            type: 'warning'
          });
        }
        return of('');
      }),
      mergeMap(() => {
        return this.sendMailNotification();
      }),
      mergeMap(() => {
        const message = alreadySent ? 'Сообщение участникам отправлено повторно.' :
          'Сообщение участникам отправлено.';
        return this.alertService.message({ message: message, type: 'success' });
      })
    ).subscribe();
  }

  private sendMailNotification(): Observable<any> {
    this.sendingMailNotifications = true;
    return this.expressService.mailNotification(this.agenda.agendaID).pipe(
      tap(agenda => {
        this.agenda = agenda;
        this.calculateActionPermissions();
        this.sendingMailNotifications = false;
      }),
      catchError(error => {
        this.sendingMailNotifications = false;
        return throwError(error);
      })
    );
  }

  meetingHeld(): void {
    this.markingMeetingPassed = true;
    const status = this.agendaStatuses.filter(status => status.code === 'post')[0];
    this.expressService.markPassed(this.agenda, status).pipe(
      mergeMap(() => {
        return this.reload();
      }),
      mergeMap((data: any) => {
        this.agenda = data.agenda;
        this.calculateActionPermissions();
        return this.loadIncludedQuestions();
      })
    ).subscribe(() => {
      this.markingMeetingPassed = false;
    }, () => {
      this.markingMeetingPassed = false;
    });
  }

  createProtocol(): any {
    this.creatingProtocol = true;
    this.expressService.generateProtocol(this.agenda, this.meetingType).pipe(
      mergeMap(() => this.reload()),
      mergeMap((data: any) => {
        this.agenda = data.agenda;
        this.calculateActionPermissions();
        return this.loadProtocol();
      }),
      tap(() => setInterval(this.activeTab = 2))
    ).subscribe(() => {
      this.creatingProtocol = false;
    }, () => {
      this.creatingProtocol = false;
    });
  }

  createProtocolPrintForm(): void {
    this.creatingProtocolPrintForm = true;
    // ChooseSigningTypeModalComponent

    this.protocolService.createReport(this.protocol.protocolID).subscribe((file: FileType) => {
      this.protocol.fileProtocol = this.protocol.fileProtocol || [];
      this.protocol.fileProtocol.push(file);
      this.creatingProtocolPrintForm = false;
    }, () => {
      this.creatingProtocolPrintForm = false;
    });
  }

  sendProtocolToApproval() {
    this.sendingProtocolToApproval = true;
    this.modalService.show(AttachFileModalComponent, {
      class: 'modal-lg',
      ignoreBackdropClick: true,
      initialState: {
        folderId: this.protocol.folderId,
        fileTitle: 'Проект протокола',
        okBtnText: 'Передать',
        title: 'Передать на утверждение',
        extPattern: 'docx',
        saveCallback: (file: FileType) => {
          this.protocol.fileProtocolDraft = file;
          this.protocol.approval = this.protocol.approval || new ProtocolApproval();
          this.protocol.approval.approvalCycle.approvalCycleNum = '1';
          this.protocol.approval.approvalCycle.approvalCycleDate = new Date();
          this.protocol.approval.approvalCycle.agreed.push(this.createInitiatorAgreed(this.agenda.initiator, 'approval', 'Утверждение'));
          this.protocol.status = ProtocolStatus.fromAgendaStatus(this.agenda.status);
          this.protocolService.update(this.protocol.protocolID, this.protocolService.diff(this.origProtocol, this.protocol)).pipe(
            mergeMap(() => {
              return this.activityRestService.getProcessDefinitions({
                key: 'sdoprotocolapprove_approveProtocolAs',
                latest: true
              });
            }),
            mergeMap((response) => {
              const processId = response.data[0].id;
              return this.activityRestService.initProcess({
                processDefinitionId: processId,
                variables: [{ 'name': 'EntityIdVar', 'value': this.protocol.protocolID }]
              });
            }),
            mergeMap(() => {
              return this.reload();
            }),
            mergeMap((data: any) => {
              this.agenda = data.agenda;
              this.showProcesses = true;
              this.showApprovalList = true;
              this.processManagerConfig = {
                sysName: this.platformConfig.systemCode.toLowerCase(),
                linkVarValue: this.protocol ? this.protocol.protocolID : this.agenda.agendaID
              };
              this.calculateActionPermissions();
              return this.loadIncludedQuestions();
            })
          ).subscribe(() => {
            this.sendingProtocolToApproval = false;
          }, error => {
            this.alertService.message({ type: 'danger', message: 'Произошла ошибка при отправке на утверждение.' });
            this.sendingProtocolToApproval = false;
            console.error(error);
          });
        },
        cancelCallback: () => {
          this.sendingProtocolToApproval = false;
        }
      }
    });
  }

  private createInitiatorAgreed(initiator: AgendaUser, approvalTypeCode: string, approvalType: string): ProtocolApprovalCycleAgreed {
    const agreed = new ProtocolApprovalCycleAgreed();
    agreed.approvalTypeCode = approvalTypeCode;
    agreed.approvalType = approvalType;
    agreed.agreedBy = new ProtocolApprovalCycleAgreedBy();
    agreed.agreedBy.accountName = initiator.accountName;
    agreed.agreedBy.fioFull = initiator.fioFull;
    agreed.agreedBy.iofShort = initiator.iofShort;
    agreed.agreedBy.post = initiator.post;
    return agreed;
  }

  editProtocol(): void {
    this.editingProtocol = true;
  }

  saveProtocol(): void {
    this.savingProtocol = true;
    this.updateProtocol().subscribe(() => {
      this.savingProtocol = false;
      this.editingProtocol = false;
    }, () => {
      this.savingProtocol = false;
    });
  }

  createInstruction(questionId: string): void {
    const question = this.questions[questionId];
    const url = window.location.protocol + '//' + window.location.host +
      '/sdo/instruction/#/app/instruction/instruction/createFromMeeting/' + questionId + '/' + question.questionPrimaryID +
      '?expressMeetingID=' + this.agenda.agendaID;
    window.open(url, '_blank');
  }

  createProtocolWithInstructionsPrintForm(): any {
    this.creatingProtocolWithInstructionsPrintForm = true;

    this.modalService.show(ChooseSigningTypeModalComponent, {
      class: 'modal-lg',
      initialState: {
        saveCallback: (result: boolean) => {
          this.protocolService.previewPrintForm(this.protocol, this.meetingType, this.instructions.map(instr => ({
            questionId: instr.question.questionID,
            instructions: instr.instructions
          })), this.agenda, result)
            .subscribe(
              (blob) => {
                try {
                  fs.saveAs(blob, `Протокол с поручениями.docx`);
                } catch (e) {
                  console.log('error', e);
                } finally {
                  this.creatingProtocolWithInstructionsPrintForm = false;
                }
              },
              () => {
                this.creatingProtocolWithInstructionsPrintForm = false;
              });

        },
        cancelCallback: () => {
          this.creatingProtocolWithInstructionsPrintForm = false;
        }
      }
    });
  }

  isActive(index: number): boolean {
    return index === this.activeTab;
  }

  activate(index: number): boolean {
    if (this.activeTab === index) {
      return false;
    } else {
      this.activeTab = index;
      return true;
    }
  }

  copyExpressAgenda(): void {
    const questions = _.map(this.questions, question => {
      return {
        meetingType: question.meetingType,
        questionConsider: {
          itemNumber: question.questionConsider.itemNumber,
          question: question.questionConsider.question,
          responsiblePrepare: question.questionConsider.responsiblePrepare
        }
      };
    });

    const data = {
      id: null,
      data: {
        meetingFullName: this.agenda.meetingFullName,
        meetingShortName: this.agenda.meetingShortName,
        meetingPlaceCode: this.agenda.meetingPlaceCode,
        meetingTheme: this.agenda.meetingTheme,
        meetingType: this.agenda.meetingType,
        meetingCodeTheme: this.agenda.meetingCodeTheme,
        meetingTime: this.agenda.meetingTime,
        meetingTimeEnd: this.agenda.meetingTimeEnd,
        meetingPlace: this.agenda.meetingPlace,
        attended: this.agenda.attended,
        initiator: this.agenda.initiator,
        questions: questions
      },
      origId: this.agenda.agendaID,
      express: true
    };

    this.$state.go('app.meeting.agenda', data);
  }

  isAvailableToCopy(): boolean {
    return this.session.hasPermission('MEETING_AGENDA_AS_COPY');
  }
  isAvailableToHistory(): boolean {
    return this.session.hasPermission('SDO_MEETING_AGENDA_HISTORY');
  }
  isAvailableToJson(): boolean {
    return this.session.hasPermission('SDO_MEETING_AGENDA_JSON');
  }

  onDeleteScan(file: FileType): void {
    this.showAttachScanModal();
  }

  private updateProtocol(): Observable<any> {
    return this.protocolService.update(this.protocol.protocolID, this.protocolService.diff(this.origProtocol, this.protocol)).pipe(
      tap((protocol) => {
        this.setProtocol(protocol);
      })
    );
  }

  get isDeletableProtocol(): boolean {
    return _.some(this.session.groups(), i => i === 'MGGT_MEETING_ADD_SCAN');
  }

  get isAttachableProtocol(): boolean {
    return _.some(this.session.groups(), i => i === 'MGGT_MEETING_ADD_SCAN');
  }

  private deleteFile(file: FileType): Observable<any> {
    return from(this.fileRestService.deleteFile(file.idFile, this.platformConfig.systemCode)).pipe(
      catchError(error => {
        console.error(error);
        this.toastr.error(`Произошла ошибка при удалении файла ${file.nameFile}.`);
        return throwError(error);
      })
    )
  }

  private getFolderContent(): Observable<any> {
    return from(this.fileRestService.getFolderContent(this.origProtocol.folderId, true, "SDO"));
  }

  private deleteFilesIfExist(files: FileType[]): Observable<any> {
    if (files && files.length) {
      return this.getFolderContent().pipe(map((_files) => {
        const filesForRemove = files.filter(f => _files.some(_f => _f.objectGuid === f.idFile));
        if (filesForRemove.length) {
          return forkJoin(filesForRemove.map(f => this.deleteFile(f)));
        }
        return of({});
      }));
    } else {
      return of({});
    }
  }


  showAttachScanModal(): void {
    const status = _.find(this.dicts.statusAc, i => i.code === 'approved');
    this.modalService.show(AttachFileModalComponent, {
      class: 'modal-lg',
      keyboard: false,
      backdrop: 'static',
      initialState: {
        folderId: this.protocol.folderId,
        saveCallback: (file: FileType) => {
          this.deleteFilesIfExist(this.origProtocol.fileProtocol).pipe(
            mergeMap(() => {
              const oldAgenda = angular.copy(this.agenda), agenda = angular.copy(this.agenda);
              agenda.status = <AgendaStatus>status;
              return this.agendaService.update(this.agenda.agendaID, this.agendaService.diff(oldAgenda, agenda));
            }),
            mergeMap(() => {
              this.protocol.fileProtocol = [file];
              this.protocol.status = <ProtocolStatus>status;
              return this.updateProtocol();
            }),
            mergeMap(() => {
              return this.reload();
            })
          ).subscribe((data) => {
            this.agenda = data.agenda;
            this.calculateActionPermissions();
          }, error => {
            console.log(error);
          })
        },
        cancelCallback: (file: FileType) => {
          this.protocol.fileProtocol = angular.copy(this.origProtocol.fileProtocol);
          if (file) {
            this.deleteFilesIfExist([file]).subscribe();
          }
        }
      }
    });
  }

}
