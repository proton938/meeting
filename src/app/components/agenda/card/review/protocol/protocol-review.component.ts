import * as angular from "angular";
import {Component, Input, OnInit} from '@angular/core';
import {ProtocolUserType} from "../../../../../models/Protocol";

@Component({
  selector: 'mggt-protocol-review',
  templateUrl: './protocol-review.component.html'
})
export class ProtocolReviewComponent implements OnInit {

  @Input() document;

  reviewBy:ProtocolUserType[] = [];
  factReviewBy:ProtocolUserType[] = [];

  constructor() {
  }

  ngOnInit() {
    this.reviewBy = angular.copy(this.document.review).filter(f =>{
      return f.reviewBy && f.reviewBy.fioFull;
    });
    this.factReviewBy = angular.copy(this.document.review).filter(f => {
      return f.factReviewBy && f.factReviewBy.fioFull;
    })
  }


}
