import {Component, Input, OnInit} from '@angular/core';
import {SearchResultDocument} from "@reinform-cdp/search-resource";

@Component({
  selector: 'mggt-subtasks-doc',
  templateUrl: './subtasks-doc.component.html',
  styleUrls: ['./subtasks-doc.component.scss']
})
export class SubtasksDocComponent implements OnInit {

  @Input() subTasks: SearchResultDocument[];
  @Input() dicts: any;

  statuses: { [key: string]: string } = {};
  priorities: { [key: string]: string } = {};

  constructor() {
  }

  ngOnInit() {
    this.dicts.statuses.forEach(st => this.statuses[st.name] = st.color);
    this.dicts.priorities.forEach(pr => this.priorities[pr.name] = pr.color);
  }

}
