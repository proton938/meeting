import {Component, Input} from '@angular/core';
import {BsModalRef} from 'ngx-bootstrap';

@Component({
  selector: 'choose-signing-type-modal',
  templateUrl: './choose-signing-type-modal.component.html'
})
export class ChooseSigningTypeModalComponent {
  @Input() saveCallback: (result: boolean) => void;
  @Input() cancelCallback: () => void;

  constructor(private bsModalRef: BsModalRef) {}


  save(result) {
    if (this.saveCallback) this.saveCallback(result);
    this.bsModalRef.hide();
  }

  cancel() {
    if (this.cancelCallback) this.cancelCallback();
    this.bsModalRef.hide();
  }
}
