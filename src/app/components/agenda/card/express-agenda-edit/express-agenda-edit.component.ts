import { BsModalService } from 'ngx-bootstrap';
import { Observable, Subject } from 'rxjs';
import * as angular from 'angular';
import * as _ from 'lodash';
import { ToastrService } from 'ngx-toastr';
import { StateService, Transition } from '@uirouter/core';
import { SessionStorage } from '@reinform-cdp/security';
import { Component, Input, OnInit, ViewEncapsulation, EventEmitter, Output } from '@angular/core';
import { AlertService, LoadingStatus } from '@reinform-cdp/widgets';
import {
  Agenda,
  AgendaAttended,
  AgendaMeetingHistoryUser,
  AgendaQuestion,
  AgendaQuestionStatus, AgendaStatus,
} from '../../../../models/Agenda';
import { Question, QuestionConsider, QuestionResponsible } from '../../../../models/Question';
import { MeetingType } from '../../../../models/nsi/MeetingType';
import { MeetingPlace } from '../../../../models/nsi/MeetingPlace';
import { FileType } from '../../../../models/FileType';
import { EmailGroups } from '../../../../models/nsi/EmailGroups';
import { MeetingTypeService } from '../../../../services/meeting-type.service';
import { AgendaService } from '../../../../services/agenda.service';
import { QuestionService } from '../../../../services/question.service';
import {
  catchError, debounceTime, distinctUntilChanged, filter, map, mergeMap, switchMap,
  tap
} from 'rxjs/internal/operators';
import { concat, EMPTY, forkJoin, from, of, throwError, merge } from 'rxjs/index';
import { ISerachUsersParams, NsiResourceService, UserBean } from '@reinform-cdp/nsi-resource';
import { SearchResult, SearchResultDocument } from '@reinform-cdp/search-resource';
import { formatDate } from '@angular/common';
import { FileResourceService } from '@reinform-cdp/file-resource';
import { InstructionsService } from '../../../../services/instructions.service';
import { ExpressService } from '../../../../services/express.service';
import { RenameFileModalComponent } from '../../../common/rename-file/rename-file.modal';
import { MeetingTheme } from '../../../../models/nsi/MeetingTheme';
import { AddThemeModalComponent } from '../add-theme/add-theme.modal';
import { ProtocolService } from '../../../../services/protocol.service';
import { ProtocolDocumentWrapper, Protocol, ProtocolQuestion } from '../../../../models/Protocol';
import {SolrMediatorService} from '../../../../services/solr-mediator.service';

@Component({
  selector: 'mggt-express-agenda-edit',
  templateUrl: './express-agenda-edit.component.html',
  styleUrls: ['./express-agenda-edit.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ExpressAgendaEditComponent implements OnInit {

  loadingStatus: LoadingStatus;
  success = false;

  @Input() agenda: Agenda;
  origAgenda: Agenda;
  questions: Question[];
  files: FileType[] = [];
  fakeFiles: FileType[] = [];

  agendaStatuses: AgendaStatus[];
  meetingTypes: MeetingType[];
  meetingThemes: MeetingTheme[];
  meetingPlaces: MeetingPlace[];
  fileQueue: File[];
  fileQueueToDisplay: FileType[];
  protocol: ProtocolDocumentWrapper;
  copyDocument: ProtocolDocumentWrapper;

  emailGroups: EmailGroups[];
  emailGroupsList: EmailGroups[];

  initiators$: Observable<UserBean[]>;
  initiatorsLoading = false;
  initiatorsInput$ = new Subject<string>();

  currentUser: AgendaMeetingHistoryUser = new AgendaMeetingHistoryUser;

  attended$: Observable<UserBean[]>;
  attendedLoading = false;
  attendedInput$ = new Subject<string>();

  responsibles$ = [];
  responsiblesLoading = [];
  responsiblesInput$ = [];
  responsibles1 = [];

  saving: boolean;
  uploading: boolean;

  intersectionsWarnings: string[];

  minDate = new Date();
  origId: string;
  copyAgenda: any;
  editing = false;
  disabledDate = false;
  showAddQuestionButton = false;

  creating = false;

  @Output() public edit: EventEmitter<any> = new EventEmitter<any>();
  @Input() public reload: () => Observable<any>;

  constructor(public session: SessionStorage,
    private $state: StateService,
    private agendaService: AgendaService,
    private questionService: QuestionService,
    private nsiRestService: NsiResourceService,
    private meetingTypesService: MeetingTypeService,
    private solrMediator: SolrMediatorService,
    private fileResourceService: FileResourceService,
    private instructionsService: InstructionsService,

    private expressService: ExpressService,
    private alertService: AlertService,
    private modalService: BsModalService,
    private transition: Transition,
    public protocolService: ProtocolService,
    private toastr: ToastrService) {
  }

  ngOnInit() {

    this.currentUser.login = this.session.login();
    this.currentUser.fioFull = this.session.fullName();
    this.currentUser.post = this.session.post();
    this.loadingStatus = LoadingStatus.LOADING;
    this.origAgenda = angular.copy(this.agenda);
    this.origId = this.transition.params()['origId'];
    this.copyAgenda = this.transition.params()['data'];
    const status = _.get(this.agenda, 'status.code', '');

    this.creating = !this.agenda.agendaID;

    switch (status) {
      case 'assign':
      case 'post':
        this.showAddQuestionButton = true;
        break;
      case '':
        this.showAddQuestionButton = true;
        break;
      default:
        this.showAddQuestionButton = false;
        break;
    }

    if (this.agenda.agendaID) {
      this.editing = true;
      if (this.agenda.meetingDate.getTime() < this.minDate.setHours(0, 0, 0, 0)) {
        this.disabledDate = true;
        this.minDate = this.agenda.meetingDate;
      }
    }

    if (!this.agenda.agendaID) {
      const question = new Question();
      question.questionConsider = new QuestionConsider();
      question.questionConsider.itemNumber = '1';
      this.questions = [question];
      this.addResponsiblesInput(0);
    } else if (!this.meetingTypesService.checkMeetingType(this.agenda.meetingType)) {
      this.toastr.warning('У вас не прав для просмотра этой страницы.', 'Ошибка');
    }

    if (this.origId && !this.agenda.agendaID) {
      this.agenda = new Agenda();
      this.agenda.meetingFullName = this.copyAgenda.meetingFullName;
      this.agenda.meetingShortName = this.copyAgenda.meetingShortName;
      this.agenda.meetingPlaceCode = this.copyAgenda.meetingPlaceCode;
      this.agenda.meetingTheme = this.copyAgenda.meetingTheme;
      this.agenda.meetingType = this.copyAgenda.meetingType;
      this.agenda.meetingCodeTheme = this.copyAgenda.meetingCodeTheme;
      this.agenda.meetingTime = this.copyAgenda.meetingTime;
      this.agenda.meetingTimeEnd = this.copyAgenda.meetingTimeEnd;
      this.agenda.meetingPlace = this.copyAgenda.meetingPlace;
      this.agenda.attended = this.copyAgenda.attended;
      this.agenda.initiator = this.copyAgenda.initiator;
      this.questions = this.copyAgenda.questions;
    }

    this.agenda.filesAttach = this.agenda.filesAttach || [];
    this.fileQueueToDisplay = this.agenda.filesAttach;
    this.fileQueue = [];

    this.initiators$ = concat(
      of([]),
      this.initiatorsInput$.pipe(
        debounceTime(300),
        distinctUntilChanged(),
        filter(term => term && term.length > 2),
        tap(() => this.initiatorsLoading = true),
        switchMap((term: string) => this.searchInitiator(term)),
        catchError(() => of([])),
        tap(() => this.initiatorsLoading = false)
      )
    );
    this.attended$ = concat(
      of([]),
      this.attendedInput$.pipe(
        debounceTime(300),
        distinctUntilChanged(),
        filter(term => term && term.length > 2),
        tap(() => this.attendedLoading = true),
        switchMap((term: string) => this.searchAttendee(term)),
        catchError(() => of([])),
        tap(() => this.attendedLoading = false)
      )
    );

    return forkJoin([
      this.loadDictionaries(),
      this.loadIncludedQuestions(),
      this.loadProtocol(),
    ]).subscribe((result: any) => {
      this.updateEmailGroupsAfterAttended();
      this.loadingStatus = LoadingStatus.SUCCESS;
      this.success = true;
      if (localStorage.getItem('toSave') !== null) {
        this.save();
      }
    }, () => {
      this.loadingStatus = LoadingStatus.ERROR;
    });
  }

  isAvailableToMeetingAgendaCreate(): boolean {
    return this.session.hasPermission('MEETING_AGENDA_CREATE');
  }

  loadIncludedQuestions(): Observable<any> {
    if (this.origId && this.questions) {
      this.questions.forEach((q, ind) => this.addResponsiblesInput(ind));
      return of('');
    }
    if (!this.agenda.agendaID) {
      return of('');
    }
    this.clearResponsiblesInputs();
    return this.agendaService.includedQuestions(this.agenda.agendaID).pipe(
      mergeMap(included => {
        included = included.filter(question => question.status !== AgendaQuestionStatus.EXCLUDED);
        return included && included.length ? forkJoin(included.map((question: AgendaQuestion) => {
          return this.questionService.get(question.questionId);
        })) : of([]);
      }), tap(result => {
        if (result.length > 0) {
          this.questions = result.map(wr => wr.document.question);
          this.questions.forEach((q, ind) => this.addResponsiblesInput(ind));
        } else {
          this.questions = [new Question()];
          this.addResponsiblesInput(0);
        }
      })
    );
  }

  loadProtocol(): Observable<any> {
    return this.protocolService.getByAgendaId(this.agenda.agendaID).pipe(
      catchError(error => {
        return of({});
      }),
      tap((wrapper: ProtocolDocumentWrapper) => {
        if (wrapper && wrapper.document) {
          this.setProtocol(wrapper.document.protocol);
        }
      })
    );
  }

  setProtocol(protocol: Protocol) {
    const documentProtocol = {
      document: {
        protocol
      }
    };
    this.protocol = angular.copy(documentProtocol);
    this.copyDocument = angular.copy(documentProtocol);
  }

  loadDictionaries(): Observable<any> {
    return forkJoin([
      this.meetingTypesService.getExpressMeetingTypes(),
      this.nsiRestService.get('mggt_meeting_Theme'),
      this.nsiRestService.get('mggt_meeting_MeetingPlace'),
      this.nsiRestService.get('EmailGroups'),
      this.nsiRestService.get('mggt_meeting_statusAc'),
    ]).pipe(
      map((values: any[][]) => {
        this.meetingTypes = values[0];
        this.meetingThemes = values[1];
        this.meetingPlaces = values[2];
        this.emailGroupsList = values[3].filter(emailGroup => {
          return emailGroup.sendSystem && emailGroup.sendSystem.find(sendSystem => {
            return sendSystem.code === 'SDO_MEETING';
          });
        });
        this.agendaStatuses = values[4].map(assignStatusDetails => {
          const { code, color, name } = assignStatusDetails;
          return {
            code,
            color,
            name
          };
        });
        if (!this.agenda.meetingType) {
          const express = _.find(this.meetingTypes, (el: MeetingType) => el.meetingType === 'EXPRESS');
          if (express) {
            this.agenda.meetingType = express.meetingType;
            this.meetingTypeChanged();
          }
        }
        if (this.origId) {
          this.agenda.meetingTheme = this.meetingThemes.find(el => el.code === this.agenda.meetingCodeTheme).name;
        }
        return true;
      })
    );
  }

  meetingTypeChanged() {
    const meetingType = this.agenda.meetingType ? _.find(this.meetingTypes, (meetingType: MeetingType) => {
      return meetingType.meetingType === this.agenda.meetingType;
    }) : null;
    if (meetingType) {
      this.agenda.meetingFullName = meetingType.meetingFullName;
      this.agenda.meetingShortName = meetingType.meetingShortName;
      if (!this.agenda.meetingTime) {
        this.agenda.meetingTime = new Date('01-01-1970 ' + meetingType.meetingTimeStart);
        this.updateIntersectionsWarnings();
      }
      if (meetingType.meetingPlace && meetingType.meetingPlace.length) {
        const meetingPlace = meetingType.meetingPlace[0];
        this.agenda.meetingPlaceCode = meetingPlace.code;
        this.agenda.meetingPlace = meetingPlace.name;
      } else {
        this.agenda.meetingPlaceCode = null;
        this.agenda.meetingPlace = null;
      }
    } else {
      this.agenda.meetingFullName = null;
      this.agenda.meetingShortName = null;
    }
  }

  meetingPlaceChanged() {
    if (this.agenda.meetingPlaceCode) {
      const meetingPlace: MeetingPlace = _.find(this.meetingPlaces, (meetingPlace: MeetingPlace) => {
        return meetingPlace.code === this.agenda.meetingPlaceCode;
      });
      if (meetingPlace) {
        this.agenda.meetingPlace = meetingPlace.name;
      }
    } else {
      this.agenda.meetingPlace = null;
    }
  }

  meetingThemeChanged(theme: MeetingTheme) {
    this.agenda.meetingTheme = theme ? theme.name : null;
  }

  searchAttendee(query: string) {
    return from(<any>this.nsiRestService.searchUsers({
      fio: query
    }, 'OR')).pipe(
      map((users: UserBean[]) => {
        users = users.filter((u: any) => !u.lock);
        return users.map(this._convertServerUser);
      })
    );
  }

  openResponsibles() {
    this.responsibles1 = this.agenda.attended.map((u: any) => ({
      departmentPrepareFull: u.departmentFullName,
      departmentPrepareShort: null,
      departmentPrepareCode: u.departmentCode,
      contractorPrepare: u.accountName,
      contractorPrepareFIO: u.fioFull,
      contractorPreparePhone: u.telephoneNumber
    }));
  }

  searchResponsibles(query: string): Observable<QuestionResponsible[]> {
    const params: ISerachUsersParams = {
      fio: query
    };
    return from(Promise.resolve(this.agenda.attended)).pipe(
      map(users => {
        return users.map((u: any) => {
          return {
            departmentPrepareFull: u.departmentFullName,
            departmentPrepareShort: null,
            departmentPrepareCode: u.departmentCode,
            contractorPrepare: u.accountName,
            contractorPrepareFIO: u.fioFull,
            contractorPreparePhone: u.telephoneNumber
          };
        });
      })
    );
  }

  searchInitiator(query: string) {
    return from(<any>this.nsiRestService.searchUsers({ fio: query })).pipe(
      map((users: UserBean[]) => {
        users = users.filter((u: any) => !u.lock);
        return users.map(user => ({
          accountName: user.accountName,
          post: user.post,
          iofShort: user.iofShort,
          fioFull: user.displayName,
        }));
      })
    );
  }

  addQuestion() {
    this.addResponsiblesInput(this.questions.length);
    this.questions.push(new Question());
    this.setQuestionsNumeration();
  }

  deleteQuestion(index: number) {

    const question: Question = this.questions[index];
    const questionsId = this.questions[index].questionID;
    if (questionsId) {
      this.instructionsService.getQuestionInstructions(question).pipe(
        mergeMap(response => {
          if (response.numFound > 0) {
            return from(<any>this.alertService.message({
              message: 'Удалите поручения, созданные по данному вопросу',
              type: 'warning'
            }));
          } else {
            return from(<any>this.alertService.confirm({
              message: 'Вы действительно хотите удалить данный вопрос?',
              okButtonText: 'Ок',
              type: 'warning'
            }));
          }
        })
      ).subscribe(() => {
        this.questions.splice(index, 1);
        this.setQuestionsNumeration();
        if (this.protocol && this.protocol.document && this.protocol.document.protocol && this.protocol.document.protocol.protocolID) {
          this.protocol.document.protocol.question = this.protocol.document.protocol.question.filter((q) => q.questionID !== questionsId);
        }
      });
    } else {
      this.questions.splice(index, 1);
      this.responsibles$.splice(index, 1);
      this.responsiblesLoading.splice(index, 1);
      this.responsiblesInput$.splice(index, 1);
      this.setQuestionsNumeration();
    }

  }

  swapQuestions(ind1: number, ind2: number) {
    const question1 = this.questions[ind1];
    const itemNumber = question1.questionConsider.itemNumber;
    const question2 = this.questions[ind2];

    question1.questionConsider.itemNumber = question2.questionConsider.itemNumber;
    question2.questionConsider.itemNumber = itemNumber;

    this.questions[ind1] = question2;
    this.questions[ind2] = question1;

    this.setQuestionsNumeration();
  }

  setQuestionsNumeration() {
    _.each(this.questions, (item, index) => {
      if (_.isEmpty(item.questionConsider)) {
        item.questionConsider = new QuestionConsider();
        const newItemNumber = `000${index + 1}`;

        item.questionConsider.itemNumber = newItemNumber.slice(newItemNumber.length - 3, newItemNumber.length);
      }
    });
  }

  cancel() {
    from(<any>this.alertService.confirm({
      message: 'Данные не будут сохранены, продолжить?',
      okButtonText: 'Продолжить',
      type: 'danger'
    })).subscribe(() => {
      if (this.agenda.agendaID || this.origId) {
        this.edit.emit(false);
        this.$state.go('app.meeting.agenda', { id: this.agenda.agendaID || this.origId, editing: false });
      } else {
        this.$state.go('app.meeting.showcase-express');
      }
    });
  }


  save() {
    if (localStorage.getItem('toSave') !== null) {
      localStorage.removeItem('toSave');
    }
    this.agenda.mailing = false;
    const iObservable = this.validate();
    iObservable.pipe(
      catchError((msg) => {
        this.toastr.error(msg);
        return throwError(msg);
      }),
      mergeMap(() => {
        this.saving = true;
        if (!this.agenda.agendaID) {
          return this.createAgenda();
        } else {
          return this.updateAgenda();
        }
      }),
      mergeMap((agenda) => {
        this.agenda = agenda;
        this.origAgenda = angular.copy(agenda);
        return this.loadIncludedQuestions();
      }),
      mergeMap(() => {
        if (this.protocol && this.protocol.document && this.protocol.document.protocol && this.protocol.document.protocol.protocolID) {
          this.questions.forEach((question: Question) => {
            const existedQuestion = this.protocol.document.protocol.question.find(q => q.questionID === question.questionID);
            if (existedQuestion) {
              this.updateQuestion(question, existedQuestion);
            } else {
              const newQuestion = new ProtocolQuestion();
              this.updateQuestion(question, newQuestion);
              this.protocol.document.protocol.question.push(newQuestion);
            }
          });

          this.protocol.document.protocol.question = this.protocol.document.protocol.question.sort((q1, q2) => {
            return parseInt(q1.itemNumber) - parseInt(q2.itemNumber);
          });

          return this.protocolService.updateDocument(this.protocol.document.protocol.protocolID, this.copyDocument, this.protocol);
        }

        return of(null);
      })
    ).subscribe(() => {
      this.saving = false;
      if (this.creating) {
        window.location.href = `/sdo/meeting/#/app/meeting/agenda/${this.agenda.agendaID}?express=true`;
      } else {
        this.edit.emit(false);
        this.reload();
      }
    }, (e) => {
      console.log('error', e);
      this.saving = false;
    });
  }

  onFakeFileChange(e) {
    console.log(e);
  }

  onDeleteFakeFile(e) {
    this.files = this.files.filter((f) => (f.nameFile !== e.nameFile));
  }

  updateQuestion(question: Question, protocolQuestion: ProtocolQuestion) {
    protocolQuestion.questionID = question.questionID;
    protocolQuestion.question = question.questionConsider.question;
    protocolQuestion.itemNumber = question.questionConsider.itemNumber;
    protocolQuestion.decisionText = question.questionConsider.decisionText;
  }

  createAgenda(): Observable<Agenda> {
    this.agenda.status = this.agendaStatuses.filter(status => status.code === 'assign')[0];
    return this.expressService.createAgenda(this.agenda, this._getQuestionsToSave()).pipe(
      mergeMap(agenda => {
        this.agenda = agenda;
        this.origAgenda = angular.copy(agenda);
        return this.loadIncludedQuestions();
      }),
      mergeMap(() => this.uploadFiles(this.agenda.folderId)),
      mergeMap(() => {
        return this.expressService.updateAgenda(this.agenda.agendaID,
          this.agendaService.diff(this.origAgenda, this.agenda), this._getQuestionsToSave());
      })
    );
  }

  updateAgenda(): Observable<Agenda> {
    this.updateHistory();
    return this.uploadFiles(this.agenda.folderId).mergeMap(() => {
      return this.expressService.updateAgenda(this.agenda.agendaID,
        this.agendaService.diff(this.origAgenda, this.agenda), this._getQuestionsToSave());
    });
  }

  updateHistory() {
    const changeDate = new Date();
    if (this.origAgenda.meetingPlace !== this.agenda.meetingPlace) {
      this.agenda.meetingPlaceHistory = this.agenda.meetingPlaceHistory || [];
      this.agenda.meetingPlaceHistory.push({
        meetingPlaceOld: this.origAgenda.meetingPlace,
        user: this.currentUser,
        changeDate,
      });
    }
    if (this.origAgenda.meetingDate.getDate() !== this.agenda.meetingDate.getDate()) {
      this.agenda.meetingDateHistory = this.agenda.meetingDateHistory || [];
      this.agenda.meetingDateHistory.push({
        meetingDateOld: this.origAgenda.meetingDate,
        user: this.currentUser,
        changeDate,
      });
    }
    if (this.origAgenda.meetingTime.getTime() !== this.agenda.meetingTime.getTime()) {
      this.agenda.meetingTimeHistory = this.agenda.meetingTimeHistory || [];
      this.agenda.meetingTimeHistory.push({
        meetingTimeOld: this.origAgenda.meetingTime,
        user: this.currentUser,
        changeDate,
      });
    }
    if (this.origAgenda.meetingTimeEnd.getTime() !== this.agenda.meetingTimeEnd.getTime()) {
      this.agenda.meetingTimeEndHistory = this.agenda.meetingTimeEndHistory || [];
      this.agenda.meetingTimeEndHistory.push({
        meetingTimeEndOld: this.origAgenda.meetingTimeEnd,
        user: this.currentUser,
        changeDate,
      });
    }
    if (this.origAgenda.attended.toString() !== this.agenda.attended.toString()) {
      this.agenda.attendedHistory = this.agenda.attendedHistory || [];
      this.agenda.attendedHistory.push({
        attendedOld: this.origAgenda.attended,
        user: this.currentUser,
        changeDate,
      });
    }
  }

  createTheme() {
    Observable.create(subscriber => {
      const options = {
        initialState: {},
        'class': 'modal-md'
      };
      const ref = this.modalService.show(AddThemeModalComponent, options);
      const subscr = this.modalService.onHide.subscribe(() => {
        const component = <AddThemeModalComponent>ref.content;
        if (component.submit) {
          subscriber.next(component.theme);
        } else {
          subscriber.error('canceled');
        }
        subscr.unsubscribe();
      });
    }).pipe(
      tap(theme => {
        from(this.nsiRestService.getDicts(['mggt_meeting_Theme'])).pipe(
          tap(response => {
            this.meetingThemes = _.orderBy(response.mggt_meeting_Theme, ['name'], ['asc']);
            this.nsiRestService.updateDictFromCache('mggt_meeting_Theme', response.mggt_meeting_Theme);
            const newTheme = this.meetingThemes.find(th => th.name === theme);
            this.meetingThemeChanged(newTheme);
            this.agenda.meetingCodeTheme = newTheme.code;
          }),
          catchError(error => {
            return EMPTY;
          })
        ).subscribe();
      }),
      catchError(error => {
        return EMPTY;
      })
    ).subscribe();
  }

  validate(): Observable<any> {
    if (!this.agenda.meetingType) {
      return throwError('Не указан вид совещания');
    }
    if (!this.agenda.initiator) {
      return throwError('Не указан инициатор');
    }
    if (!this.agenda.meetingPlace) {
      return throwError('Не указано место проведения');
    }
    if (!this.agenda.meetingTheme) {
      return throwError('Не указана тема совещания');
    }
    if (!this.agenda.meetingDate) {
      return throwError('Не указана дата совещания');
    }
    if (!this.agenda.meetingTime) {
      return throwError('Не указано время начала');
    }
    if (!this.agenda.meetingTimeEnd) {
      return throwError('Не указано время окончания');
    }
    if (!this._validateMeetingTimeEnd()) {
      return throwError('Введенное время окончания совещания меньше времени начала совещания');
    }
    if (!this.agenda.attended || !this.agenda.attended.length) {
      return throwError('Не указаны участники');
    }
    if (this.agenda.meetingDate.getTime() < this.minDate.setHours(0, 0, 0, 0)) {
      return throwError('Дата совещания не может быть ранее текущей даты');
    }
    return of('');
  }

  attendedAdded(attended: AgendaAttended) {
    if (!this.agenda.meetingDate || !this.agenda.meetingTime || !this.agenda.meetingTimeEnd) {
      this.alertService.message({
        message: 'Для проверки занятости участников, введите время начала и окончания совещания'
      });
      return;
    }
    const meetingDateTime = this.getDateTime(this.agenda.meetingDate, this.agenda.meetingTime);
    const meetingDateTimeEnd = this.getDateTime(this.agenda.meetingDate, this.agenda.meetingTimeEnd);
    this.checkAttended([attended], meetingDateTime, meetingDateTimeEnd).subscribe((messages) => {
      if (messages.length > 0) {
        this.alertService.message({ message: messages.join('<br/>') });
      }
    });
  }

  updateResponsibles() {
    this.responsibles1 = this.agenda.attended.map((u: any) => ({
      departmentPrepareFull: u.departmentFullName,
      departmentPrepareShort: null,
      departmentPrepareCode: u.departmentCode,
      contractorPrepare: u.accountName,
      contractorPrepareFIO: u.fioFull,
      contractorPreparePhone: u.telephoneNumber
    }));
  }

  updateQuestionsResponsibles() {
    const attendedAccounts: string[] = this.agenda.attended ? this.agenda.attended.map(user => {
      return user.accountName;
    }) : [];
    this.questions = this.questions.map((question: Question) => {
      const responsiblePrepare = question.questionConsider.responsiblePrepare;
      if (responsiblePrepare) {
        const isAttended = attendedAccounts.indexOf(responsiblePrepare.contractorPrepare) !== -1;
        if (!isAttended) {
          question.questionConsider.responsiblePrepare = null;
        }
      }
      return question;
    });
  }

  updateIntersectionsWarnings() {
    this.intersectionsWarnings = [];

    if (this.agenda.attended && this.agenda.attended.length && (!this.agenda.meetingDate || !this.agenda.meetingTime || !this.agenda.meetingTimeEnd)) {
      this.intersectionsWarnings = ['Для проверки занятости участников, введите дату, время начала и окончания совещания'];
      return;
    }
    if (this.agenda.attended && this.agenda.attended.length) {
      const meetingDateTime = this.getDateTime(this.agenda.meetingDate, this.agenda.meetingTime);
      const meetingDateTimeEnd = this.getDateTime(this.agenda.meetingDate, this.agenda.meetingTimeEnd);

      this.checkAttended(this.agenda.attended, meetingDateTime, meetingDateTimeEnd).subscribe((messages) => {
        this.intersectionsWarnings = messages || [];
      });
    }
  }

  checkAttended(attended: AgendaAttended[], startDateTime: string, endDateTime: string): Observable<string[]> {
    const refusedStatusName = _.find(this.agendaStatuses, { code: 'refused' }).name;

    return forkJoin(
      attended.map(att => {
        return this.solrMediator.searchExt({
          types: [ this.solrMediator.types.agenda ],
          pageSize: 200,
          fields: [
            { name: 'attendedLogin', value: att.accountName }
            // { name: 'meetingDateTimeSs', value: { to: endDateTime } },
            // { name: 'meetingDateTimeEndSs', value: { from: startDateTime } }
          ]
        });
      })
    ).pipe(
      map((results: SearchResult[]) => {
        const messages: string[] = [];
        results.forEach((result: SearchResult, index) => {
          const att = attended[index];
          if (result.numFound > 0) {
            result.docs.forEach((doc: SearchResultDocument | any) => {
              if (doc.meetingStatus === refusedStatusName) {
                return;
              }

              if (!this.agenda.agendaID || doc.documentId !== this.agenda.agendaID) {
                const message = 'Внимание: <span class="semibold">' + att.fioFull + '</span> с ' + this.formatTime(doc.meetingTime) +
                  ' до ' + this.formatTime(doc.meetingTimeEnd) + ' участвует в совещании "<u>' + doc.meetingShortName + '</u>"';
                messages.push(message);
              }
            });
          }
        });
        return messages;
      })
    );
  }

  onRemoveEmailGroups(emailGroup: { value: EmailGroups }) {
    this.agenda.attended = this.agenda.attended.filter(user => {
      return emailGroup.value.users.indexOf(user.accountName) === -1;
    });
  }

  updateAttendedAfterEmailGroups(emailGroup: EmailGroups) {
    this.attendedLoading = true;

    const usersDef = emailGroup.users.map(user => {
      return this.nsiRestService.ldapUser(user);
    });

    forkJoin(usersDef).subscribe(users => {
      const usersInEmailGroups = users.map(this._convertServerUser);
      if (!_.isArray(this.agenda.attended)) {
        this.agenda.attended = [];
      }

      this.agenda.attended = _.uniqBy(_.union(this.agenda.attended, usersInEmailGroups), 'accountName');
      this.updateIntersectionsWarnings();
      this.attendedLoading = false;
    }, () => {
      this.attendedLoading = false;
    });
  }

  updateEmailGroupsAfterAttended() {
    const attendedAccounts: string[] = this.agenda.attended ? this.agenda.attended.map(user => {
      return user.accountName;
    }) : [];
    this.emailGroups = _
      .chain(this.emailGroupsList)
      .filter(emailGroup => {
        return _.every(emailGroup.users, user => {
          return attendedAccounts.indexOf(user) > -1;
        });
      })
      .value();
  }

  formatTime(time: string): string {
    const match = /^(\d{2}):(\d{2}):(\d{2})$/.exec(time);
    const t = new Date(0, 0, 0, parseInt(match[1]), parseInt(match[2]), parseInt(match[3]));
    return formatDate(t, 'HH:mm', 'en');
  }

  getDateTime(date: Date, time: Date): string {
    return formatDate(this._buildDate(date, time), 'yyyy-MM-ddTHH:mm:ss', 'en');
  }

  /**
   *  Показываем сообщение об ошибке, что выбрано начало совещания позже конца
   */
  showMeetingTimeEndError() {
    if (!this._validateMeetingTimeEnd()) {
      this.toastr.error('Введенное время окончания совещания меньше времени начала совещания');
    }
  }

  private _convertServerUser(user: any): AgendaAttended {
    return {
      internal: true,
      accountName: user.accountName,
      post: user.post,
      iofShort: user.iofShort,
      fioFull: user.displayName,
      name: user.departmentFullName,
      code: user.departmentCode,
      role: {
        name: user.departmentFullName,
        code: user.departmentCode
      },
      prefect: null
    };
  }

  _getQuestionsToSave(): Question[] {
    const questionsToSave: Question[] = this.questions.filter(q => {
      return _.get(q, 'questionConsider.question', false)
          || _.get(q, 'questionConsider.responsiblePrepare', false);
    });

    if (questionsToSave.length > 0) {
      return questionsToSave;
    }
  }

  /**
   *  Получаем объект Date из параметоров
   */
  private _buildDate(date: Date, time: Date): Date {
    const result = new Date(date.getTime());
    result.setHours(time.getHours(), time.getMinutes());
    return result;
  }

  /**
   *  Проверка времени, что начало раньше конца
   */
  private _validateMeetingTimeEnd(): boolean {
    if (this.agenda.meetingTime && this.agenda.meetingTimeEnd) {
      const date = this.agenda.meetingDate || new Date();
      const meetingDateTime: Date = this._buildDate(date, this.agenda.meetingTime);
      const meetingDateTimeEnd: Date = this._buildDate(date, this.agenda.meetingTimeEnd);
      return meetingDateTime.getTime() < meetingDateTimeEnd.getTime();
    } else {
      return true;
    }
  }

  /**
   *  Добавляем файл в очередь, имитируя загрузку
   */
  addFileToQueue(file) {
    this.fileQueue.push(file);
    const fileType: FileType = FileType.create(undefined, file.name, file.size, false, new Date().toISOString(), file.type, 'MkaDocOther');
    this.fileQueueToDisplay.push(fileType);
  }

  /**
   *  Загрузка файла с проверкой имени
   */
  validateAndUploadFile(file, entityID: string, folderID: string): Observable<FileType> {
    this.uploading = true;
    const fd = new FormData();
    fd.append('file', file);
    fd.append('folderGuid', folderID);
    fd.append('fileType', 'MkaDocOther');
    fd.append('docEntityID', entityID);
    fd.append('docSourceReference', 'UI');
    return from(<any>this.fileResourceService.handleFileUploadNew(fd)).pipe(
      map((res: any) => {
        const fileType: FileType = FileType.create(res.guid, file.name, file.size, false,
          new Date().toISOString(), file.type, 'MkaDocOther');
        this.removeFileQueued(file);
        this.uploading = false;
        return fileType;
      }),
      catchError(err => {
        const { data } = err;
        let reason = '';
        // Иногда приходит HTML, например, при ошибке большого файла
        try {
          reason = JSON.parse(data).message;
        } catch (error) {
        }
        if (~reason.indexOf('File already exists')) {
          return this.renameFile(file.name).pipe(
            mergeMap(newName => {
              const _file = new File([file], newName, { type: file.type });
              return this.validateAndUploadFile(_file, this.agenda.agendaID, this.agenda.folderId);
            }),
            catchError(cancel => {
              this.removeFileQueued(file);
              this.uploading = false;
              return EMPTY;
            })
          );
        } else {
          this.removeFileQueued(file);
          this.uploading = false;
          return EMPTY;
        }
      })
    );
  }

  renameFile(oldName: string): Observable<string> {
    const options = {
      initialState: {
        oldName: oldName
      },
      'class': 'modal-md'
    };
    const ref = this.modalService.show(RenameFileModalComponent, options);
    return this.modalService.onHide.pipe(
      mergeMap(() => {
        const component = <RenameFileModalComponent>ref.content;
        if (component.submit) {
          const newName = component.newName + '.' + component.extension;
          return of(newName);
        } else {
          return throwError('canceled');
        }
      })
    );
  }

  /**
   *  Удаление файла из filenet или из очереди
   */
  removeFile(file: FileType) {
    if (file.idFile) {
      from(this.fileResourceService.deleteFile(file.idFile)).subscribe(() => {
        const ind = this.agenda.filesAttach.findIndex(el => el.idFile === file.idFile);
        this.agenda.filesAttach.splice(ind, 1);
      });
    } else {
      let ind = this.fileQueue.findIndex(el => el.name === file.nameFile);
      this.fileQueue.splice(ind, 1);
      ind = this.fileQueueToDisplay.findIndex(el => el.nameFile === file.nameFile);
      this.fileQueueToDisplay.splice(ind, 1);
    }
  }

  /**
   *  Удаление файлa из очереди после загрузки или после отмены переименования
   */
  removeFileQueued(file) {
    const ind = this.fileQueue.findIndex(el => el.name === file.name);
    this.fileQueue.splice(ind, 1);
  }

  uploadFiles(folderGuid: string): Observable<any> {
    return Observable.create(observer => {
      const requests$ = [];
      this.files.forEach(file => {
        requests$.push(this.uploadFile(file, folderGuid, 'default'));
      });
      if (requests$.length) {
        forkJoin(requests$).subscribe(res => {
          this.files = this.files.map((file, index) => {
            return {
              ...file,
              idFile: res[index].guid
            };
          });
          this.agenda.filesAttach = this.files;
          this.toastr.success('Файлы успешно загружены!');
          observer.next(res);
        }, error => console.error(error), () => observer.complete());
      } else {
        observer.next([]);
        observer.complete();
      }
    });
  }

  uploadFile(file, folderGuid: string, fileType: string): Observable<any> {
    const fd = new FormData();
    fd.append('file', file, file.nameFile);
    if (folderGuid) { fd.append('folderGuid', folderGuid); }
    if (fileType) { fd.append('fileType', fileType); }
    fd.append('docEntityID', '-');
    fd.append('docSourceReference', 'TEST-001');
    return from(this.fileResourceService.handleFileUpload(fd));
  }

  clearResponsiblesInputs() {
    this.responsiblesInput$ = [];
    this.responsiblesLoading = [];
    this.responsibles$ = [];
  }

  addResponsiblesInput(index) {
    this.responsiblesInput$.push(new Subject<string>());
    this.responsiblesLoading.push(false);
    this.responsibles$.push(concat(
      of([]),
      this.responsiblesInput$[index].pipe(
        switchMap((term: string) => this.searchResponsibles(term)),
        catchError(() => of([])),
        tap(() => this.responsiblesLoading[index] = false)
      )
    ));
  }

  trackByFn(index, item) {
    if (!item) { return null; }
    return item.itemNumber;
  }


}
