import {Component, OnInit} from '@angular/core';
import {BsModalRef} from 'ngx-bootstrap';
import { SessionStorage } from '@reinform-cdp/security';
import {SearchResult, SearchResultDocument} from '@reinform-cdp/search-resource';
import {ToastrService} from 'ngx-toastr';
import {StateService} from '@uirouter/core';
import {AgendaService} from '../../../../services/agenda.service';
import {Agenda, AgendaAttended, AgendaMeetingHistoryUser, AgendaStatus} from '../../../../models/Agenda';
import {MeetingPlace} from '../../../../models/nsi/MeetingPlace';
import {forkJoin, Observable, of, throwError} from 'rxjs';
import {catchError, map, mergeMap} from 'rxjs/operators';
import {formatDate} from '@angular/common';
import * as _ from 'lodash';
import * as angular from 'angular';
import * as jsonpatch from 'fast-json-patch';
import {SolrMediatorService} from '../../../../services/solr-mediator.service';

@Component({
  selector: 'meeting-modal-content',
  templateUrl: './reschedule-agenda.modal.html'
})
export class RescheduleAgendaModalComponent implements OnInit {

  onReschedule: () => void;
  agendaStatuses: AgendaStatus[];
  agenda: Agenda;
  origAgenda: Agenda;
  meetingPlaces: MeetingPlace[];
  intersectionsWarnings: string[];
  minDate = new Date();
  currentUser: AgendaMeetingHistoryUser = new AgendaMeetingHistoryUser;

  constructor(
    private bsModalRef: BsModalRef,
    private $state: StateService,
    private agendaService: AgendaService,
    private solrMediator: SolrMediatorService,
    private toastr: ToastrService,
    public session: SessionStorage
  ) {
  }

  ngOnInit() {
    this.currentUser.login = this.session.login();
    this.currentUser.fioFull = this.session.fullName();
    this.currentUser.post = this.session.post();
    this.origAgenda = angular.copy(this.agenda);
  }

  save() {
    this.agenda.mailing = false;
    const iObservable = this.validate();
    iObservable.pipe(
      catchError((msg) => {
        this.toastr.error(msg);
        return throwError(msg);
      }),
      mergeMap(() => {
        return this.updateAgenda();
    })
  ).subscribe((agenda) => {
      this.agenda = agenda;
      this.bsModalRef.hide();
      this.onReschedule();
      // this.$state.go('app.meeting.agenda', {id: this.agenda.agendaID, editing: false});
    });
  }

  updateAgenda(): Observable<Agenda> {
    this.updateHistory();
    return this.agendaService.update(this.agenda.agendaID, this.agendaService.diff(this.origAgenda, this.agenda));
  }

  updateHistory() {
    const changeDate = new Date();
    if (this.origAgenda.meetingPlace !== this.agenda.meetingPlace) {
      this.agenda.meetingPlaceHistory = this.agenda.meetingPlaceHistory || [];
      this.agenda.meetingPlaceHistory.push({
        meetingPlaceOld: this.origAgenda.meetingPlace,
        user: this.currentUser,
        changeDate,
      });
    }
    if (this.origAgenda.meetingDate.getDate() !== this.agenda.meetingDate.getDate()) {
      this.agenda.meetingDateHistory = this.agenda.meetingDateHistory || [];
      this.agenda.meetingDateHistory.push({
        meetingDateOld: this.origAgenda.meetingDate,
        user: this.currentUser,
        changeDate,
      });
    }
    if (this.origAgenda.meetingTime.getTime() !== this.agenda.meetingTime.getTime()) {
      this.agenda.meetingTimeHistory = this.agenda.meetingTimeHistory || [];
      this.agenda.meetingTimeHistory.push({
        meetingTimeOld: this.origAgenda.meetingTime,
        user: this.currentUser,
        changeDate,
      });
    }
    if (this.origAgenda.meetingTimeEnd.getTime() !== this.agenda.meetingTimeEnd.getTime()) {
      this.agenda.meetingTimeEndHistory = this.agenda.meetingTimeEndHistory || [];
      this.agenda.meetingTimeEndHistory.push({
        meetingTimeEndOld: this.origAgenda.meetingTimeEnd,
        user: this.currentUser,
        changeDate,
      });
    }
    if (this.origAgenda.attended.toString() !== this.agenda.attended.toString()) {
      this.agenda.attendedHistory = this.agenda.attendedHistory || [];
      this.agenda.attendedHistory.push({
        attendedOld: this.origAgenda.attended,
        user: this.currentUser,
        changeDate,
      });
    }
  }

  cancel() {
    this.bsModalRef.hide();
  }

  updateIntersectionsWarnings() {
    this.intersectionsWarnings = [];

    if (this.agenda.attended && this.agenda.attended.length
      && (!this.agenda.meetingDate || !this.agenda.meetingTime || !this.agenda.meetingTimeEnd)) {
      this.intersectionsWarnings = ['Для проверки занятости участников, введите дату, время начала и окончания совещания'];
      return;
    }
    if (this.agenda.attended && this.agenda.attended.length) {
      const meetingDateTime = this.getDateTime(this.agenda.meetingDate, this.agenda.meetingTime);
      const meetingDateTimeEnd = this.getDateTime(this.agenda.meetingDate, this.agenda.meetingTimeEnd);

      this.checkAttended(this.agenda.attended, meetingDateTime, meetingDateTimeEnd).subscribe((messages) => {
        this.intersectionsWarnings = messages || [];
      });
    }
  }

  checkAttended(attended: AgendaAttended[], startDateTime: string, endDateTime: string): Observable<string[]> {
    const refusedStatusName = _.find(this.agendaStatuses, { code: 'refused'}).name;

    return forkJoin(
      attended.map(att => {
        return this.solrMediator.searchExt({
          types: [ this.solrMediator.types.agenda ],
          pageSize: 200,
          fields: [
            {name: 'attendedLogin', value: att.accountName}
            // {name: 'meetingDateTimeSs', value: {to: endDateTime}},
            // {name: 'meetingDateTimeEndSs', value: {from: startDateTime}}
          ]
        });
      })
    ).pipe(
      map((results: SearchResult[]) => {
        const messages: string[] = [];
        results.forEach((result: SearchResult, index) => {
          const att = attended[index];
          if (result.numFound > 0) {
            result.docs.forEach((doc: SearchResultDocument | any) => {
              if (doc.meetingStatus === refusedStatusName) {
                return;
              }
              if (!this.agenda.agendaID || doc.documentId !== this.agenda.agendaID) {
                const message = 'Внимание: <span class="semibold">' + att.fioFull + '</span> с ' + this.formatTime(doc.meetingTime) +
                  ' до ' + this.formatTime(doc.meetingTimeEnd) + ' участвует в совещании "<u>' + doc.meetingShortName + '</u>"';
                messages.push(message);
              }
            });
          }
        });
        return messages;
      })
    );
  }

  showMeetingTimeEndError() {
    if (!this._validateMeetingTimeEnd()) {
      this.toastr.error('Введенное время окончания совещания меньше времени начала совещания');
    }
  }

  meetingPlaceChanged() {
    if (this.agenda.meetingPlaceCode) {
      const meetingPlace: MeetingPlace = _.find(this.meetingPlaces, (meetingPlace: MeetingPlace) => {
        return meetingPlace.code === this.agenda.meetingPlaceCode;
      });
      if (meetingPlace) {
        this.agenda.meetingPlace = meetingPlace.name;
      }
    } else {
      this.agenda.meetingPlace = null;
    }
  }

  private _validateMeetingTimeEnd(): boolean {
    if (this.agenda.meetingTime && this.agenda.meetingTimeEnd) {
      const date = this.agenda.meetingDate || new Date();
      const meetingDateTime: Date = this._buildDate(date, this.agenda.meetingTime);
      const meetingDateTimeEnd: Date = this._buildDate(date, this.agenda.meetingTimeEnd);
      return meetingDateTime.getTime() < meetingDateTimeEnd.getTime();
    } else {
      return true;
    }
  }

  validate(): Observable<any> {
    if (!this.agenda.meetingPlace) {
      return throwError('Не указано место проведения');
    }
    if (!this.agenda.meetingDate) {
      return throwError('Не указана дата совещания');
    }
    if (!this.agenda.meetingTime) {
      return throwError('Не указано время начала');
    }
    if (!this.agenda.meetingTimeEnd) {
      return throwError('Не указано время окончания');
    }
    if (!this._validateMeetingTimeEnd()) {
      return throwError('Введенное время окончания совещания меньше времени начала совещания');
    }
    if (this.agenda.meetingDate.getTime() < this.minDate.setHours(0, 0, 0, 0)) {
      return throwError('Дата совещания не может быть ранее текущей даты');
    }
    const diff = jsonpatch.compare(JSON.parse(angular.toJson(this.agenda)), JSON.parse(angular.toJson(this.origAgenda)));
    if (!this.origAgenda || diff.length === 0) {
      return throwError('Данные не изменены');
    }
    return of('');
  }


  getDateTime(date: Date, time: Date): string {
    return formatDate(this._buildDate(date, time), 'yyyy-MM-ddTHH:mm:ss', 'en');
  }

  formatTime(time: string): string {
    const match = /^(\d{2}):(\d{2}):(\d{2})$/.exec(time);
    const t = new Date(0, 0, 0, parseInt(match[1]), parseInt(match[2]), parseInt(match[3]));
    return formatDate(t, 'HH:mm', 'en');
  }

  private _buildDate(date: Date, time: Date): Date {
    const result = new Date(date.getTime());
    result.setHours(time.getHours(), time.getMinutes());
    return result;
  }

}
