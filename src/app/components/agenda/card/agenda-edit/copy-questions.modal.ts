import {Component, OnInit} from '@angular/core';
import {BsModalRef} from "ngx-bootstrap";
import * as _ from "lodash";

@Component({
  selector: 'meeting-modal-content',
  templateUrl: './copy-questions.modal.html'
})
export class CopyQuestionsModalComponent implements OnInit {

  checked: boolean[];
  questions: { questionId: string }[];
  submit: boolean;

  constructor(private bsModalRef: BsModalRef) {
  }

  ngOnInit() {
    this.submit = false;
    this.checked = new Array(this.questions.length);
  }

  getCheckedCount() {
    const result = _.countBy(this.checked, ch => ch).true;
    return result ? result : 0;
  }

  save() {
    this.submit = true;
    this.bsModalRef.hide();
  }

  cancel() {
    this.submit = false;
    this.bsModalRef.hide();
  }

}
