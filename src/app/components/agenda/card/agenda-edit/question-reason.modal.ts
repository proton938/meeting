import {Component, OnInit} from '@angular/core';
import {BsModalRef} from "ngx-bootstrap";

@Component({
  selector: 'meeting-modal-content',
  templateUrl: './question-reason.modal.html'
})
export class QuestionReasonModalComponent implements OnInit {

  reason: string;
  submit: boolean;

  constructor(private bsModalRef: BsModalRef) {
  }

  ngOnInit() {
    this.submit = false;
  }


  save() {
    this.submit = true;
    this.bsModalRef.hide();
  }

  cancel() {
    this.submit = false;
    this.bsModalRef.hide();
  }

}
