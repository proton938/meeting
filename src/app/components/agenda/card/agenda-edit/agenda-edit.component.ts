import {StateService} from '@uirouter/core';
import {Subject, Observable} from "rxjs/Rx";
import {FileType} from "../../../../models/FileType";
import {concat, from, of} from "rxjs/index";
import {catchError, debounceTime, distinctUntilChanged, filter, map, switchMap, tap} from "rxjs/internal/operators";
import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {LoadingStatus} from "@reinform-cdp/widgets";
import {ToastrService} from "ngx-toastr";
import {Agenda, AgendaQuestion, AgendaQuestionStatus} from "../../../../models/Agenda";
import {Question, QuestionDocumentWrapper, QuestionMaterialGroup} from "../../../../models/Question";
import {MeetingType} from "../../../../models/nsi/MeetingType";
import {MeetingPlace} from "../../../../models/nsi/MeetingPlace";
import {RoleParticipant} from "../../../../models/nsi/RoleParticipant";
import {AuthorizationService, SessionStorage} from "@reinform-cdp/security";
import {BsLocaleService} from "ngx-bootstrap/datepicker";
import {MeetingTypeService} from "../../../../services/meeting-type.service";
import {NsiResourceService} from "@reinform-cdp/nsi-resource";
import {BsModalService} from "ngx-bootstrap";
import {QuestionService} from "../../../../services/question.service";
import * as _ from "lodash";
import {AgendaService} from "../../../../services/agenda.service";
import {QuestionReasonModalComponent} from "./question-reason.modal";
import {CopyQuestionsModalComponent} from "./copy-questions.modal";

@Component({
  selector: 'meeting-agenda-edit',
  templateUrl: './agenda-edit.component.html',
  styleUrls: ['./agenda-edit.component.scss']
})
export class AgendaEditComponent implements OnInit {

  AGENDA_QUESTION_STATUS_EXCLUDED = "excluded";
  AGENDA_QUESTION_STATUS_INCLUDED = "included";
  AGENDA_QUESTION_STATUS_ADDED = "added";

  MATERIAL_TYPE_INQUIRY_SSMKA = 'Справка СС МКА';
  MATERIAL_TYPE_INQUIRY_PZZ = 'Справка ПЗЗ';
  MATERIAL_TYPE_INQUIRY = 'Справка';
  MATERIAL_TYPE_INQUIRIES = ['Справка СС МКА', 'Справка ПЗЗ', 'Справка'];
  MATERIAL_TYPE_PRESENTATION = 'Презентация';
  MATERIAL_TYPE_GPZU_SCHEME = 'Чертеж ГПЗУ';
  MATERIAL_TYPE_OTHER = 'Дополнительные материалы';

  hasQuestionViewPermission: boolean;

  loadingStatus: LoadingStatus;
  success: boolean;
  loadingQuestionsStatus: LoadingStatus;
  loadingQuestionsSuccess: boolean;

  @Input() agenda: Agenda;
  @Output() saveAgenda: EventEmitter<any> = new EventEmitter<any>();
  @Output() publishAgenda: EventEmitter<any> = new EventEmitter<any>();
  @Output() cancel: EventEmitter<any> = new EventEmitter<any>();
  questions: { [key: string]: Question };
  included: AgendaQuestion[] = [];
  includedCount: number;
  candidates: string[] = [];
  restorables: { [key: string]: boolean };

  meetingTypes: MeetingType[];
  meetingPlaces: MeetingPlace[];

  departments: any[];
  internalAttendees: any[];
  externalAttendees: any[];
  updatingUsersListInProgress: boolean = false;
  meetingAttendees: any[];

  availableToCopy: any;

  attendees$ = [];
  attendeesLoading = [];
  attendeesInput$ = [];

  constructor(private $state: StateService,
              private authorizationService: AuthorizationService,
              private meetingTypeService: MeetingTypeService,
              private nsiResourceService: NsiResourceService,
              private questionService: QuestionService,
              private agendaService: AgendaService,
              private modalService: BsModalService,
              private session: SessionStorage,
              private localeService: BsLocaleService,
              private toastr: ToastrService) {
  }

  ngOnInit() {
    this.localeService.use('ru');
    this.questions = {};
    this.restorables = {};
    this.loadingStatus = LoadingStatus.LOADING;
    this.hasQuestionViewPermission = this.authorizationService.check('MEETING_QUESTION_CARD');

    if (!this.authorizationService.check('MEETING_AGENDA_FORM')) {
      this.toastr.warning("У вас нет прав для просмотра этой страницы.", "Ошибка");
      return Promise.reject("У вас нет прав для просмотра этой страницы.");
    }
    return this.loadDictionaries().then(() => {
      this.loadingStatus = LoadingStatus.SUCCESS;
      this.success = true;
    }).catch(() => {
      this.loadingStatus = LoadingStatus.ERROR;
    });
  }

  loadDictionaries(): Promise<any> {
    return Promise.all([
      this.meetingTypeService.getNonExpressMeetingTypes(),
      this.nsiResourceService.get('mggt_meeting_MeetingPlace'),
      this.nsiResourceService.departments(),
      this.nsiResourceService.get('mggt_meeting_RolesParticipants')
    ]).then((values: [MeetingType[], MeetingPlace[], any[], RoleParticipant[]]) => {
      this.meetingTypes = values[0];
      this.meetingPlaces = values[1];
      this.departments = values[2];

      this.meetingAttendees = values[3].filter((el) => {
        if (el.LevelRoleCode === 'organization') {
          return el;
        }
      }).map((el: any) => {
        el.name = el.RoleName;
        el.code = el.RoleCode;
        el.role = {
          roleCode: el.RoleCode,
          roleName: el.RoleName
        };
        return el;
      });

      this.meetingTypeChanged();
      this.prepareAttendees();
    });
  }

  meetingTypeChanged() {
    if (this.agenda.meetingType && !this.agenda.agendaID) {
      let meetingType: MeetingType = this.meetingTypes.find(meetingType => {
        return meetingType.meetingType === this.agenda.meetingType;
      });
      this.agenda.meetingFullName = meetingType.meetingFullName;
      this.agenda.meetingShortName = meetingType.meetingShortName;
      if (!this.agenda.meetingTime) {
        this.agenda.meetingTime = new Date("1970-01-01T" + meetingType.meetingTimeStart);
      }
      if (!this.agenda.meetingPlace) {
        this.agenda.meetingPlace = meetingType.meetingPlace && meetingType.meetingPlace[0].name;
      }
      this.availableToCopy = meetingType.creatingCopies;
    }
    else if (this.agenda.meetingType && this.agenda.agendaID) {
      let meetingType: MeetingType = this.meetingTypes.find(meetingType => {
        return meetingType.meetingType === this.agenda.meetingType;
      });
      this.availableToCopy = meetingType.creatingCopies;
      this.agenda.meetingFullName = meetingType.meetingFullName;
      this.agenda.meetingShortName = meetingType.meetingShortName;
    } else {
      this.agenda.meetingFullName = null;
      this.agenda.meetingShortName = null;
    }
    this.loadQuestions(this.agenda.meetingType, this.agenda.meetingDate);
  }

  meetingDateChanged(meetingDate) {
    this.loadQuestions(this.agenda.meetingType, meetingDate);
  }

  searchIncluded(meetingType: string, meetingDate: Date): Promise<AgendaQuestion[]> {
    return this.agendaService.searchIncludedQuestions(meetingType, meetingDate).toPromise().then(questionsIDs => {
      return questionsIDs.map(id => <AgendaQuestion> {questionId: id});
    });
  }

  loadIncluded(agendaID: string): Promise<AgendaQuestion[]> {
    return this.agendaService.includedQuestions(agendaID).toPromise();
  }

  searchCandidates(meetingType: string, meetingDate: Date): Promise<string[]> {
    return this.agendaService.searchCandidateQuestions(meetingType, meetingDate).toPromise();
  }

  loadCandidates(agendaID: string, meetingType: string, meetingDate: Date): Promise<string[]> {
    return this.agendaService.candidateQuestions(agendaID, meetingType, meetingDate).toPromise();
  }

  getIncludedCount(): Promise<number> {
    if (this.agenda.agendaID) {
      return this.agendaService.includedQuestionsCount(this.agenda.agendaID).toPromise();
    } else {
      return Promise.resolve(this.included.length);
    }
  }

  loadQuestions(meetingType, meetingDate) {
    if (!meetingType || !meetingDate) {
      return;
    }
    this.loadingQuestionsStatus = LoadingStatus.LOADING;
    this.loadingQuestionsSuccess = false;
    let getIncluded = this.agenda.agendaID ? this.loadIncluded(this.agenda.agendaID) : this.searchIncluded(meetingType, meetingDate);
    let getCandidates = this.agenda.agendaID ? this.loadCandidates(this.agenda.agendaID, meetingType, meetingDate) : this.searchCandidates(meetingType, meetingDate);
    Promise.all([getIncluded, getCandidates]).then((result: [AgendaQuestion[], string[]]) => {
      this.included = result[0].filter(inc => inc);
      this.candidates = result[1];
      return this.getIncludedCount();
    }).then((includedCount) => {
      this.includedCount = includedCount;
      let questionsIDs: string[] = _.union(this.included.map(question => {
        return question.questionId
      }), this.candidates);
      return Promise.all(
        questionsIDs.map(questionID => {
          return this.questionService.get(questionID).toPromise()
        })
      )
    }).then((result: QuestionDocumentWrapper[]) => {
      result.forEach(wrapper => {
        let question: Question = wrapper.document.question;
        this.questions[question.questionID] = question;
      });
      let excludedQuestionsIds: string[] = this.included.filter(question => {
        return question.status === AgendaQuestionStatus.EXCLUDED;
      }).map(question => {
        return question.questionId;
      });
      return Promise.all(
        excludedQuestionsIds.map(questionID => {
          return this.agendaService.isRestorable(this.agenda.agendaID, questionID).toPromise()
        })
      )
    }).then((result: any[]) => {
      this.restorables = {};
      _.each(result, (item: any) => {
        _.extend(this.restorables, item);
      });
      return true;
    }).then(() => {
      this.loadingQuestionsStatus = LoadingStatus.SUCCESS;
      this.loadingQuestionsSuccess = true;
    }).catch(() => {
      this.included = this.candidates = [];
      this.loadingQuestionsStatus = LoadingStatus.ERROR;
    });
  }

  getMaterialGroupDocuments(question: Question, materialType: string) {
    if (!question.questionConsider.materials) {
      return [];
    }
    let materialGroup = question.questionConsider.materials.find(materialGroup => {
      return materialGroup.materialType === materialType;
    });
    return materialGroup ? materialGroup.document : [];
  }

  getMaterialGroupsDocuments(question: Question, materialTypes: string[]) {
    if (!question.questionConsider.materials) {
      return [];
    }
    let materialGroup = question.questionConsider.materials.find(materialGroup => {
      return _.indexOf(materialTypes, materialGroup.materialType) >= 0;
    });
    return materialGroup ? materialGroup.document : [];
  }

  exclude(ind: number) {
    if (this.agenda.agendaPublished) {
      const options = {
        initialState: {},
        'class': 'modal-lg'
      };
      const ref = this.modalService.show(QuestionReasonModalComponent, options);
      let subscription = this.modalService.onHide.subscribe(() => {
        let component = <QuestionReasonModalComponent> ref.content;
        if (component.submit) {
          this.included[ind].status = AgendaQuestionStatus.EXCLUDED;
          this.included[ind].reasonChange = component.reason;
          this.included[ind].fio = this.session.fullName();
          this.included[ind].login = this.session.login();
          this.cleanFields(this.included[ind]);
          this.includedCount--;
        }
        subscription.unsubscribe();
      });
    } else {
      this.candidates.push(this.included[ind].questionId);
      this.included.splice(ind, 1);
      this.includedCount--;
    }
  }

  include(ind: number) {
    let question: AgendaQuestion = new AgendaQuestion();
    question.questionId = this.candidates[ind];
    if (this.agenda.agendaPublished) {
      const options = {
        initialState: {},
        'class': 'modal-lg'
      };
      const ref = this.modalService.show(QuestionReasonModalComponent, options);
      let subscription = this.modalService.onHide.subscribe(() => {
        let component = <QuestionReasonModalComponent> ref.content;
        if (component.submit) {
          question.status = AgendaQuestionStatus.ADDED;
          question.reasonChange = component.reason;
          question.fio = this.session.fullName();
          question.login = this.session.login();
          this.cleanFields(question);
          this.included.push(question);
          this.candidates.splice(ind, 1);
          this.includedCount++;
        }
        subscription.unsubscribe();
      });
    } else {
      this.included.push(question);
      this.candidates.splice(ind, 1);
      this.includedCount++;
    }
  }

  getUpdates() {
    return this.included.filter(q => this.isUpdate(q)).sort((q1, q2) => {
      return (q1.dateChange ? q1.dateChange.getTime() : 0) - (q2.dateChange ? q2.dateChange.getTime() : 0)
    })
  }

  isUpdate(question: AgendaQuestion) {
    return question.status === AgendaQuestionStatus.ADDED || question.status === AgendaQuestionStatus.EXCLUDED;
  }

  isPrepared(questionId: string) {
    let question = this.questions[questionId];
    return this.hasMaterials(question, this.MATERIAL_TYPE_INQUIRIES) &&
      this.hasMaterial(question, this.MATERIAL_TYPE_PRESENTATION) && question.questionConsider.materialsChecked;
  }

  hasMaterial(question: Question, materialType: string) {
    return question.questionConsider.materials.some(group => {
      return group.materialType === materialType && group.document && group.document.length > 0;
    });
  }

  hasMaterials(question: Question, materialTypes: string[]) {
    return _.some(materialTypes, (materialType) => {
      return _.some(question.questionConsider.materials, (group: QuestionMaterialGroup) => {
        return group.materialType === materialType && group.document && group.document.length > 0;
      });
    });
  }

  canRestore(questionID: string): boolean {
    return this.restorables[questionID];
  }

  restore(ind: number) {
    const options = {
      initialState: {},
      'class': 'modal-lg'
    };
    const ref = this.modalService.show(QuestionReasonModalComponent, options);
    let subscription = this.modalService.onHide.subscribe(() => {
      let component = <QuestionReasonModalComponent> ref.content;
      if (component.submit) {
        this.included[ind].status = AgendaQuestionStatus.ADDED;
        this.included[ind].reasonChange = component.reason;
        this.included[ind].fio = this.session.fullName();
        this.included[ind].login = this.session.login();
        this.cleanFields(this.included[ind]);
        this.includedCount++;
      }
      subscription.unsubscribe();
    });
  }

  cleanFields(question: AgendaQuestion) {
    question.dateChange = null;
  }

  validate(skipQuestionsValidtion?: boolean) {
    if (!this.agenda.meetingType) {
      this.toastr.warning("Не выбран вид совещания");
      return false;
    }
    if (!this.agenda.meetingDate) {
      this.toastr.warning("Не выбрана дата совещания");
      return false;
    }
    if (!this.agenda.meetingTime || isNaN(this.agenda.meetingTime.getTime())) {
      this.toastr.warning("Не выбрано время начала заседания");
      return false;
    }
    if (!this.included.length && !skipQuestionsValidtion) {
      this.toastr.warning("Список вопросов повестки заседания не должен быть пустым.");
      return false;
    }
    return true;
  }

  save(questionsToCopy?: string[], stayInEditMode?: boolean, skipQuestionsValidtion: boolean = false) {
    if (!this.validate(skipQuestionsValidtion)) {
      return;
    }
    this.saveAttendees();
    return this.saveAgenda.emit({
      questions: this.included,
      questionsToCopy: questionsToCopy,
      stayInEditMode: stayInEditMode
    });
  }

  publish() {
    this.publishAgenda.emit(this.agenda.agendaID);
  }

  cancelEdit() {
    this.cancel.emit();
  }

  showCopyQuestionsDialog() {
    this.agendaService.getLastHeldQuestion(this.agenda.meetingType, this.agenda.meetingDate).toPromise().then(questions => {
      const options = {
        initialState: {
          questions: questions
        },
        'class': 'modal-lg'
      };
      return new Promise((resolve: (any) => void, reject) => {
        const ref = this.modalService.show(CopyQuestionsModalComponent, options);
        let subscription = this.modalService.onHide.subscribe(() => {
          let component = <CopyQuestionsModalComponent> ref.content;
          if (component.submit) {
            resolve(component.questions.filter((q, index) => component.checked[index]));
          }
          subscription.unsubscribe();
        });
      });
    }).then((result: { questionId: string }[]) => {
      return this.save(result.map(q => q.questionId), true, true);
    }).then(() => {
      this.loadQuestions(this.agenda.meetingType, this.agenda.meetingDate);
    });
  }

  prepareAttendees() {

    this.internalAttendees = [];
    this.externalAttendees = [];

    if (this.agenda.attended && this.agenda.attended.length) {

      this.agenda.attended.map((el) => {
        if (el.internal && el.internal === true) {
          this.internalAttendees.push({id: Math.random(), result: el, department: el.name});
        }
        else {
          this.externalAttendees.push(el);
        }
      });

      if (!this.internalAttendees.length) {
        this.internalAttendees.push({id: Math.random()});
      }

      if (!this.externalAttendees.length && !this.agenda.agendaID) {
        this.externalAttendees = this.meetingAttendees;
      }

    }
    else {
      this.internalAttendees.push({id: Math.random()});
      if (!this.agenda.agendaID) {
        this.externalAttendees = this.meetingAttendees;
      }
    }
    this.internalAttendees.forEach((att, ind) => this.addAttendeesInput(ind));
  }

  departmentsChange(department, attendee) {

    this.updatingUsersListInProgress = true;
    attendee.users = [];

    this.nsiResourceService.ldapUsers(null, department.description).then((users: any) => {
      attendee.users = users;
      attendee.result = {};
      this.updatingUsersListInProgress = false;
    });

  }

  searchAttendee(query: string) {
    return from(this.nsiResourceService.searchUsers({fio: query})).pipe(
      map((users: any[]) => {
        return users.map(user => ({
          accountName: user.accountName,
          post: user.post,
          iofShort: user.iofShort,
          fioFull: user.displayName,
          internal: true
        }))
      })
    );
  }

  internalAttendeeChange(user, attendee) {
    user.name = user.departmentFullName;
    user.code = user.departmentCode;
    user.internal = true;
    user.fioFull = user.displayName;
    attendee.result = user;
  }

  addInternalAttendee() {
    this.internalAttendees.push({id: Math.random()});
    this.addAttendeesInput(this.internalAttendees.length - 1);
  }

  removeInternalAttendee(attendee) {
    if (this.internalAttendees.length === 1) {
      this.internalAttendees[0] = {};
      return;
    }

    let index = this.internalAttendees.findIndex(att => att.id === attendee.id);
    this.internalAttendees.splice(index, 1);
    this.removeAttendeesInput(index);
  }

  saveAttendees() {
    this.agenda.attended = [];

    let internalAttendees = this.internalAttendees.filter((el) => {
      if (el.result) return el;
    }).map((el) => {
      return el.result;
    });

    this.agenda.attended = this.agenda.attended.concat(internalAttendees).concat(this.externalAttendees);
  }

  clearAttendees() {
    this.externalAttendees = [];
  }

  clearAttendeesInputs() {
    this.attendeesInput$ = [];
    this.attendeesLoading = [];
    this.attendees$ = [];
  }

  removeAttendeesInput(index) {
    this.attendeesInput$.splice(index, 1);
    this.attendeesLoading.splice(index, 1);
    this.attendees$.splice(index, 1);
  }

  addAttendeesInput(index) {
    this.attendeesInput$.push(new Subject<string>());
    this.attendeesLoading.push(false);
    this.attendees$.push(concat(
      of([]),
      this.attendeesInput$[index].pipe(
        debounceTime(300),
        distinctUntilChanged(),
        filter((term: string) => term && term.length > 2),
        tap(() => this.attendeesLoading[index] = true),
        switchMap((term: string) => this.searchAttendee(term)),
        catchError(() => of([])),
        tap(() => this.attendeesLoading[index] = false)
      )
    ))
  }
}
