import {ToastrService} from 'ngx-toastr';
import {Component, OnInit} from '@angular/core';
import {BsModalRef} from 'ngx-bootstrap';
import {NsiResourceService} from '@reinform-cdp/nsi-resource';
import {AlertService} from '@reinform-cdp/widgets';
import {catchError, map, mergeMap, tap} from 'rxjs/internal/operators';
import {Observable} from 'rxjs/Rx';
import {from, of, throwError} from 'rxjs/index';
import {MeetingTheme} from '../../../../models/nsi/MeetingTheme';
import {INsiSearchParams} from '@reinform-cdp/nsi-resource/dist/models/INsiSearchParams';

@Component({
  selector: 'add-theme-modal',
  templateUrl: './add-theme.modal.html'
})
export class AddThemeModalComponent implements OnInit {

  theme = '';
  submit: boolean;

  constructor(private bsModalRef: BsModalRef, private toastr: ToastrService,
              private nsiRestService: NsiResourceService, private alertService: AlertService) {
  }

  ngOnInit() {
    this.submit = false;
  }

  addTheme(theme: string, themes: MeetingTheme[]): Observable<string> {
    const findInExistingThemes = themes.find((th: any) => {
      return th.name === theme && (th.deleted === 0);
    });
    const findInDeletedThemes: any = themes.find((th: any) => {
      return th.name === theme.trim() && (th.deleted === 1);
    });
    const codes = themes.map(t => {
      return t.code;
    });
    const numbers: number[] = [0];
    codes.forEach(c => {
      try {
        const number = parseInt(c.substr(4)) || 0;
        numbers.push(number);
      } catch (error) {

      }
    });

    const max = Math.max(...numbers);
    if (findInExistingThemes) {
      this.toastr.error('Указанная тема уже существует!');
      return of('');
    } else {
        return from(this.nsiRestService.addNewItemToDict({
          'ValueDict': {
            'NickDict': 'mggt_meeting_Theme',
            'Element': {
              'Deleted': 0,
              'SortValue': `order${max + 1}`,
              'ElementParent': null,
              'Values': [{
                'NickAttr': 'code',
                'Deleted': 0,
                'Value': `соde${max + 1}`
              }, {
                'NickAttr': 'name',
                'Deleted': 0,
                'Value': theme.trim()
              }]
            }
          }
        })).pipe(
          map(response => {
            this.toastr.success('Указанная тема успешно добавлена в словарь!');
            return 'created';
          }), catchError(error => {
            this.toastr.error('Ошибка при добавлении темы!');
            return throwError(error);
          })
        );
      }
  }

  create() {
    if (this.theme.trim() === '') {
      this.toastr.warning(`Поле 'Тема' обязательно для заполнения!`);
    } else {
      from(this.nsiRestService.get('mggt_meeting_Theme', <INsiSearchParams>{deleted: true})).pipe(
        mergeMap(response => {
          return this.addTheme(this.theme, response);
        }),
        tap(response => {
          if (response === 'created') {
            this.submit = true;
            this.bsModalRef.hide();
          }
        })
      ).subscribe();
    }
  }

  cancel() {
    this.submit = false;
    this.bsModalRef.hide();
  }

}
