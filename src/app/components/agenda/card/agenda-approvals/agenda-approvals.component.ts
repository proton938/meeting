import {Component, Input, OnInit} from '@angular/core';
import { Protocol, ProtocolApprovalHistory } from './../../../../models/Protocol';
import { Agenda } from './../../../../models/Agenda';

@Component({
  selector: 'mggt-agenda-approvals',
  templateUrl: './agenda-approvals.component.html',
  styleUrls: ['./agenda-approvals.component.scss']
})
export class AgendaApprovalsComponent implements OnInit {

  @Input() agenda: Agenda;
  @Input() protocol: Protocol;
  cycleShow: { [key: string]: boolean } = {};

  constructor() {
  }

  ngOnInit() {
    if (!this.protocol.approvalHistory) {
      this.protocol.approvalHistory = new ProtocolApprovalHistory();
    }

    this.protocol.approvalHistory.approvalCycle = this.protocol.approvalHistory.approvalCycle.sort((a, b) => parseInt(b.approvalCycleNum) - parseInt(a.approvalCycleNum));
  }

}
