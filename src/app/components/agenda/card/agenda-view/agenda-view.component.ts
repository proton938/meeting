import { PlatformConfig } from '@reinform-cdp/core';
import { ICdpBpmProcessManagerConfig } from '@reinform-cdp/bpm-components';
import { StateService, Transition } from '@uirouter/core';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { LoadingStatus } from '@reinform-cdp/widgets';
import { SessionStorage } from '@reinform-cdp/security';
import { Agenda, AgendaQuestion } from '../../../../models/Agenda';
import { MeetingType } from '../../../../models/nsi/MeetingType';
import { ExternalAgree } from '../../../../models/nsi/ExternalAgree';
import { Question } from '../../../../models/Question';
import { FileType } from '../../../../models/FileType';
import { MeetingTypeService } from '../../../../services/meeting-type.service';
import { AuthorizationService } from '@reinform-cdp/security';
import { NsiResourceService } from '@reinform-cdp/nsi-resource';
import { SearchResultDocument } from '@reinform-cdp/search-resource';
import { CdpReporterPreviewService } from '@reinform-cdp/reporter-resource';
import { AgendaService } from '../../../../services/agenda.service';
import { QuestionService } from '../../../../services/question.service';
import * as _ from 'lodash';
import { formatAs } from '../../../../services/date-util.service';
import { tap } from 'rxjs/internal/operators';

@Component({
  selector: 'meeting-agenda-view',
  templateUrl: './agenda-view.component.html',
  styleUrls: ['./agenda-view.component.scss']
})
export class AgendaViewComponent implements OnInit {

  AGENDA_QUESTION_STATUS_EXCLUDED = 'excluded';
  AGENDA_QUESTION_STATUS_INCLUDED = 'included';
  AGENDA_QUESTION_STATUS_ADDED = 'added';

  MATERIAL_TYPE_INQUIRY_SSMKA = 'Справка СС МКА';
  MATERIAL_TYPE_INQUIRY_PZZ = 'Справка ПЗЗ';
  MATERIAL_TYPE_INQUIRY = 'Справка';
  MATERIAL_TYPE_INQUIRIES = ['Справка СС МКА', 'Справка ПЗЗ', 'Справка'];
  MATERIAL_TYPE_PRESENTATION = 'Презентация';
  MATERIAL_TYPE_GPZU_SCHEME = 'Чертеж ГПЗУ';
  MATERIAL_TYPE_OTHER = 'Дополнительные материалы';

  MEETING_TYPE_PZZ = 'PZZ';

  insideOrder: SearchResultDocument[];
  dicts: any = {
    statuses: [],
    themes: [],
    priorities: [],
    difficulties: [],
    executors: [],
    _themes: []
  };

  hasQuestionViewPermission: boolean;

  loadingStatus: LoadingStatus;
  success = false;

  @Input() agenda: Agenda;
  @Input() public reload: () => void;
  fileAgenda: FileType;
  meetingType: MeetingType;
  included: AgendaQuestion[];
  includedCount: number;
  added: string[];
  excluded: string[];
  lastChangeDate: Date;
  questions: { [key: string]: Question };
  @Output() public edit: EventEmitter<any> = new EventEmitter<any>();
  pzzExternalAgree: { [key: string]: ExternalAgree };
  externalAgree: any;
  regularExternal: boolean;

  activeTab = 0;

  processManagerConfig: ICdpBpmProcessManagerConfig;

  showMeetingHeld = false;
  showCreateProtocol = false;
  showMailNotification = false;
  showCopyExpressAgenda = false;
  showEditAgenda = false;
  showRescheduleAgenda = false;
  showCancelAgenda = false;
  showDeleteAgenda = false;
  showCreateInstruction = false;
  showProtocolActions = false;

  constructor(private $state: StateService,
    private nsiResourceService: NsiResourceService,
    private authorizationService: AuthorizationService,
    private meetingTypeService: MeetingTypeService,
    private agendaService: AgendaService,
    private transition: Transition,
    private reporterPreviewService: CdpReporterPreviewService,
    private platformConfig: PlatformConfig,
    private questionService: QuestionService,
    private session: SessionStorage,
  ) {
  }

  ngOnInit() {
    this.calculateActionPermissions();
    this.loadingStatus = LoadingStatus.LOADING;
    const id = this.transition.params()['id'];

    this.processManagerConfig = {
      sysName: this.platformConfig.systemCode.toLowerCase(),
      linkVarValue: this.agenda.agendaID
    };

    this.agendaService.getInsideAgenda(id).pipe(
      tap(response => {
        this.insideOrder = response;
        this.hasQuestionViewPermission = this.authorizationService.check('MEETING_QUESTION_CARD');
        this.questions = {};
        return Promise.all([
          this.loadDictionaries(),
          this.loadIncludedQuestions()
        ]).then(() => {
          return this.nsiResourceService.getDictsFromCache(['InstructionPriority', 'InstructionStatus']);
        }).then(response => {
          this.dicts.statuses = response.InstructionStatus;
          this.dicts.priorities = response.InstructionPriority;
        }).then(response => {
          this.loadingStatus = LoadingStatus.SUCCESS;
          this.success = true;
        }).catch((e) => {
          console.log(e);
          this.loadingStatus = LoadingStatus.ERROR;
        });
      })
    ).subscribe();
  }

  calculateActionPermissions() {
    const status = _.get(this, 'agenda.status.code', '');

    this.showMeetingHeld = false;
    this.showCreateProtocol = false;
    this.showMailNotification = false;
    this.showCopyExpressAgenda = false;
    this.showEditAgenda = false;
    this.showRescheduleAgenda = false;
    this.showCancelAgenda = false;
    this.showDeleteAgenda = false;
    this.showCreateInstruction = false;
    this.showProtocolActions = false;

    switch (status) {
      case 'assign':
        this.showMeetingHeld = true;
        this.showMailNotification = true;
        this.showCopyExpressAgenda = true;
        this.showEditAgenda = true;
        this.showRescheduleAgenda = true;
        this.showCancelAgenda = true;
        this.showDeleteAgenda = true;
        this.showCreateInstruction = true;
        break;
      case 'post':
        this.showCreateProtocol = true;
        this.showCopyExpressAgenda = true;
        this.showEditAgenda = true;
        this.showProtocolActions = true;
        this.showCreateInstruction = true;
        break;
      case 'refused':
        this.showCopyExpressAgenda = true;
        break;
      case 'approval':
        this.showCopyExpressAgenda = true;
        break;
      case 'approved':
        this.showCopyExpressAgenda = true;
        break;
      case '':
        this.showMeetingHeld = true;
        this.showCreateProtocol = true;
        this.showMailNotification = true;
        this.showCopyExpressAgenda = true;
        this.showEditAgenda = true;
        this.showRescheduleAgenda = true;
        this.showCancelAgenda = true;
        this.showDeleteAgenda = true;
        this.showCreateInstruction = true;
        this.showProtocolActions = true;
        break;
    }
  }

  loadDictionaries(): Promise<any> {
    return Promise.all([
      this.meetingTypeService.getMeetingType(this.agenda.meetingType),
      this.nsiResourceService.get('mggt_meeting_ExternalAgree')
    ]).then((result: [MeetingType, ExternalAgree[]]) => {
      this.meetingType = result[0];
      this.regularExternal = this.meetingType && !this.meetingType.Express && this.meetingType.meetingExternal;

      this.pzzExternalAgree = {};
      result[1].forEach(p => {
        this.pzzExternalAgree[p.code] = p;
      });
      return true;
    });
  }

  loadIncludedQuestions(): Promise<any> {
    return Promise.all([
      this.agendaService.includedQuestions(this.agenda.agendaID).toPromise(),
      this.agendaService.includedQuestionsCount(this.agenda.agendaID).toPromise()
    ]).then((result: [AgendaQuestion[], number]) => {
      this.included = result[0].filter(inc => inc);
      this.includedCount = result[1];
      return Promise.all(
        this.included.map(q => this.questionService.get(q.questionId).toPromise())
      );
    }).then(wrappers => {
      wrappers.forEach(wrapper => {
        const question: Question = wrapper.document.question;
        if (question.questionConsider && question.questionConsider.externalAgree) {
          question.questionConsider.externalAgree.forEach((externalAgree: any) => {
            externalAgree.hasComment = _.find(question.comments, { 'author': { 'organizationCode': externalAgree.organizationCode } });
          });
        }
        this.questions[question.questionID] = question;
      });

      this.lastChangeDate = _.chain(this.included).map((question) => {
        return question.dateChange;
      }).max().value();
      this.added = _.chain(this.included).filter((question) => {
        return question.status === this.AGENDA_QUESTION_STATUS_ADDED;
      }).map((question) => {
        return question.itemNumber;
      }).value();
      this.excluded = _.chain(this.included).filter((question) => {
        return question.status === this.AGENDA_QUESTION_STATUS_EXCLUDED;
      }).map((question) => {
        return question.itemNumber;
      }).value();

    });
  }

  createReport() {
    this.agendaService.createReport(this.agenda.agendaID).toPromise().then(() => {
      this.reload();
    });
  }

  getMaterialGroupDocuments(question: Question, materialType: string) {
    if (!question.questionConsider || !question.questionConsider.materials) {
      return [];
    }
    const materialGroup = question.questionConsider.materials.find(materialGroup => {
      return materialGroup.materialType === materialType;
    });
    return materialGroup ? materialGroup.document : [];
  }

  getMaterialGroupsDocuments(question: Question, materialTypes: string[]) {
    if (!question.questionConsider || !question.questionConsider.materials) {
      return [];
    }
    const materialGroup = question.questionConsider.materials.find(materialGroup => {
      return _.indexOf(materialTypes, materialGroup.materialType) >= 0;
    });
    return materialGroup ? materialGroup.document : [];
  }

  getFileLink(id) {
    return `/filestore/v1/files/${id}?systemCode=sdo`;
  }

  previewPzzApproval() {
    return this.agendaService.get(this.agenda.agendaID).toPromise().then(wrapper => {
      const fileName = 'Лист согласования ПЗЗ на ' + formatAs(this.agenda.meetingDate, 'dd.MM.yyyy') + '.docx';
      this.reporterPreviewService.nsi2Preview({
        options: {
          jsonSourceDescr: '',
          onProcess: 'Сформировать лист согласования',
          placeholderCode: '',
          strictCheckMode: true,
          xwpfResultDocumentDescr: fileName,
          xwpfTemplateDocumentDescr: ''
        },
        jsonTxt: JSON.stringify(wrapper),
        rootJsonPath: '$',
        nsiTemplate: {
          templateCode: 'ssPZZApproval'
        }
      });
    });
  }

  editAgenda() {
    this.edit.emit(true);
  }

  createProtocol() {
    this.$state.go('app.meeting.protocol', {
      id: null,
      agendaID: this.agenda.agendaID,
      editing: true
    });
  }

  renumberQuestions() {
    this.agendaService.renumberQuestions(this.agenda.agendaID).toPromise().then(() => {
      this.loadIncludedQuestions();
    });
  }

  isActive(index: number): boolean {
    return index === this.activeTab;
  }

  activate(index: number): boolean {
    if (this.activeTab === index) {
      return false;
    } else {
      this.activeTab = index;
      return true;
    }
  }

  //TODO: voting
  // createGkVoting() {
  //   let protocolId: string = null;
  //   this.agendaResource.generateGkVoting({}, {
  //     id: this.agenda.agendaID
  //   }, (protocolDocumentWrapper: ProtocolDocumentWrapper) => {
  //     let protocol : Protocol = protocolDocumentWrapper.document.protocol;
  //     this.protocolTaskService.getProcessDefinition('ss_approval_gk')
  //       .then((process: oasiBpmRest.IProcessDefinition) => {
  //         return this.protocolTaskService.initProcess(
  //           process.id,
  //           this.protocolTaskService.getInitProcessProperties(protocol, this.session.login())
  //         );
  //       }).then((id) => {
  //       protocol.bpmProcess = new ProtocolProcess();
  //       protocol.bpmProcess.bpmProcessId = id;
  //       protocolId = protocol.protocolID;
  //       return this.protocolResource.update({
  //         id: protocolId
  //       }, {
  //         document: {
  //           protocol
  //         }
  //       });
  //     }).then(() => {
  //       this.getTaskId(protocol.bpmProcess.bpmProcessId).then((taskId) => {
  //         this.$state.go("app.ssmka.process.RegisterSS", {
  //           documentId: protocolId,
  //           taskId
  //         });
  //       });
  //     });
  //   });
  // }
  //
  // getTaskId(processId: any) {
  //   let deferred = this.$q.defer();
  //   this.activityRestService.getTasks({
  //     processInstanceId: processId
  //   }).then((result: any) => {
  //     deferred.resolve(result.data[0].id);
  //   }, () => {
  //     deferred.reject();
  //   });
  //   return deferred.promise;
  // }

}
