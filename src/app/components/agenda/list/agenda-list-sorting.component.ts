import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Sorting} from "../../../models/Sorting";

@Component({
  selector: 'meeting-agenda-list-sorting',
  templateUrl: './agenda-list-sorting.component.html',
  styleUrls: ['./agenda-list-sorting.component.scss']
})
export class AgendaListSortingComponent implements OnInit {

  @Input() sorting: Sorting;
  @Output() sortingChanged: EventEmitter<any> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  change(sortingName: string, sortingValue: string, sortingText: string, hidden?: string[]) {
    this.sorting = new Sorting(sortingName, sortingValue, sortingText, hidden);
    this.sortingChanged.emit(this.sorting);
  }

  changeValue() {
    if (this.sorting) {
      this.sorting.value = this.sorting.value === 'asc' ? 'desc' : 'asc';
      this.sortingChanged.emit(this.sorting);
    }
  }

}
