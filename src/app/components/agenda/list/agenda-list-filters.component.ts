import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Filter} from "./agenda-list.component";
import {BsLocaleService} from "ngx-bootstrap/datepicker";
import {MeetingType} from "../../../models/nsi/MeetingType";
import {MeetingTypeService} from "../../../services/meeting-type.service";

@Component({
  selector: 'meeting-agenda-list-filters',
  templateUrl: './agenda-list-filters.component.html',
  styleUrls: ['./agenda-list-filters.component.scss']
})
export class AgendaListFiltersComponent implements OnInit {

  @Input() filter: Filter;
  @Output() filterChanged: EventEmitter<any> = new EventEmitter();
  @Input() isExternal: boolean = false;

  meetingHeld: {meetingHeld: string, meetingHeldName: string}[];
  meetingTypes: MeetingType[];

  constructor(private meetingTypeService: MeetingTypeService,
              private localeService: BsLocaleService) {
  }

  ngOnInit() {
    this.localeService.use('ru');
    this.loadDictionaries();
  }

  loadDictionaries(): Promise<any> {
    this.meetingHeld = [{
      meetingHeld: 'false',
      meetingHeldName: 'Планируется'
    }, {
      meetingHeld: 'true',
      meetingHeldName: 'Проведено'
    }];
    return Promise.all([
      this.isExternal ? this.meetingTypeService.getNonExpressExternalMeetingTypes()
       : this.meetingTypeService.getNonExpressMeetingTypes()
    ]).then((result: [MeetingType[]]) => {
      this.meetingTypes = result[0];
    })
  }

  search() {
    this.filterChanged.emit(this.filter);
  }

  _onDateChange() {}


  clear() {
    this.filter.meetingHeld = null;
    this.filter.meetingDate.clear();
    this.filter.meetingTypes = [];
    this.filter.meetingNumber = null;
  }
}
