import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MeetingInstructionComponent } from './meeting-instruction.component';

describe('MeetingInstructionComponent', () => {
  let component: MeetingInstructionComponent;
  let fixture: ComponentFixture<MeetingInstructionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MeetingInstructionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MeetingInstructionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
