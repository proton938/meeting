import * as _ from "lodash";
import {SearchResultDocument} from "@reinform-cdp/search-resource";
import {Component, Input, OnInit} from '@angular/core';
import {formatDate} from "@angular/common";

@Component({
  selector: 'mggt-meeting-instruction',
  templateUrl: './meeting-instruction.component.html',
  styleUrls: ['./meeting-instruction.component.scss']
})
export class MeetingInstructionComponent implements OnInit {

  @Input() statuses: any[];
  @Input() instructions: SearchResultDocument[];

  statusColorMap: { [key: string] : string};

  constructor() {
  }


  ngOnInit() {
    this.instructions = _.orderBy(this.instructions, 'docDateInst')
    this.statusColorMap = {};
    this.statuses.forEach(st => this.statusColorMap[st.name] = st.color);
  }

  getDate(val: any) {
    let date: Date = null;
    if (typeof val === 'string') {
      date = new Date(val);
    } else if (_.isDate(val)) {
      date = <Date> val;
    }
    return date;
  }

  formatDate(val: any) {
    let date = this.getDate(val);
    if (date) {
      return formatDate(date, 'dd.MM.yyyy', 'en');
    }
  }

  formatDateTime(val: any) {
    let date = this.getDate(val);
    if (date) {
      return formatDate(date, 'dd.MM.yyyy HH:mm', 'en');
    }
  }

  outDated(planDate) {
    let date = this.getDate(planDate);
    let startOfDay = new Date();
    return date < startOfDay;
  }

  isOutdated(instruction: SearchResultDocument) {
    if (instruction.reportDateInst) {
      return false;
    }
    let date = this.getDate(instruction.planDateInst);
    let startOfDay = new Date();
    startOfDay.setHours(0, 0, 0, 0);
    return date < startOfDay;
  }


}
