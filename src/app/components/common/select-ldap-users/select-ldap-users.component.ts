import * as _ from "lodash";
import {ToastrService} from "ngx-toastr";
import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {NsiResourceService, UserBean} from "@reinform-cdp/nsi-resource";
import {concat, from, of, throwError} from "rxjs/index";
import {catchError, debounceTime, distinctUntilChanged, map, switchMap, tap} from "rxjs/internal/operators";
import {Observable, Subject} from "rxjs/Rx";

@Component({
  selector: 'mggt-select-ldap-users',
  templateUrl: './select-ldap-users.component.html',
  styleUrls: ['./select-ldap-users.component.scss']
})
export class SelectLdapUsersComponent implements OnInit {

  @Input() model: UserBean | UserBean[];
  @Output() modelChange: EventEmitter<UserBean | UserBean[]> = new EventEmitter<UserBean | UserBean[]>();
  @Input() multiple: boolean;
  @Input() disabled: boolean;
  @Input() placeholder: string;

  users$: Observable<UserBean[]>;
  usersLoading = false;
  usersInput$ = new Subject<string>();

  constructor(private toastr: ToastrService, private nsiRestService: NsiResourceService) {
  }

  ngOnInit() {
    this.users$ = concat(
      of([]),
      this.usersInput$.pipe(
        debounceTime(100),
        distinctUntilChanged(),
        tap(() => this.usersLoading = true),
        switchMap(term => this.search(term).pipe(
          catchError(() => of([])),
          tap(() => this.usersLoading = false)
        ))
      )
    );
  }

  onChange(data: UserBean | UserBean[]) {
    this.modelChange.emit(data);
  }

  search(search: string): Observable<UserBean[]> {
    if (search && search.length > 2) {
      return from(this.nsiRestService.searchUsers({fio: search})).pipe(
        map(_response => {
          return _.orderBy(_response, 'displayName');
        }), catchError(error => {
          this.toastr.error('Ошибка при получении информации о пользователях!');
          return throwError(error);
        })
      );
    } else {
      return of([]);
    }
  }
}
