import {Component, Input} from '@angular/core';
import {BsModalRef} from 'ngx-bootstrap';
import {FileResourceService} from '@reinform-cdp/file-resource';
import {FileType} from "../../../models/FileType";

@Component({
  selector: 'attach-file-modal',
  templateUrl: './attach-file-modal.component.html'
})
export class AttachFileModalComponent {
  @Input() title: string = 'Загрузка файла';
  @Input() okBtnText: string = 'Сохранить';
  @Input() folderId: string = '';
  @Input() fileType: string = 'default';
  @Input() fileTitle: string = 'Файл';
  @Input() extPattern: string = 'pdf';
  @Input() saveCallback: (file: FileType) => void;
  @Input() cancelCallback: (file: FileType) => void;

  files: any[] = [];

  constructor(private bsModalRef: BsModalRef,
              private fileService: FileResourceService) {}

  onRemoveFile(file) {
    this.fileService.deleteFile(file.idFile);
  }

  save() {
    if (this.saveCallback) this.saveCallback(this.getFile());
    this.bsModalRef.hide();
  }

  cancel() {
    if (this.cancelCallback) this.cancelCallback(this.getFile());
    this.bsModalRef.hide();
  }

  getFile(): FileType {
    if (!this.files || !this.files.length) {
      return null;
    }
    let f = this.files[0];
    return FileType.create(f.idFile, f.nameFile, f.sizeFile, f.signed, new Date(f.dateFile).toISOString(), f.typeFile, f.classFile);
  }
}
