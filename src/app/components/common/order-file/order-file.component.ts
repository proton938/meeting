import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {DomSanitizer, SafeUrl} from '@angular/platform-browser';
import * as fs from 'file-saver';
import { ExFileType} from '@reinform-cdp/widgets';

@Component({
  selector: 'mggt-order-file',
  templateUrl: './order-file.component.html',
  styleUrls: ['./order-file.component.scss']
})
export class OrderFileComponent implements OnInit {

  @Input() file: ExFileType;
  extension: string;
  name: string;
  size: string;
  // date: Date;
  @Output() onDelete: EventEmitter<ExFileType> = new EventEmitter<ExFileType>();
  availableExtensions: string[] = ['csv', 'doc', 'docx', 'dwg', 'jpg', 'jpeg', 'pdf', 'png', 'rar', 'sig', 'tiff', 'tif', 'txt', 'xml', 'zip', 'ppt', 'pptx'];

  link: SafeUrl;

  constructor(private sanitizer: DomSanitizer) {
  }

  ngOnInit() {
    let correctExtension = false;
    this.extension = this.file.nameFile.split('.').pop().toLowerCase();
    this.availableExtensions.forEach(ex => {
      if (ex === this.extension)
        correctExtension = true;
    });
    if (!correctExtension)
      this.extension = 'unknown';

    this.name = this.file.nameFile;
    let sizeKB = this.file.sizeFile / 1024.0;
    let isKB = sizeKB < 1024.0;
    this.size = (isKB) ? sizeKB.toFixed(1).toString() + ' Кб' : (sizeKB / 1024.0).toFixed(2).toString() + ' Мб';
    // this.date = this.file.lastModifiedDate;

    this.link = this.sanitizer.bypassSecurityTrustUrl(window.URL.createObjectURL(this.file));
  }

  save(file: ExFileType) {
    fs.saveAs(<Blob>file, file.nameFile);
  }

  delete(file: ExFileType) {
    this.onDelete.emit(file);
  }

}
