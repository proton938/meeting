import { Component, Inject, Input, OnInit } from '@angular/core';
import { PlatformConfig } from "@reinform-cdp/core";

@Component({
    selector: '[mggt-approval-list-row]',
    templateUrl: './row.component.html'
})
export class ApprovalRowComponent implements OnInit {

    @Input() list: any[];
    @Input() level: any;
    @Input() withAdditional: boolean = false;
    @Input() approvalTypes: { [key: string]: any };
    isShowAdditionalApprovals: boolean = false;
    constructor(@Inject('$localStorage') public $localStorage: any, private platformConfig: PlatformConfig) { }

    ngOnInit() {
        this.isShowAdditionalApprovals = this.withAdditional;
    }

    toggle() {
        this.isShowAdditionalApprovals = !this.isShowAdditionalApprovals;
    }

    getFileLink(id) {
        return `/filestore/v1/files/${id}?systemCode=${this.platformConfig.systemCode}`;
      }
    
      getApprovalResultColor(approvalTypeCode: string, approvalResult: string) {
        if (this.approvalTypes) {
          let approvalType = this.approvalTypes[approvalTypeCode];
          if (approvalType) {
            if (approvalResult === approvalType.buttonYes) {
              return 'label-' + approvalType.buttonYesColor;
            } else if (approvalResult === approvalType.buttonNo) {
              return 'label-' + approvalType.buttonNoColor;
            }
          }
        }
      }
}
