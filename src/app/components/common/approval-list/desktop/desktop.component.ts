import * as _ from "lodash";
import { Component, Input, OnInit } from '@angular/core';
import { FileResourceService } from "@reinform-cdp/file-resource";
import { NsiResourceService } from "@reinform-cdp/nsi-resource";
import { PlatformConfig } from "@reinform-cdp/core";
import { from } from "rxjs/index";
import { copy } from "angular";

@Component({
  selector: 'mggt-desktop-approval-list',
  templateUrl: './desktop.component.html'
})
export class DesktopApprovalListComponent implements OnInit {

  @Input() list: any;
  approvalTree = [];
  approvalTypes: { [key: string]: any };
  copiedList = [];

  constructor(private fileResourceService: FileResourceService,
    private nsiResourseService: NsiResourceService,
    private platformConfig: PlatformConfig) {
  }

  ngOnInit() {
    this.copiedList = copy(this.list.agreed)
    from(this.nsiResourseService.get('mggt_order_ApprovalType')).subscribe((approvalTypes: any[]) => {
      this.approvalTypes = {};
      _.each(approvalTypes, at => {
        this.approvalTypes[at.approvalTypeCode] = at;
      });
      this.initTree();
    });
  }

  initTree() {
    let firstLevel = this.copiedList.filter(item => {
      return !this.copiedList.some(i => i.added && i.added.some(added => added.additionalAgreed.accountName === item.agreedBy.accountName));
    });
    this.copiedList = this.copiedList.filter(item => firstLevel.some(i => i.agreedBy.accountName !== item.agreedBy.accountName));
    this.approvalTree = firstLevel;
    this.approvalTree.forEach(item => {
      if (item.added) {
        item.items = this.getAdditionalApprovals(item.added);
      }
    });
  }

  getAdditionalApprovals(list) {
    let items = [];
    let level = [];
    items = list.map(item => {
      return this.copiedList.find(i => i.agreedBy.accountName === item.additionalAgreed.accountName);
    });
    level = level.concat(items);
    this.copiedList = this.copiedList.filter(item => level.some(i => i.agreedBy.accountName !== item.agreedBy.accountName));

    items.forEach(item => {
      if (item.added) {
        item.items = this.getAdditionalApprovals(item.added);
      }
    });

    return items;
  }

  getFileLink(id) {
    return `/filestore/v1/files/${id}?systemCode=${this.platformConfig.systemCode}`;
  }

  getApprovalResultColor(approvalTypeCode: string, approvalResult: string) {
    if (this.approvalTypes) {
      let approvalType = this.approvalTypes[approvalTypeCode];
      if (approvalType) {
        if (approvalResult === approvalType.buttonYes) {
          return 'label-' + approvalType.buttonYesColor;
        } else if (approvalResult === approvalType.buttonNo) {
          return 'label-' + approvalType.buttonNoColor;
        }
      }
    }
  }

}
