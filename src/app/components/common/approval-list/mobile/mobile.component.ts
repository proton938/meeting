import {PlatformConfig} from "@reinform-cdp/core";
import {Component} from '@angular/core';
import {DesktopApprovalListComponent} from "../desktop/desktop.component";
import {FileResourceService} from "@reinform-cdp/file-resource";
import {NsiResourceService} from "@reinform-cdp/nsi-resource";

@Component({
  selector: 'mggt-mobile-approval-list',
  templateUrl: './mobile.component.html'
})
export class MobileApprovalListComponent extends DesktopApprovalListComponent {

  constructor(fileResourceService: FileResourceService,
              nsiResourseService: NsiResourceService,
              platformConfig: PlatformConfig) {
    super(fileResourceService, nsiResourseService, platformConfig);
  }

}
