import {Component, Inject, Input, OnInit} from '@angular/core';

@Component({
  selector: 'mggt-approval-list',
  templateUrl: './approval-list.component.html'
})
export class ApprovalListComponent implements OnInit {

  @Input() list: any;

  constructor(@Inject('$localStorage') public $localStorage: any) { }

  ngOnInit() {
  }

}
