import {Component, Input, OnInit} from '@angular/core';
import {BsModalRef} from "ngx-bootstrap";

@Component({
  selector: 'meeting-modal-content',
  templateUrl: './rename-file.modal.html'
})
export class RenameFileModalComponent implements OnInit {

  @Input() oldName: string;
  newName: string = '';
  extension: string;

  submit: boolean = false;

  constructor(private bsModalRef: BsModalRef) {
  }

  ngOnInit() {
    let arr = this.oldName.split('.');
    this.extension = arr[arr.length - 1];
  }

  rename() {
    this.submit = true;
    this.bsModalRef.hide();
  }

  cancel() {
    this.bsModalRef.hide();
  }

}
