import {Component, Input, OnInit} from '@angular/core';
import {Question} from "../../models/Question";

@Component({
  selector: 'meeting-question-address',
  templateUrl: './question-address.component.html'
})
export class QuestionAddressComponent implements OnInit {

  @Input() question: Question;
  address: string;
  prefectDistrictString: string;

  constructor() {
  }

  ngOnInit() {
    this.address = this.question.questionConsider.address
    let arr: string[] = [];
    if (this.question.questionConsider.prefect) {
      arr = arr.concat(this.question.questionConsider.prefect);
    }
    if (this.question.questionConsider.district) {
      arr = arr.concat(this.question.questionConsider.district);
    }
    this.prefectDistrictString = arr.length > 0 ? '(' + arr.join(', ') + ')' : '';
  }

}
