import {Component, Input} from '@angular/core';

@Component({
  selector: 'meetings-question-decision',
  templateUrl: './question-decision.component.html'
})
export class QuestionDecisionComponent {

  @Input() decision: string;
  @Input() decisionCode: string;
  @Input() small: boolean;

}
