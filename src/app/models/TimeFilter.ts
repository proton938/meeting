export class TimeFilter {
   time: Date;

   constructor() {
      this.time = undefined;
   }

   clear() {
      this.time = undefined;
   }

   isEmpty(): boolean {
      return !this.time;
   }

   toString(): string {
      let hours = this.time.getHours() ? this.time.getHours() : '00';
      let minutes = this.time.getMinutes() ? this.time.getMinutes() : '00';
      let formattedTime = (this.time) ? hours + ':' + minutes : '*';
      return `${formattedTime}`;
   }
}
