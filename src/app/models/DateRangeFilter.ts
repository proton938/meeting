import { formatDate } from "@angular/common";

export class DateRangeFilter {
   from: Date;
   to:Date;

   constructor() {
      this.from = undefined;
      this.to = undefined;
   }

   clear() {
      this.from = undefined;
      this.to = undefined;
   }

   isEmpty(): boolean {
      return ((this.from === undefined) && (this.to === undefined));
   }

   toString(): string {
      let formatedMinDate = (this.from) ? `${formatDate(this.from, "yyyy-MM-dd", 'en')}T00:00:00Z` : '*';
      let formatedMaxDate = (this.to) ? `${formatDate(this.to, "yyyy-MM-dd", 'en')}T00:00:00Z` : '*';
      return `[${formatedMinDate} TO ${formatedMaxDate}]`;
   }
}
