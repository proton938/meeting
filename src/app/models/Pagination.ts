class Pagination {
    totalPages: number;
    pageNumber: number;
    pageSize: number;
}