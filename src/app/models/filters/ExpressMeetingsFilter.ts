import * as _ from 'lodash';
import {DateRangeFilter} from '../DateRangeFilter';

export class ExpressMeetingsFilter {

  meetingDate: DateRangeFilter = new DateRangeFilter();
  meetingType: string[] = [];
  // meetingNumber: string[] = [];
  meetingTheme: string[] = [];
  meetingNumber: string;
  authorCreateName: string;
  initiatorName: string;
  statusName: string[] = [];

  clear() {
    this.meetingDate.clear();
    this.meetingType = [];
    // this.meetingNumber = [];
    this.meetingTheme = [];
    this.meetingNumber = null;
    this.authorCreateName = null;
    this.initiatorName = null;
    this.statusName = [];
  }

  isEmpty(): boolean {
    return this.meetingDate.isEmpty() &&
      this.meetingType.length === 0 &&
      // this.meetingNumber.length === 0 &&
      this.meetingTheme.length === 0 &&
      !this.meetingNumber &&
      !this.authorCreateName &&
      !this.initiatorName &&
      this.statusName.length === 0;
  }

  count(): number {
    return (this.meetingDate.isEmpty() ? 0 : 1) +
      (this.meetingType.length === 0 ? 0 : 1) +
      // (this.meetingNumber.length === 0 ? 0 : 1) +
      (this.meetingTheme.length === 0 ? 0 : 1) +
      (!this.meetingNumber ? 0 : 1) +
      (!this.authorCreateName ? 0 : 1) +
      (!this.initiatorName ? 0 : 1) +
      (this.statusName.length === 0 ? 0 : 1);
  }

  toString(): string {
    let result = '';

    if (!this.meetingDate.isEmpty()) {
      result += (result) ? ' AND ' : '';
      result += `(meetingDate:${this.meetingDate.toString()})`;
    }

    if (this.meetingType.length) {
      const contents = this.meetingType.map(el => 'meetingType:(' + el + ')');
      const content = '(' + contents.join(' OR ') + ')';
      result += (result) ? ' AND ' : '';
      result += `${content}`;
    }

    // if (this.meetingNumber.length) {
    //   const contents = this.meetingNumber.map(el => 'meetingNumber:(' + el + ')');
    //   const content = '(' + contents.join(' OR ') + ')';
    //   result += (result) ? ' AND ' : '';
    //   result += `${content}`;
    // }

    if (this.meetingTheme.length) {
      const contents = this.meetingTheme.map(el => {
        return 'meetingThemeName:(*' + el.split(/\s+/).join('* AND *') + '*)';
      });
      const content = '(' + contents.join(' OR ') + ')';
      result += result ? ' AND ' : '';
      result += content;
    }

    if (this.meetingNumber) {
      const contents = _.map(this.meetingNumber.split(' '), v => {
        return v.trim();
      });
      const content = contents.join('\\\\ ');
      result += result ? ' AND ' : '';
      result += `meetingNumber:(*${content}*)`;
    }

    if (this.authorCreateName) {
      // const contents = _.map(this.authorCreateName.split(/\s+/), v => {
      //   return v.trim();
      // });
      // const content = contents.join('* AND *');
      result += result ? ' AND ' : '';
      result += `authorCreateFioFull:(${this.authorCreateName})`;
    }

    if (this.initiatorName) {
      // const contents = _.map(this.initiatorName.split(/\s+/), v => {
      //   return v.trim();
      // });
      // const content = contents.join('* AND *');
      result += result ? ' AND ' : '';
      result += `initiatorFioFull:(${this.initiatorName})`;
    }

    if (this.statusName.length) {
      const contents = this.statusName.map(el => 'statusName:(*' + el.split(/\s+/).join('* AND *') + '*)');
      const content = contents.join(' OR ');
      result += result ? ' AND ' : '';
      result += content;
    }

    if (result) {
      return `${result}`;
    }

    return '';

  }

}
