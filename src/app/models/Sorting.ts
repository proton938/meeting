export class Sorting {
  name: string;
  value: string;
  text: string;
  hidden: string[];

  constructor(name: string, value: string, text: string, hidden?: string[]) {
    this.name = name;
    this.value = value;
    this.text = text;
    this.hidden = hidden;
  }
}
