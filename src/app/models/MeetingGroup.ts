class MeetingGroup<T> {
    totalElements: number;
    groupName: string;
    content: T[];
}

class PresentationPrepareGroup {
    totalElements: number;
    groupName: string;
    content: MeetingGroup<string>[];
}

class DecisionGroup {
    totalElements: number;
    decisionTypeCode: string;
    decisionType: string;
    content: string[];
}