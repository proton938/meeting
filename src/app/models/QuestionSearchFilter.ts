class QuestionSearchFilter {
    address: string;
    prefect: string;
    cadastralNumber: string;
    basisNumber: string;
    gpzuNumber: string;
    customer: string;
    rightHolder: string;
    departmentPrepare: string;
    contractorPrepare: string;
    presentationPrepare: string;
    questionCategory: string;
    questionStatus: boolean;
    considerationDate: DateRange;
    decisionType: string;
    meetingTypes: string[];
}

class DateRange {
    startDate: Date;
    endDate: Date;
}