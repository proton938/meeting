export class DemoDocument {
   document: Autotest;
}
export class Autotest {
   documentID: string;
   folderGUID: string;
   main: TestMain;

  constructor() {
    this.documentID = '';
    this.folderGUID = '';
    this.main = new TestMain();
  }
}

export class TestMain {
  regNumber: string;
  testName: string;
  testVersion: string;
  startDate: string;
  systemCode: { name: string };
  isFullTest: string;

  constructor() {
    this.regNumber = '';
    this.testName = '';
    this.testVersion = '';
    this.startDate = '2018-09-07'/*new Date().toISOString()*/;

    this.systemCode = {name: 'Демо'};
    this.isFullTest = 'отсутствует';
  }
}
