import {SessionStorage} from "@reinform-cdp/security";
import {OasiShowcaseTabModel} from "../OasiShowcaseTabModel";

export class OasiShowcaseMKAMeetingsTabBuilder {

  constructor(private session: SessionStorage,
              private meetingTypes: any,
              private oasiInstructionExecutors: any) {
  }

  build(): OasiShowcaseTabModel[] {
    let result: OasiShowcaseTabModel[] = [];

    result.push({
      alias: 'adminMeetingsAll',
      title: 'Административные совещания МКА',
      filter: this.buildAdminMeetingsAllFilter()
    });

    return result;
  }

  private isAssistantView() {
    let result = false;

    this.oasiInstructionExecutors.forEach(e => {
      if (e.assistantview && e.assistantview.length && e.assistantview.length > 0) {
        e.assistantview.forEach(v => {
          if (this.session.login() === v) {
            result = true;
          }
        });
      }
    });

    return result;
  }

  private buildAdminMeetingsAllFilter() {
    let result = this.getMeetingTypesFilter();
    return '(' + result + ')';
  }

  private buildMyAdminMeetingsFilter() {
    let result = this.getMeetingTypesFilter();
    result = result + ' AND (initiatorAccountName:(' + this.session.login() + '))';
    return '(' + result + ')';
  }

  private buildParticipantAdminMeetingsFilter() {
    let result = this.getMeetingTypesFilter();
    result = result + ' AND (attendedLogin:(' + this.session.login() + '))';
    return '(' + result + ')';
  }

  private buildMyDirectorAdminMeetingsFilter() {
    let result = this.getMeetingTypesFilter();
    let oasiInstructionExecutors = this.getOasiInstructionExecutors().map((el) => {
      return 'initiatorAccountName:(' + el + ')'
    }).join(' OR ');
    return '(' + result + ' AND (' + oasiInstructionExecutors + '))';
  }

  private buildMyDirectorParticipantAdminMeetingsFilter() {
    let result = this.getMeetingTypesFilter();
    let oasiInstructionExecutors = this.getOasiInstructionExecutors().map((el) => {
      return 'attendedLogin:(' + el + ')'
    }).join(' OR ');
    return '(' + result + ' AND (' + oasiInstructionExecutors + '))';
  }

  private getMeetingTypesFilter() {

    let meetingTypes = this.meetingTypes.map(el => 'meetingType:(' + el.meetingType + ')');

    return '(' + meetingTypes.join(' OR ') + ')';
  }

  private getOasiInstructionExecutors() {

    let oasiInstructionExecutors = this.oasiInstructionExecutors.filter((el) => {
      if (el.assistantview && el.assistantview.indexOf(this.session.login()) !== -1) {
        return el;
      }
    }).map((el) => el.creator);

    return oasiInstructionExecutors;

  }

}
