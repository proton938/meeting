export class OasiShowcaseTabModel {
   alias: string;
   title: string;
   filter: string;

   constructor(alias: string, title: string, filter: string) {
      this.alias = alias;
      this.title = title;
      this.filter = filter;
   }
}
