import {FileType} from "./FileType";
import {DocumentFromMKA} from "./DocumentFromMKA";

export class AgendaDocumentWrapper {
  document: AgendaDocument;
}

export class AgendaDocument {
  agenda: Agenda;
}

export class Agenda {
  agendaID: string;
  meetingType: string;
  meetingFullName: string;
  meetingShortName: string;
  meetingTheme: string;
  meetingCodeTheme: string;
  meetingDate: Date;
  meetingDateHistory: AgendaMeetingDateHistory[];
  meetingNumber: string;
  meetingPlace: string;
  meetingPlaceCode: string;
  meetingPlaceHistory: AgendaMeetingPlaceHistory[];
  meetingTime: Date;
  meetingTimeEnd: Date;
  meetingTimeHistory: AgendaMeetingTimeHistory[];
  meetingTimeEndHistory: AgendaMeetingTimeEndHistory[];
  meetingHeld: boolean;
  mailing: boolean;
  mailingInfo: AgendaMailingInfo[];
  agendaPublished: Date;
  fileAgenda: FileType;
  authorCreate?: AgendaUser;
  initiator?: AgendaUser;
  attended: AgendaAttended[] = [];
  attendedHistory: AgendaAttendedHistory[];
  filesAttach: FileType[];
  folderId: string;
  review?: AgendaReview[];
  receivedFromMKA: DocumentFromMKA;
  status: AgendaStatus;
  outlookId: string;
  protocol: FileType[];
}

export class AgendaStatus {
  code: string;
  name: string;
  color: string;
}

export class AgendaReview {
  reviewBy: AgendaUserType; // Ознакомился
  factReviewBy: AgendaUserType; // Ознакомился фактически (делегант)
  sentReviewBy: AgendaUserType; // Кто отправил на ознакомление
  reviewPlanDate: Date = null; //Плановая дата ознакомления
  reviewFactDate: Date = null; //Фактическая дата ознакомления
  reviewNote: string = '' //Комментарий
}

export class AgendaUserType {
  accountName: string; //Логин
  fio: string; // ФИО
  post: string; // должность
  email: string; //E-mail
  phoneNumber: string; //Номер телефона
  iofShort: string; //ИОФ краткое в именительном падеже
}


export class AgendaAttended {
  attendedNum?: number;
  internal: boolean;
  accountName: string;
  post: string;
  iofShort: string;
  fioFull: string;
  name: string;
  code: string;
  role: any;
  prefect: string;
}

export class AgendaUser {
  post: string;
  iofShort: string;
  fioFull: string;
  accountName: string;
}

export class AgendaQuestion {
  agendaId: string;
  questionId: string;
  itemNumber: string;
  status: AgendaQuestionStatus;
  dateChange: Date;
  reasonChange: string;
  fio: string;
  login: string;
}

export class AgendaQuestionStatus {
  static EXCLUDED = "excluded";
  static INCLUDED = "included";
  static ADDED = "added";
}

export class AgendaQuestionAuthor {
  accountName: string;
  fioFull: string;
}

export class AgendaPrintBasis {
  basisType: string;
  number: string;
  date: Date;
  note: string;
}

export class AgendaSearchFilter {
  meetingHeld: boolean;
  meetingTypes: string[];
  meetingNumber: string;
  meetingDate: DateRange;
}

export class AgendaMailingInfo {
  mailingDateTime: Date;
  mailingRecipient: AgendaMailingRecipient[];
}

export class AgendaMailingRecipient {
  post: string;
  iofShort: string;
  fioFull: string;
  accountName: string;
}

export class AgendaMeetingDateHistory {
  meetingDateOld: Date;
  user: AgendaMeetingHistoryUser;
  changeDate: Date;
}

export class AgendaMeetingPlaceHistory {
  meetingPlaceOld: string;
  user: AgendaMeetingHistoryUser;
  changeDate: Date;
}

export class AgendaMeetingTimeHistory {
  meetingTimeOld: Date;
  user: AgendaMeetingHistoryUser;
  changeDate: Date;
}

export class AgendaMeetingTimeEndHistory {
  meetingTimeEndOld: Date;
  user: AgendaMeetingHistoryUser;
  changeDate: Date;
}

export class AgendaAttendedHistory {
  attendedOld: AgendaAttended[];
  user: AgendaMeetingHistoryUser;
  changeDate: Date;
}

export class AgendaMeetingHistoryUser {
  login: string;
  fioFull: string;
  post: string;
}

export class DateRange {
  startDate: Date;
  endDate: Date;
}
