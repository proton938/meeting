class AuthUser {
    login: string;
    authenticated: boolean;
    user: any;
    permissions: string[];
    meetingTypes: string[];
}