import {ShowcaseSorting} from "../ShowcaseSorting";

export class ShowcaseExpressMkaMeetingsSortingBuilder {

  build(): ShowcaseSorting[] {
    let result: ShowcaseSorting[] = [];

    result.push(new ShowcaseSorting('meetingDate', 'desc', 'Дата', {'width': '110px'}));
    result.push(new ShowcaseSorting('meetingTime', 'desc', 'Время начала и окончания', {'width': '120px'}));
    result.push(new ShowcaseSorting('meetingThemeName', 'none', 'Совещание', {}));
    result.push(new ShowcaseSorting('meetingPlaceName', 'none', 'Место проведения', {'width': '150px'}));
    result.push(new ShowcaseSorting('initiatorFioFull', 'none', 'Инициатор', {'width': '18%'}));
    result.push(new ShowcaseSorting('authorCreateFioFull', 'none', 'Исполнитель', {'width': '18%'}));

    return result;
  }
}
