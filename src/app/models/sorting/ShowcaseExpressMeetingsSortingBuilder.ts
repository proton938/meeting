import {ShowcaseSorting} from "../ShowcaseSorting";

export class ShowcaseExpressMeetingsSortingBuilder {

  build(): ShowcaseSorting[] {
    let result: ShowcaseSorting[] = [];

    result.push(new ShowcaseSorting('statusName', 'none', 'Статус', {}));
    result.push(new ShowcaseSorting('meetingDate', 'desc', 'Дата', {'width': '110px'}, 'text-center'));
    result.push(new ShowcaseSorting('meetingTime', 'desc', 'Время начала и окончания', {'width': '120px'}, 'text-center'));
    result.push(new ShowcaseSorting('meetingThemeName', 'none', 'Совещание', {}, 'text-center'));
    result.push(new ShowcaseSorting('meetingPlaceName', 'none', 'Место проведения', {'width': '150px'}, 'text-center'));
    result.push(new ShowcaseSorting('initiatorFioFull', 'none', 'Инициатор', {'width': '18%'}, 'text-center'));
    result.push(new ShowcaseSorting('authorCreateFioFull', 'none', 'Исполнитель', {'width': '18%'}, 'text-center'));

    return result;
  }
}
