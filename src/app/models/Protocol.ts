import { QuestionParameters } from "./Question";
import { FileType } from "./FileType";
import { DocumentFromMKA } from "./DocumentFromMKA";
import { AgendaStatus } from "./Agenda";

export class ProtocolDocumentWrapper {
  document: ProtocolDocument;
}

export class ProtocolDocument {
  protocol: Protocol;
}

export class Protocol {
  protocolID: string;
  agendaID: string;
  bpmProcess: ProtocolProcess;
  meetingType: string;
  meetingDate: Date;
  meetingNumber: string;
  meetingTime: Date;
  protocolApproved: boolean;
  attended: ProtocolAttended[];
  attendedNum: number;
  mailingInfo: ProtocolMailingInfo[];
  draftingGroupMKA: ProtocolDraftingGroupMKA;
  draftingGroupInvest: ProtocolDraftingGroupInvest;
  protocolGroup: ProtocolProtocolGroup;
  approvedBy: ProtocolApprovedBy;
  fileProtocol: FileType[];
  fileConclusion: FileType;
  questionsNum: number;
  question: ProtocolQuestion[];
  meetingFullName: string;
  secretary: ProtocolApprovedBy;
  secretaryResponsible: SecretaryResponsible;
  commentSecretaryResponsible: ProtocolComment[];
  commentChairman: ProtocolComment[];
  dsConclusionSecretaryResponsible: DsType;
  dsProtocolChairman: DsType;
  dsConclusionChairman: DsType;
  fileProtocolDraft: FileType;
  fileConclusionDraft: any;
  meetingHeld: boolean;
  protocolApprovedDate: Date;
  meetingTimeEnd: string;
  meetingPlace: string;
  folderId: string;
  meetingTheme: string;
  review?: ProtocolReview[];
  receivedFromMKA: DocumentFromMKA;
  approval?: ProtocolApproval;
  status: ProtocolStatus;
  approvalHistory: ProtocolApprovalHistory;
}

export class ProtocolStatus {
  code: string;
  name: string;
  color: string;
  static fromAgendaStatus(status: AgendaStatus): ProtocolStatus {
    return { code: status.code, name: status.name, color: status.color };
  }
}

export class ProtocolApprovalHistory {
  approvalCycle: ProtocolApprovalHistoryCycle[] = [];
}

export class ProtocolApprovalHistoryCycle {
  approvalCycleNum: string;
  approvalCycleDate: Date;
  approvalCycleFile: FileType[];
  agreed: ProtocolApprovalHistoryCycleAgreed[];
}

export class ProtocolApprovalHistoryCycleAgreed {
  approvalNum: string;
  approvalTime: string;
  agreedBy: ProtocolApprovalCycleAgreedBy;
  approvalPlanDate: Date;
  approvalFactDate: Date;
  approvalResult: string;
  approvalType: string;
  approvalTypeCode: string;
  approvalNote: string;
  fileApproval: FileType[];
}

export class ProtocolApproval {
  approvalCycle: ProtocolApprovalCycle;
  constructor() {
    this.approvalCycle = new ProtocolApprovalCycle();
  }
}

export class ProtocolApprovalCycle {
  approvalCycleNum: string;
  approvalCycleDate: Date;
  agreed: ProtocolApprovalCycleAgreed[];
  constructor(approvalCycleNum = 1) {
    this.approvalCycleNum = '' + approvalCycleNum;
    this.approvalCycleDate = new Date();
    this.agreed = [];
  }
}

export class ProtocolApprovalCycleAgreed {
  approvalNum: string;
  approvalTime: string;
  agreedBy: ProtocolApprovalCycleAgreedBy;
  approvalPlanDate: Date;
  approvalFactDate: Date;
  approvalResult: string;
  approvalType: string;
  approvalTypeCode: string;
  approvalNote: string;
  fileApproval: FileType[];
  agreedDs: ProtocolApprovalCycleAgreedDs;
}

export class ProtocolApprovalCycleAgreedDs {
  agreedDsLastName: string;
  agreedDsName: string;
  agreedDsPosition: string;
  agreedDsCN: string;
  agreedDsFrom: Date;
  agreedDsTo: Date;
}

export class ProtocolApprovalCycleAgreedBy {
  post: string;
  fioFull: string;
  accountName: string;
  fioShort: string;
  iofShort: string;
  phone: string;
}

export class ProtocolReview {
  reviewBy: ProtocolUserType; // Ознакомился
  factReviewBy: ProtocolUserType; // Ознакомился фактически (делегант)
  sentReviewBy: ProtocolUserType; // Кто отправил на ознакомление
  reviewPlanDate: Date = null; //Плановая дата ознакомления
  reviewFactDate: Date = null; //Фактическая дата ознакомления
  reviewNote: string = '' //Комментарий
}

export class ProtocolUserType {
  organization: string; //Организация
  organizationShort: string; //Организация - краткое название
  organizationCode: string; //>Организация - код
  accountName: string; //Логин
  fioFull: string; // ФИО
  fioShort: string; //ФИО краткое в именительном падеже
  post: string; // должность
  phone: string; // телефон
  email: string; //E-mail

}

export class ProtocolComment {
  commentText: string;
  commentDate: string;
}

export class ProtocolAttended {
  // fioShort: string;
  // role: string;
  accountName: string;
  post: string;
  iofShort: string;
  fioFull: string;
  name?: string;
  code?: string;
  role: {
    roleCode: string;
    roleName: string;
  };
  prefect: string;
  internal: boolean;
}

export class ProtocolSigner {
  iofShort: any;
  post: string;
  fioShort: string;
  accountName: string;
}

export class ProtocolDraftingGroupMKA {
  member: ProtocolSigner[];
}

export class ProtocolDraftingGroupInvest {
  member: ProtocolSigner[];
}

export class ProtocolProtocolGroup {
  member: ProtocolSigner[];
}

export class ProtocolPerson {
  post: string;
  iofShort: string;
  fioFull: string;
  accountName: string;
}

export class ProtocolApprovedBy extends ProtocolPerson { }

export class SecretaryResponsible extends ProtocolPerson { }

export class ProtocolQuestion {
  questionID: string;
  itemNumber: string;
  decisionType: string;
  decisionTypeCode: string;
  decisionText: string;
  address: string;
  cadastralNumber: string;
  question: string;
  customer: string;
  activity: boolean;
  materials: ProtocolQuestionMaterialGroup[];
  speaker: ProtocolUser;
  resultsVote: ProtocolQuestionVoteResult;
  recommendationGK: string;
  parameters: QuestionParameters;
  excluded: boolean;
}

export class ProtocolQuestionVoteResult {
  no: number;
  yes: number;
  abstain: number;
  total: number;
}

export class ProtocolQuestionMaterialGroup {
  idBase: string;
  materialsFIO: string;
  accountName: string;
  materialType: string;
  document: FileType[];
}
export class ProtocolProcess {
  bpmProcessId: string;
}
export class ProtocolUser {
  organization: string;
  organizationShort: string;
  organizationCode: string;
  post: string;
  fioFull: string;
  fioShort: string;
  phone: string;
  email: string;
  accountName: string;
}

export class ProtocolSearchFilter {
  protocolApproved: boolean;
  meetingTypes: string[];
  meetingNumber: string;
  meetingDate: DateRange;
  approvedBy: string[];
}

export class ProtocolMailingInfo {
  mailingDateTime: Date;
}

export class DateRange {
  startDate: Date;
  endDate: Date;
}

export class ConclusionSecretaryResponsible {
  dsLastName: string;
  dsName: string;
  dsPosition: string;
  dsCN: string;
  dsFrom: Date;
  dsTo: Date;
}

export class DsType {
  dsLastName: string;
  dsName: string;
  dsPosition: string;
  dsCN: string;
  dsFrom: Date;
  dsTo: Date;
}
