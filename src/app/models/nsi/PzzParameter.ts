export class PzzParameter {
    code: string;
    sortNumber: string;
    name: string;
    type: string;
}
