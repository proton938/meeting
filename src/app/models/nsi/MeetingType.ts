export class MeetingType {

  public static readonly SSMKA = 'SS MKA';
  public static readonly PZZ = 'PZZ';
  public static readonly TEST = 'TEST';

  meetingType: string;
  meetingFullName: string;
  meetingShortName: string;
  meetingWeekly: boolean;
  meetingDay: string;
  meetingTimeStart: string;
  meetingTimeEnd: string;
  meetingPlace: [{
    code: string;
    name: string;
  }];
  meetingAgendaDay: string;
  meetingAgendaTime: string;
  startNumber: number;
  creatingOutside: boolean;
  planningMeeting: boolean;
  creatingCopies: any;
  Express: boolean;
  meetingColor: string;
  meetingExternal?: boolean;
  approvedLogin?: string;
}
