export class RoleParticipant {
    RoleCode: string;
    RoleName: string;
    LevelRoleCode: string;
    account_name: string;
    actuality: boolean;
    PostName: string;
    mask: string;
    groupRoles: string;
    quorum: boolean;
}
