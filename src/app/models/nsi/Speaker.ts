export class Speaker {
    group: string;
    name: string;
    nameShort: string;
    code: string;
    children: Speaker[];
}
