export class ResponsibleDepartment {
    departmentOO: string;
    departmentPrepareCode: string;
    departmentPrepareFull: string;
    departmentPrepareShort: string;
    group: string;
}
