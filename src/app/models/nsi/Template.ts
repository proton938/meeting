export class TemplateMeetingType {
    meetingType: string;
    meetingShortName: string;
}

export class Template {
    meetingType: TemplateMeetingType[];
    meetingAgendaTemplate: string;
    meetingProtocolTemplate: string;
    meetingExcerptTemplate: string;
    meetingAgendaClass: string;
    meetingProtocolProjectClass: string;
    meetingProtocolClass: string;
}
