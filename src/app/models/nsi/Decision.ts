export class Decision {
    decisionType: string;
    decisionTypeCode: string;
    meetingTypeCode: string;
    questionCode: string;
    recommendation: string;
}
