import {Prefect} from "./Prefect";

export class District {
    name: string;
    perfectId: Prefect[];
}
