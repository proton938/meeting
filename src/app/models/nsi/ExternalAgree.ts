export class ExternalAgree {
    code: string;
    icon: string;
    name: string;
    shortName: string;
}
