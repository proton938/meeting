import {QuestionBasis} from "./Question";

class BallotPaper {
	ballotID: string;
	agendaID: string;
	protocolID: string;
	meetingType: string;
	meetingFullName: string;
	meetingDate: Date;
	meetingNumber: string;
	meetingTime: string;
	voting: BallotPaperVoting;
	question: BallotPaperQuestion[];
	file: BallotPaperFile;
	dsL: boolean;
	ds: BallotPaperDS;
}

class BallotPaperDS {
    dsLastName?: string;
    dsName?: string;
    dsPosition?: string;
    dsCN?: string;
    dsFrom?: Date;
    dsTo?: Date;
}

class BallotPaperFile {
	idFile: string;
	nameFile: string;
	sizeFile: string;
	signed: boolean;
	dateFile: Date;
	typeFile: string;
	classFile: string;
}

class BallotPaperVoting {
	accountName: string;
	post: string;
	iofShort: string;
	fioFull: string;
	name: string;
	code: string;
	role: string;
}

class BallotPaperQuestion {
	questionID: string;
	itemNumber: string;
	basis: QuestionBasis;
	rightHolder: string;
	prefect: string;
	address: string;
	cadastralNumber: string;
	question: string;
	resultsVote: BallotPaperQuestionVotingResult;
	excluded: boolean;
}

class BallotPaperQuestionVotingResult {
	resultsVoteName: string;
	resultsVoteCode: string;
	resultsVoteComment: string;
}

const ballotPaperForbiddenQuestion: string = 'notVoit';
