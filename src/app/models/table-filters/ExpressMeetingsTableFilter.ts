import {DateRangeFilter} from '../DateRangeFilter';
import * as _ from 'lodash';
import {TimeFilter} from '../TimeFilter';

export class ExpressMeetingsTableFilter {

   meetingDate: DateRangeFilter = new DateRangeFilter();
   meetingThemeName: string;
   statusName: string;
   meetingTime: TimeFilter = new TimeFilter();
   meetingTimeEnd: TimeFilter = new TimeFilter();
   meetingPlaceName: string;
   authorCreateFioFull: string;
   initiatorFioFull: string;

   clear() {
      this.meetingDate.clear();
      this.meetingTime.clear();
      this.meetingTimeEnd.clear();
      this.meetingThemeName = null;
      this.statusName = null;
      this.meetingPlaceName = null;
      this.authorCreateFioFull = null;
      this.initiatorFioFull = null;
   }

   isEmpty(): boolean {
      return this.meetingDate.isEmpty() &&
         !this.meetingThemeName &&
         this.meetingTime.isEmpty() &&
         this.meetingTimeEnd.isEmpty() &&
         !this.statusName &&
         !this.meetingPlaceName &&
         !this.authorCreateFioFull &&
         !this.initiatorFioFull;
   }

   count(): number {
      return (this.meetingDate.isEmpty() ? 0 : 1) +
         (!this.meetingThemeName ? 0 : 1) +
         (!this.statusName ? 0 : 1) +
         (this.meetingTime.isEmpty() ? 0 : 1) +
         (this.meetingTimeEnd.isEmpty() ? 0 : 1) +
         (!this.meetingPlaceName ? 0 : 1) +
         (!this.authorCreateFioFull ? 0 : 1) +
         (!this.initiatorFioFull ? 0 : 1);
   }

   toString(): string {
      let result = '';

      if (!this.meetingDate.isEmpty()) {
         result += (result) ? ' AND ' : '';
         result += `meetingDate:${this.meetingDate.toString()}`;
      }

      if (this.meetingThemeName) {
         const contents = _.map(this.meetingThemeName.split(' '), v => v.trim());
         const content = contents.join('\\ ');
         result += (result) ? ' AND ' : '';
         result += `meetingThemeName:(*${content}*)`;
      }

     if (this.statusName) {
       const contents = _.map(this.statusName.split(' '), v => v.trim());
       const content = contents.join('\\ ');
       result += (result) ? ' AND ' : '';
       result += `statusName:(*${content}*)`;
     }

      if (!this.meetingTime.isEmpty() && !this.meetingTimeEnd.isEmpty()) {
         const from = this.meetingTime.toString().split(':');
         const start = from.join('\\:');
         const to = this.meetingTimeEnd.toString().split(':');
         const end = to.join('\\:');
         result += (result) ? ' AND ' : '';
         result += `meetingTime:[${start} TO ${end}]`;
      }

      if (!this.meetingTime.isEmpty() && this.meetingTimeEnd.isEmpty()) {
         const contents = this.meetingTime.toString().split(':');
         const content = contents.join('\\:');
         result += (result) ? ' AND ' : '';
         result += `meetingTime:[${content} TO *]`;
      }

      if (!this.meetingTimeEnd.isEmpty() && this.meetingTime.isEmpty()) {
         const contents = this.meetingTimeEnd.toString().split(':');
         const content = contents.join('\\:');
         result += (result) ? ' AND ' : '';
         result += `meetingTime:[* TO ${content}]`;
      }

      if (this.meetingPlaceName) {
         const contents = _.map(this.meetingPlaceName.split(' '), v => v.trim());
         const content = contents.join('\\ ');
         result += (result) ? ' AND ' : '';
         result += `meetingPlaceName:(*${content}*)`;
      }

      if (this.authorCreateFioFull) {
         // const contents = _.map(this.authorCreateFioFull.split(' '), v => v.trim());
         // const content = contents.join('\\ ');
         result += (result) ? ' AND ' : '';
         result += `authorCreateFioFull:(${this.authorCreateFioFull})`;
      }

      if (this.initiatorFioFull) {
         // const contents = _.map(this.initiatorFioFull.split(' '), v => v.trim());
         // const content = contents.join('\\ ');
         result += (result) ? ' AND ' : '';
         result += `initiatorFioFull:(${this.initiatorFioFull})`;
      }

      if (result) {
         return `(${result})`;
      }

      return '';

   }

}
