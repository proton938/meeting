import {Question} from "./Question";

class QuestionSearchResult {
    count: number;
    questions: Question[];
}
