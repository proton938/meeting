class NormalizedParticipant {
    accountName: string;
    shortFullName: string;
    fullName?: string;
    role: string;
    checked?: boolean;
    disabled?: boolean;
    levelRoleCode?: string;
    mask?: string;
    post?: string;
    roleCode?: string;
    quorum?: boolean;
}