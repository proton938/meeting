export class ShowcaseSorting {
   code: string;
   value: string;
   name: string;
   cssClass: string = '';
   width: any;

   constructor(code: string, value: string, name: string, width: any, cssClass = '') {
      this.code = code;
      this.value = value;
      this.name = name;
      this.width = width;
      this.cssClass = cssClass;
   }
}
