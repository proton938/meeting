import {BsModalService} from "ngx-bootstrap";
import {Injectable} from '@angular/core';
import {Observable} from "rxjs/Rx";
import {AddUserModalComponent} from "../components/tasks/review-mka-doc/add-user/add-user.modal";

@Injectable({
  providedIn: 'root'
})
export class ModalService {

  constructor(private modalService: BsModalService) {
  }

  addUser(): Observable<string> {
    return Observable.create(subscriber => {
      const options = {
        initialState: {},
        'class': 'modal-md'
      };
      const ref = this.modalService.show(AddUserModalComponent, options);
      let subscr = this.modalService.onHide.subscribe(() => {
        let component = <AddUserModalComponent> ref.content;

        if (component.submit) {
          subscriber.next(component.user);
        } else {
          subscriber.error('canceled');
        }
        subscr.unsubscribe();
      });
    });
  }

}
