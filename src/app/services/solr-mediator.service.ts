import {Injectable} from '@angular/core';
import {Observable, from} from 'rxjs';
import {map} from 'rxjs/internal/operators';
import {
  CdpSearchByIds, CdpSolrResourceService, SolarResourceService, CdpSearchExtData
} from '@reinform-cdp/search-resource';
import {every, isString, uniq, isArray, concat, compact} from 'lodash';
import {CommonQueryService} from './common-query.service';
import {copy} from 'angular';

@Injectable({
  providedIn: 'root'
})
export class SolrMediatorService {
  types: any = {
    instruction: 'SDO_INSTRUCTION_INSTRUCTION',
    memo: 'SDO_INSTRUCTION_MEMO',
    order: 'SDO_ORDER_ORDER',
    agenda: 'SDO_MEETING_AGENDA',
    protocol: 'SDO_MEETING_PROTOCOL',
    question: 'SDO_MEETING_QUESTION'
  };

  constructor(private solr: SolarResourceService,
              private cdpSolr: CdpSolrResourceService,
              private commonQuery: CommonQueryService) {}

  query(params: any, isNew: boolean = true): Observable<any> {
    const p: any = copy(params);
    if (isNew) {
      if (p.common) {
        this.commonToQuery(p.common, p, isNew);
      } else {
        delete p.common;
      }
      if (p.query) {
        p.query = p.query.replace(/documentId:|sys_documentId:/g, 'sys_documentId:');
      } else {
        delete p.query;
      }
      return this.cdpSolr.query(p).pipe(map(this.modifyDocs.bind(this)));
    } else {
      return from(<any>this.solr.query(p)).pipe(map(this.modifyDocs.bind(this)));
    }
  }

  searchExt(params: any, isNew: boolean = true): Observable<any> {
    if (isNew) {
      return this.cdpSolr.searchExt(params).pipe(map(this.modifyDocs.bind(this)));
    } else {
      return from(<any>this.solr.searchExt(params)).pipe(map(this.modifyDocs.bind(this)));
    }
  }

  getByIds(data: CdpSearchByIds[] | string[], isNew = true): Observable<any> {
    if (every(data, i => isString(i))) { isNew = false; }
    if (isNew) {
      return this.cdpSolr.getByIds(<CdpSearchByIds[]>data).pipe(map(this.modifyDocs.bind(this)));
    } else {
      return from(<any>this.solr.getByIds({ids: <string[]>data})).pipe(map(this.modifyDocs.bind(this)));
    }
  }

  getDocs(res: any) {
    if (res) {
      if (isArray(res)) { // after getByIds
        return compact(concat(res.map(i => i.docs || [])));
      } else { // after query or other
        return res.docs || [];
      }
    } else {
      return [];
    }
  }

  modifyDocs(response: any): any {
    const modifyItem = item => {
      if (!item.documentId && item.sys_documentId) { item.documentId = item.sys_documentId; }
      return item;
    };
    const modify = r => {
      if (r && r.docs && r.docs.length) { r.docs.forEach(i => modifyItem(i)); }
      return r;
    };
    return isArray(response) ? response.map(i => modify(i)) : modify(response);
  }

  commonToQuery(common: string, params: any, isNew: boolean = true) {
    const fieldCollection: any = {
      [this.types.agenda]: [
        {code: 'meetingNumber'},
        {code: 'meetingThemeName'},
        {code: 'initiatorFioFull'}
      ],
      [this.types.question]: [
        {code: 'question'}
      ],
      [this.types.protocol]: [
        {code: 'approvedByFioFull'},
        {code: 'meetingFullName'}
      ]
    };
    const fields: string[] = [];
    params.types.forEach(t => fields.push(...fieldCollection[t]));
    const commonQuery: string = this.commonQuery.commonToQuery(common, uniq(fields), isNew);
    if (commonQuery) {
      params.query = params.query ? params.query + ' AND (' + commonQuery + ')' : commonQuery;
    }
    delete params.common;
  }
}
