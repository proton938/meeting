import {ToastrService} from "ngx-toastr";
import { Injectable } from '@angular/core';
import {
  ActivityResourceService, ActivityProcessHistoryManager, IDefaultVar, IInfoAboutProcessInit, IProcessCategory, ITask,
  ITaskVariable
} from "@reinform-cdp/bpm-components";
import {Observable} from "rxjs/Rx";
import {forkJoin, from, of, throwError} from "rxjs/index";
import {catchError, mergeMap, tap} from "rxjs/internal/operators";
import {AlertService} from "@reinform-cdp/widgets";
import * as _ from "lodash";

@Injectable({
  providedIn: 'root'
})
export class ActivitiService {

  constructor(
    private toastr: ToastrService,
    private activityRestService: ActivityResourceService, private activityProcessHistoryManager: ActivityProcessHistoryManager,
    private alertService: AlertService
  ) { }

  getEntityIdVar(task: ITask): Observable<string> {
    let _EntityIdVar = task.variables.find(v => {return v.name === 'EntityIdVar';});
    if (_EntityIdVar) {
      return of(<string>_EntityIdVar.value);
    } else {
      this.toastr.error('Отсутствует переменная задачи\'EntityIdVar\'');
      return throwError('Отсутствует переменная задачи\'EntityIdVar\'');
    }
  }

  deleteProcesses(ids: string[]): Observable<any> {
    let observables = _.map(ids, id => {
      return from(this.activityRestService.deleteProcess(id));
    });

    if (observables.length > 0) {
      return forkJoin(observables).pipe(
        tap(response => {
          if(observables.length === 1) {
            this.toastr.success('Процесс успешно удален!');
          } else {
            this.toastr.error('Процессы успешно удалены!');
          }
        }), catchError(error => {
          if(observables.length === 1) {
            this.toastr.error('Ошибка при удалении процесса!');
          } else {
            this.toastr.error('Ошибка при удалении процессов!');
          }
          return throwError(error);
        })
      );
    } else {
      return of('');
    }
  }


  private getVars(executorLogin: string, planDate: Date): ITaskVariable[] {
    let vars = [];

    if (executorLogin) {
      vars.push({
        name: 'ResponsibleExecutor_VAR',
        value: executorLogin
      });
    }

    if (planDate) {
      vars.push({
        name: 'TaskTime_VAR',
        value: this.buildTime(planDate)
      });
    }

    return vars;
  }

  private buildTime(date: Date): string {
    let result = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);
    result += 'T23:59:00.000';
    let _date = new Date(result);
    let pad = (value) => {
      return value < 10 ? '0' + value : value;
    };

    let createOffset = (date) => {
      let sign = (date.getTimezoneOffset() > 0) ? '-' : '+';
      let offset = Math.abs(date.getTimezoneOffset());
      let hours = pad(Math.floor(offset / 60));
      let minutes = pad(offset % 60);
      return sign + hours + ':' + minutes;
    };

    let offset = createOffset(_date);
    result += offset;
    return result;
  }

}
