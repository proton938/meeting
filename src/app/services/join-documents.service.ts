import {map} from "rxjs/internal/operators";
import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {FileType} from "../models/FileType";

@Injectable({
  providedIn: 'root'
})
export class JoinDocumentsService {

  rootReporter = "/reporter/v1";

  constructor(private http: HttpClient,) {
  }

  joinDocumentsUpdate(idFile: string, appendIdFiles: string[], sectionStart: string): Observable<FileType> {
    let url: string = `${this.rootReporter}/word/joinDocuments/update`;
    return this.http.put(url, {
      appendedDocuments: appendIdFiles.map(f => {
        return {sectionStart: sectionStart, versionSeriesGuid: f};
      }),
      versionSeriesGuid: idFile
    }).pipe(
      map((result: any) => {
        return FileType.create(result.versionSeriesGuid, result.fileName, result.fileSize.toString(), false,
          new Date().toISOString(), result.fileType, result.mimeType)
      })
    )
  }

  joinDocumentsPreview(idFile: string, appendIdFiles: string[], sectionStart: string, resFileName: string): Observable<Blob> {
    let url: string = `${this.rootReporter}/word/joinDocuments/preview`;
    return <Observable<Blob>>this.http.post(url, {
      appendedDocuments: appendIdFiles.map(f => {
        return {sectionStart: sectionStart, versionSeriesGuid: f};
      }),
      resFileName: resFileName,
      versionSeriesGuid: idFile
    }, {responseType: 'blob'})
  }

}
