import * as angular from "angular";
import { Operation } from "fast-json-patch";
import { Injectable } from '@angular/core';
import * as _ from "lodash";
import { SessionStorage } from "@reinform-cdp/security";
import { forkJoin, Observable, of } from "rxjs/index";
import { Agenda, AgendaDocumentWrapper, AgendaQuestion, AgendaQuestionStatus, AgendaStatus } from '../models/Agenda';
import { HttpClient } from "@angular/common/http";
import { DateFormatService } from "./date-format.service";
import { map, mergeMap } from "rxjs/internal/operators";
import { QuestionService } from "./question.service";
import { AgendaService } from "./agenda.service";
import { Question, QuestionDocumentWrapper } from "../models/Question";
import { Protocol } from "../models/Protocol";
import { MeetingType } from "../models/nsi/MeetingType";
import { ProtocolService } from "./protocol.service";
import { parseDates } from "./date-util.service";

@Injectable({
  providedIn: 'root'
})
export class ExpressService {

  constructor(private http: HttpClient, private questionService: QuestionService,
    private agendaService: AgendaService, private protocolService: ProtocolService,
    private dateFormatService: DateFormatService,
    private session: SessionStorage) {
  }

  createQuestions(questions: Question[], agenda: Agenda): Observable<Question[]> {
    if (questions && questions.length) {
      return forkJoin(questions.map(q => this.createQuestion(q, agenda)));
    } else {
      return of([]);
    }
  }

  createQuestion(question: Question, agenda: Agenda): Observable<Question> {
    question.meetingType = agenda.meetingType;
    question.questionConsider.meetingDate = agenda.meetingDate;
    question.questionConsider.meetingNumber = agenda.meetingNumber;
    question.questionConsider.questionStatus = false;
    return this.questionService.create(question)
  }

  createAgenda(agenda: Agenda, questions: Question[]): Observable<Agenda> {
    return <Observable<Agenda>>this.http.post(`/app/sdo/meeting/express/agenda/create`, {
      document: { agenda: this.dateFormatService.formatAgendaDates(agenda) },
    }).pipe(
      mergeMap((wrapper: AgendaDocumentWrapper) => {
        let agenda = parseDates(wrapper.document.agenda);
        return this.createQuestions(questions, agenda).pipe(
          mergeMap((newQuestions: Question[]) => {
            if (newQuestions) {
              return this.agendaService.setAgendaQuestions(agenda.agendaID, newQuestions.map(q => <AgendaQuestion>{ questionId: q.questionID }));
            } else {
              return of([]);
            }
          }),
          mergeMap(() => {
            return this.agendaService.publish(agenda.agendaID);
          }),
          map(() => agenda)
        )
      })
    );
  }

  deleteAgenda(id: string): Observable<any> {
    localStorage[`${this.session.login()}.adminMeetings.revision`] = _.uniqueId();
    return <Observable<any>>this.http.delete(`/app/sdo/meeting/express/agenda/${id}`);
  }

  updateAgenda(id: string, agendaDiff: Operation[], questions: Question[]): Observable<Agenda> {
    return this.agendaService.update(id, agendaDiff).pipe(
      mergeMap((agenda: Agenda) => {
        return this.agendaService.includedQuestions(id).pipe(
          mergeMap((includedQuestions: AgendaQuestion[]) => {
            return this.updateAgendaQuestions(id, questions, agenda).pipe(
              mergeMap((savedQuestions: Question[]) => {
                let agendaQuestions = [];
                includedQuestions.filter(iq => savedQuestions.some(sq => sq.questionID === iq.questionId)).forEach(iq => agendaQuestions.push(iq));
                savedQuestions.filter(sq => !includedQuestions.some(iq => iq.questionId === sq.questionID)).forEach(sq => agendaQuestions.push({ questionId: sq.questionID }));
                includedQuestions.filter(iq => !savedQuestions.some(sq => sq.questionID === iq.questionId)).forEach(iq => {
                  iq.status = AgendaQuestionStatus.EXCLUDED;
                  iq.reasonChange = "Автоматическое исключение вопроса из повестки.";
                  agendaQuestions.push(iq);
                });

                agendaQuestions.forEach((aq) => {
                  const question = savedQuestions.find((q) => q.questionID === aq.questionId);

                  if (question) {
                    aq.itemNumber = question.questionConsider.itemNumber;
                  }
                });

                agendaQuestions.sort((aq1, aq2) => {
                  const question1 = savedQuestions.find((q) => q.questionID === aq1.questionId);
                  const question2 = savedQuestions.find((q) => q.questionID === aq2.questionId);

                  if (question1 && question2) {
                    const itemNumber1 = question1.questionConsider.itemNumber;
                    const itemNumber2 = question2.questionConsider.itemNumber;
  
                    return +itemNumber1 - (+itemNumber2);
                  }
                  
                  return 0;
                });

                return this.agendaService.setAgendaQuestions(id, agendaQuestions);
              }),
            );
          })
        ).pipe(
          map(() => {
            return parseDates(agenda);
          })
        );
      })
    );
  }

  addAgendaQuestion(id: string, question: Question, agenda: Agenda): Observable<AgendaQuestion[]> {
    return this.createQuestion(question, agenda).pipe(
      mergeMap(savedQuestion => {
        return this.agendaService.includedQuestions(id).pipe(
          mergeMap((includedQuestions: AgendaQuestion[]) => {
            includedQuestions.push(<AgendaQuestion>{ questionId: savedQuestion.questionID });
            return this.agendaService.setAgendaQuestions(id, includedQuestions);
          })
        );
      })
    )

  }

  deleteAgendaQuestion(id: string, questionId: string): Observable<AgendaQuestion[]> {
    return this.agendaService.includedQuestions(id).pipe(
      mergeMap((includedQuestions: AgendaQuestion[]) => {
        let question = includedQuestions.find(q => q.questionId === questionId);
        question.status = AgendaQuestionStatus.EXCLUDED;
        question.reasonChange = "Автоматическое исключение вопроса из повестки.";
        return this.agendaService.setAgendaQuestions(id, includedQuestions);
      })
    );
  }

  swapAgendaQuestion(id: string, id1: string, id2: string): Observable<AgendaQuestion[]> {
    return this.agendaService.includedQuestions(id).pipe(
      mergeMap((includedQuestions: AgendaQuestion[]) => {
        let index1 = includedQuestions.findIndex(iq => iq.questionId === id1);
        let index2 = includedQuestions.findIndex(iq => iq.questionId === id2);

        let q = includedQuestions[index1];
        includedQuestions[index1] = includedQuestions[index2];
        includedQuestions[index2] = q;

        let itemNumber = includedQuestions[index1].itemNumber;
        includedQuestions[index1].itemNumber = includedQuestions[index2].itemNumber;
        includedQuestions[index2].itemNumber = itemNumber;

        return this.agendaService.setAgendaQuestions(id, includedQuestions);
      })
    );
  }

  updateAgendaQuestions(id: string, questions: Question[], agenda: Agenda): Observable<Question[]> {
    if (questions && questions.length) {
      return forkJoin(questions.map(question => this.updateAgendaQuestion(id, question, agenda)));
    } else {
      return of([]);
    }
  }

  updateAgendaQuestion(id: string, question: Question, agenda: Agenda): Observable<Question> {
    if (!question.questionID) {
      return this.createQuestion(question, agenda);
    }
    return this.questionService.get(question.questionID).pipe(
      map(wrapper => wrapper.document.question),
      mergeMap(storedQuestion => {
        let newQuestion: Question = angular.copy(storedQuestion);
        newQuestion.questionConsider.itemNumber = question.questionConsider.itemNumber.toString();
        newQuestion.questionConsider.question = question.questionConsider.question;
        newQuestion.questionConsider.responsiblePrepare = question.questionConsider.responsiblePrepare;
        return this.questionService.update(question.questionID, this.questionService.diff(storedQuestion, newQuestion));
      })
    )
  }

  markPassed(agenda: Agenda, status: AgendaStatus): Observable<any> {
    let newAgenda = angular.copy(agenda);
    newAgenda.meetingHeld = true;
    newAgenda.status = status;
    return this.agendaService.update(agenda.agendaID, this.agendaService.diff(agenda, newAgenda)).pipe(
      mergeMap(() => {
        return this.agendaService.includedQuestions(agenda.agendaID);
      }),
      mergeMap((questions: AgendaQuestion[]) => {
        return questions && questions.length ? forkJoin(questions.filter(q => q.status !== AgendaQuestionStatus.EXCLUDED)
          .map(q => this.questionService.get(q.questionId))
        ) : of([]);
      }),
      mergeMap((wrappers: QuestionDocumentWrapper[]) => {
        return wrappers && wrappers.length ? forkJoin(wrappers.map(w => {
          let question = w.document.question;
          let newQuestion = angular.copy(question);
          newQuestion.questionConsider.questionStatus = true;
          return this.questionService.update(question.questionID, this.questionService.diff(question, newQuestion));
        })) : of([])
      })
    );
  }

  generateProtocol(agenda: Agenda, meetingType: MeetingType): Observable<string> {
    return this.agendaService.createProtocol(agenda.agendaID, true).pipe(
      mergeMap((protocol: Protocol) => {
        protocol.meetingPlace = agenda.meetingPlace;
        protocol.meetingFullName = meetingType.meetingFullName;
        protocol.meetingTimeEnd = meetingType.meetingTimeEnd;
        protocol.protocolApproved = true;
        protocol.protocolApprovedDate = new Date();
        protocol.approvedBy = agenda.authorCreate;
        return this.protocolService.create(protocol)
      })
    );
  }

  mailNotification(id: string): Observable<Agenda> {
    return <Observable<any>>this.http.post(`/app/sdo/meeting/express/agenda/${id}/notification`, {}).pipe(
      map((wrapper: AgendaDocumentWrapper) => {
        return parseDates(wrapper.document.agenda);
      })
    );
  }

  mailDeleteNotification(id: string): Observable<any> {
    return <Observable<any>>this.http.post(`/app/sdo/meeting/express/agenda/${id}/delete/notification`, {});
  }

  mailCancelNotification(agenda: Agenda, status: AgendaStatus): Observable<any> {
    const newAgenda = angular.copy(agenda);
    const id = agenda.agendaID;
    newAgenda.status = status;
    return this.agendaService.update(id, this.agendaService.diff(agenda, newAgenda)).pipe(
      mergeMap(() => {
        return <Observable<any>>this.http.post(`/app/sdo/meeting/express/agenda/${id}/cancel/notification`, {})
      }));
  }

}
