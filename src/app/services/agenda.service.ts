import {from, Observable, of, throwError} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import * as _ from 'lodash';
import {catchError, map} from 'rxjs/internal/operators';
import {formatAsDate, parseDates} from './date-util.service';
import {SessionStorage} from '@reinform-cdp/security';
import {Agenda, AgendaDocumentWrapper, AgendaQuestion} from '../models/Agenda';
import {SearchExtDataItem, SearchResultDocument} from '@reinform-cdp/search-resource';
import {Protocol, ProtocolDocumentWrapper} from '../models/Protocol';
import * as jsonpatch from 'fast-json-patch';
import {compare, Operation} from 'fast-json-patch';
import {DateFormatService} from './date-format.service';
import {ToastrService} from 'ngx-toastr';
import * as angular from 'angular';
import {SolrMediatorService} from './solr-mediator.service';

@Injectable({
  providedIn: 'root'
})
export class AgendaService {

  url = '/app/sdo/meeting/agenda';

  constructor(private http: HttpClient,
              private dateFormatService: DateFormatService,
              private solrMediator: SolrMediatorService,
              private toastr: ToastrService,
              private session: SessionStorage) {
  }

  get(id: string): Observable<AgendaDocumentWrapper> {
    return <Observable<AgendaDocumentWrapper>>this.http.get(`${this.url}/${id}`).pipe(
      map(result => parseDates(result))
    );
  }

  create(agenda: Agenda): Observable<string> {
    return <Observable<string>>this.http.post(`${this.url}/create`, {
      document: {agenda: this.dateFormatService.formatAgendaDates(agenda)}
    }).pipe(map((result: AgendaDocumentWrapper) => {
      return result.document.agenda.agendaID;
    }));
  }

  diff(a1: Agenda, a2: Agenda) {
    return compare(
      {document: {agenda: this.dateFormatService.formatAgendaDates(a1)}},
      {document: {agenda: this.dateFormatService.formatAgendaDates(a2)}}
    );
  }

  update(id: string, diff: Operation[]): Observable<Agenda> {
    return <Observable<Agenda>>this.http.patch(`${this.url}/${id}`, diff)
      .pipe(map((result: AgendaDocumentWrapper) => {
        return parseDates(result.document.agenda);
      }));
  }

  delete(id: string): Observable<any> {
    localStorage[`${this.session.login()}.adminMeetings.revision`] = _.uniqueId();
    return <Observable<any>>this.http.delete(`${this.url}/${id}`);
  }

  updateDocument(id: string, left: AgendaDocumentWrapper, right: AgendaDocumentWrapper, message?: string): Observable<Agenda> {
    let diff = jsonpatch.compare(JSON.parse(angular.toJson(left)), JSON.parse(angular.toJson(right)));
    if (diff.length > 0) {
      return this.update(id, diff).pipe(
        map(response => {
          this.toastr.success(message ? message : 'Повестка совещания успешно обновлена!');
          return response;
        }), catchError(error => {
          this.toastr.error('Ошибка при изменении данных!');
          return throwError(error);
        })
      );
    } else {
      return of(right.document.agenda);
    }
  }

  setAgendaQuestions(id: string, questions: any[]): Observable<AgendaQuestion[]> {
    let body = <any> angular.copy(questions);
    return <Observable<AgendaQuestion[]>>this.http.post(`${this.url}/${id}/questions/update`, body).pipe(map((result: AgendaQuestion[]) => {
      return result.map(q => parseDates(q));
    }));
  }

  copyAgendaQuestions(id: string, questionsIds: string[]): Observable<any> {
    return <Observable<string>>this.http.post(`${this.url}/${id}/questions/copy`, questionsIds);
  }

  searchIncludedQuestions(meetingType: string, meetingDate: Date): Observable<string[]> {
    return <Observable<string[]>>this.http.get(`${this.url}/questions/included?meetingType=${meetingType}&meetingDate=${formatAsDate(meetingDate)}`);
  }

  searchCandidateQuestions(meetingType: string, meetingDate: Date): Observable<string[]> {
    return <Observable<string[]>>this.http.get(`${this.url}/questions/candidate?meetingType=${meetingType}&meetingDate=${formatAsDate(meetingDate)}`);
  }

  includedQuestions(id: string): Observable<AgendaQuestion[]> {
    return <Observable<AgendaQuestion[]>>this.http.get(`${this.url}/${id}/questions/included`).pipe(map((result: AgendaQuestion[]) => {
      return result.map(q => parseDates(q));
    }));
  }

  candidateQuestions(id: string, meetingType: string, meetingDate: Date): Observable<string[]> {
    return <Observable<string[]>>this.http.get(`${this.url}/${id}/questions/candidate?meetingType=${meetingType}&meetingDate=${formatAsDate(meetingDate)}`);
  }

  includedQuestionsCount(id: string): Observable<number> {
    return this.http.get(`${this.url}/${id}/questions/included/count`).pipe(
      map((result: { count: number }) => {
        return result.count;
      })
    );
  }

  publish(id: string): Observable<any> {
    return this.http.post(`${this.url}/${id}/publish`, {});
  }

  isRestorable(id: string, questionId: string): Observable<any> {
    return this.http.get(`${this.url}/${id}/questions/available?questionId=${questionId}`);
  }

  searchPublished(page: number, size: number, sort: string, filter): Observable<any> {
    return this.http.post(`${this.url}/search/published?page=${page}&size=${size}&sort=${sort}`, filter).pipe(
      map(result => parseDates(result))
    )
  }

  getLastHeldQuestion(meetingType: string, meetingDate: Date): Observable<any> {
    return this.http.get(`${this.url}/lastHeld/questions?meetingType=${meetingType}&planDate=${formatAsDate(meetingDate)}`);
  }

  renumberQuestions(id): Observable<any> {
    return this.http.post(`${this.url}/${id}/questions/renumber`, {});
  }

  createReport(id): Observable<any> {
    return this.http.post(`${this.url}/${id}/report/create`, {});
  }

  createProtocol(id, inheritAgendaMeetingNumber: boolean): Observable<Protocol> {
    return this.http.get(`${this.url}/${id}/protocol/generate?inheritAgendaMeetingNumber=${inheritAgendaMeetingNumber}`).pipe(
      map((result: ProtocolDocumentWrapper) => {
        return parseDates(result.document.protocol);
      })
    );
  }

  getInsideAgenda(id: string): Observable<SearchResultDocument[]> {
    return this.solrMediator.searchExt({
      page: 0,
      pageSize: 10000,
      fields: [
        { name: 'agendaID', value: id }
      ],
      types: [ this.solrMediator.types.agenda ]
    }).pipe(
      map(response => response.docs || [])
    );
  }

}
