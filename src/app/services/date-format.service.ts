import { Injectable } from '@angular/core';
import { formatAsDate, formatAsDateTime, formatAsTime } from "./date-util.service";
import * as angular from "angular";
import { Agenda } from "../models/Agenda";
import { Question } from "../models/Question";
import { Protocol } from "../models/Protocol";

@Injectable({
  providedIn: 'root'
})
export class DateFormatService {

  constructor() {
  }

  formatQuestionDates(question: Question): any {
    let obj = <any>angular.copy(question);
    if (obj.comments) {
      obj.comments.forEach(comment => formatAsDateTime(comment, 'date'));
    }
    if (obj.questionConsider.materials) {
      obj.questionConsider.materials.forEach(mat => {
        if (mat.document) {
          mat.document.forEach(doc => formatAsDateTime(doc, 'dateFile'));
        }
      })
    }
    if (obj.questionRG && obj.questionRG.materials) {
      obj.questionRG.materials.forEach(mat => {
        formatAsDate(mat, 'approvedCancelDate');
        formatAsDate(mat, 'approvedDate');
        if (mat.remark) {
          mat.remark.forEach(remark => {
            formatAsDate(remark, 'remarkDate');
            formatAsDate(remark, 'remarkFixedDate');
          })
        }
      });
      obj.questionRG.materials.forEach(mat => formatAsDate(mat, 'approvedDate'));
    }
    if (obj.questionConsider.externalProtocol) {
      formatAsDate(obj.questionConsider.externalProtocol, 'protocolDate');
    }
    if (obj.questionConsider.basis) {
      formatAsDate(obj.questionConsider.basis, 'date');
    }
    if (obj.questionConsider.passDate) {
      formatAsDateTime(obj.questionConsider, 'passDate');
    }
    if (obj.questionConsider.planDate) {
      formatAsDate(obj.questionConsider, 'planDate');
    }
    formatAsDate(obj.questionConsider, 'meetingDate');
    return obj;
  }

  formatAgendaDates(agenda: Agenda): any {
    let body = <any>angular.copy(agenda);
    if (body.filesAttach && body.filesAttach.length) {
      body.filesAttach.forEach(file => formatAsDateTime(file, 'dateFile'));
    }
    if (body.protocol && body.protocol.length) {
      body.protocol.forEach(file => formatAsDateTime(file, 'dateFile'));
    }
    if (body.fileAgenda) {
      formatAsDateTime(body.fileAgenda, 'dateFile')
    }
    if (body.attendedHistory && body.attendedHistory.length) {
      body.attendedHistory.forEach(attended => formatAsDateTime(attended, 'changeDate'));
    }
    if (body.meetingDateHistory && body.meetingDateHistory.length) {
      body.meetingDateHistory.forEach(meetingDate => {
        formatAsDateTime(meetingDate, 'changeDate');
        formatAsDate(meetingDate, 'meetingDateOld');
      });
    }
    if (body.meetingTimeHistory && body.meetingTimeHistory.length) {
      body.meetingTimeHistory.forEach(meetingTime => {
        formatAsDateTime(meetingTime, 'changeDate');
        formatAsTime(meetingTime, 'meetingTimeOld');
      });
    }
    if (body.meetingTimeEndHistory && body.meetingTimeEndHistory.length) {
      body.meetingTimeEndHistory.forEach(meetingTimeEnd => {
        formatAsDateTime(meetingTimeEnd, 'changeDate');
        formatAsTime(meetingTimeEnd, 'meetingTimeEndOld');
      });
    }
    if (body.meetingPlaceHistory && body.meetingPlaceHistory.length) {
      body.meetingPlaceHistory.forEach(place => formatAsDateTime(place, 'changeDate'));
    }
    if (body.mailingInfo && body.mailingInfo.length) {
      body.mailingInfo.forEach(place => formatAsDateTime(place, 'mailingDateTime'));
    }
    formatAsDate(body, 'meetingDate');
    formatAsTime(body, 'meetingTime');
    formatAsTime(body, 'meetingTimeEnd');
    formatAsDate(body, 'agendaPublished');
    return body;
  }


  formatProtocolDates(protocol: Protocol): any {
    let body = <any>angular.copy(protocol);
    if (body.fileConclusionDraft) {
      formatAsDateTime(body.fileConclusionDraft, 'dateFile');
    }
    if (body.fileConclusion) {
      formatAsDateTime(body.fileConclusion, 'dateFile');
    }
    if (body.fileProtocolDraft) {
      formatAsDateTime(body.fileProtocolDraft, 'dateFile');
    }
    if (body.fileProtocol) {
      body.fileProtocol.forEach(file => {
        formatAsDateTime(file, 'dateFile');
      })
    }
    if (body.receivedFromMKA) {
      formatAsDateTime(body.receivedFromMKA, 'receivedDate');
    }
    if (body.bpmProcess) {
      if (body.bpmProcess.bpmProcessStart) {
        formatAsDateTime(body.bpmProcess.bpmProcessStart, 'date');
        if (body.bpmProcess.bpmProcessStart.note) {
          formatAsDateTime(body.bpmProcess.bpmProcessStart.note, 'noteDate');
        }
      }
      if (body.bpmProcess.bpmProcessDel) {
        formatAsDateTime(body.bpmProcess.bpmProcessDel, 'date');
        if (body.bpmProcess.bpmProcessDel.note) {
          formatAsDateTime(body.bpmProcess.bpmProcessDel.note, 'noteDate');
        }
      }
    }
    if (body.review) {
      body.review.forEach(review => {
        formatAsDateTime(review, 'reviewPlanDate');
        formatAsDateTime(review, 'reviewFactDate');
      })
    }
    if (body.fileProtocolDraft) {
      formatAsDateTime(body.fileProtocolDraft, 'dateFile');
    }
    formatAsDate(body, 'meetingDate');
    formatAsTime(body, 'meetingTime');
    formatAsTime(body, 'meetingTimeEnd');
    formatAsDate(body, 'protocolApprovedDate');
    if (body.approval && body.approval.approvalCycle && body.approval.approvalCycle.agreed) {
      formatAsDateTime(body.approval.approvalCycle, 'approvalCycleDate');
      body.approval.approvalCycle.agreed.forEach(agreed => {
        formatAsDateTime(agreed, 'approvalPlanDate');
        formatAsDateTime(agreed, 'approvalFactDate');
        if (agreed.agreedDs) {
          formatAsDate(agreed.agreedDs, 'agreedDsFrom');
          formatAsDate(agreed.agreedDs, 'agreedDsTo');
        }
        if (agreed.fileApproval) {
          agreed.fileApproval.forEach(file => {
            formatAsDateTime(file, 'dateFile');
          })
        }
      })
    }
    if (body.approvalHistory && body.approvalHistory.approvalCycle) {
      body.approvalHistory.approvalCycle.forEach(cycle => {
        formatAsDateTime(cycle, 'approvalCycleDate');
        if (cycle.agreed) {
          cycle.agreed.forEach(agreed => {
            formatAsDateTime(agreed, 'approvalPlanDate');
            formatAsDateTime(agreed, 'approvalFactDate');
            if (agreed.agreedDs) {
              formatAsDate(agreed.agreedDs, 'agreedDsFrom');
              formatAsDate(agreed.agreedDs, 'agreedDsTo');
            }
            if (agreed.fileApproval) {
              agreed.fileApproval.forEach(file => {
                formatAsDateTime(file, 'dateFile');
              })
            }
          })
        }
        if (cycle.approvalCycleFile) {
          cycle.approvalCycleFile.forEach(file => {
            formatAsDateTime(file, 'dateFile');
          })
        }
      })
    }
    if (body.commentSecretaryResponsible) {
      body.commentSecretaryResponsible.forEach(commentSecretaryResponsible => {
        formatAsDateTime(commentSecretaryResponsible, 'commentDate');
      });
    }
    if (body.commentChairman) {
      body.commentChairman.forEach(commentChairman => {
        formatAsDateTime(commentChairman, 'commentDate');
      });
    }
    if (body.dsConclusionSecretaryResponsible) {
      formatAsDate(body.dsConclusionSecretaryResponsible, 'dsFrom');
      formatAsDate(body.dsConclusionSecretaryResponsible, 'dsTo');
    }
    if (body.dsConclusionChairman) {
      formatAsDate(body.dsConclusionChairman, 'dsFrom');
      formatAsDate(body.dsConclusionChairman, 'dsTo');
    }
    if (body.dsProtocolSecretaryResponsible) {
      formatAsDate(body.dsProtocolSecretaryResponsible, 'dsFrom');
      formatAsDate(body.dsProtocolSecretaryResponsible, 'dsTo');
    }
    if (body.dsProtocolChairman) {
      formatAsDate(body.dsProtocolChairman, 'dsFrom');
      formatAsDate(body.dsProtocolChairman, 'dsTo');

      console.log('body.dsProtocolChairman.dsFrom', body.dsProtocolChairman.dsFrom);
      console.log('body.dsProtocolChairman.dsTo', body.dsProtocolChairman.dsTo);
    }
    if (body.question) {
      body.question.forEach(question => {
        if (question.basis) {
          formatAsDate(question.basis, 'date');
        }
      });
      if (body.question.materials) {
        body.question.materials.forEach(material => {
          if (material.document) {
            material.document.forEach(document => {
              formatAsDateTime(document, 'dateFile');
            });
          }
        });
      }
    }
    return body;
  }
}
