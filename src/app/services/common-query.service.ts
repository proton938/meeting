import {Injectable} from '@angular/core';
import {toString} from 'lodash';

@Injectable({
  providedIn: 'root'
})
export class CommonQueryService {

  commonToQuery(query: string = '', fields: any[] = [], isNewSearch: boolean = false): string {
    let result = '';
    query = query.trim();
    const reg = /\d{2}(.|\/)\d{2}(.|\/)\d{4}/;
    fields.forEach((f) => {
      if (f.type === 'date' && query.match(reg)) {
        result += result ? ' OR ' : '';
        const date = query.replace(/^(\d{2}).(\d{2}).(\d{4})/, '$3-$2-$1');
        result += `(${f.code}:[${date}T00:00:00Z TO ${date}T23:59:59Z])`;
      } else {
        result += result ? ' OR ' : '';
        result += this._onceValue(f.code, toString(query), false, isNewSearch);
      }
    });
    return result;
  }

  private _onceValue(propName: string, value: string, hardOneValue: boolean = true, isNewSearch: boolean = false): string {
    let result = '';
    value = value.trim();
    if (value.match(/\s+/)) {
      value = '(*' + this.escapingSpecialCharacters(value.replace(/\s+/g, '* AND *')) + '*)';
    } else if (!hardOneValue) {
      value = '*' + this.escapingSpecialCharacters(value) + '*';
    }
    result += isNewSearch ? '(' + propName + ':' + value + ')' : '(' + propName + ':' + this.escapingSpecialCharacters(value) + ')';
    return result;
  }

  private _multipleValue(propName: string, values: string[]): string {
    let result = '';
    values = values.map(v => {
      v = v.trim();
      if (v.match(/\s+/)) {
        v = '(*' + this.escapingSpecialCharacters(v.replace(/\s+/g, '* AND *')) + '*)';
      }
      return v;
    });
    result += '(' + propName + ':' + values.join(' OR ' + propName + ':') + ')';
    return result;
  }

  private regExpSpecialCharacters(): any[] {
    return [/\\/g, /\+/g, /-/g, /\!/g, /\(/g, /\)/g, /:/g, /\^/g, /\[/g, /\]/g,
      /\{/g, /\}/g, /~/g, /\?/g, /\|/g, /&/g, /;/g, /\//g, /"/g];
  }

  private specialCharacters(): string [] {
    return ['\\\\', '\\+', '\\-', '\\!', '\\(', '\\)', '\\:', '\\^', '\\[', '\\]',
      '\\{', '\\}', '\\~', '\\?', '\\|', '\\&', '\\;', '\\/', '\\"'];
  }

  private escapingSpecialCharacters(value: string): string {
    let result = value;
    const regExpSpecialCharacters: string[] = this.regExpSpecialCharacters();
    const specialCharacters: string[] = this.specialCharacters();
    regExpSpecialCharacters.forEach((ch, index) => {
      result = result.replace(ch, specialCharacters[index]);
    });
    return result;
  }
}
