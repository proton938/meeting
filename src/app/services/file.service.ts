import {Injectable} from '@angular/core';
import {FileResourceService, IFileType} from "@reinform-cdp/file-resource";
import {NsiResourceService} from "@reinform-cdp/nsi-resource";
import {DropzoneDirective} from "ngx-dropzone-wrapper";

@Injectable({
  providedIn: 'root'
})
export class FileService {

  constructor(private nsiResourceService: NsiResourceService, private fileResourceService: FileResourceService) {
  }

  uploadDropzone(dropzone: DropzoneDirective, entityID: string, fileType: string, folderGuid: string,
                 success: (fileType: IFileType) => Promise<any>, error: (string) => void): Promise<any> {
    const nDropzone = dropzone.dropzone();
    if (nDropzone.files.length > 0) {
      return new Promise((resolve, reject) => {
        nDropzone.on("sending", function (file, xhr, formData) {
          formData.append("folderGuid", folderGuid);
          formData.append("fileType", fileType);
          formData.append("docEntityID", entityID);
          formData.append("docSourceReference", 'UI');
        });
        nDropzone.on('success', (file, fileGuid, c) => {
          return Promise.resolve(this.fileResourceService.getFileType(fileGuid)).then((fileType: IFileType) => {
            return success(fileType);
          }).then(() => {
              let queuedFiles = nDropzone.getQueuedFiles();
              if (queuedFiles.length === 0) {
                resolve();
              } else {
                nDropzone.processQueue();
              }
            }
          );
        });
        nDropzone.on('error', (file, errorMessage) => {
          error(errorMessage);
          nDropzone.removeFile(file);
        });
        nDropzone.processQueue();
      });
    } else {
      return Promise.resolve();
    }
  }

}
