import {Injectable} from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import * as _ from 'lodash';
import {copy} from 'angular';
import {formatDate} from '@angular/common';
import {AlertService} from '@reinform-cdp/widgets';

@Injectable({
  providedIn: 'root'
})
export class HelperService {

  beforeUnload: any = {
    saving: false,
    init: () => {
      this.beforeUnload.event = this.beforeUnload.event.bind(this);
      window.addEventListener('beforeunload', this.beforeUnload.event);
    },
    destroy: () => {
      window.removeEventListener('beforeunload', this.beforeUnload.event);
    },
    event(e) {
      if (this.beforeUnload.saving) {
        e.returnValue = 'Внесенные изменения не сохранятся!';
      }
    },
    start: () => {
      this.beforeUnload.saving = true;
    },
    stop: () => {
      this.beforeUnload.saving = false;
    }
  };

  constructor(protected toastr: ToastrService,
              private alertService: AlertService) {}

  error(error: any): void {
    const err = error ? error.error : '';
    if (err && err.userMessages && err.userMessages.length) {
      err.userMessages.forEach(msg => this.toastr.error(msg));
    } else if (_.isString(error)) {
      this.toastr.error(error);
    }
  }

  success(message: string): void {
    this.toastr.success(message);
  }

  warning(message: string): void {
    this.toastr.warning(message);
  }

  toJson(obj: any) {
    const r: any = copy(obj);
    this.transformObject(r);
    return JSON.stringify(r);
  }

  get alert() {
    return this.alertService;
  }

  private transformObject(o) {
    if (_.isArray(o)) {
      o.forEach(i => this.transformObject(i));
    } else if (_.isObject(o)) {
      _.forIn(o, (val: any, key: string) => {
        if (_.isDate(val)) {
          o[key] = formatDate(val, 'yyyy-MM-ddTHH:mm:ss', 'en');
        } else if (_.isArray(val) || _.isObject(val)) {
          this.transformObject(o[key]);
        }
      });
    }
  }
}
