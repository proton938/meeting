import {Injectable} from '@angular/core';
import {MeetingType} from "../models/nsi/MeetingType";
import {NsiResourceService} from "@reinform-cdp/nsi-resource";
import {AuthorizationService} from "@reinform-cdp/security";

@Injectable({
  providedIn: 'root'
})
export class MeetingTypeService {

  constructor(private nsiResourceService: NsiResourceService,
              private authorizationService: AuthorizationService) {
  }

  getMeetingType(meetingType: string): Promise<MeetingType> {
    if (!this.checkMeetingType(meetingType)) {
      return Promise.resolve(null);
    }
    return Promise.resolve(this.nsiResourceService.getBy('mggt_meeting_MeetingTypes', {nickAttr: 'meetingType', values: [meetingType]})).then((meetingTypes: MeetingType[]) => {
      return meetingTypes && meetingTypes.length ? meetingTypes[0] : null;
    })
  }

  getMeetingTypes(): Promise<MeetingType[]> {
    return Promise.resolve(this.nsiResourceService.get('mggt_meeting_MeetingTypes')).then((meetingTypes: MeetingType[]) => {
      return meetingTypes.filter(meetingType => this.checkMeetingType(meetingType.meetingType))
    })
  }

  getExpressMeetingTypes(): Promise<MeetingType[]> {
    return this.getMeetingTypes().then(meetingTypes => meetingTypes.filter(mt => mt.Express && !mt.meetingExternal));
  }

  getExpressNonExternalMeetingTypes(): Promise<MeetingType[]> {
    return this.getMeetingTypes().then(meetingTypes => meetingTypes.filter(mt => mt.Express && mt.meetingExternal === true));
  }

  getNonExpressExternalMeetingTypes(): Promise<MeetingType[]> {
    return this.getMeetingTypes().then(meetingTypes => meetingTypes.filter(mt => !mt.Express && mt.meetingExternal === true));
  }

  getNonExpressMeetingTypes(): Promise<MeetingType[]> {
    return this.getMeetingTypes().then(meetingTypes => meetingTypes.filter(mt => !mt.Express && !mt.meetingExternal));
  }

  checkMeetingType(meetingType: string): boolean {
    return this.authorizationService.check('MEETING_TYPE_' + this.normalizeMeetingType(meetingType));
  }

  normalizeMeetingType(meetingType: string): string {
    return meetingType.replace(/[^a-zA-Z0-9_]/g, '');
  }

}
