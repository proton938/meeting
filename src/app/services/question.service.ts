import {Observable} from 'rxjs/index';
import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Question, QuestionComment, QuestionDocumentWrapper, QuestionMaterialGroup} from "../models/Question";
import {map} from "rxjs/internal/operators";
import {formatAsDateTime, parseDates} from "./date-util.service";
import {compare, generate, observe, Operation} from "fast-json-patch";
import * as angular from "angular";
import {FileType} from "../models/FileType";
import {DateFormatService} from "./date-format.service";

@Injectable({
  providedIn: 'root'
})

export class QuestionService {
  constructor(private http: HttpClient, private dateFormatService: DateFormatService) {
  }

  get(id: string): Observable<QuestionDocumentWrapper> {
    return <Observable<QuestionDocumentWrapper>>this.http.get(`/app/sdo/meeting/question/${id}`).pipe(
      map(result => parseDates(result))
    );
  }

  create(question: Question): Observable<Question> {
    return <Observable<Question>>this.http.post(`/app/sdo/meeting/question/create`, {document: {question: this.dateFormatService.formatQuestionDates(question)}})
      .pipe(map((result: QuestionDocumentWrapper) => {
        return parseDates(result.document.question);
      }));
  }

  diff(q1: Question, q2: Question) {
    return compare(
      {document: {question: this.dateFormatService.formatQuestionDates(q1)}},
      {document: {question: this.dateFormatService.formatQuestionDates(q2)}}
    );
  }

  saveComment(question: Question, comment: QuestionComment): Observable<Question> {
    let doc: any = angular.copy({document: {question: {comments: question.comments}}});
    let observer = observe(doc);
    doc.document.question.comments = doc.document.question.comments || [];
    doc.document.question.comments.push({
      date: formatAsDateTime(comment.date),
      author: comment.author,
      comment: comment.comment
    });
    return this.update(question.questionID, generate(observer));
  }

  addMaterial(question: Question, materialType: string, login: string, fullName: string, fileType: FileType): Observable<Question> {
    let doc: any = angular.copy({document: {question: {questionConsider: {materials: question.questionConsider.materials}}}});
    let observer = observe(doc);
    doc.document.question.questionConsider.materials = doc.document.question.questionConsider.materials || [];
    let materialGroup: QuestionMaterialGroup = doc.document.question.questionConsider.materials.find(mat => mat.materialType === materialType);
    if (!materialGroup) {
      materialGroup = {
        idBase: undefined,
        materialType: materialType,
        accountName: login,
        materialsFIO: fullName,
        document: []
      };
      doc.document.question.questionConsider.materials.push(materialGroup);
    }
    materialGroup.document = materialGroup.document || [];
    materialGroup.document.push(fileType);
    return this.update(question.questionID, generate(observer));
  }

  deleteMaterial(question: Question, idFile: string) {
    let doc: any = angular.copy({document: {question: {questionConsider: {materials: question.questionConsider.materials}}}});
    let observer = observe(doc);
    doc.document.question.questionConsider.materials.forEach((materialGroup: QuestionMaterialGroup) => {
      if (materialGroup.document) {
        materialGroup.document = materialGroup.document.filter(doc => doc.idFile !== idFile);
      }
    });
    return this.update(question.questionID, generate(observer));
  }

  update(id: string, diff: Operation[]): Observable<Question> {
    return <Observable<Question>>this.http.patch(`/app/sdo/meeting/question/${id}`, diff)
      .pipe(map((result: QuestionDocumentWrapper) => {
        return parseDates(result.document.question);
      }));
  }

  isPlanDateEditable(id: string): Observable<boolean> {
    return <Observable<boolean>>this.http.get(`/app/sdo/meeting/question/${id}/planDate/editable`).pipe(
      map((result: { editable: boolean }) => {
        return result.editable;
      })
    );
  }

  getQuestionsIdsWithHistoryContaining(id: string): Observable<string[]> {
    return <Observable<string[]>>this.http.get(`/app/sdo/meeting/question/${id}/history`);
  }

  copy(id: string): Observable<string> {
    return <Observable<string>>this.http.post(`/app/sdo/meeting/question/${id}/copy`, {});
  }

}
