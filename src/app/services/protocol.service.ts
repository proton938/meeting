import { CdpReporterResourceService, CdpReporterPreviewService } from '@reinform-cdp/reporter-resource';
import { forkJoin, from, Observable, of, throwError } from 'rxjs/index';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Protocol, ProtocolDocumentWrapper, ProtocolQuestion, ProtocolPerson } from '../models/Protocol';
import { catchError, map, mergeMap } from 'rxjs/internal/operators';
import { parseDates } from './date-util.service';
import { FileType } from '../models/FileType';
import { NsiResourceService } from '@reinform-cdp/nsi-resource';
import * as angular from 'angular';
import { compare, generate, observe, Operation } from 'fast-json-patch';
import { DateFormatService } from './date-format.service';
import { ToastrService } from 'ngx-toastr';
import { SearchExtDataItem, SearchResultDocument } from '@reinform-cdp/search-resource';
import { MeetingType } from '../models/nsi/MeetingType';
import { Agenda } from '../models/Agenda';
import { ProtocolInstructionsUtils } from '../utils/protocol-instructions.utils';
import { FileResourceService } from '@reinform-cdp/file-resource';
import { JoinDocumentsService } from './join-documents.service';
import {SolrMediatorService} from './solr-mediator.service';

@Injectable({
  providedIn: 'root'
})
export class ProtocolService {

  url = '/app/sdo/meeting/protocol';

  constructor(private http: HttpClient,
    private dateFormatService: DateFormatService,
    private toastr: ToastrService,
    private solrMediator: SolrMediatorService,
    private reporterResourceService: CdpReporterResourceService,
    private nsiRestService: NsiResourceService,
    private fileResourceService: FileResourceService,
    public reporterPreviewService: CdpReporterPreviewService,
    private joinDocumentsService: JoinDocumentsService) {
  }

  get(id: string): Observable<ProtocolDocumentWrapper> {
    return <Observable<ProtocolDocumentWrapper>>this.http.get(`${this.url}/${id}`).pipe(
      map(result => parseDates(result))
    );
  }

  getByAgendaId(agendaId: string): Observable<ProtocolDocumentWrapper> {
    return <Observable<ProtocolDocumentWrapper>>this.http.get(`${this.url}/find?agendaId=${agendaId}`).pipe(
      map(result => parseDates(result))
    );
  }

  create(protocol: Protocol): Observable<string> {
    return <Observable<string>>this.http.post(`${this.url}/create`, {
      document: { protocol: this.dateFormatService.formatProtocolDates(protocol) }
    }).pipe(map((result: ProtocolDocumentWrapper) => {
      return result.document.protocol.protocolID;
    }));
  }

  diff(p1: Protocol, p2: Protocol) {
    return compare(
      { document: { protocol: this.dateFormatService.formatProtocolDates(p1) } },
      { document: { protocol: this.dateFormatService.formatProtocolDates(p2) } }
    );
  }

  addMaterial(protocol: Protocol, fileType: FileType): Observable<Protocol> {
    const doc: any = angular.copy({ document: { protocol: { fileProtocol: protocol.fileProtocol } } });
    const observer = observe(doc);
    doc.document.protocol.fileProtocol = doc.document.protocol.fileProtocol || [];
    doc.document.protocol.fileProtocol = doc.document.protocol.fileProtocol
      .filter(file => file.typeFile !== fileType.typeFile && file.idFile !== fileType.idFile);
    doc.document.protocol.fileProtocol.push(fileType);
    return this.update(protocol.protocolID, generate(observer));
  }

  update(id: string, diff: Operation[]): Observable<Protocol> {
    return <Observable<Protocol>>this.http.patch(`${this.url}/${id}`, diff)
      .pipe(map((result: ProtocolDocumentWrapper) => {
        return parseDates(result.document.protocol);
      }));
  }

  updateDocument(id: string, left: ProtocolDocumentWrapper, right: ProtocolDocumentWrapper, message?: string): Observable<Protocol> {
    const diff = this.diff(left.document.protocol, right.document.protocol);
    if (diff.length > 0) {
      return this.update(id, diff).pipe(
        map(response => {
          this.toastr.success(message ? message : 'Протокол совещания успешно обновлен!');
          return response;
        }), catchError(error => {
          this.toastr.error('Ошибка при изменении данных!');
          return throwError(error);
        })
      );
    } else {
      return of(right.document.protocol);
    }
  }

  getByMeetingNumber(meetingType: string, meetingNumber: string, year: number): Observable<ProtocolDocumentWrapper> {
    return <Observable<ProtocolDocumentWrapper>>this.http
      .get(`${this.url}/findId?meetingType=${meetingType}&meetingNumber=${meetingNumber}&year=${year}`).pipe(
        mergeMap((data: { id: string }) => {
          return data && data.id ? this.get(data.id) : Promise.resolve(null);
        })
      );
  }

  createReport(id: string): Observable<FileType> {
    return <Observable<FileType>>this.http.post(`${this.url}/${id}/report/create`, {});
  }

  sendDecision(id: string): Observable<any> {
    return <Observable<any>>this.http.post(`${this.url}/${id}/decision/send`, {});
  }

  getPrimaryQuestions(id: string): Observable<any> {
    return <Observable<any>>this.http.get(`${this.url}/${id}/questions/primary`);
  }

  getInsideProtocol(id: string): Observable<SearchResultDocument[]> {
    return this.solrMediator.searchExt({
      page: 0,
      pageSize: 10000,
      fields: [
        new SearchExtDataItem('protocolMKAID', id), // TODO нет такого поля
        //new SearchExtDataItem('docTypeCode', 'AGENDA'),
      ]
    }).pipe(
      map(response => response.docs || [])
    );
  }

  previewPrintForm(protocol: Protocol, meetingType: MeetingType,
    instructions: { questionId: string, instructions: SearchResultDocument[] }[],
    agenda: Agenda,
    signingElectronically: boolean): Observable<Blob> {


    return from(this.nsiRestService.ldapUser(meetingType.approvedLogin)).pipe(
      mergeMap((response) => {
        const approvedBy = {
          post: response.post,
          iofShort: response.iofShort,
          fioFull: response.displayName,
          accountName: response.accountName,
        };

        return of(approvedBy);
      }),
      mergeMap((approvedBy) => {
        return this.generatePrintFormParts(protocol, meetingType, instructions, agenda, approvedBy, signingElectronically);
      }),
      mergeMap((result: { file: FileType, appends: FileType[] }) => {
        return this.joinDocumentsService.joinDocumentsPreview(result.file.idFile, result.appends
          .map(a => a.idFile), 'CONTINUOUS', 'Протокол с поручениями.docx').pipe(
          mergeMap(blob => {
            return this.deletePrintFormParts(result.appends, result.file).pipe(
              map(_ => blob)
            );
          })
        );
      })
    );
  }


  generatePrintForm(protocol: Protocol, meetingType: MeetingType,
    instructions: { questionId: string, instructions: SearchResultDocument[] }[],
    agenda: Agenda,
    signingElectronically: boolean): Observable<FileType> {



    return from(this.nsiRestService.ldapUser(meetingType.approvedLogin)).pipe(
      mergeMap((response) => {
        const approvedBy = {
          post: response.post,
          iofShort: response.iofShort,
          fioFull: response.displayName,
          accountName: response.accountName,
        };

        return of(approvedBy);
      }),
      mergeMap((approvedBy) => {
        return this.generatePrintFormParts(protocol, meetingType, instructions, agenda, approvedBy, signingElectronically);
      }),
      mergeMap((result: { file: FileType, appends: FileType[] }) => {
        return this.joinDocumentsService.joinDocumentsUpdate(result.file.idFile, result.appends.map(a => a.idFile), 'CONTINUOUS').pipe(
          mergeMap(resultFile => {
            return this.deletePrintFormParts(result.appends).pipe(
              map(_ => resultFile)
            );
          })
        );
      }),
    );
  }

  private deletePrintFormParts(appends: FileType[], file?: FileType) {
    const ids = appends.map(a => a.idFile);
    if (file) {
      ids.push(file.idFile);
    }
    return forkJoin(ids.map(id => this.fileResourceService.deleteFile(id)));
  }

  private generatePrintFormParts(protocol: Protocol, meetingType: MeetingType,
    instructions: { questionId: string, instructions: SearchResultDocument[] }[],
    agenda: Agenda,
    approvedBy: ProtocolPerson,
    signingElectronically: boolean): Observable<{ file: FileType, appends: FileType[] }> {
    const protocolJson = {
      document: {
        protocol: ProtocolInstructionsUtils.getProtocolWithInstructions(protocol,
          meetingType, instructions, agenda, approvedBy, signingElectronically)
      }
    };

    return this.getPrintFormHead(protocol, protocolJson, signingElectronically).pipe(
      mergeMap(file => {
        const observables = protocol.question.map(q => {
          const instr = instructions.find(i => i.questionId === q.questionID);
          return this.getPrintFormQuestion(protocol, q, instr ? instr.instructions : []);
        });
        observables.push(this.getPrintFormPart(protocol, 'ssProtocolASSign', protocolJson, 'Протокол с поручениями_подпись.docx'));
        return forkJoin(observables).pipe(
          map(files => {
            return { file: file, appends: files };
          })
        );
      })
    );
  }

  private getPrintFormHead(protocol: Protocol, protocolJson: any, signingElectronically: boolean): Observable<FileType> {
    const template = signingElectronically ? 'ssProtocolASES' : 'ssProtocolAS';
    return this.getPrintFormPart(protocol, template, protocolJson, 'Протокол с поручениями.docx');
  }

  private getPrintFormQuestion(protocol: Protocol, q: ProtocolQuestion, instructions: SearchResultDocument[]): Observable<FileType> {
    const qJson = { document: {
      question: ProtocolInstructionsUtils.getProtocolQuestionWithInstructions(protocol, q.questionID, instructions || [])
    } };

    const template = this.getQuestionTemplate(q, instructions || []);
    return this.getPrintFormPart(protocol, template, qJson, `Протокол с поручениями_${q.questionID}.docx`);
  }

  private getQuestionTemplate(question: ProtocolQuestion, instructions: SearchResultDocument[]): string {
    if (question.decisionText && instructions && instructions.length) {
      return 'ssQuestionAllInfo';
    } else if (question.decisionText && !(instructions && instructions.length)) {
      return 'ssQuestionOnlyDec';
    } else if (!question.decisionText && instructions && instructions.length) {
      return 'ssQuestionOnlyInstr';
    } else {
      return 'ssQuestionWithoutInfo';
    }
  }

  private getPrintFormPart(protocol: Protocol, template: string, json: any, fileName: string): Observable<FileType> {
    const request = {
      options: {
        jsonSourceDescr: '',
        onProcess: fileName,
        placeholderCode: '',
        strictCheckMode: true,
        xwpfResultDocumentDescr: fileName,
        xwpfTemplateDocumentDescr: ''
      },
      jsonTxt: JSON.stringify(json),
      rootJsonPath: '$',
      nsiTemplate: {
        templateCode: template
      },
      filenetDestination: {
        fileName: fileName,
        fileType: 'default',
        folderGuid: protocol.folderId,
        fileAttrs: [
          { attrName: 'docEntityID', attrValues: [protocol.protocolID], attrNameRu: null },
          { attrName: 'docSourceReference', attrValues: ['UI'], attrNameRu: null }
        ]
      }
    };
    return from(this.reporterResourceService.nsi2Filenet(request)).pipe(
      map((result: any) => {
        return FileType.create(result.versionSeriesGuid, result.fileName, result.fileSize.toString(), false,
          new Date().toISOString(), result.fileType, result.mimeType);
      })
    );
  }

}
