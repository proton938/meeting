import {Injectable} from '@angular/core';
import {FacetedSearchResult} from '@reinform-cdp/search-resource';
import {Question} from '../models/Question';
import {Protocol, ProtocolPerson} from '../models/Protocol';
import {Agenda, AgendaUser} from '../models/Agenda';
import {MeetingType} from '../models/nsi/MeetingType';
import {Observable, from, of} from 'rxjs';
import {map, mergeMap} from 'rxjs/internal/operators';
import * as _ from 'lodash';
import {ProtocolService} from './protocol.service';
import {SolrMediatorService} from './solr-mediator.service';

export class ProtocolQuestionsInstructions {
  questionMap: { [key: string]: string };
  instructions: FacetedSearchResult;
}

export class ProtocolWithInstructionsUser {
  login: string;
  fioFull: string;
}

export class ProtocolWithInstructionsAttended {
  name: string;
  code: string;
  attendedPerson?: string;
  attendedPersonFIOShort?: string;
  attendedPersonFIO?: {
    post: string;
    iofShort: string;
    fioFull: string;
  }[];
}

export class ProtocolInstruction {
  documentId: string;
  instructionsNumber: string;
  statusId: {
    code: string;
    name: string;
    color: string;
  };
  instruction: {
    number: string;
    beginDate: Date;
    creator: ProtocolWithInstructionsUser;
    content: string;
    description: string;
    planDate: Date;
    factDate: Date;
    executor: ProtocolWithInstructionsUser;
    coExecutorFio: string;
    coExecutor: ProtocolWithInstructionsUser[];
    questionId: string;
    questionPrimaryID: string;
    reportContent: string;
  };
  reportExecutor: ProtocolWithInstructionsUser;
}

export class ProtocolQuestionWithInstructions {
  questionID: string;
  questionNumber: string;
  itemNumber: string;
  excluded: boolean;
  decisionType: string;
  decisionTypeCode: string;
  decisionText: string;
  question: string;
  activity: boolean;
  instructions?: ProtocolInstruction[];
}

export class ProtocolWithInstructionsAuthorCreate {
  post: string;
  iofShort: string;
  fioShort: string;
  fioFull: string;
  accountName: string;
}

export class ProtocolWithInstructions {
  protocolID: string;
  signingElectronically: boolean;
  initiatorIofShort: string;
  meetingType: string;
  meetingFullName: string;
  meetingDate: string;
  meetingNumber: string;
  authorCreate?: ProtocolWithInstructionsAuthorCreate;
  meetingTime: string;
  meetingTimeEnd: string;
  meetingPlace: string;
  protocolApproved: boolean;
  protocolApprovedDate: Date;
  attendedNum: number;
  attended: ProtocolWithInstructionsAttended[];
  questionsNum: number;
  approvedBy: ProtocolPerson;
  secretary: ProtocolPerson;
  question: ProtocolQuestionWithInstructions[];
  dsProtocolChairman: {
    dsLastName: string;
    dsName: string;
    dsPosition: string;
    dsCN: string;
    dsFrom: Date;
    dsTo: Date;
  };
  meetingTheme: string;
  meetingNumberShort: string;
}
@Injectable({
  providedIn: 'root'
})
export class InstructionsService {
  constructor(private solrMediator: SolrMediatorService,
              private protocolService: ProtocolService) {
  }

  getQuestionInstructions(question: Question): Observable<FacetedSearchResult> {
    if (!question.questionPrimaryID) {
      return of({docs: [], numFound: 0, pageSize: 0, start: 0, facets: null});
    }
    return this.solrMediator.searchExt({
      types: [ this.solrMediator.types.instruction ],
      pageSize: 200,
      sort: 'docDateInst desc',
      fields: [{
        name: 'questionPrimaryID',
        value: question.questionPrimaryID
      }]
    });
  }

  getProtocolInstructions(protocol: Protocol): Observable<ProtocolQuestionsInstructions> {
    let questionMap = {};
    return this.protocolService.getPrimaryQuestions(protocol.protocolID).pipe(
      mergeMap((result: any) => {
        questionMap = _.invert(result);
        const primaryQuestionsIds = _.keys(questionMap);
        if (primaryQuestionsIds && primaryQuestionsIds.length) {
          const solrFieldsFilter: any[] = [{
            name: 'questionPrimaryID',
            value: primaryQuestionsIds
          }];
          return this.getNextProtocolMeetingDate(protocol.meetingDate, protocol.meetingType).pipe(
            mergeMap((nextProtocolMeetingDate: Date) => {
              if (nextProtocolMeetingDate) {
                solrFieldsFilter.push({name: 'docDateInst', value: {to: nextProtocolMeetingDate}});
              }
              return this.solrMediator.searchExt({
                types: [ this.solrMediator.types.instruction ],
                pageSize: 200,
                fields: solrFieldsFilter
              });
            }),
            map((searchResult) => {
              return {questionMap: questionMap, instructions: searchResult};
            })
          );
        } else {
          return of({questionMap: questionMap, instructions: new FacetedSearchResult()});
        }
      })
    );
  }

  getNextProtocolMeetingDate(meetingDate: Date, meetingType: string): Observable<Date> {
    const nextMeetingFrom = new Date(meetingDate.getTime());
    nextMeetingFrom.setDate(nextMeetingFrom.getDate() + 1);
    return this.solrMediator.searchExt({
      types: [ this.solrMediator.types.protocol ],
      pageSize: 1,
      sort: 'meetingDate asc',
      fields: [
        {name: 'meetingType', value: meetingType},
        {name: 'meetingDate', value: {from: nextMeetingFrom}}
      ]
    }).pipe(
      map((result: FacetedSearchResult) => {
        if (result.numFound > 0) {

          const futureMeetingDates: Date[] = _.map(result.docs, (doc: any) => {
            return new Date(doc.meetingDate);
          });

          const dateInst = _.min(futureMeetingDates);
          dateInst.setDate(dateInst.getDate() - 1);
          dateInst.setHours(23, 59, 59, 999);
          return dateInst;
        } else {
          return null;
        }
      })
    );
  }

  getAgendaInstructions(agenda: Agenda, meetingType: MeetingType, primaryQuestionsIds: string[]): Observable<FacetedSearchResult> {
    const solrFieldsFilter = [];

    solrFieldsFilter.push({
      name: 'questionPrimaryID',
      value: primaryQuestionsIds
    });

    if (meetingType.Express) {
      return this.solrMediator.searchExt({
        types: [ this.solrMediator.types.instruction ],
        pageSize: 200,
        fields: solrFieldsFilter
      });
    }

    const nextMeetingFrom = this.getMeetingDateTime(agenda.meetingDate, agenda.meetingTime);
    nextMeetingFrom.setMinutes(nextMeetingFrom.getMinutes() + 1, 0, 0);
    return this.getNextAgendaMeetingDate(agenda.meetingDate, agenda.meetingTime, agenda.meetingType).pipe(
      mergeMap(nextAgendaMeetingDate => {
        if (nextAgendaMeetingDate) {
          solrFieldsFilter.push({name: 'docDateInst', value: {to: nextAgendaMeetingDate}});
        }
        return this.solrMediator.searchExt({
          types: [ this.solrMediator.types.instruction ],
          pageSize: 200,
          fields: solrFieldsFilter
        });
      })
    );
  }

  getNextAgendaMeetingDate(meetingDate: Date, meetingTime: Date, meetingType: string): Observable<Date> {
    const nextMeetingFrom = this.getMeetingDateTime(meetingDate, meetingTime);
    nextMeetingFrom.setMinutes(nextMeetingFrom.getMinutes() + 1, 0, 0);
    return this.solrMediator.searchExt({
      types: [ this.solrMediator.types.agenda ],
      pageSize: 1,
      // sort: 'meetingDateTimeSs asc',
      fields: [
        {name: 'meetingType', value: meetingType}
        // {name: 'meetingDateTimeSs', value: {from: nextMeetingFrom}}
      ]
    }).pipe(
      map((result: FacetedSearchResult) => {
        if (result.numFound > 0) {

          const futureMeetingDates: Date[] = _.map(result.docs, (doc: any) => {
            return new Date(doc.meetingDateTimeSs);
          });

          const dateInst = _.min(futureMeetingDates);
          dateInst.setMinutes(dateInst.getMinutes() - 1, 59, 999);
          return dateInst;
        } else {
          return;
        }
      })
    );
  }

  private getMeetingDateTime(meetingDate: Date, meetingTime: Date): Date {
    const meetingDateTime = new Date(meetingDate.getTime());
    meetingDateTime.setHours(meetingTime.getHours(), meetingTime.getMinutes(), 0, 0);
    return meetingDateTime;
  }

}
