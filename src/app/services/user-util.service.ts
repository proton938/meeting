import {Injectable} from '@angular/core';
import {QuestionUserType} from "../models/Question";
import {UserBean} from "@reinform-cdp/nsi-resource";

@Injectable({
  providedIn: 'root'
})
export class UserUtilService {
  constructor() {
  }

  toQuestionUserType(user: UserBean, organizationCode?: string) {
    let result: QuestionUserType = {
      accountName: user.accountName,
      fioFull: user.displayName,
      fioShort: user.fioShort,
      phone: user.telephoneNumber,
      email: user.mail,
      post: user.post,
      organization: user.departmentFullName,
      organizationShort: null,
      organizationCode: organizationCode
    };
    return result;
  }

}
