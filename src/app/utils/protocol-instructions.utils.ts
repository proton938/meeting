import {SearchResultDocument} from "@reinform-cdp/search-resource";
import {Protocol, ProtocolAttended, ProtocolPerson, ProtocolQuestion} from "../models/Protocol";
import {MeetingType} from "../models/nsi/MeetingType";
import * as _ from "lodash";
import {
  ProtocolWithInstructions, ProtocolWithInstructionsAttended, ProtocolQuestionWithInstructions,
  ProtocolInstruction, ProtocolWithInstructionsAuthorCreate
} from "../services/instructions.service";
import {Agenda, AgendaUser} from "../models/Agenda";
import {formatAs} from "../services/date-util.service";

// @dynamic
export class ProtocolInstructionsUtils {

  static getProtocolQuestionsWithInstructions(protocol: Protocol, instructions: { questionId: string, instructions: SearchResultDocument[] }[]): ProtocolQuestionWithInstructions[] {
    return protocol.question.map((q, index) => {
      let instr = instructions.find(i => i.questionId === q.questionID);
      return ProtocolInstructionsUtils._getQuestionWithInstructions(protocol.protocolID, q, index, instr ? instr.instructions : []);
    });
  }

  static getProtocolQuestionWithInstructions(protocol: Protocol, questionId: string, instructions?: SearchResultDocument[]): ProtocolQuestionWithInstructions {
    let index = protocol.question.findIndex(q => q.questionID === questionId);
    let q = protocol.question[index];
    return ProtocolInstructionsUtils._getQuestionWithInstructions(protocol.protocolID, q, index, instructions);
  }

  static _getQuestionWithInstructions(protocolId: string, q: ProtocolQuestion, index: number, instructions?: SearchResultDocument[]): ProtocolQuestionWithInstructions {
    let instr = instructions ? instructions.map((i, index) => ProtocolInstructionsUtils.convert(i, index + 1)) : null;
    return {
      questionID: q.questionID,
      questionNumber: (index + 1).toString(),
      itemNumber: q.itemNumber,
      excluded: q.excluded,
      decisionType: q.decisionType,
      decisionTypeCode: q.decisionTypeCode,
      decisionText: q.decisionText,
      question: q.question,
      activity: q.activity,
      instructions: instr
    };
  }

  static getProtocolWithInstructions(protocol: Protocol, meetingType: MeetingType, instructions: { questionId: string, instructions: SearchResultDocument[] }[],
                                     agenda?: Agenda, approvedBy?: ProtocolPerson, signingElectronically?:  boolean): ProtocolWithInstructions {
    const meetingNumber = protocol.meetingNumber || agenda.meetingNumber;
    let protocolWithInstructions;
    try {
      protocolWithInstructions = {
        protocolID: protocol.protocolID,
        initiatorIofShort: agenda && agenda.initiator && !_.isEmpty(agenda.initiator) ? agenda.initiator.iofShort || agenda.initiator.fioFull : undefined,
        signingElectronically: signingElectronically,
        authorCreate: ProtocolInstructionsUtils.convertAuthorCreate(agenda ? agenda.authorCreate : null),
        meetingType: meetingType.meetingType,
        meetingFullName: meetingType.meetingFullName,
        meetingDate: formatAs(protocol.meetingDate, 'dd.MM.yyyy'),
        meetingNumber: meetingNumber,
        meetingTime: formatAs(agenda ? agenda.meetingTime : protocol.meetingTime, 'dd.MM.yyyy HH:mm'),
        meetingTimeEnd: agenda ? formatAs(agenda.meetingTimeEnd, 'dd.MM.yyyy HH:mm') : protocol.meetingTimeEnd,
        meetingPlace: protocol.meetingPlace,
        protocolApproved: protocol.protocolApproved,
        protocolApprovedDate: protocol.protocolApprovedDate,
        attendedNum: protocol.attendedNum ? protocol.attendedNum : protocol.attended.filter(att => att.code).length,
        attended: _.chain(protocol.attended).groupBy(att => att.code).map((atts: ProtocolAttended[]) => {
          let result: ProtocolWithInstructionsAttended = {
            name: atts[0].name,
            code: atts[0].code
          };
          let hasPerson = _.some(atts, att => att.fioFull || att.iofShort || att.post);
          if (hasPerson) {
            result.attendedPerson = atts.map(att => att.iofShort).join(', ');
            result.attendedPersonFIOShort = atts.map(att => ProtocolInstructionsUtils.convertFioFullToFioShort(att.fioFull)).join(', ')
            result.attendedPersonFIO = atts.map(att => {
              return {
                post: att.post,
                iofShort: att.iofShort,
                fioFull: att.fioFull
              }
            });
          }
          return result;
        }).value(),
        questionsNum: protocol.questionsNum,
        approvedBy: approvedBy ? approvedBy : protocol.approvedBy,
        secretary: protocol.secretary,
        question: ProtocolInstructionsUtils.getProtocolQuestionsWithInstructions(protocol, instructions),
        dsProtocolChairman: protocol.dsProtocolChairman,
        meetingTheme: agenda ? agenda.meetingTheme : protocol.meetingTheme,
        meetingNumberShort: meetingNumber && (meetingNumber.indexOf('-') !== -1) ? meetingNumber.split('-')[1].substring(1, meetingNumber.split('-')[1].length - 1) : ''
      };
    } catch (e) {
      console.log('error', e);
    }
    return protocolWithInstructions;
  }

  private static convertAuthorCreate(authorCreate: AgendaUser): ProtocolWithInstructionsAuthorCreate {
    if (!authorCreate) {
      return undefined;
    }
    return {
      accountName: authorCreate.accountName,
      fioFull: authorCreate.fioFull,
      iofShort: authorCreate.iofShort,
      post: authorCreate.post,
      fioShort: ProtocolInstructionsUtils.convertFioFullToFioShort(authorCreate.fioFull)
    };
  }

  private static convert(solrDoc: SearchResultDocument | any, index: number): ProtocolInstruction {
    return {
      documentId: solrDoc.documentId,
      instructionsNumber: index.toString(),
      statusId: {
        code: solrDoc.statusIdInst,
        name: solrDoc.condition,
        color: null
      },
      instruction: {
        number: solrDoc.docNumberInst,
        beginDate: solrDoc.docDateInst,
        creator: {
          login: solrDoc.creatorLogInst,
          fioFull: solrDoc.creatorInst
        },
        content: solrDoc.contentInst,
        description: solrDoc.descriptionInst,
        planDate: solrDoc.planDateInst,
        factDate: solrDoc.factDateInst,
        executor: {
          login: solrDoc.executorLogInst,
          fioFull: solrDoc.executorInst
        },
        coExecutorFio: solrDoc.coExecutorFIO ? solrDoc.coExecutorFIO.map(fio => ProtocolInstructionsUtils.convertFioFullToFioShort(fio)).join(', ') : '',
        coExecutor:  solrDoc.coExecutorLogin ? solrDoc.coExecutorLogin.map((val, ind) => {
          return {
            login: val,
            fioFull: solrDoc.coExecutorFIO[ind]
          }
        }) : [],
        questionId: solrDoc.questionId,
        questionPrimaryID: solrDoc.questionPrimaryID,
        reportContent: solrDoc.reportContentInst
      },
      reportExecutor: null
    };
  }

  private static convertFioFullToFioShort(fioFull) {
    if (!fioFull) {
      return null;
    }
    let items: string[] = fioFull.split(' ');
    let result = items[0];
    if (items.length > 1) {
      result += ' ' + items.slice(1).map(i => i.charAt(0) + '.').join(' ');
    }
    return result;
  }

}
