import {Directive, ElementRef, Input, OnInit} from '@angular/core';
import {AuthorizationService} from "@reinform-cdp/security";

@Directive({
  selector: '[meetingMyAuth]'
})
export class MyAuthDirective implements OnInit {

  @Input('meetingMyAuth') permission: string;
  private element: HTMLElement;

  constructor(private elRef: ElementRef, private authorizationService: AuthorizationService) {
    this.element = elRef.nativeElement;
  }

  ngOnInit() {
    this.defineVisibility(this.element, this.permission);
  }

  setVisible(element: HTMLElement, visible: boolean) {
    if (visible) {
      element.classList.remove('hidden')
    } else {
      element.classList.add('hidden')
    }
  }

  defineVisibility(element: HTMLElement, subject: string) {
    this.setVisible(element, this.authorizationService.check(subject));
  }

}
