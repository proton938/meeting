import {Directive, ElementRef, Input, OnInit} from '@angular/core';
import {AuthorizationService} from "@reinform-cdp/security";

@Directive({
  selector: '[meetingFileIcon]'
})
export class FileIconDirective implements OnInit {

  @Input('meetingFileIcon') fileName: string;
  @Input('meetingFileIconSmall') small: boolean;
  private element: HTMLElement;

  private formats = ['csv', 'doc', 'docx', 'dwg', 'pdf', 'ppt', 'pptx', 'rar', 'txt', 'xml', 'zip'];

  constructor(private elRef: ElementRef, private authorizationService: AuthorizationService) {
    this.element = elRef.nativeElement;
  }

  ngOnInit() {
    let ind = this.fileName.lastIndexOf(".");
    let ext = this.fileName.substr(ind + 1, this.fileName.length);
    let format: string = this.formats.find(f => f === ext);
    if (!format) {
      format = 'unknown';
    }
    let size = this.small ? 'small' : 'md';
    this.element.innerHTML = `<img class="file-icon file-icon--${size} icon_${format}"/>`;
  }

}
