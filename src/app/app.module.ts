import {BlockUIModule} from 'ng-block-ui';
import * as angular from 'angular';
import {AppComponent} from './app.component';
import {ModuleWithProviders, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {states} from './states.config';
import {UIRouterUpgradeModule} from '@uirouter/angular-hybrid';
import {WidgetsModule} from '@reinform-cdp/widgets';
import {FormsModule} from '@angular/forms';
import {LaddaModule} from 'angular2-ladda';
import {CdpBpmComponentsModule} from '@reinform-cdp/bpm-components';
import {
  PaginationModule, TabsModule, BsDatepickerModule, AlertModule, ButtonsModule, BsDropdownModule,
  CollapseModule, TimepickerModule
} from 'ngx-bootstrap';
import {ToastrModule} from 'ngx-toastr';
import { environment } from '../environments/environment';
import {QuestionListComponent} from './components/question/list/question-list.component';
import {SearchResourceModule, SolarResourceServiceProvider} from '@reinform-cdp/search-resource';
import {NgSelectModule} from '@ng-select/ng-select';
import {InstructionsService} from "./services/instructions.service";
import {MeetingTypeService} from "./services/meeting-type.service";
import {ProtocolService} from "./services/protocol.service";
import {QuestionService} from "./services/question.service";
import {OrderFileComponent} from './components/common/order-file/order-file.component';
import {QuestionDecisionComponent} from "./components/common/question-decision.component";
import {QuestionInstructionsComponent} from "./components/question/card/question-instructions.component";
import {QuestionCommentModalComponent} from "./components/question/card/question-comment.modal";
import {QuestionExternalAgreeComponent} from "./components/question/card/question-external-agree.component";
import {QuestionIndicatorsViewComponent} from "./components/question/card/question-indicators-view.component";
import {QuestionViewComponent} from "./components/question/card/question-view.component";
import {QuestionComponent} from "./components/question/card/question.component";
import {QuestionViewRgComponent} from './components/question/card/question-view-rg/question-view-rg.component';
import {QuestionEditComponent} from './components/question/card/question-edit/question-edit.component';
import {UserUtilService} from "./services/user-util.service";
import {MyAuthDirective} from './directives/my-auth.directive';
import {QuestionAddressComponent} from './components/common/question-address.component';
import {QuestionIndicatorsEditComponent} from "./components/question/card/question-indicators-edit.component";
import {CalendarComponent} from './components/calendar/calendar.component';
import {FullCalendarModule} from "ng-fullcalendar";
import {AgendaService} from "./services/agenda.service";
import {AgendaComponent} from './components/agenda/card/agenda.component';
import {AgendaViewComponent} from './components/agenda/card/agenda-view/agenda-view.component';
import {AgendaEditComponent} from './components/agenda/card/agenda-edit/agenda-edit.component';
import {QuestionReasonModalComponent} from './components/agenda/card/agenda-edit/question-reason.modal';
import {FileIconDirective} from "./directives/file-icon.directive";
import {CopyQuestionsModalComponent} from './components/agenda/card/agenda-edit/copy-questions.modal';
import {AddMaterialsModalComponent} from './components/question/card/add-materials.modal';
import {NsiResourceServiceProvider} from "@reinform-cdp/nsi-resource";
import {ProtocolComponent} from './components/protocol/card/protocol.component';
import {ProtocolViewComponent} from './components/protocol/card/protocol-view/protocol-view.component';
import {DropzoneModule} from "ngx-dropzone-wrapper";
import {FileResourceModule, FileResourceServiceProvider} from "@reinform-cdp/file-resource";
import {QuestionListSortingComponent} from './components/question/list/question-list-sorting.component';
import {QuestionListFiltersComponent} from './components/question/list/question-list-filters.component';
import {AgendaListComponent} from './components/agenda/list/agenda-list.component';
import {AgendaProjectsListComponent} from './components/agenda/list/agenda-projects-list.component';
import {AgendaListSortingComponent} from './components/agenda/list/agenda-list-sorting.component';
import {ChooseSigningTypeModalComponent} from './components/agenda/card/choose-signing-type-modal/choose-signing-type-modal.component';
import {ProtocolEditComponent} from './components/protocol/card/protocol-edit/protocol-edit.component';
import {AgendaListFiltersComponent} from './components/agenda/list/agenda-list-filters.component';
import {defineLocale} from 'ngx-bootstrap/chronos';
import {ruLocale} from 'ngx-bootstrap/locale';
import {AgendaProjectsListSortingComponent} from "./components/agenda/list/agenda-projects-list-sorting.component";
import {ShowcaseComponent} from './components/showcase/showcase.component';
import {ProtocolListComponent} from './components/protocol/list/protocol-list.component';
import {ProtocolListSortingComponent} from './components/protocol/list/protocol-list-sorting.component';
import {ProtocolListFiltersComponent} from './components/protocol/list/protocol-list-filters.component';
import {QuestionPlanningListComponent} from './components/question/list/planning/question-planning-list.component';
import {QuestionPlanningListSortingComponent} from './components/question/list/planning/question-planning-list-sorting.component';
import {QuestionPresentationListComponent} from './components/question/list/presentation/question-presentation-list.component';
import {QuestionPresentationListSortingComponent} from './components/question/list/presentation/question-presentation-list-sorting.component';
import {HttpErrorInterceptor} from "./services/service-error.interceptor";
import {HTTP_INTERCEPTORS} from "@angular/common/http";
import { QuestionListViewPlainComponent } from './components/question/list/question-list-view-plain.component';
import { QuestionListViewGrouppedComponent } from './components/question/list/question-list-view-groupped.component';
import { QuestionListViewCardComponent } from './components/question/list/question-list-view-card.component';
import {CdpReporterResourceModule, CdpReporterPreviewServiceProvider} from "@reinform-cdp/reporter-resource";
import {FileService} from "./services/file.service";
import { ProtocolMaterialsModalComponent } from './components/protocol/card/protocol-view/protocol-materials.modal';
import { ExpressAgendaEditComponent } from './components/agenda/card/express-agenda-edit/express-agenda-edit.component';
import { ExpressAgendaViewComponent } from './components/agenda/card/express-agenda-view/express-agenda-view.component';
import { DropzoneComponent } from './components/common/dropzone/dropzone.component';
import { ShowcaseExpressComponent } from './components/showcase/express/express.component';
import { ShowcaseExpressMKAComponent } from './components/showcase/express-mka/express-mka.component';
import { MeetingListMKAExpressComponent } from './components/showcase/express-mka/meetings-list.component';
import { MeetingListExpressComponent } from './components/showcase/express/meetings-list.component';
import { MeetingInstructionComponent } from './components/common/meeting-instruction/meeting-instruction.component';
import { RenameFileModalComponent } from './components/common/rename-file/rename-file.modal';
import { ProtocolInstructionsComponent } from './components/protocol/card/protocol-view/protocol-instructions.component';
import {MeetingMkaShowcaseComponent} from './components/showcase/meeting-mka/meeting-mka.component';
import {ModalService} from "./services/modal.service";
import {ActivitiService} from "./services/activity.service";
import {SelectLdapUsersComponent} from "./components/common/select-ldap-users/select-ldap-users.component";
import {AddUserModalComponent} from "./components/tasks/review-mka-doc/add-user/add-user.modal";
import {AgendaReviewMKADocComponent} from "./components/tasks/review-mka-doc/agenda-review-doc/agenda-review-mka-doc.component";
import {SubtasksDocComponent} from "./components/agenda/card/order-subtasks/subtasks-doc.component";
import {ProtocolReviewMkaDocComponent} from "./components/tasks/review-mka-doc/protocol-review/protocol-review-mka-doc.component";
import {ProtocolReviewComponent} from "./components/agenda/card/review/protocol/protocol-review.component";
import {ProtocolSignComponent} from "./components/tasks/protocol-sign/protocol-sign.component";
import {AgendaReviewComponent} from "./components/agenda/card/review/agenda/agenda-review.component";
import {RescheduleAgendaModalComponent} from './components/agenda/card/reschedule-agenda/reschedule-agenda.modal';
import {AddThemeModalComponent} from './components/agenda/card/add-theme/add-theme.modal';
import {AttachFileModalComponent} from "./components/common/attach-file-modal/attach-file-modal.component";
import { ApprovalListComponent } from './components/common/approval-list/approval-list.component';
import { DesktopApprovalListComponent } from './components/common/approval-list/desktop/desktop.component';
import { MobileApprovalListComponent } from './components/common/approval-list/mobile/mobile.component';
import { ApprovalRowComponent } from './components/common/approval-list/desktop/row.component';
import { AgendaApprovalsComponent } from './components/agenda/card/agenda-approvals/agenda-approvals.component';
import { MeetingEditProtocolAsComponent } from './components/tasks/approve-protocol-as/meeting-edit-protocol-as/meeting-edit-protocol-as.component';
import {JoinDocumentsService} from "./services/join-documents.service";

import {SkeletonModule} from '@reinform-cdp/skeleton';
import {CdpLoggerModule} from '@reinform-cdp/logger';
import {UrlService, TransitionService, Transition} from '@uirouter/core';
import { CoreModule } from '@reinform-cdp/core';
import {DependenciesTreeComponent} from './components/dependencies-tree.component';
import {setAngularJSGlobal, UpgradeModule} from '@angular/upgrade/static';
import { AgendaJsonEditorComponent } from './components/agenda/card/agenda-json-editor/agenda-json-editor.component';
declare let document: any;
setAngularJSGlobal(angular);


@NgModule({
  imports: [
    FormsModule,
    BrowserModule,
    LaddaModule,
    ToastrModule.forRoot(),
    TabsModule.forRoot(),
    PaginationModule.forRoot(),
    BsDatepickerModule.forRoot(),
    AlertModule.forRoot(),
    UIRouterUpgradeModule.forRoot({states: states}),
    WidgetsModule.forRoot(),
    CdpBpmComponentsModule.forRoot(),
    SearchResourceModule.forRoot(),
    FileResourceModule.forRoot(),
    CdpReporterResourceModule.forRoot(),
    DropzoneModule,
    NgSelectModule,
    ButtonsModule.forRoot(),
    BsDropdownModule.forRoot(),
    TimepickerModule.forRoot(),
    FullCalendarModule,
    BlockUIModule.forRoot(),
    CollapseModule,
    CoreModule.forRoot('SDO', '/main/', 'Главная', '/app/sdo/bpm'),
    SkeletonModule.forRoot(),
    CdpLoggerModule.forRoot(),

  ],
  declarations: [
    AppComponent,
    ApprovalListComponent,
    DesktopApprovalListComponent,
    QuestionListComponent,
    MobileApprovalListComponent,
    ApprovalRowComponent,
    QuestionComponent,
    QuestionViewComponent,
    AgendaApprovalsComponent,
    QuestionIndicatorsViewComponent,
    QuestionIndicatorsEditComponent,
    QuestionExternalAgreeComponent,
    ChooseSigningTypeModalComponent,
    QuestionCommentModalComponent,
    QuestionInstructionsComponent,
    QuestionDecisionComponent,
    QuestionViewRgComponent,
    QuestionEditComponent,
    QuestionAddressComponent,
    MyAuthDirective,
    FileIconDirective,
    CalendarComponent,
    SubtasksDocComponent,
    AgendaComponent,
    AgendaViewComponent,
    AgendaEditComponent,
    AgendaReviewComponent,
    ProtocolReviewComponent,
    QuestionReasonModalComponent,
    CopyQuestionsModalComponent,
    AddMaterialsModalComponent,
    ProtocolComponent,
    ProtocolViewComponent,
    QuestionListSortingComponent,
    QuestionListFiltersComponent,
    AgendaListComponent,
    AgendaProjectsListComponent,
    AgendaListSortingComponent,
    AgendaProjectsListSortingComponent,
    ProtocolEditComponent,
    AgendaListFiltersComponent,
    ShowcaseComponent,
    ProtocolListComponent,
    ProtocolListSortingComponent,
    ProtocolListFiltersComponent,
    QuestionPlanningListComponent,
    QuestionPlanningListSortingComponent,
    QuestionPresentationListComponent,
    QuestionPresentationListSortingComponent,
    QuestionListViewPlainComponent,
    QuestionListViewGrouppedComponent,
    QuestionListViewCardComponent,
    ProtocolMaterialsModalComponent,
    ExpressAgendaEditComponent,
    ExpressAgendaViewComponent,
    DropzoneComponent,
    ShowcaseExpressComponent,
    OrderFileComponent,
    ShowcaseExpressMKAComponent,
    MeetingListExpressComponent,
    MeetingInstructionComponent,
    RenameFileModalComponent,
    ProtocolInstructionsComponent,
    MeetingMkaShowcaseComponent,
    MeetingListMKAExpressComponent,
    SelectLdapUsersComponent,
    AddUserModalComponent,
    AgendaReviewMKADocComponent,
    ProtocolReviewMkaDocComponent,
    ProtocolSignComponent,
    RescheduleAgendaModalComponent,
    AddThemeModalComponent,
    AttachFileModalComponent,
    MeetingEditProtocolAsComponent,
    DependenciesTreeComponent,
    AgendaJsonEditorComponent
  ],

  exports: [AppComponent, CdpBpmComponentsModule, AgendaViewComponent],

  entryComponents: [QuestionReasonModalComponent, CopyQuestionsModalComponent, QuestionCommentModalComponent, AddMaterialsModalComponent,
    ProtocolMaterialsModalComponent, RenameFileModalComponent, AddUserModalComponent, RescheduleAgendaModalComponent, AddThemeModalComponent,
    AttachFileModalComponent, ChooseSigningTypeModalComponent, DependenciesTreeComponent]
})
export class MeetingSystem {
  constructor(private upgrade: UpgradeModule) { }
  ngDoBootstrap() {
    this.upgrade.bootstrap(document, ['meeting.system'], { strictDi: false });
  }
}


export const ng1Module = angular.module('meeting.system', [
 'ui.router',
  'ui.router.upgrade',
  'cdp.skeleton',
  'cdp.core',
  'cdp.bpm.components',
  'cdp.widgets',
  'cdp.reporter.resource',
  'cdp.logger',
  'cdp.security'
])
  .config(['cdpNsiResourceServiceProvider', (nsiResourceServiceProvider: NsiResourceServiceProvider) => {
    nsiResourceServiceProvider.root = '/mdm/api/v1';
  }]).config(['cdpFileResourceServiceProvider', (fileResourceServiceProvider: FileResourceServiceProvider) => {
    fileResourceServiceProvider.root = '/filestore/v1';
  }])
  .config(['cdpSolarResourceServiceProvider', (solarResourceServiceProvider: SolarResourceServiceProvider) => {
    solarResourceServiceProvider.root = '/app/sdo/search';
  }])
  .config(['cdpReporterPreviewServiceProvider', (cdpReporterPreviewServiceProvider: CdpReporterPreviewServiceProvider) => {
    cdpReporterPreviewServiceProvider.rootReporter = '/reporter/v1';
  }]);


console.log(environment.logoUrl);


ng1Module.run([
  '$transitions',
  '$q',
  function($transitions: TransitionService, $q: ng.IQService) {
    $transitions.onStart({}, function(trans: Transition) {
      if (window.sessionStorage && window.sessionStorage['cdp-reload']) {
        delete window.sessionStorage['cdp-reload'];
        window.location.reload();
        console.log('New version arrived');
      } else {
        trans.options().reload = false;
      }
    });
  }
]);

